import {
  IsOptional
} from 'class-validator';
import { Company } from 'src/database/entities/company.schema';
import { Customer } from 'src/database/entities/customer.schema';
import { Insurer } from 'src/database/entities/insurer.schema';
import { Sale } from 'src/database/entities/sale.schema';
import { User } from 'src/database/entities/user.schema';
export class CreateFinancingDto {
  // #region Properties (21)

  @IsOptional()
  public amount: number;
  @IsOptional()
  public apr: number;
  @IsOptional()
  public aprPeriod: string;
  @IsOptional()
  public bills: any;
  @IsOptional()
  public code: string;
  @IsOptional()
  public company: Partial<Company>;
  @IsOptional()
  public coordinator: Partial<User>;
  @IsOptional()
  public createdAt: Date;
  @IsOptional()
  public createdBy: Partial<User>;
  @IsOptional()
  public currentBalance: number;
  @IsOptional()
  public customer: Partial<Customer>;
  @IsOptional()
  public description: string;
  @IsOptional()
  public downPayment: number;
  @IsOptional()
  public financedAt: Date;
  @IsOptional()
  public financer: Partial<Insurer>;
  @IsOptional()
  public payments: any;
  //TODO: PaymentItem;
  @IsOptional()
  public policies: Sale[]
  @IsOptional()
  public status: string;
  @IsOptional()
  public type: string;
  @IsOptional()
  public updatedAt: Date;
  @IsOptional()
  public updatedBy: User;

  // #endregion Properties (21)
}
