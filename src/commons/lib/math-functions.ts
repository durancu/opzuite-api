export function roundAmount(amount: number): number {
  return Math.round((amount || 0) * 100) / 100;
}
