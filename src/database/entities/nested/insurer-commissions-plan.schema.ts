import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document, SchemaTypes } from "mongoose";
import { Insurer } from "../insurer.schema";

@Schema()
export class InsurerCommissionsPlan extends Document {
  // #region Properties (4)

  @Prop({ type: SchemaTypes.Map })
  public agencyCommissionPlan: any;
  @Prop({ type: SchemaTypes.Map })
  public agentCommissionPlan: any;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Insurer' })
  public insurer: Insurer;
  @Prop({ type: [SchemaTypes.Map] })
  public sharersCommissionPlan: any;

  // #endregion Properties (4)
}

export const InsurerCommissionsPlanSchema = SchemaFactory.createForClass(InsurerCommissionsPlan);
