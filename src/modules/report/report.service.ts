import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp, isEmpty, kebabCase, merge, pickBy } from 'lodash';
import { Model, Types } from 'mongoose';
import { GroupingCriteria } from "src/commons/enum/grouping-criteria.enum";
import { ReportParamsFilter } from "src/database/entities/nested/report-params-filter.schema";
import { ReportParams } from "src/database/entities/nested/report-params.schema";
import { Report } from 'src/database/entities/report.schema';
import { ReportParamsDto } from 'src/dto/report/report-params.dto';
import { AuthenticatedRequest } from 'src/modules/auth/interface/authenticated-request.interface';
import { CreateReportDto } from '../../dto/report/create-report.dto';
import { UpdateReportDto } from '../../dto/report/update-report.dto';
import { MetricsService } from '../sale/metrics.service';
import { SaleService } from '../sale/sale.service';


@Injectable({ scope: Scope.REQUEST })
export class ReportService {

  constructor(
    @InjectModel(Report.name) private reportModel: Model<Report>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
    private metricsService: MetricsService,
    private saleService: SaleService,
  ) { }

  /**
   * @returns Promise
   */
  async findAll(): Promise<any> {
    return await this.reportModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async findByCode(code: string): Promise<CreateReportDto> {
    const report: any = await this.reportModel
      .findOne({ code: code })
      .populate({
        path: 'createdBy',
        select: 'role name',
      })
      .exec();

    if (!report) {
      throw new NotFoundException(`Report (code: ${code}) not found.`);
    }
    return report;
  }

  async buildReportChartData(code: string, params: ReportParamsDto) {
    const report: any = await this.reportModel
      .findOne({ code: code })
      .populate({
        path: 'createdBy',
        select: 'role name',
      })
      .exec();

    if (!report) {
      throw new NotFoundException(`Report (code: ${code}) not found.`);
    }

    //console.log('params before cleaning', params);

    const reportParams: ReportParams = report.params;
    const cleanedParams: any = pickBy(params);
    const appliedParams: any = isEmpty(cleanedParams)
      ? reportParams
      : Object.assign(reportParams, cleanedParams);

    const results: any = await this.metricsReport(code, appliedParams);

    const reportData: CreateReportDto = { ...report['_doc'] };

    const metrics = results.metrics || [];
    const series = {
      name: reportData.name,
      data: [],
    };
    const categories = [];

    metrics.map((metric: any) => {
      if (metric._id.label) {
        series.data.push(metric.totalPremium);
        categories.push(metric._id.label);
      }
    });

    const response = {
      series: series,
      categories: categories,
    };
    return response;
  }

  /**
   * @param  {ReportParams} params
   * @returns Promise<any>
   */
  async metricsReport(code: string, params: ReportParamsDto): Promise<any> {
    let metrics: any[];

    let hasInsurerFilter = false;

    if (params) {

      if (params.filters) {

        hasInsurerFilter = Boolean(params.filters.find(
          (filter: ReportParamsFilter) =>
            filter.field === 'BROKER' || filter.field === 'CARRIER',
        ));
      }

      if (
        params.groupByModel === GroupingCriteria.BROKER ||
        params.groupByModel === GroupingCriteria.CARRIER ||
        hasInsurerFilter
      ) {
        metrics = await this.metricsService.getSalesMetricsByCoverages(params);
      } else {
        metrics = await this.metricsService.getSalesMetricsByEndorsements(params);
      }
    }

    const results = {
      metrics: metrics.filter(
        ({ _id }: any) => _id && Object.keys(_id).length > 0,
      ),
      records: [],
    };

    if (params.withRecords) {
      const queryParamsFilters = {};

      if (params.filters) {
        params.filters.forEach((filter: ReportParamsFilter) => {
          if (filter.field) {
            queryParamsFilters[filter.field] = filter.value;
          }
        });
      }

      results.records = await this.saleService.search({
        filter: queryParamsFilters,
        sortOrder: 'desc',
        sortField: 'soldAt',
        pageNumber: 1,
        pageSize: 50,
      });
    }
    return results;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  async findById(id: string): Promise<Report> {
    const report: Report = await this.reportModel.findOne({ _id: id }).exec();
    if (!report) {
      throw new NotFoundException(`Report (id: ${id}) was not found`);
    }

    return report;
  }

  /**
   * @param  {CreateReportDto} reportDto
   * @returns Promise
   */
  async save(reportDto: CreateReportDto): Promise<Report> {
    return await this.reportModel.create({
      ...reportDto,
      key: kebabCase(reportDto.name),
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    });
  }

  /**
   * @param  {string} code
   * @param  {UpdateReportDto} data
   * @returns Promise
   */
  async update(code: string, report: UpdateReportDto): Promise<Report> {
    const reportFound: Report = await this.reportModel
      .findOne({ code: code })
      .exec();

    if (!reportFound) {
      throw new NotFoundException(`Report (code: ${code}) not found.`);
    }

    const reportData: UpdateReportDto = {
      ...reportFound['_doc'],
      ...report,
      updatedBy: this.req.user.id,
    };

    return await this.reportModel.findOneAndUpdate(
      { _id: new Types.ObjectId(reportFound._id) },
      reportData,
      { new: true },
    );
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async deleteByCode(code: string): Promise<Report> {
    const report: Report = await this.reportModel.findOne({ code: code }).exec();

    if (!report) {
      throw new NotFoundException(`Report (code: ${code}) not found.`);
    }

    return await this.reportModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */

  async deleteAll(): Promise<any> {
    return await this.reportModel.deleteMany({}).exec();
  }

  /**
   * @returns Promise
   */

  async batchDelete(codes: string[]): Promise<any> {
    return await this.reportModel.deleteMany({ code: { $in: codes } }).exec();
  }

  async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let conditions = {};

    conditions = {
      $and: [{ deleted: false }],
      $or: [
        { createdBy: new Types.ObjectId(this.req.user.id) },
        { scope: 'public' },
      ],
    };

    if (queryParams.filter && Object.keys(queryParams.filter).length > 0) {
      const filterQueries = Object.keys(queryParams.filter).map((key) => {
        return {
          [key]: {
            $regex: new RegExp(
              '.*' + escapeRegExp(queryParams.filter[key]) + '.*',
              'i',
            ),
          },
        };
      });
      conditions['$or'] = merge(conditions['$or'], filterQueries);
    }

    const query = this.reportModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query
      .unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy',
      });
    query.unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true });

    query
      .unwind({ path: '$updatedBy', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'updatedBy',
        foreignField: '_id',
        as: 'updatedBy',
      });
    query.unwind({ path: '$updatedBy', preserveNullAndEmptyArrays: true });

    query
      .unwind({ path: '$createdBy.location', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'locations',
        localField: 'createdBy.location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$createdBy.location', preserveNullAndEmptyArrays: true });

    query.append({
      $project: {
        id: '$_id',
        scope: '$scope',
        name: '$name',
        description: '$description',
        permissions: '$permissions',
        deleted: '$deleted',
        code: '$code',
        params: '$params',
        createdAt: '$createdAt',
        createdByName: '$createdBy.name',
        createdByCode: '$createdBy._id',
        updatedAt: '$updatedAt',
        updatedByName: '$updatedBy.name',
        updatedByCode: '$updatedBy._id',
        locationName: { $arrayElemAt: ['$location.business.name', 0] },
        locationCode: { $arrayElemAt: ['$location.code', 0] },
      },
    });


    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.skip(skipCriteria).limit(limitCriteria).sort(sortCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  async getCatalog(filterCriteria: any): Promise<any> {
    return await this.reportModel
      .find(filterCriteria)
      .select('name description code key scope _id')
      .sort({ name: 1 })
      .exec();
  }
}
