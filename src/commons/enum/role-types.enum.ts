export enum RoleTypes {
    'EMPLOYEE' = 'employee',
    'ADMIN' = 'admin',
    'DEVELOPER' = 'developer',
}