import { IsNotEmpty, IsNotEmptyObject, IsOptional, IsString } from 'class-validator';
import { BusinessInfo } from 'src/database/entities/nested/business-info.schema';
export class CreateLocationDto {
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  alias?: string;

  @IsNotEmptyObject()
  business?: BusinessInfo;

  @IsNotEmpty()
  @IsString()
  payFrequency?: string;

  @IsOptional()
  default?: boolean;

  @IsOptional()
  ipAddress?: string;
}
