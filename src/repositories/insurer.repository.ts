import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Insurer } from 'src/database/entities/insurer.schema';

export class InsurerRepository {
  // #region Constructors (1)

  constructor(@InjectModel(Insurer.name) private readonly insurerModel: Model<Insurer>) { }

  // #endregion Constructors (1)

  // #region Public Methods (6)

  public async findAll(): Promise<Insurer[]> {
    return await this.insurerModel.find({}).exec();
  }





  // #endregion Public Methods (6)
}