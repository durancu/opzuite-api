import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { nanoid } from 'nanoid';
import { Company } from '../company.schema';
import { Endorsement } from '../endorsement.schema';
import { Sale } from '../sale.schema';
import { User } from '../user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } })
export class ItemPayment extends Document {
  // #region Properties (12)

  @Prop({ default: 0, required: false })
  public amount: number;
  @Prop({ default: 0, required: false })
  public balance: number;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company', required: true })
  public company: Company;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
  public createdBy: User;
  @Prop({ default: new Date() })
  public date: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Endorsement', required: true })
  public endorsement: Endorsement;
  @Prop({ type: SchemaTypes.Map, required: true })
  public endorsementItem: any;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Sale', required: true })
  public sale: Sale;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public updatedBy: User;

  // #endregion Properties (12)
}

export const ItemPaymentSchema = SchemaFactory.createForClass(ItemPayment);
