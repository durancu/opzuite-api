import { IsNotEmpty, IsOptional } from 'class-validator';
import { Location } from 'src/database/entities/location.schema';
import { PayStub } from 'src/database/entities/nested/pay-stub.schema';
export class CreatePayrollDto {
  @IsOptional()
  location?: Partial<Location>;

  @IsOptional()
  payPeriodEndedAt?: Date;

  @IsOptional()
  payPeriodStartedAt?: Date;

  @IsOptional()
  periodCode?: string;

  @IsOptional()
  @IsNotEmpty()
  scope?: string; //can be Company (C), Location (L), Individual (I)

  payStubs?: PayStub[];
}
