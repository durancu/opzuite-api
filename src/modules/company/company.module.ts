import { Global, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Company, CompanySchema } from 'src/database/entities/company.schema';
import { CompanyController } from './company.controller';
import { CompanyService } from './company.service';

@Global()
@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Company.name, schema: CompanySchema },
    ]),
  ],
  controllers: [
    CompanyController
  ],
  providers: [
    CompanyService
  ],
  exports: [
    CompanyService
  ],
})
export class CompanyModule { }
