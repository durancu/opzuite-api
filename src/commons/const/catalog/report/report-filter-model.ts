export const REPORT_FILTER_MODEL = [
  {
    id: 'sale',
    name: 'Sale',
    filterFields: [
      {
        id: 'broker',
        name: 'Broker',
        type: 'select',
        catalog: 'brokers',
      },
      {
        id: 'carrier',
        name: 'Carrier',
        type: 'select',
        catalog: 'carriers',
      },
      { id: 'country', name: 'Country', type: 'select', catalog: 'countries' },
      {
        id: 'customer',
        name: 'Customer',
        type: 'select',
        catalog: 'customers',
      },
      {
        id: 'location',
        name: 'Location',
        type: 'select',
        catalog: 'locations',
      },
      {
        id: 'seller',
        name: 'Seller',
        type: 'select',
        catalog: 'users',
      },
      { id: 'status', name: 'Status', type: 'select', catalog: 'saleStatus' },
      { id: 'type', name: 'Type', type: 'select', catalog: 'saleTypes' },
    ],
    groupByFields: [
      { id: 'broker', name: 'Broker' },
      { id: 'carrier', name: 'Carrier' },
      { id: 'country', name: 'Country' },
      { id: 'customer', name: 'Customer' },
      { id: 'location', name: 'Location' },
      { id: 'seller', name: 'Seller' },
      { id: 'status', name: 'Status' },
      { id: 'type', name: 'Type' },
      { id: 'day', name: 'Day' },
      { id: 'month', name: 'Month' },
      { id: 'year', name: 'Year' },
    ],
  },
  /* {
    id: 'customer',
    name: 'Customer',
    filterFields: [
      { id: 'type', name: 'Type', type: 'select', catalog: 'customerTypes' },
      { id: 'country', name: 'Country', type: 'select', catalog: 'countries' },
      {
        id: 'location',
        name: 'Location',
        type: 'select',
        catalog: 'locations',
      },
    ],
    groupByFields: [
      { id: 'type', name: 'Type' },
      { id: 'country', name: 'Country' },
      { id: 'location', name: 'Location' },
    ],
  }, */
];
