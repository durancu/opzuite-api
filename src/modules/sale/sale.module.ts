import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Company, CompanySchema } from 'src/database/entities/company.schema';
import { Endorsement, EndorsementSchema } from 'src/database/entities/endorsement.schema';
import { Insurer, InsurerSchema } from 'src/database/entities/insurer.schema';
import { Location, LocationSchema } from 'src/database/entities/location.schema';
import { Role, RoleSchema } from 'src/database/entities/role.schema';
import { Sale, SaleSchema } from 'src/database/entities/sale.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { EndorsementRepository } from 'src/repositories/endorsement.repository';
import { InsurerRepository } from 'src/repositories/insurer.repository';
import { LocationRepository } from 'src/repositories/location.repository';
import { SaleRepository } from 'src/repositories/sale.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { InsurerModule } from '../insurer/insurer.module';
import { LocationModule } from '../location/location.module';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';
import { MetricsService } from './metrics.service';
import { SaleModuleInitializerService } from './sale-module-initializer.service';
import { SaleController } from './sale.controller';
import { SaleService } from './sale.service';

@Module({
  imports: [
    InsurerModule,
    LocationModule,
    UserModule,
    MongooseModule.forFeature([
      { name: Sale.name, schema: SaleSchema },
      { name: Endorsement.name, schema: EndorsementSchema },
      { name: User.name, schema: UserSchema },
      { name: Insurer.name, schema: InsurerSchema },
      { name: Location.name, schema: LocationSchema },
      { name: Role.name, schema: RoleSchema },
      { name: Company.name, schema: CompanySchema },
    ]),
  ],
  controllers: [
    SaleController
  ],
  providers: [
    SaleRepository,
    SaleService,
    EndorsementRepository,
    UserRepository,
    MetricsService,
    InsurerRepository,
    UserRepository,
    LocationRepository,
    UserService,
    SaleModuleInitializerService
  ],
  exports: [
    EndorsementRepository,
    SaleRepository,
    SaleService,
    MetricsService,
    UserRepository,
    LocationRepository,
  ]
})
export class SaleModule { }
