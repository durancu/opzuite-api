import { IsOptional } from "class-validator";

export class MetricsParamsFilterDto {
    // #region Properties (3)

    @IsOptional()
    public field: string;
    @IsOptional()
    public operator: string;
    @IsOptional()
    public value: any;

    // #endregion Properties (3)
}