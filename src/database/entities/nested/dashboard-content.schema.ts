import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class DashboardContent extends Document {
    // #region Properties (3)

    @Prop()
    public columnsWidth: number;
    @Prop()
    public content: any;
    @Prop()
    public type: string;

    // #endregion Properties (3)
}

export const DashboardContentSchema = SchemaFactory.createForClass(DashboardContent);