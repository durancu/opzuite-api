import { PermissionType } from "src/commons/enum/permission-type.enum";
import { Company } from "src/database/entities/company.schema";
import { Location } from "src/database/entities/location.schema";
import { EmployeeInfo } from "src/database/entities/nested/employee-info.schema";
import { Role } from "src/database/entities/role.schema";

export interface JwtPayload {
  // #region Properties (17)

  readonly authorizedIpAddresses: string[];
  readonly company: Partial<Company>;
  readonly country: string;
  readonly email: string;
  readonly employeeInfo: EmployeeInfo;
  readonly firstName: string;
  readonly lastName: string;
  readonly location: Partial<Location>;
  readonly permissions: PermissionType[];
  readonly phone: string;
  readonly preferences: any;
  readonly role: Partial<Role>;
  readonly status: string;
  readonly sub: string;
  readonly type:string; 
  readonly upn: string;
  readonly username: string;

  // #endregion Properties (17)
}
