
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { nanoid } from 'nanoid';
import { Address, AddressSchema } from './address.schema';
import { VehicleInfo, VehicleInfoSchema } from './vehicle.info.schema';


@Schema()
export class ContactInfo extends Document {
  // #region Properties (36)

  @Prop({ type: AddressSchema })
  public address: Address;
  @Prop({ default: '' })
  public autoInsuranceCompany: string;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop()
  public dob: Date;
  @Prop({ default: '' })
  public driverLicense: string;
  @Prop({ default: '' })
  public driverLicenseState: string;
  @Prop({ default: '' })
  public educationLevel: string;
  @Prop({ default: '' })
  public email: string;
  @Prop({ default: '' })
  public employer: string;
  @Prop({ default: '' })
  public employmentStatus: string;
  @Prop({ default: '' })
  public ethnicity: string;
  @Prop({ default: '' })
  public firstName: string;
  @Prop({ default: '' })
  public gender: string;
  @Prop({ type: Boolean, default: false })
  public hasAutoInsurance: boolean;
  @Prop({ type: Boolean, default: false })
  public hasHealthInsurance: boolean;
  @Prop({ type: Boolean, default: false })
  public hasHomeInsurance: boolean;
  @Prop({ type: Boolean, default: false })
  public hasLifeInsurance: boolean;
  @Prop({ default: '' })
  public healthInsuranceCompany: string;
  @Prop({ default: '' })
  public homeInsuranceCompany: string;
  @Prop({ default: '' })
  public identityCardId: string;
  @Prop({ default: '' })
  public identityCardType: string;
  @Prop({ default: '' })
  public immigrantStatus: string;
  @Prop({ default: '' })
  public language: string;
  @Prop({ default: '' })
  public lastName: string;
  @Prop({ default: '' })
  public lifeInsuranceCompany: string;
  @Prop({ default: '' })
  public maritalStatus: string;
  @Prop({ default: '' })
  public mobilePhone: string;
  @Prop({ default: '' })
  public notes: string;
  @Prop({ default: '' })
  public phone: string;
  @Prop({ default: '' })
  public relationship: string;
  @Prop({ default: '' })
  public ssn: string;
  @Prop({ default: 'CDT' })
  public timezone: string;
  @Prop({ type: [VehicleInfoSchema] })
  public vehicles: VehicleInfo[];
  @Prop({ default: '' })
  public website: string;
  @Prop()
  public yearlyGrossIncome: number;

  // #endregion Properties (36)
}

export const ContactInfoSchema = SchemaFactory.createForClass(ContactInfo);
