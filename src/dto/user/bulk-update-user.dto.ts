import {
  IsOptional,
  MinLength
} from 'class-validator';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { Location } from 'src/database/entities/location.schema';
import { Address } from 'src/database/entities/nested/address.schema';
import { Communication } from 'src/database/entities/nested/communication.schema';
import { EmailSettings } from 'src/database/entities/nested/email-settings.schema';
import { Role } from 'src/database/entities/role.schema';
import { User } from 'src/database/entities/user.schema';

export class BulkUpdateUserDto {
  // #region Properties (12)

@IsOptional()
  address?: Address;
@IsOptional()
  communication?: Communication;
@IsOptional()
  emailSettings?: EmailSettings;
@IsOptional()
  language?: string;
@IsOptional()
  location?: Partial<Location>;
@IsOptional()
  @IsOptional()
@IsOptional()
  @MinLength(8, { message: ' The min length of password is 8 ' })
@IsOptional()
  password?: string;
@IsOptional()
  permissions?:PermissionType[];
@IsOptional()
  phone?: string;
@IsOptional()
  preferences?:any
@IsOptional()
  role?: Partial<Role>;
@IsOptional()
  supervisor?: Partial<User>;
@IsOptional()
  timezone?: string;

  // #endregion Properties (12)
}
