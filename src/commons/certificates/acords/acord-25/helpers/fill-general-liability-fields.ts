import { LimitPer } from "src/commons/enum/limit-per.enum";
import { PdfAnswer } from "src/commons/enum/pdf-answer.enum";
import { PdfCheckBox } from "src/commons/enum/pdf-checkbox.enum";
import { PolicyBasedForm } from "src/commons/enum/policy-based-form";
import { CoverageDetails } from "src/database/entities/nested/coverage-details.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillGeneralLiabilityFields = (policiesByProduct: any, template: Acord25PdfTemplateV2014NowCerts) => {
    if (policiesByProduct['GENERAL_LIABILITY']) {
        const policy = policiesByProduct['GENERAL_LIABILITY'];

        template.setField('GeneralLiability_CoverageIndicator', PdfCheckBox.ON);

        template.setField('GeneralLiability_InsurerLetterCode', policy.letter);
        template.setField('Policy_GeneralLiability_PolicyNumberIdentifier', policy.policyNumber);
        template.setField('Policy_GeneralLiability_EffectiveDate', policy.effectiveAt?.toLocaleDateString());
        template.setField('Policy_GeneralLiability_ExpirationDate', policy.expiresAt?.toLocaleDateString());

        const coverageDetails: CoverageDetails = policy.coverage.details;

        if (coverageDetails) {
            template.setField('GeneralLiability_EachOccurrence_LimitAmount', coverageDetails.eachOccurrenceLimit);
            template.setField('GeneralLiability_ClaimsMadeIndicator', coverageDetails.policyBasedForm === PolicyBasedForm.CLAIMS ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_OccurrenceIndicator', coverageDetails.policyBasedForm === PolicyBasedForm.OCCUR ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_OtherCoverageIndicator', coverageDetails.includeOtherCoverageA ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_OtherCoverageDescription', coverageDetails.includeOtherCoverageA ? coverageDetails.otherCoverageADescription : '');
            template.setField('GeneralLiability_OtherCoverageIndicator_B', coverageDetails.includeOtherCoverageB ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_OtherCoverageDescription_B', coverageDetails.includeOtherCoverageB ? coverageDetails.otherCoverageBDescription : '');
            template.setField('GeneralLiability_GeneralAggregate_LimitAppliesPerPolicyIndicator', coverageDetails.generalAggregateLimitAppliesPer === LimitPer.POLICY ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_GeneralAggregate_LimitAppliesPerProjectIndicator', coverageDetails.generalAggregateLimitAppliesPer === LimitPer.PROJECT ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_GeneralAggregate_LimitAppliesPerLocationIndicator', coverageDetails.generalAggregateLimitAppliesPer === LimitPer.LOCATION ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_GeneralAggregate_LimitAppliesToOtherIndicator', coverageDetails.generalAggregateLimitAppliesPer === LimitPer.OTHER ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('GeneralLiability_GeneralAggregate_LimitAppliesToCode', coverageDetails.generalAggregateLimitAppliesPer === LimitPer.OTHER ? coverageDetails.generalAggregateLimitAppliesPerOther : '');
            template.setField('CertificateOfInsurance_GeneralLiability_AdditionalInsuredCode', coverageDetails.additionalInsured ? PdfAnswer.YES : PdfAnswer.EMPTY);
            template.setField('Policy_GeneralLiability_SubrogationWaivedCode', coverageDetails.waiverOfSubrogation ? PdfAnswer.YES : PdfAnswer.EMPTY);
            template.setField('GeneralLiability_FireDamageRentedPremises_EachOccurrenceLimitAmount', coverageDetails.damageToRentedPremisesLimit);
            template.setField('GeneralLiability_MedicalExpense_EachPersonLimitAmount', coverageDetails.medicalExpensesLimit);
            template.setField('GeneralLiability_PersonalAndAdvertisingInjury_LimitAmount', coverageDetails.personalAndAdvertisingInjuryLimit);
            template.setField('GeneralLiability_GeneralAggregate_LimitAmount', coverageDetails.generalAggregateLimit);
            template.setField('GeneralLiability_ProductsAndCompletedOperations_AggregateLimitAmount', coverageDetails.productAndCompletedOperationsAggregateLimit);
            template.setField('GeneralLiability_OtherCoverageLimitDescription', coverageDetails.other1LimitName);
            template.setField('GeneralLiability_OtherCoverageLimitAmount', coverageDetails.other1LimitAmount);
        }

        delete policiesByProduct['GENERAL_LIABILITY'];
    }
}