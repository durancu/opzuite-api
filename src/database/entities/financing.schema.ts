import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { Customer } from './customer.schema';
import { Insurer } from './insurer.schema';
import { Sale, SaleSchema } from './sale.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Financing extends Document {
    // #region Properties (21)

    @Prop({ default: 0, required: false })
    public amount: number;
    @Prop({ required: false })
    public apr: number;
    @Prop({ required: false })
    public aprPeriod: string;
    @Prop({ type: [SchemaTypes.Map], required: false })
    public bills: any;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company', required: true })
    public company: Company;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public coordinator: User;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
    public createdBy: User;
    @Prop({ required: false })
    public currentBalance: number;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Customer', required: false })
    public customer: Customer;
    @Prop()
    public description: string;
    @Prop({ default: 0, required: false })
    public downPayment: number;
    @Prop()
    public financedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Insurer', required: false })
    public financer: Insurer;
    @Prop({ type: [SchemaTypes.Map], required: false })
    public payments: any;
    @Prop({ type: [SaleSchema], required: false })
    public policies: Sale[]
    @Prop()
    public status: string;
    @Prop()
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;

    // #endregion Properties (21)
}

export const FinancingSchema = SchemaFactory.createForClass(Financing);

FinancingSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
