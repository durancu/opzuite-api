import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Customer, CustomerSchema } from 'src/database/entities/customer.schema';
import { CustomerModuleInitializerService } from './customer-module-initializer.service';
import { CustomerController } from './customer.controller';
import { CustomerService } from './customer.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Customer.name, schema: CustomerSchema },
    ]),
  ],
  controllers: [
    CustomerController
  ],
  providers: [
    CustomerService,
    CustomerModuleInitializerService
  ],
  exports: [
    CustomerService
  ]
})
export class CustomerModule { }
