import { IsOptional } from "class-validator";

export class CreateRoleDto {
  // #region Properties (4)

  @IsOptional()
  description?: string;
  @IsOptional()
  hierarchy?: number;
  @IsOptional()
  name?: string;
  @IsOptional()
  permissions?: string[];

  // #endregion Properties (4)
}
