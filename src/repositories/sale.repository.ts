
import { BadRequestException, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model, Types } from 'mongoose';
import { SearchResult } from 'src/commons/interfaces/search-result.interface';
import { calculateAggregatePagination, executeAggregatorWithPagination } from 'src/commons/queries/mongo/helpers/query';
import { Sale } from 'src/database/entities/sale.schema';
import { ExtendedSaleDto } from 'src/dto/sale/extended-sale.dto';
import { UpdateSaleDto } from 'src/dto/sale/update-sale.dto';
import { buildPoliciesReadyToRenewAggregator } from 'src/modules/sale/queries/mongo/aggregators/get-ready-renewals.aggregator';
import { buildQueryConditions, extractParamFilters } from 'src/modules/sale/sale.utils';
import { buildSearchAggregator } from '../modules/sale/queries/mongo/aggregators/search.aggregator';


export class SaleRepository {
    // #region Constructors (1)

    constructor(@InjectModel(Sale.name) private readonly saleModel: Model<Sale>) { }

    // #endregion Constructors (1)

    // #region Public Methods (6)

    public async batchDeleteSales(filter: FilterQuery<Sale>): Promise<any> {
        return this.saleModel.deleteMany(filter).exec();
    }

    public async findById(id: string, options: any): Promise<Sale> {

        const query = this.saleModel.findOne({ _id: id });

        if ('populate' in options) {
            const { populate } = options;
            populate.map((field: any) => {
                query.populate(field, populate['field'])
            });
        }

        return await query.exec();
    }

    public async createSale(
        saleDto: ExtendedSaleDto,
        //session: ClientSession
    ) {
        let sale: Sale = await this.saleModel.create({
            ...saleDto
        });

        try {
            //sale = await sale.save({session});
            sale = await sale.save();
        } catch (error) {
            throw new InternalServerErrorException(error);
        }

        if (!sale) {
            throw new BadRequestException('Sale not created');
        }
        return sale;
    }

    public async deleteAll(): Promise<any> {
        return this.saleModel.deleteMany({}).exec();
    }

    public async deleteByCode(code: string): Promise<any> {
        this.saleModel.findOneAndDelete({ code: code }).then((deleted: Sale) => {
            if (!deleted) {
                throw new NotFoundException(`Sale with code: ${code} not found`);
            }
        })
    }

    public async findAll(): Promise<any> {
        const query = this.saleModel.aggregate();
        query
            .unwind({ path: '$seller', preserveNullAndEmptyArrays: true })
            .lookup({
                from: 'users',
                localField: 'seller',
                foreignField: '_id',
                as: 'seller',
            })
            .unwind({ path: '$seller', preserveNullAndEmptyArrays: true });

        query
            .unwind({ path: '$customer', preserveNullAndEmptyArrays: true })
            .lookup({
                from: 'customers',
                localField: 'customer',
                foreignField: '_id',
                as: 'customer',
            })
            .unwind({ path: '$customer', preserveNullAndEmptyArrays: true });

        query
            .unwind({ path: '$location', preserveNullAndEmptyArrays: true })
            .lookup({
                from: 'locations',
                localField: 'location',
                foreignField: '_id',
                as: 'location',
            })
            .unwind({ path: '$location', preserveNullAndEmptyArrays: true })

            .append({
                $project: {
                    code: '$code',
                    items: '$items',
                    lineOfBusiness: '$lineOfBusiness',
                    soldAt: '$soldAt',
                    fees: { $round: ['$fees', 2] },
                    tips: { $round: ['$tips', 2] },
                    chargesPaid: { $round: ['$chargesPaid', 2] },
                    premium: { $round: ['$premium', 2] },
                    amountReceivable: { $round: ['$amountReceivable', 2] },
                    totalCharge: { $round: ['$totalCharge', 2] },
                    downPayment: { $round: ['$downPayment', 2] },
                    sellerName: {
                        $concat: ['$seller.firstName', ' ', '$seller.lastName'],
                    },
                    locationName: { $ifNull: ['$location.business.name', ''] },
                    customerId: '$customer._id',
                    customerName: '$customer.name',
                    createdBy: '$createdBy',
                    updatedBy: '$updatedBy',
                    seller: 1,
                    customer: 1,
                    location: 1,
                },
            })
            .sort({ soldAt: -1 });

        return query.exec();
    }

    public async findByCode(code: string, layout?: string) {
        const saleQuery = this.saleModel.findOne({ code: code });

        if (layout === 'FULL') {
            saleQuery.populate('seller', 'role name');
            saleQuery.populate('customer', 'type business.usDOT name',);
            saleQuery.populate('items.carrier', 'name');
            saleQuery.populate('items.broker', 'name');
        }

        const sale: Sale = await saleQuery.exec();
        if (!sale) {
            throw new NotFoundException(`Sale (code: ${code}) not found.`);
        }

        return sale;
    }

    public async search(queryParams?: any): Promise<SearchResult> {

        const queryFilters: any = extractParamFilters(queryParams);
        const conditions = buildQueryConditions(queryParams, queryFilters);

        const aggregator = buildSearchAggregator(conditions);

        const result = await executeAggregatorWithPagination(aggregator, queryParams, this.saleModel);
        return result;
    }


    public async getPoliciesReadyToRenew(
        queryParams?: any
    ): Promise<any> {
        const queryFilters: any = extractParamFilters(queryParams);
        const conditions = buildQueryConditions(queryParams, queryFilters);

        const aggregator = buildPoliciesReadyToRenewAggregator(queryParams.monthsToExpiration || 3, conditions);

        const resultFacet = calculateAggregatePagination(queryParams);
        aggregator.push({
            $facet: {
                totalCount: [{ $count: 'count' }],
                results: resultFacet
            },
        });

        const query = this.saleModel.aggregate(aggregator);

        const [result] = await query.exec();
        const totalCount = result.totalCount.length > 0 ? result.totalCount[0].count : 0;
        const entities = result.results;

        return {
            entities: entities,
            totalCount: totalCount,
            page: queryParams.pageNumber,
            limit: queryParams.pageSize,
        };
    }

    async update(id: string, updateSaleDto: UpdateSaleDto): Promise<any> {
        return await this.saleModel.findOneAndUpdate(
            { _id: new Types.ObjectId(id) },
            { ...updateSaleDto },
            { new: true },
        );
    }

    // #endregion Public Methods (6)
}

