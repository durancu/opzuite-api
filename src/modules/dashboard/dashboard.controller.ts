import {
  Body,
  Controller, HttpCode,
  Param,
  Post,
  Scope,
  UseGuards
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { ReportParamsDto } from 'src/dto/report/report-params.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { RolesGuard } from '../auth/guard/roles.guard';
import { DashboardService } from './dashboard.service';

@ApiTags('Dashboard')
@Controller({ path: 'dashboards', scope: Scope.REQUEST })
export class DashboardController {
  constructor(private dashboardService: DashboardService) { }

  @Post('/charts/:code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, RolesGuard)
  async getChartByCode(
    @Param('code') code: string,
    @Body() params: any,
  ): Promise<any> {
    const queryParams: ReportParamsDto = params.queryParams || {};
    return await this.dashboardService.getChartByCode(code, queryParams);
  }
}
