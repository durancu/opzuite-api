interface SearchQueryParams {
    sortField?: string;
    sortOrder?: string;
    pageNumber?: number;
    pageSize?: number;
    filter?: {
        [key: string]: string;
        type?: string;
    };
    searchText?: string;
    searchColumns?: string[];
}