import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Query, Req, Scope, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { Location } from 'src/database/entities/location.schema';
import { CreatePayrollDto } from 'src/dto/payroll/create-payroll.dto';
import { PayrollDto } from 'src/dto/payroll/payroll.dto';
import { UpdatePayrollDto } from 'src/dto/payroll/update-payroll.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { LocationService } from 'src/modules/location/location.service';
import { Payroll } from '../../database/entities/payroll.schema';
import { InitPayrollDto } from '../../dto/payroll/init-payroll.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { PayrollService } from './payroll.service';

@ApiTags('Payroll')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'payrolls', scope: Scope.REQUEST })
export class PayrollController {
  // #region Constructors (1)

  constructor(
    private payrollService: PayrollService,
    private locationService: LocationService,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (9)

  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_CREATE)
  //@UseFilters(MongoFilter)
  public async createPayroll(
    @Req() req: Request,
    @Body() payroll: CreatePayrollDto,
  ): Promise<PayrollDto> {
    return await this.payrollService.save(payroll);
  }

  @Delete('/:code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_CANCEL)
  public async deletePayrollByCode(@Param('code') code: string): Promise<Payroll> {
    return await this.payrollService.deleteByCode(code);
  }

  @Post('/generate')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS)
  public async generate(
    @Req() req: Request,
    @Query('force') force: boolean,
    @Query('all') all: boolean,
  ): Promise<any> {
    //for each location
    return this.payrollService.generatePayroll(force, all);
  }

  @Get()
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_READ)
  public async getAllPayrolls(): Promise<Payroll[]> {
    return await this.payrollService.findAll();
  }

  @Get('/available-dates')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_CREATE)
  public async getAvailablePayPeriod(
    @Query('location') location: Partial<Location>,
  ): Promise<InitPayrollDto[]> {
    return await this.payrollService.getAvailablePayPeriod(location);
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_READ)
  public async getPayrollByCode(@Param('code') code: string): Promise<Payroll> {
    return await this.payrollService.findByCode(code);
  }

  @Post('/init')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_CREATE)
  //@UseFilters(MongoFilter)
  public async initPayroll(
    @Req() req: Request,
    @Body() payroll: InitPayrollDto,
  ): Promise<PayrollDto> {
    return await this.payrollService.initPayroll(payroll);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_READ)
  public async searchPayrolls(@Body() query: any): Promise<any> {
    return await this.payrollService.search(query.queryParams);
  }

  @Put('/:code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.PAYROLLS_UPDATE)
  //@UseFilters(MongoFilter)
  public async updatePayroll(
    @Param('code') code: string,
    @Body() payroll: UpdatePayrollDto,
  ): Promise<Payroll> {
    return await this.payrollService.update(code, payroll);
  }

  // #endregion Public Methods (9)
}
