import { IsBoolean, IsDateString, IsMongoId, IsOptional } from "class-validator";
import { Company } from "src/database/entities/company.schema";
import { Customer } from "src/database/entities/customer.schema";
import { Endorsement } from "src/database/entities/endorsement.schema";
import { Location } from "src/database/entities/location.schema";
import { Product } from "src/database/entities/nested/product.schema";
import { SaleItemVersion } from "src/database/entities/nested/sale-item-version.schema";
import { SaleItem } from "src/database/entities/nested/sale-item.schema";
import { Sale } from "src/database/entities/sale.schema";
import { User } from "src/database/entities/user.schema";

export class ExtendedSaleDto {
  // #region Properties (47)

  @IsOptional()
  public id?: Partial<Sale>;
  @IsOptional()
  @IsBoolean()
  public autoRenew?: boolean;
  @IsOptional()
  @IsDateString()
  public cancelledAt?: Date;
  @IsOptional()
  public checksumSanity?: number;
  @IsOptional()
  public code?: string;
  @IsMongoId()
  public company?: Partial<Company>;
  @IsOptional()
  public country?: string;
  @IsOptional()
  public createdAt?: Date;
  @IsOptional()
  public createdBy?: Partial<User>;
  @IsMongoId()
  public customer?: Partial<Customer>;
  @IsOptional()
  public details?: any;
  @IsDateString()
  @IsOptional()
  public effectiveAt?: Date;
  @IsOptional()
  public endorsements?: Partial<Endorsement>[];
  @IsOptional()
  public expiresAt?: Date;
  public isChargeItemized?: boolean;
  @IsOptional()
  public itemVersions?: SaleItemVersion[];
  @IsOptional()
  public items?: SaleItem[];
  public lineOfBusiness?: string;
  @IsOptional()
  public location?: Partial<Location>;
  @IsOptional()
  public monthlyPayment?: number;
  @IsOptional()
  public number?: string;
  @IsOptional()
  public products?: Partial<Product>[];
  @IsOptional()
  public renewalFrequency?: string;
  @IsOptional()
  public renewalReferences?: string[]
  @IsOptional()
  public renewalReferenceCodes?: string[];
  @IsOptional()
  public renewedBy?: Partial<User>;
  @IsOptional()
  public renewedWith?: Partial<Sale>;
  @IsOptional()
  public seller?: Partial<User>;
  @IsOptional()
  public soldAt?: Date;
  @IsOptional()
  public states?: string;
  @IsOptional()
  public status?: string;
  @IsOptional()
  public totalAgencyCommission?: number;
  @IsOptional()
  public totalAgentCommission?: number;
  @IsOptional()
  public totalCoveragesDownPayment?: number;
  @IsOptional()
  public totalCoveragesPremium?: number;
  @IsOptional()
  public totalFinanced?: number;
  @IsOptional()
  public totalFinancedPaid?: number;
  @IsOptional()
  public totalNonPremium?: number;
  @IsOptional()
  public totalPaid?: number;
  @IsOptional()
  public totalPayables?: number;
  @IsOptional()
  public totalPermits?: number;
  @IsOptional()
  public totalPremium?: number;
  @IsOptional()
  public totalReceivables?: number;
  @IsOptional()
  public totalReceived?: number;
  @IsOptional()
  public totalTaxesAndFees?: number;
  @IsOptional()
  public type?: string;
  @IsOptional()
  public updatedAt?: Date;
  @IsOptional()
  public updatedBy?: Partial<User>;
  @IsOptional()
  public wasRenewed?: boolean;

  // #endregion Properties (47)
}
