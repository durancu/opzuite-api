import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { DashboardContent, DashboardContentSchema } from "./dashboard-content.schema";

@Schema()
export class DashboardLayout extends Document {
  // #region Properties (5)

  @Prop({ type: [DashboardContentSchema], default: [] })
  public bottom: DashboardContent;
  @Prop({ type: [DashboardContentSchema], default: [] })
  public left: DashboardContent;
  @Prop({ type: [DashboardContentSchema], default: [] })
  public main: DashboardContent;
  @Prop({ type: [DashboardContentSchema], default: [] })
  public right: DashboardContent;
  @Prop({ type: [DashboardContentSchema], default: [] })
  public top: DashboardContent;

  // #endregion Properties (5)
}

export const DashboardLayoutSchema = SchemaFactory.createForClass(DashboardLayout);