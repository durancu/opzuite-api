import { IsOptional } from 'class-validator';

export class UpdatePasswordDto {
  // #region Properties (2)

  @IsOptional()
  public newPassword?: string;
  @IsOptional()
  public oldPassword?: string;

  // #endregion Properties (2)
}
