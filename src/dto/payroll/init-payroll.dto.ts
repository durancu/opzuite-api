import { IsNotEmpty, IsOptional } from 'class-validator';
import { Company } from 'src/database/entities/company.schema';
import { Location } from 'src/database/entities/location.schema';
export class InitPayrollDto {
  @IsOptional()
  location?: Partial<Location>;

  @IsNotEmpty()
  payPeriodEndedAt?: Date;

  @IsNotEmpty()
  payPeriodStartedAt?: Date;

  @IsNotEmpty()
  periodCode?: string;

  @IsOptional()
  @IsNotEmpty()
  scope?: string; //can be Company (C), Location (L), Individual (I)

  @IsOptional()
  company?: Partial<Company>;
}
