import {
  ConflictException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectModel } from '@nestjs/mongoose';
import { compare } from 'bcrypt';
import { pick } from 'lodash';
import { Model, Types } from 'mongoose';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserStatus } from 'src/commons/enum/user-status.enum';
import { generateRandomToken } from 'src/commons/lib/user-functions';
import { User } from 'src/database/entities/user.schema';
import {
  NotificationTypes,
  SendgridService,
} from '../sendgrid/sendgrid.service';
import { AccessToken } from './interface/access-token.interface';
import { JwtPayload } from './interface/jwt-payload.interface';

@Injectable()
export class AuthService {
  // #region Constructors (1)

  constructor(
    private jwtService: JwtService,
    private sendgridService: SendgridService,
    @InjectModel('User') private userModel: Model<User>,
  ) {}

  // #endregion Constructors (1)

  // #region Public Methods (6)

  /*   public async confirmEmailAddressByToken(
      confirmToken: string,
    ): Promise<User> {
      const user: User = await this.userService.confirmEmailAddressByToken(confirmToken);
  
      if (!user) {
        return null;
      }
  
      //await this.sendgridService.sendNotification(NotificationTypes.EMPLOYEE_WELCOME, user, { password: password });
  
      return user;
    } */
  public async confirmEmailAddressByToken(confirmToken: string): Promise<User> {
    const user: User = await this.userModel
      .findOne({ confirmToken: confirmToken })
      .exec();

    if (!user) {
      return null;
    }

    const token = generateRandomToken();

    const confirmedUser: Partial<User> = {
      ...user['_doc'],
      confirmToken: null,
      status: UserStatus.RESET_REQUIRED,
      passwordToken: token,
    };

    return await this.userModel
      .findOneAndUpdate(
        { _id: new Types.ObjectId(confirmedUser._id) },
        { ...confirmedUser },
        { new: true },
      )
      .exec();
  }

  public async forgotPassword(email: string): Promise<User> {
    const user = await this.generatePasswordToken(email);
    return user;
  }

  public async generatePasswordToken(email: string): Promise<User> {
    const userFound: User = await this.userModel
      .findOne({ email: email, status: { $ne: UserStatus.ARCHIVED } })
      .populate('company')
      .exec();

    if (!userFound) {
      throw new NotFoundException(`User (email: ${email}) not found.`);
    }

    if (userFound.passwordToken && userFound.passwordToken !== null) {
      throw new ConflictException(
        `You already requested a token. Please check your email.`,
      );
    }

    const token = await generateRandomToken();

    const user: User = {
      ...userFound['_doc'],
      passwordToken: token,
    };

    const updated: User = await this.userModel.findOneAndUpdate(
      { _id: userFound._id },
      user,
      {},
    );

    await this.sendgridService.sendNotification(
      NotificationTypes.EMPLOYEE_FORGOT_PASSWORD_CONFIRM_EMAIL,
      user,
      { company: userFound.company, token: user.passwordToken },
    );

    return updated;
  }

  // If `LocalStrategy#validateUser` return a `Observable`, the `request.user` is
  // bound to a `Observable<Partial<User>>`, not a `Partial<User>`.
  //
  // I would like use the current `Promise` for this case, thus it will get
  // a `Partial<User>` here directly.
  //
  public login(user: Partial<User>): Observable<AccessToken> {
    const payload: JwtPayload = {
      upn: user.email, //upn is defined in Microprofile JWT spec, a human readable principal name.
      sub: user.id,
      username: user.username,
      employeeInfo: user.employeeInfo,
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      phone: user.phone,
      company: user.company,
      location: user.location,
      permissions: user.permissions,
      country: user.country,
      preferences: user.preferences,
      role: user.role,
      type: user.type,
      authorizedIpAddresses: user.authorizedIpAddresses,
      status: user.status,
    };

    return from(this.jwtService.signAsync(payload)).pipe(
      map((accessToken) => {
        return { accessToken };
      }),
    );
  }

  public async resetPassword(token: string, password: string): Promise<User> {
    const userFound: User = await this.userModel
      .findOne({ passwordToken: token, status: { $ne: UserStatus.ARCHIVED } })
      .exec();

    if (!userFound) {
      throw new NotFoundException(`User not found or invalid token.`);
    }

    const user: User = new this.userModel({
      ...userFound['_doc'],
      password: password,
      passwordToken: null,
      status: UserStatus.CONFIRMED,
    });

    const updated = await this.userModel.findOneAndUpdate(
      { _id: new Types.ObjectId(userFound._id) },
      user,
      { new: true },
    );

    return updated;
  }

  public async validateUser(email: string, password: string): Promise<User> {
    const user: User = await this.userModel.findOne({ email }).exec();

    if (!user) {
      throw new UnauthorizedException(`User not found`);
    }

    const pass: any = user.password;

    const matchesPassword = await compare(password, pass);

    if (!matchesPassword) {
      throw new UnauthorizedException(`Email or password is not matched`);
    }

    const authUser = new this.userModel(
      pick(user, [
        '_id',
        'username',
        'email',
        'employeeInfo',
        'firstName',
        'lastName',
        'phone',
        'company',
        'location',
        'permissions',
        'country',
        'preferences',
        'role',
        'type',
        'authorizedIpAddresses',
        'status',
      ]),
    );

    authUser.id = authUser._id;
    delete authUser['_id'];

    return authUser;
  }

  // #endregion Public Methods (6)
}
