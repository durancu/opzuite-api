import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Location, LocationSchema } from 'src/database/entities/location.schema';
import { Role, RoleSchema } from 'src/database/entities/role.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { LocationRepository } from 'src/repositories/location.repository';
import { LocationModuleInitializerService } from './location-module-initializer.service';
import { LocationController } from './location.controller';
import { LocationService } from './location.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Location.name, schema: LocationSchema },
      { name: Role.name, schema: RoleSchema },
      { name: User.name, schema: UserSchema },
    ]),
  ],
  controllers: [LocationController],
  providers: [
    LocationService,
    LocationModuleInitializerService,
    LocationRepository
  ],
  exports: [
    LocationService,
    LocationRepository
  ]
})
export class LocationModule { }
