import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Query, Response, Scope, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { ExtendedSaleDto } from 'src/dto/sale/extended-sale.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';

import { CreateSaleDto } from '../../dto/sale/create-sale.dto';
import { UpdateSaleDto } from '../../dto/sale/update-sale.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { SaleService } from './sale.service';
import { sanitizeSale } from './sale.utils';

@ApiTags('Sale')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'sales', scope: Scope.REQUEST })
export class SaleController {
  // #region Constructors (1)

  constructor(private saleService: SaleService) { }

  // #endregion Constructors (1)

  // #region Public Methods (11)

  @Post('')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_CREATE,
    PermissionType.APPLICATIONS_CREATE,
  )
  public async createSale(@Body() sale: CreateSaleDto): Promise<ExtendedSaleDto> {
    sanitizeSale(sale);
    return await this.saleService.create(sale);
  }

  /*   @Post('/batch-create')
    @HttpCode(201)
    @UseGuards(JwtAuthGuard, PermissionsGuard)
    @HasPermissions(
      PermissionType.POLICIES_CREATE,
      PermissionType.APPLICATIONS_CREATE,
    )
    public async createSales(@Body() sales: CreateSaleDto[]): Promise<ExtendedSaleDto> {
      const cleanSales: CreateSaleDto[] = sales.map((sale: CreateSaleDto) => {
        sanitizeSale(sale);
        return sale;
      });
      return await this.saleService.batchCreate(cleanSales);
    } */

  @Delete('/:code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_DELETE,
    PermissionType.APPLICATIONS_DELETE,
  )
  public async deleteSale(@Param('code') code: string): Promise<ExtendedSaleDto> {
    return await this.saleService.deleteByCode(code);
  }

  @Post('/delete')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_DELETE,
    PermissionType.APPLICATIONS_DELETE,
  )
  public async deleteSales(@Body() body: any): Promise<ExtendedSaleDto> {
    const codes: string[] = body['codes'];
    return await this.saleService.batchDelete(codes);
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_READ,
    PermissionType.APPLICATIONS_READ,
  )
  public async findAll(@Response() res): Promise<any> {
    return res.json(await this.saleService.findAll());
  }

  @Post('renewals')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_READ,
    PermissionType.APPLICATIONS_READ,
  )
  public async getPoliciesReadyToRenew(
    @Body() query: any,
  ): Promise<any> {
    return await this.saleService.getPoliciesReadyToRenew(query.queryParams);
  }

  @Get('/:code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_READ,
    PermissionType.APPLICATIONS_READ,
  )
  public async getSaleByCode(
    @Param('code') code: string,
    @Query('layout') layout?: string,
  ): Promise<any> {
    return await this.saleService.findByCode(code, layout);
  }

  @Post('/renew')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.POLICIES_RENEW)
  public async renewSale(
    @Body() sale: ExtendedSaleDto,
  ): Promise<ExtendedSaleDto> {
    sanitizeSale(sale);
    return await this.saleService.renew(sale);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_READ,
    PermissionType.APPLICATIONS_READ,
  )
  public async searchSales(
    @Body() query: any,
    @Response() res: any
  ): Promise<any> {
    return res.json(await this.saleService.search(query.queryParams));
  }

  @Put('/:code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.POLICIES_UPDATE,
    PermissionType.APPLICATIONS_UPDATE,
  )
  public async updateSale(
    @Param('code') code: string,
    @Body() sale: UpdateSaleDto,
  ): Promise<ExtendedSaleDto> {
    sanitizeSale(sale);
    return await this.saleService.update(code, sale);
  }

  // #endregion Public Methods (11)
}
