export enum MetricsCalMethod {
  COVERAGES = 'coverages',
  ENDORSEMENTS = 'endorsements',
}