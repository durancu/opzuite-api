import { IsNotEmpty, IsOptional } from 'class-validator';
import { Address } from 'src/database/entities/nested/address.schema';

export class UpdateProfileDto {
  // #region Properties (27)

  @IsOptional()
  public address?: Address;
  @IsOptional()
  public dob?: string;
  @IsNotEmpty()
  public firstName?: string;
  @IsOptional()
  public gender?: string;
  @IsOptional()
  public lastName?: string;
  @IsOptional()
  public mobilePhone?: string;
  @IsOptional()
  public phone?: string;

  // #endregion Properties (27)
}
