export const POLICY_STATUS = [
  { name: '', id: 'NONE' },
  { name: 'Agent interviewed the customer', id: 'CUSTOMER_INTERVIEWED' },
  { name: 'Checked Customer Company and Permissions', id: 'CUSTOMER_VALIDATED' },
  { name: 'Processing Customer company creation', id: 'CUSTOMER_COMPANY_CREATION' },
  { name: 'Processing Customer permissions request', id: 'CUSTOMER_PERMISSIONS_REQUEST' },
  { name: 'Retrieving Quotes from Insurers', id: 'QUOTING' },
  { name: 'Quotes Received from Insurers', id: 'QUOTES_RECEIVED' },
  { name: 'Quote sent to Customer', id: 'QUOTE_SENT_TO_CUSTOMER' },
  { name: 'Customer paid', id: 'CUSTOMER_PAID' },
  { name: 'Documents and Binder sent to Insurer', id: 'DOC_AND_BINDER_SENT_TO_INSURER' },
  { name: 'Documents sent to Financer', id: 'DOC_SENT_TO_FINANCER' },
  { name: 'Insurer reported policy to institutions', id: 'INSURER_REPORTED_INSTITUTIONS' },
  { name: 'Active', id: 'ACTIVE' },
  { name: 'Pending', id: 'PENDING' },
  { name: 'Cancelled', id: 'CANCELLED' },
]

