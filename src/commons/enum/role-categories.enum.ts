export enum RoleCategories {
  SUPERADMIN = 'super-administrator',
  ADMIN = 'administrator',
  COMPANY_MANAGER = 'company-manager',
  COMPANY_OFFICER = 'company-officer',
  COUNTRY_MANAGER = 'country-manager',
  REGION_MANAGER = 'region-manager',
  LOCATION_MANAGER = 'location-manager',
  SELLER = 'csr',
  CERTIFICATES = 'certificates',
  ENDORSEMENTS = 'endorsements',
  TRAINEE = 'csr-in-training'
}

export const RoleHierarchyEdges = {
  SUPERADMIN: 1,
  ADMIN: 20,
  OWNER: 30,
  OFFICER: 50,
  COUNTRY: 100,
  REGION: 200,
  LOCATION: 300,
  AGENT: 400,
  TRAINEE: 500,
};
