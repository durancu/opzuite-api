import {
  Body,
  Controller, Delete, Get,
  HttpCode,
  Param, Post,
  Put, Scope, UseGuards, UseInterceptors
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { SystemSetting } from 'src/database/entities/system-setting.schema';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { CreateSystemSettingDto } from '../../dto/system-setting/create-system-setting.dto';
import { UpdateSystemSettingDto } from '../../dto/system-setting/update-system-setting.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { PermissionsGuard } from '../auth/guard/permissions.guard';
import { SettingService } from './settings.service';

@ApiTags('System')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'system', scope: Scope.REQUEST })
export class SystemController {
  constructor(
    private settingService: SettingService,
  ) { }

  //SYSTEM SETTINGS
  @Get('settings')
  @HttpCode(200)
  @HasPermissions(PermissionType.SYSTEM_READ)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  async getAllSystemSettings(
  ): Promise<SystemSetting[]> {
    return await this.settingService.findAll();
  }

  @Get('settings/catalog')
  @HttpCode(200)
  @HasPermissions(PermissionType.SYSTEM_READ)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  async settingsCatalog(
  ): Promise<SystemSetting[]> {
    return await this.settingService.catalog();
  }

  @Get('settings/:key')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.SYSTEM_READ)
  async getSystemSettingByKey(
    @Param('key') key: string,
  ): Promise<SystemSetting> {
    return await this.settingService.findByKey(key);
  }

  @Post('settings')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.SYSTEM_CREATE)
  async createSystemSetting(@Body() system: CreateSystemSettingDto): Promise<SystemSetting> {
    return await this.settingService.save(system);
  }

  @Put('settings/:key')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.SYSTEM_UPDATE)
  async updateSystemSetting(
    @Param('key') key: string,
    @Body() system: UpdateSystemSettingDto,
  ): Promise<SystemSetting> {
    return await this.settingService.update(key, system);
  }

  @Delete('settings/:key')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.SYSTEM_DELETE)
  async deleteSystemSettingByKey(
    @Param('key') key: string,
  ): Promise<SystemSetting> {
    return await this.settingService.deleteByKey(key);
  }


}
