import { Company } from "src/database/entities/company.schema";
import { Location } from "src/database/entities/location.schema";
import { PayStub } from "src/database/entities/nested/pay-stub.schema";
import { User } from "src/database/entities/user.schema";

export class PayrollDto {
    code?: string;
    country?: string;
    executedBy?: Partial<User>;
    company?: Partial<Company>;
    location?: Partial<Location>;
    payExecutedAt?: Date;
    payPeriodEndedAt?: Date;
    payPeriodStartedAt?: Date;
    payStubs?: Partial<PayStub>[];
    periodCode?: string;
    scope?: string; //can be COMPANY (C), LOCATION (L), INDIVIDUAL (I)
    status?: string; //can be: IN-PROGRESS, EXECUTED
  
    totalExtraBonus?: number;
    totalExtraBonusDescription?: string;
    totalDiscount?: number;
    totalDiscountDescription?: string;
    totalDownPayment?: number;
    totalFees?: number;
    totalNetSalary?: number;
    totalPermits?: number;
    totalReimbursement?: number;
    totalReimbursementDescription?: string;
    totalRegularSalary?: number;
    totalSaleBonus?: number;
    totalSales?: number;
    totalTips?: number;
    totalPremium?: number;
    totalNonPremium?: number;
    totalTaxesAndFees?: number;
  
    createdBy?: Partial<User>;
    updatedBy?: Partial<User>;
  }
  
  
  