import { Role } from "src/database/entities/role.schema";
import { RoleHierarchyEdges } from "../enum/role-categories.enum";

//ADMIN ACCESS BY ROLE FUNCTIONS
export const hasLocationManagerRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy < RoleHierarchyEdges.AGENT;
};

export const hasRegionManagerRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy < RoleHierarchyEdges.LOCATION;
};

export const hasCountryManagerRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy < RoleHierarchyEdges.REGION;
};

export const hasOfficerManagerRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy < RoleHierarchyEdges.COUNTRY;
};

export const hasOwnerRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy < RoleHierarchyEdges.OFFICER;
};

export const hasAdminRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy < RoleHierarchyEdges.OWNER;
};

export const hasSuperAdminRoleAccess = (role: Partial<Role>) => {
  return role.hierarchy === RoleHierarchyEdges.SUPERADMIN;
};

//ROLE RANGES BASED FUNCTIONS

export const inLocationManagerRolesRange = (role: Partial<Role>) => {
  return (
    role.hierarchy > RoleHierarchyEdges.LOCATION &&
    role.hierarchy < RoleHierarchyEdges.AGENT
  );
};

export const inRegionManagerRolesRange = (role: Partial<Role>) => {
  return (
    role.hierarchy > RoleHierarchyEdges.REGION &&
    role.hierarchy < RoleHierarchyEdges.LOCATION
  );
};

export const inCountryManagerRolesRange = (role: Partial<Role>) => {
  return (
    role.hierarchy > RoleHierarchyEdges.COUNTRY &&
    role.hierarchy < RoleHierarchyEdges.REGION
  );
};

export const inOfficerManagerRolesRange = (role: Partial<Role>) => {
  return (
    role.hierarchy > RoleHierarchyEdges.OFFICER &&
    role.hierarchy < RoleHierarchyEdges.COUNTRY
  );
};

export const inOwnerRolesRange = (role: Partial<Role>) => {
  return (
    role.hierarchy > RoleHierarchyEdges.OWNER &&
    role.hierarchy < RoleHierarchyEdges.OFFICER
  );
};

export const inAdminRolesRange = (role: Partial<Role>) => {
  return (
    role.hierarchy > RoleHierarchyEdges.ADMIN && role.hierarchy < RoleHierarchyEdges.OWNER
  );
};

export const inSuperAdminRolesRange = (role: Partial<Role>) => {
  return role.hierarchy === RoleHierarchyEdges.SUPERADMIN;
};
