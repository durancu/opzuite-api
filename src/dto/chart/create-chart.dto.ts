import { IsOptional } from "class-validator";
import { PermissionType } from "src/commons/enum/permission-type.enum";
import { ChartOptions } from "src/database/entities/nested/chart-options.schema";
import { Report } from "src/database/entities/report.schema";

export class CreateChartDto {
  // #region Properties (7)

  @IsOptional()
  description?: string;
  @IsOptional()
  library?: string;
  @IsOptional()
  name?: string;
  @IsOptional()
  options?: ChartOptions;
  @IsOptional()
  permissions?: PermissionType[];
  @IsOptional()
  report?: Partial<Report>;
  @IsOptional()
  scope?: string;

  // #endregion Properties (7)
}
