export const SALE_STATUS = [
  { name: 'Active', id: 'ACTIVE' },
  { name: 'Cancelled', id: 'CANCELLED' },
  { name: 'Pending', id: 'PENDING' },
  { name: 'Suspended', id: 'SUSPENDED' },
  { name: 'Closed', id: 'CLOSED' }, 
]