import { Acord25PDFTemplate } from "./acord-25-pdf-template";

export class Acord25PdfTemplateV2014NowCerts extends Acord25PDFTemplate {
    // #region Constructors (1)



    constructor() {
        super();
        this.fieldPrefix = 'topmostSubform[0].Page1[0].';
        this.fieldSuffix = '[0]';
    }

    // #endregion Constructors (1)
}