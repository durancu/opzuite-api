import { IsOptional } from "class-validator";

export class CreateSystemSettingDto {
  // #region Properties (3)

  @IsOptional()
  description?: string;
  @IsOptional()
  key?: string;
  @IsOptional()
  value?: any;

  // #endregion Properties (3)
}
