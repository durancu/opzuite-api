import { getAlphabeticLetterFromNumber } from "src/commons/certificates/helper-functions";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillInsurersFields = (carriersWithCoverages: any[], template: Acord25PdfTemplateV2014NowCerts) => {
    if (carriersWithCoverages) {
        if (carriersWithCoverages.length > 6) {
            //TODO: Split array to keep first 6 in carriersWithCoverages, and a new one otherCarriersWithCoverages with the others
            fillFirstSixCarriersWithCoverages(carriersWithCoverages.slice(0, 6), template);
            fillOtherCarriersWithCoverages(carriersWithCoverages.slice(6), template);
        } else {
            fillFirstSixCarriersWithCoverages(carriersWithCoverages, template);
        }
    }
}

function fillFirstSixCarriersWithCoverages(carriersWithCoverages: any[], template: Acord25PdfTemplateV2014NowCerts) {
    for (let i = 1; i <= carriersWithCoverages.length; i++) {
        const carrierWithCoverages = carriersWithCoverages[i - 1];
        const letter = getAlphabeticLetterFromNumber(i);
        carrierWithCoverages['letter'] = letter;
        template.setField(`Insurer_FullName_${letter}`, carrierWithCoverages.name);
        template.setField(`Insurer_NAICCode_${letter}`, carrierWithCoverages.naicCode);
    }
}

function fillOtherCarriersWithCoverages(carriersWithCoverages: any[], template: Acord25PdfTemplateV2014NowCerts) {
    //TODO: Add otherCarriersWithCoverages to Description section or to 101 form
    return null;
}
