import { isEmpty } from "lodash";
import { Certificate } from "src/database/entities/certificate.schema";
import { Cohort } from "src/database/entities/nested/cohort.schema";
import { SaleItem } from "src/database/entities/nested/sale-item.schema";
import { VehicleInfo } from "src/database/entities/nested/vehicle.info.schema";
import { Sale } from "src/database/entities/sale.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../../acord-25/models/acord-25-pdf-template-v-2014-now-certs";


export const fillRemarksByPolicyFields = (certificate: Partial<Certificate>, policy: Sale, template: Acord25PdfTemplateV2014NowCerts) => {
    let vehicleLines = '';
    let cohortLines = '';
    if (!isEmpty(policy.items) && !isEmpty(certificate.vehiclesIncluded)) {
        policy.items.map((coverage: SaleItem) => {
            if (!isEmpty(coverage.vehicles)) {
                coverage.vehicles.map((vehicle: VehicleInfo) => {
                    const vehicleIsIncluded = certificate.vehiclesIncluded.some((includedVehicle: VehicleInfo) => {
                        return includedVehicle['vinNumber'] === vehicle['vinNumber']
                    });
                    if (vehicleIsIncluded) {
                        vehicleLines = vehicleLines.concat(
                            `${vehicle.year}, ${vehicle.make}, VIN: ${vehicle.vinNumber} \n`
                        )
                    }
                })
            }
        });
    }

    if (!isEmpty(policy.items) && !isEmpty(certificate.cohortsIncluded)) {
        policy.items.map((coverage: SaleItem) => {
            if (!isEmpty(coverage.cohorts)) {
                coverage.cohorts.map((cohort: Cohort) => {
                    const cohortIsIncluded = certificate.cohortsIncluded.some((includedCohort: Cohort) => {
                        return includedCohort['id'] === cohort['id']
                    });
                    if (cohortIsIncluded) {
                        cohortLines = cohortLines.concat(
                            `${cohort.firstName} ${cohort.lastName} \n`
                        )
                    }
                })
            }
        });
    }

    template.setField('AdditionalRemark_FormIdentifier_A', certificate.code);
    template.setField('AdditionalRemark_FormName_A', certificate.masterTemplate.name);

    if (!isEmpty(vehicleLines) || !isEmpty(cohortLines)) {
        template.hasAdditionalRemarksSchedule = true;
    }

    if (!isEmpty(vehicleLines)) {
        vehicleLines = `\n Vehicles \n ${vehicleLines} \n`;
    }
    if (!isEmpty(cohortLines)) {
        cohortLines = `\n Drivers \n ${cohortLines} \n`;
    }

    template.setField('AdditionalRemark_RemarkText_A', vehicleLines + cohortLines);
}


