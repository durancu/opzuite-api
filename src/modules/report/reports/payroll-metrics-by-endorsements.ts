import { createDateRangeFilterExpressions } from "src/commons/lib/aggregator-functions";
import { ReportParams } from "src/database/entities/nested/report-params.schema";
export async function calculatePayrollMetricsByEndorsements(
    params: ReportParams,
  ): Promise<any> {
    let conditions = {};
    conditions = {
      $and: [
        { deleted: false },
        { 'sale.deleted': false },
        { 'sale.type': 'POLICY' },
      ],
    };

    if (params.dateFrom || params.dateTo) {
      conditions['$and'].push({
        endorsedAt: createDateRangeFilterExpressions(
          params.dateFrom,
          params.dateTo,
        ),
      });
    }

    const query = this.endorsementModel.aggregate();

    query
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'sales',
        localField: 'sale',
        foreignField: '_id',
        as: 'sale',
      })
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .addFields({
        followUpPerson: { $ifNull: ['$followUpPerson', '$sale.seller'] },
      })
      .addFields({
        seller: { $ifNull: ['$seller', '$sale.seller'] },
      })
      .addFields({
        endorsedAt: { $ifNull: ['$endorsedAt', '$sale.soldAt'] },
      });

    query
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'users',
        localField: 'seller',
        foreignField: '_id',
        as: 'seller',
      })
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false });

    query
      .unwind({ path: '$seller.role', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'roles',
        localField: 'seller.role',
        foreignField: '_id',
        as: 'role',
      })
      .unwind({ path: '$role', preserveNullAndEmptyArrays: false });

    query
      .unwind({ path: '$sale.location', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'locations',
        localField: 'sale.location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: false })

      .unwind({ path: '$sale.customer', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'customers',
        localField: 'sale.customer',
        foreignField: '_id',
        as: 'customer',
      })
      .unwind({ path: '$customer', preserveNullAndEmptyArrays: false });

    if (conditions) {
      query.match(conditions);
    }

    query.group({
      seller: { $first: '$seller.name' },
      location: { $first: '$location.business.name' },
      role: { $first: '$seller.role.name' },
      employeeCategory: { $first: '$seller.employeeInfo.category' },
      country: { $first: '$seller.country' },
      customer: { $first: '$customer.name' },
      totalPremium: {
        $sum: '$totalPremium',
      },
      totalAgentCommission: {
        $sum: '$totalAgentCommission',
      },
      totalTaxesAndFees: {
        $sum: '$totalTaxesAndFees',
      },
      totalNonPremium: {
        $sum: '$totalNonPremium',
      },
      totalAgencyCommission: {
        $sum: '$totalAgencyCommission',
      },
      totalNonCommissionablePremium: {
        $sum: '$totalNonCommissionablePremium',
      },
      _id: {
        id: '$seller',
        label: '$seller.name',
      },
    });

    let sortCriteria = {};
    sortCriteria['totalPremium'] = -1;

    if (params.sortField && params.sortField.length > 0) {
      const order = params.sortOrder && params.sortOrder === 'asc' ? 1 : -1;
      sortCriteria = {};
      sortCriteria[params.sortField] = order;
    }

    query.sort(sortCriteria);

    return query.exec();
  }