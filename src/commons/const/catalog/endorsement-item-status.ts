export const PERMIT_ENDORSEMENT_ITEM_STATUS = [
  { name: 'Entered in system', id: 'ENTERED' },
  { name: 'Applications sent', id: 'SENT' },
  { name: 'Application processed', id: 'PROCESSED' },
  { name: 'Customer documentation received', id: 'CUSTOMER_RECEIVED' },
  { name: 'Customer documentation approved', id: 'CUSTOMER_APPROVED' },
  { name: 'Suspended for miscellaneous reason', id: 'SUSPENDED' },
  { name: 'Complete', id: 'COMPLETE' },
]

export const POLICY_ENDORSEMENT_ITEM_STATUS = [
  { name: 'In Progress', id: 'IN_PROGRESS' },
  { name: 'On Hold', id: 'ON_HOLD' },
  { name: 'Pending to review', id: 'PENDING' },
  { name: 'Complete', id: 'COMPLETE' },
]

