export enum CoverageBasedForm {
    UMBRELLA = 'UMBRELLA',
    EXCESS = 'EXCESS'
}