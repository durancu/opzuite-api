import { ApiProperty } from '@nestjs/swagger';
import {
  IsOptional
} from 'class-validator';

class CreateFileDto {
  @ApiProperty({ type: 'string', format: 'binary', required: true })
  file: Express.Multer.File;

  @IsOptional()
  name?: string;

  @IsOptional()
  path?: string;

  @IsOptional()
  type?: string;

  @IsOptional()
  tags?: string;

}

export { CreateFileDto };
