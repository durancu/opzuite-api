import { BadRequestException, ConflictException } from '@nestjs/common';
import { Insurer } from 'src/database/entities/insurer.schema';

import { Types } from 'mongoose';
import { createDateRangeFilterExpressions } from 'src/commons/lib/aggregator-functions';
import * as DateFactory from 'src/commons/lib/date-functions';
import { roundAmount } from 'src/commons/lib/math-functions';
import { buildSearchTextQueries } from 'src/commons/queries/mongo/helpers/query';
import { Endorsement } from 'src/database/entities/endorsement.schema';
import { Commission } from 'src/database/entities/nested/commission.schema';
import { EndorsementItem } from 'src/database/entities/nested/endorsement-item.schema';
import { SaleItem } from 'src/database/entities/nested/sale-item.schema';
import { ExtendedSaleDto } from 'src/dto/sale/extended-sale.dto';

const PERMIT_COMMISION_PERCENT = 0.2;
const FEE_COMMISION_PERCENT = 0.3;

export async function calculateMetrics(
  saleDto: ExtendedSaleDto,
  providers: Insurer[],
): Promise<ExtendedSaleDto> {
  switch (saleDto.type) {
    case 'POLICY':
      return calculatePolicyMetrics(saleDto, providers);
    case 'PERMIT':
      return calculateApplicationMetrics(saleDto);
    default:
      throw new ConflictException(
        'Can not calculate metrics for unknown sale type',
      );
  }
}

export async function calculatePolicyMetrics(
  saleDto: ExtendedSaleDto,
  providers: Insurer[],
): Promise<ExtendedSaleDto> {
  let checksumSanity = 0;
  let totalAgencyCommission = 0;
  let totalAgentCommission = 0;
  let totalFinanced = 0;
  let totalFinancedPaid = 0;
  let totalPayables = 0;
  let totalPaid = 0;
  let totalReceivables = 0;
  let totalReceived = 0;
  let totalPremium = 0;
  let totalNonPremium = 0;
  let totalTaxesAndFees = 0;
  let totalCoveragesPremium = 0;
  let totalCoveragesDownPayment = 0;
  let totalPermits = 0;

  const sale: ExtendedSaleDto = { ...saleDto };

  if (sale.items) {
    /* ----- Loop item (coverages)  [BEGIN]----- */
    if (sale.isChargeItemized && sale.items) {
      sale.items.map((item: SaleItem) => {
        if (['ACTIVE', 'PENDING'].includes(item.status)) {
          item.premium = parseFloat(item.premium.toString() || '0');
          item.amount = parseFloat(item.amount.toString() || '0');
        } else {
          item.premium = 0;
          item.amount = 0;
        }
        switch (sale.type) {
          case 'POLICY':
            totalCoveragesPremium += item.premium;
            totalCoveragesDownPayment += item.amount;

            if (!item.broker && !item.carrier) {
              throw new ConflictException(
                `Cannot calculate profit for a coverage that is missing Carrier or MGA.`,
              );
            }

            //calculate item profits based on provider commissions
            item.agencyCommission = agencyCommisionByCoverage(providers, item);
            totalAgencyCommission += item.agencyCommission || 0;

            break;
          case 'PERMIT':
            totalPermits += item.premium;
            break;
          default:
            throw new BadRequestException(
              `Cannot calculate metrics. ${sale.type} is not a valid sale type.`,
            );
        }
      });
    }
  }

  if (sale.endorsements) {
    sale.endorsements.map((endorsement: Endorsement) => {
      endorsement.totalAgencyCommission = 0;
      endorsement.totalAgentCommission = 0;
      endorsement.totalFinanced = 0;
      endorsement.totalFinancedPaid = 0;
      endorsement.totalPayables = 0;
      endorsement.totalPaid = 0;
      endorsement.totalReceivables = 0;
      endorsement.totalReceived = 0;
      endorsement.totalNonPremium = 0;
      endorsement.totalTaxesAndFees = 0;

      endorsement.totalPremium = endorsement.amount || 0;

      if (endorsement.items) {
        endorsement.items.map((item: EndorsementItem) => {
          item.amount = item.amount || 0;
          item.amountPaid = item.amountPaid || 0;

          switch (item.accountingClass) {
            case 'PAYABLE':
              endorsement.totalPayables += item.amount;
              endorsement.totalPaid += item.amountPaid;
              break;
            case 'RECEIVABLE':
              endorsement.totalReceivables += item.amount;
              endorsement.totalReceived += item.amountPaid;
              break;
            case 'FINANCING_DIRECT_BILL':
              endorsement.totalFinanced += item.amount;
              endorsement.totalFinancedPaid += item.amountPaid;
              break;
            case 'FEE_TAX':
              endorsement.totalTaxesAndFees += item.amount;
              endorsement.totalNonPremium += item.amount;
              break;
            case 'AGENT_COMMISSION':
              endorsement.totalAgentCommission += item.amount;
              break;
            case 'AGENCY_COMMISSION':
              endorsement.totalAgencyCommission += item.amount;
              break;
            default:
              throw new BadRequestException(
                `Cannot calculate metrics. ${sale.type} is not a valid endorsement item class.`,
              );
          }
        });

        totalAgencyCommission += endorsement.totalAgencyCommission;
        totalAgentCommission += endorsement.totalAgentCommission;
        totalFinanced += endorsement.totalFinanced;
        totalFinancedPaid += endorsement.totalFinancedPaid;
        totalPayables += endorsement.totalPayables;
        totalPaid += endorsement.totalPaid;
        totalReceivables += endorsement.totalReceivables;
        totalReceived += endorsement.totalReceived;
        totalPremium += endorsement.totalPremium;
        totalNonPremium += endorsement.totalNonPremium;
        totalTaxesAndFees += endorsement.totalTaxesAndFees;
      }
    });
  }

  checksumSanity = -1 * (totalPremium + totalNonPremium - totalFinanced - totalReceivables + totalPayables);

  sale.checksumSanity = roundAmount(checksumSanity);
  sale.totalAgencyCommission = roundAmount(totalAgencyCommission);
  sale.totalAgentCommission = roundAmount(totalAgentCommission);
  sale.totalFinanced = roundAmount(totalFinanced);
  sale.totalFinancedPaid = roundAmount(totalFinancedPaid);
  sale.totalPayables = roundAmount(totalPayables);
  sale.totalPaid = roundAmount(totalPaid);
  sale.totalReceivables = roundAmount(totalReceivables);
  sale.totalReceived = roundAmount(totalReceived);
  sale.totalPremium = roundAmount(totalPremium);
  sale.totalNonPremium = roundAmount(totalNonPremium);
  sale.totalTaxesAndFees = roundAmount(totalTaxesAndFees);
  sale.totalCoveragesPremium = roundAmount(totalCoveragesPremium);
  sale.totalCoveragesDownPayment = roundAmount(totalCoveragesDownPayment);

  sale.totalPermits = roundAmount(totalPermits);

  sale.totalAgencyCommission = roundAmount(
    totalAgencyCommission + (1 - FEE_COMMISION_PERCENT) * totalTaxesAndFees,
  );
  sale.totalAgentCommission = roundAmount(
    totalAgentCommission + FEE_COMMISION_PERCENT * totalTaxesAndFees,
  );

  return sale;
}

export async function calculateApplicationMetrics(
  saleDto: ExtendedSaleDto
): Promise<ExtendedSaleDto> {
  let checksumSanity = 0;
  let totalAgencyCommission = 0;
  let totalAgentCommission = 0;
  let totalFinanced = 0;
  let totalFinancedPaid = 0;
  let totalPayables = 0;
  let totalPaid = 0;
  let totalReceivables = 0;
  let totalReceived = 0;
  let totalPremium = 0;
  let totalNonPremium = 0;
  let totalTaxesAndFees = 0;
  const totalCoveragesPremium = 0;
  const totalCoveragesDownPayment = 0;
  const totalPermits = 0;

  const sale: ExtendedSaleDto = { ...saleDto };

  if (sale.endorsements) {
    sale.endorsements.map((endorsement: Endorsement) => {
      endorsement.totalAgencyCommission = 0;
      endorsement.totalAgentCommission = 0;
      endorsement.totalFinanced = 0;
      endorsement.totalFinancedPaid = 0;
      endorsement.totalPayables = 0;
      endorsement.totalPaid = 0;
      endorsement.totalReceivables = 0;
      endorsement.totalReceived = 0;
      endorsement.totalNonPremium = 0;
      endorsement.totalTaxesAndFees = 0;

      endorsement.totalPremium = endorsement.amount || 0;

      if (endorsement.items) {
        endorsement.items.map((item: EndorsementItem) => {
          item.amount = item.amount || 0;
          item.amountPaid = item.amountPaid || 0;

          switch (item.accountingClass) {
            case 'PAYABLE':
              endorsement.totalPayables += item.amount;
              endorsement.totalPaid += item.amountPaid;
              break;
            case 'RECEIVABLE':
              endorsement.totalReceivables += item.amount;
              endorsement.totalReceived += item.amountPaid;
              break;
            case 'FINANCING_DIRECT_BILL':
              endorsement.totalFinanced += item.amount;
              endorsement.totalFinancedPaid += item.amountPaid;
              break;
            case 'FEE_TAX':
              endorsement.totalTaxesAndFees += item.amount;
              endorsement.totalNonPremium += item.amount;
              break;
            case 'AGENT_COMMISSION':
              endorsement.totalAgentCommission += item.amount;
              break;
            case 'AGENCY_COMMISSION':
              endorsement.totalAgencyCommission += item.amount;
              break;
            default:
              throw new BadRequestException(
                `Cannot calculate metrics. ${sale.type} is not a valid application item class.`,
              );
          }
        });

        totalAgencyCommission += endorsement.totalAgencyCommission;
        totalAgentCommission += endorsement.totalAgentCommission;
        totalFinanced += endorsement.totalFinanced;
        totalFinancedPaid += endorsement.totalFinancedPaid;
        totalPayables += endorsement.totalPayables;
        totalPaid += endorsement.totalPaid;
        totalReceivables += endorsement.totalReceivables;
        totalReceived += endorsement.totalReceived;
        totalPremium += endorsement.totalPremium;
        totalNonPremium += endorsement.totalNonPremium;
        totalTaxesAndFees += endorsement.totalTaxesAndFees;
      }
    });
  }

  checksumSanity =
    totalPremium +
    totalNonPremium -
    totalFinanced -
    totalReceivables +
    totalPayables;

  sale.checksumSanity = roundAmount(checksumSanity);
  sale.totalFinanced = roundAmount(totalFinanced);
  sale.totalFinancedPaid = roundAmount(totalFinancedPaid);
  sale.totalPayables = roundAmount(totalPayables);
  sale.totalPaid = roundAmount(totalPaid);
  sale.totalReceivables = roundAmount(totalReceivables);
  sale.totalReceived = roundAmount(totalReceived);
  sale.totalPremium = roundAmount(totalPremium);
  sale.totalNonPremium = roundAmount(totalNonPremium);
  sale.totalTaxesAndFees = roundAmount(totalTaxesAndFees);
  sale.totalCoveragesPremium = roundAmount(totalCoveragesPremium);
  sale.totalCoveragesDownPayment = roundAmount(totalCoveragesDownPayment);
  sale.totalPermits = roundAmount(totalPermits);

  sale.totalAgencyCommission = roundAmount(
    totalAgencyCommission +
    (1 - PERMIT_COMMISION_PERCENT) * sale.totalPremium +
    (1 - FEE_COMMISION_PERCENT) * sale.totalTaxesAndFees,
  );

  sale.totalAgentCommission = roundAmount(
    totalAgentCommission +
    PERMIT_COMMISION_PERCENT * sale.totalPremium +
    FEE_COMMISION_PERCENT * sale.totalTaxesAndFees,
  );

  return sale;
}

/**
 * @param  {string} dateRange
 */
export function getDateMatchExpressionByRange(dateRange: string): any {
  //Set filtering conditions
  const dates = DateFactory.dateRangeByName(dateRange);

  return dateRange
    ? {
      $gte: dates.start,
      $lte: dates.end
    }
    : { $lte: new Date(DateFactory.todayTz().format(DateFactory.DEFAULT_FORMAT)) };
}

export function agencyCommisionByCoverage(
  providers: Partial<Insurer>[],
  item: SaleItem,
): number {
  let agencyCommission = 0;

  const coverageProvider: Partial<Insurer> = item.broker
    ? item.broker
    : item.carrier;

  const foundProvider = providers.find(
    (provider) => provider.id === coverageProvider,
  );

  if (foundProvider) {
    let commission: Partial<Commission> = { percent: 0 };

    commission = foundProvider.commissions.find(
      (commission) => commission.coverage === item.product,
    );

    if (commission) {
      agencyCommission = roundAmount((commission.percent / 100) * item.premium);
    } else {
      //TODO: Notify admin system that provider commission has not been setup.
      console.log('Unable to calculate agency commission: provider not found.');
    }
  }

  return agencyCommission;
}

export function titleCase(str) {
  return str
    .toLowerCase()
    .split(' ')
    .map(function (word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(' ');
}

export function sanitizeSale(sale: any) {
  if (sale.items) {
    sale.items = sale.items.map((item) => {
      if (!item.broker) {
        delete item['broker'];
      }

      if (!item.carrier) {
        delete item['carrier'];
      }

      return {
        ...item,
      };
    });
  }
}

export function extractParamFilters(queryParams?: SaleQueryParams): any {
  const queryFilters: any = {};

  if (!queryParams || !queryParams.filter) {
    return queryFilters;
  }

  const { filter } = queryParams;

  const keysToExtract = [
    'saleDateFrom', 'saleDateTo',
    'effectiveDateFrom', 'effectiveDateTo',
    'expirationDateFrom', 'expirationDateTo',
    'lineOfBusiness', 'type', 'customer', 'seller',
    'carrier', 'broker', 'status', 'location', 'country', 'usDOT'
  ];

  for (const key of keysToExtract) {
    if (key in filter) {
      queryFilters[key] = filter[key];
      delete filter[key];
    }
  }

  // Special case for 'usDOT' and 'dot'
  if ('dot' in filter) {
    queryFilters['usDOT'] = filter.dot;
    delete filter['dot'];
  }

  return queryFilters;
}

export function buildQueryConditions(queryParams: SearchQueryParams | null | undefined, queryFilters: any) {
  const conditions = {
    $and: [{ deleted: false }],
  };

  if (queryParams) {
    if (
      Object.keys(queryFilters).length > 0 ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      setFiltersByParams(conditions, queryFilters);
    }

    let searchColumns: string[] = [
      'seller.name',
      'customer.name',
      'customer.business.usDOT',
      'location.business.name',
      'soldAt',
      'code',
    ];

    if (queryParams.searchText) {
      if (queryParams.searchColumns && queryParams.searchColumns.length) {
        searchColumns = queryParams.searchColumns;
      }
      const searchTextQueries = buildSearchTextQueries(
        queryParams.searchText,
        searchColumns,
      );

      if (searchTextQueries.length > 0) {
        conditions['$or'] = searchTextQueries;
      }
    }
  }

  return conditions;
}

export function setFiltersByParams(
  conditions: any,
  queryFilters: any,
): void {
  if (queryFilters['saleDateFrom'] || queryFilters['saleDateTo']) {
    conditions['$and'].push({
      soldAt: createDateRangeFilterExpressions(
        queryFilters['saleDateFrom'],
        queryFilters['saleDateTo'],
      ),
    });
  }

  if (queryFilters['effectiveDateFrom'] || queryFilters['effectiveDateTo']) {
    conditions['$and'].push({
      items: {
        $elemMatch: {
          effectiveAt: createDateRangeFilterExpressions(
            queryFilters['effectiveDateFrom'],
            queryFilters['effectiveDateTo'],
          ),
        },
      },
    });
  }

  if (queryFilters['expirationDateFrom'] || queryFilters['expirationDateTo']) {
    conditions['$and'].push({
      items: {
        $elemMatch: {
          expiresAt: createDateRangeFilterExpressions(
            queryFilters['expirationDateFrom'],
            queryFilters['expirationDateTo'],
          ),
        },
      },
    });
  }

  if (queryFilters['type']) {
    conditions['$and'].push({ type: queryFilters['type'] });
  }

  if (queryFilters['country']) {
    conditions['$and'].push({ country: queryFilters['country'] });
  }

  if (queryFilters['lineOfBusiness']) {
    conditions['$and'].push({
      items: {
        $elemMatch: { lineOfBusiness: queryFilters['lineOfBusiness'] },
      },
    });
  }

  if (queryFilters['customer']) {
    conditions['$and'].push({
      'customer._id': new Types.ObjectId(queryFilters['customer']),
    });
  }

  if (queryFilters['usDOT']) {
    conditions['$and'].push({
      'customer.business.usDOT': queryFilters['usDOT'],
    });
  }

  if (queryFilters['seller']) {
    conditions['$and'].push({
      'seller._id': new Types.ObjectId(queryFilters['seller']),
    });
  }

  if (queryFilters['broker']) {
    conditions['$and'].push({
      items: {
        $elemMatch: { broker: new Types.ObjectId(queryFilters['broker']) },
      },
    });
  }

  if (queryFilters['carrier']) {
    conditions['$and'].push({
      items: {
        $elemMatch: { carrier: new Types.ObjectId(queryFilters['carrier']) },
      },
    });
  }

  if (queryFilters['status']) {
    conditions['$and'].push({ status: queryFilters['status'] });
  }

  if (queryFilters['location']) {
    conditions['$and'].push({
      'location._id': new Types.ObjectId(queryFilters['location']),
    });
  }
}


