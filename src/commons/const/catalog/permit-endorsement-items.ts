export const PERMIT_ENDORSEMENT_ITEMS = [
  {
    name: 'Fee / Tax',
    id: 'FEE_TAX',
    description: '',
    permissions: [],
    color: '#E57010',
  },
  {
    name: 'Payable',
    id: 'PAYABLE',
    description: '',
    permissions: [],
    color: '#4781EB',
  },
  {
    name: 'Receivable',
    id: 'RECEIVABLE',
    description: '',
    permissions: [],
    color: '#7F32EC',
  },
  {
    name: 'Agent Commission',
    id: 'AGENT_COMMISSION',
    description: '',
    permissions: [],
    color: '#E1B23D',
  },
  {
    name: 'Agency Commission',
    id: 'AGENCY_COMMISSION',
    description: '',
    permissions: [],
    color: '#20c736',
  },
  {
    name: 'Financing / Direct Bill',
    id: 'FINANCING_DIRECT_BILL',
    description: '',
    permissions: [],
    color: '#2FB1A2',
  },
];
