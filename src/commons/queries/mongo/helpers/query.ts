import { escapeRegExp } from "lodash";
import { calculateLimitCriteria, calculateSkipCriteria, calculateSortCriteria } from "./search";

export function setFilterByQueryString(queryParams: any, filterQueries: any[]) {
    if (queryParams.filter && Object.keys(queryParams.filter).length > 0) {
        filterQueries = Object.keys(queryParams.filter).map((key) => {
            return {
                [key]: {
                    $regex: new RegExp(
                        '.*' + escapeRegExp(queryParams.filter[key]) + '.*',
                        'i',
                    ),
                },
            };
        });
    }
    return filterQueries;
}

export function buildSearchTextQueries(text: string, columns: string[]) {
    let searchQueries = [];
    if (text.trim().length > 0 && columns.length > 0) {
        searchQueries = columns.map((column) => {
            return {
                [column]: {
                    $regex: new RegExp('.*' + escapeRegExp(text) + '.*', 'i'),
                },
            };
        });
    }
    return searchQueries;
}


export function calculateAggregatePagination(queryParams: any) {

    const results = [];

    const sortCriteria = calculateSortCriteria(queryParams);
    if (Object.keys(sortCriteria).length > 0) {
        results.push({ $sort: sortCriteria });
    }

    const skipCriteria = calculateSkipCriteria(queryParams);
    if (skipCriteria) {
        results.push({ $skip: skipCriteria })
    }

    const limitCriteria = calculateLimitCriteria(queryParams);
    if (limitCriteria) {
        results.push({ $limit: limitCriteria })
    }

    return results;
}

export async function executeAggregatorWithPagination(aggregator: any[], queryParams: SearchQueryParams, model: any) {
    const resultFacet = calculateAggregatePagination(queryParams);

    aggregator.push({
        $facet: {
            totalCount: [{ $count: 'count' }],
            results: resultFacet,
        },
    });

    const query = model.aggregate(aggregator);

    const [result] = await query.exec();
    const totalCount = result.totalCount.length > 0 ? result.totalCount[0].count : 0;
    const entities = result.results;

    return {
        entities: entities,
        totalCount: totalCount,
        page: queryParams.pageNumber,
        limit: queryParams.pageSize,
    };
}