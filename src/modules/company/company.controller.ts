import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Scope, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { Company } from 'src/database/entities/company.schema';
import { CreateCompanyDto } from 'src/dto/company/create-company.dto';
import { UpdateCompanyDto } from 'src/dto/company/update-company.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { JwtAuthGuard } from 'src/modules/auth/guard/jwt-auth.guard';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { CompanyService } from './company.service';

@ApiTags('Company')
@Controller({ path: 'companies', scope: Scope.REQUEST })
export class CompanyController {
  // #region Constructors (1)

  constructor(
    private companyService: CompanyService,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (7)

  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.COMPANIES_CREATE)
  //@UseFilters(MongoFilter)
  public async createCompany(@Body() company: CreateCompanyDto): Promise<Company> {
    return await this.companyService.save(company);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.COMPANIES_DELETE)
  public async deleteCompanyByCode(@Param('code') code: string): Promise<Company> {
    return await this.companyService.deleteByCode(code);
  }

  @Get()
  @HttpCode(200)
  @HasPermissions(PermissionType.COMPANIES_READ)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  public async getAllCompanies(): Promise<Company[]> {
    return await this.companyService.findAll();
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.COMPANIES_READ)
  public async getCompanyByCode(@Param('code') code: string): Promise<Company> {
    return await this.companyService.findByCode(code);
  }

  @Get('my-company')
  @HttpCode(200)
  @HasPermissions(PermissionType.MY_COMPANY, PermissionType.MY_COMPANY_READ)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  public async getMyCompany(): Promise<Company> {
    return await this.companyService.getMyCompany();
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.COMPANIES_UPDATE)
  //@UseFilters(MongoFilter)
  public async updateCompany(
    @Param('code') code: string,
    @Body() company: UpdateCompanyDto,
  ): Promise<Company> {
    return await this.companyService.update(code, company);
  }

  @Put('my-company')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.MY_COMPANY, PermissionType.MY_COMPANY_UPDATE)
  //@UseFilters(MongoFilter)
  public async updateMyCompany(@Body() company: UpdateCompanyDto): Promise<Company> {
    return await this.companyService.updateMyCompany(company);
  }

  // #endregion Public Methods (7)
}
