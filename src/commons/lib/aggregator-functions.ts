import * as DateFactory from 'src/commons/lib/date-functions';
import { GroupingCriteria } from "../enum/grouping-criteria.enum";

/**
 * @param  {string} startDate?
 * @param  {string} endDate?
 */
export function createDateRangeFilterExpressions(
  startDate?: Date,
  endDate?: Date,
): any {
  startDate = startDate
    ? new Date(new Date(startDate).setHours(0, 0, 0))
    : null;
  endDate = endDate ? new Date(new Date(endDate).setHours(23, 59, 59)) : null;

  if (startDate && endDate) {
    return {
      $gte: startDate,
      $lte: endDate,
    };
  } else if (startDate) {
    return { $gte: startDate };
  } else if (endDate) {
    return { $lte: endDate };
  } else return { $lte: new Date() };
}

export function getModifiersByGroupingCriteria(groupingCriteria: string): any {
  switch (groupingCriteria) {
    case GroupingCriteria.SELLER:
      return {
        groupId: {
          id: '$seller._id',
          label: { $concat: ['$seller.name'] },
        },
      };
    case GroupingCriteria.LOCATION:
      return {
        groupId: {
          label: '$location',
        },
      };
    case GroupingCriteria.YEAR:
      return {
        groupId: {
          label: { $year: '$soldAt' },
        },
      };
    case GroupingCriteria.MONTH:
      return {
        groupId: {
          label: { $month: '$soldAt' },
        },
      };
    case GroupingCriteria.DAY:
      return {
        groupId: {
          label: { $dayOfMonth: '$soldAt' },
        },
      };
  }
}


/**
 * @param  {string} dateRange
 */
export function getDateMatchExpressionByRange(dateRange: string): any {
  //Set filtering conditions
  const dates = DateFactory.dateRangeByName(dateRange);

  return dateRange
    ? {
      $gte: dates.start,
      $lte: dates.end
    }
    : { $lte: new Date(DateFactory.todayTz().format(DateFactory.DEFAULT_FORMAT)) };
}