import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Financing, FinancingSchema } from 'src/database/entities/financing.schema';
import { FinancingDataInitializerService } from './financing-module-initializer.service';
import { FinancingController } from './financing.controller';
import { FinancingService } from './financing.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Financing.name, schema: FinancingSchema },
    ]),
  ],
  controllers: [
    FinancingController
  ],
  providers: [
    FinancingService,
    FinancingDataInitializerService,
  ],
  exports: [
    FinancingService
  ]
})
export class FinancingModule { }
