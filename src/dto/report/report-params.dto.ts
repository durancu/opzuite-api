import { IsOptional } from "class-validator";
import { ReportParamsFilterDto } from "./report-params-filter.dto";

export class ReportParamsDto{
    @IsOptional()
    model?: string;
    @IsOptional()
    filters?: ReportParamsFilterDto[];
    @IsOptional()
    groupByModel?: string;
    @IsOptional()
    groupByFields?: string[];
    @IsOptional()
    dateFrom?: Date;
    @IsOptional()
    dateTo?: Date;
    @IsOptional()
    fields?: string[];
    @IsOptional()
    withCount?: boolean;
    @IsOptional()
    withRecords?: boolean;
    @IsOptional()
    sortOrder?: string;
    @IsOptional()
    sortField?: string;
}