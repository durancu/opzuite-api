import { Acord25PDFTemplate } from "./acord-25-pdf-template";

export class Acord25PDFTemplateV2018 extends Acord25PDFTemplate {
    // #region Constructors (1)

    constructor() {
        super();
        this.fieldPrefix = 'F[0].P1[0].';
        this.fieldSuffix = '_A[0]';
    }

    // #endregion Constructors (1)
}