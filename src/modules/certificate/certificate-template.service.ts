
import {
  Inject,
  Injectable,
  NotFoundException,
  Scope
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { buildSearchTextQueries } from 'src/commons/queries/mongo/helpers/query';
import { CertificateMasterTemplate } from 'src/database/entities/certificate-master-template.schema';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';


@Injectable({ scope: Scope.REQUEST })
export class CertificateTemplateService {

  // #region Constructors (1)

  constructor(
    @InjectModel(CertificateMasterTemplate.name) private masterTemplateModel: Model<CertificateMasterTemplate>,
    @Inject(REQUEST) private request: AuthenticatedRequest,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (11)

  /**
   * @param {string[]} codes 
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.masterTemplateModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.masterTemplateModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<CertificateMasterTemplate> {
    const template: CertificateMasterTemplate = await this.masterTemplateModel
      .findOne({ code: code })
      .exec();

    if (!template) {
      throw new NotFoundException(`Certificate (code: ${code}) not found.`);
    }

    return template;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<CertificateMasterTemplate> {
    const template: CertificateMasterTemplate = await this.masterTemplateModel
      .findOne({ _id: id })
      .exec();

    if (!template) {
      throw new NotFoundException(`Certificate (id: ${id}) was not found`);
    }

    return template;
  }

  public async getCatalog(filterCriteria: any): Promise<any> {
    return await this.masterTemplateModel
      .find(filterCriteria)
      .select('code name shortName type _id')
      .sort({ name: 1 })
      .exec();
  }

  public async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let type = null;
    if (queryParams.filter) {
      if (('type' in queryParams.filter) && queryParams.filter.type.trim() !== '') {
        type = queryParams.filter.type;
        delete queryParams.filter['type'];
      }
    }

    let conditions = {};

    conditions = {
      $and: [{ deleted: false }],
    };

    if (
      type ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      if (type) {
        conditions['$and'].push({ type: type });
      }
    }

    if (queryParams.searchText) {
      let searchColumns: string[] = [
        'name',
      ];

      if (queryParams.searchColumns && queryParams.searchColumns.length) {
        searchColumns = queryParams.searchColumns;
      }
      const searchTextQueries = buildSearchTextQueries(
        queryParams.searchText,
        searchColumns,
      );

      if (searchTextQueries.length > 0) {
        conditions['$or'] = searchTextQueries;
      }
    }

    const query = this.masterTemplateModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query.unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy',
      })
      .unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true });

    query.append({
      $project: {
        id: '$_id',
        type: '$type',
        name: '$name',
        shortName: '$shortName',
        createdAt: '$createdAt',
        createdBy: '$createdBy.name',
        code: '$code',
        deleted: '$deleted',
      }
    });

    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.sort(sortCriteria).skip(skipCriteria).limit(limitCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  // #endregion Public Methods (11)
}
