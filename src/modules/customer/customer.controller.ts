import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Req,
  Response,
  Scope,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';

import { Request } from 'express';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';

import { Customer } from '../../database/entities/customer.schema';
import { CreateCustomerDto } from '../../dto/customer/create-customer.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';

import { ApiTags } from '@nestjs/swagger';
import { UpdateCustomerDto } from '../../dto/customer/update-customer.dto';
import { CustomerService } from './customer.service';

@ApiTags('Customer')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'customers', scope: Scope.REQUEST })
export class CustomerController {
  constructor(private customerService: CustomerService) { }

  @Get()
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS_READ)
  async getAllCustomers(): Promise<any> {
    const res = await this.customerService.findAll();
    return {
      data: res,
    };
  }

  @Post('/batch-create')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS)
  async batchCreateCustomers(
    @Req() req: Request,
    @Body() customers: CreateCustomerDto[],
  ): Promise<any> {
    return await this.customerService.batchCreate(customers);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS_READ)
  async searchCustomers(@Body() query: any): Promise<any> {
    return await this.customerService.search(query.queryParams);
  }

  @Get(':id')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS_READ)
  async getCustomerByCode(
    @Response() res,
    @Param('id') id: string,
    @Headers('id-field') idField: string,
  ): Promise<Customer> {
    let customer: Customer;
    if (idField && idField === 'id') {
      customer = await this.customerService.findById(id);
    } else {
      customer = await this.customerService.findByCode(id);
    }
    return res.json(customer);
  }

  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS_CREATE)
  async createCustomer(
    @Body() customer: CreateCustomerDto,
  ): Promise<CreateCustomerDto> {
    return await this.customerService.save(customer);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS_UPDATE)
  async updateCustomer(
    @Param('code') code: string,
    @Body() customer: UpdateCustomerDto,
  ): Promise<Customer> {
    return await this.customerService.update(code, customer);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CUSTOMERS_DELETE)
  async deleteCustomerById(@Param('code') code: string): Promise<Customer> {
    return await this.customerService.deleteByCode(code);
  }
}
