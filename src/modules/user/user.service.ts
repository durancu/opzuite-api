/* eslint-disable no-useless-escape */
/* eslint-disable prefer-spread */

import {
  Inject,
  Injectable,
  NotFoundException,
  Scope,
  UnauthorizedException
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { compare } from 'bcrypt';
import { escapeRegExp } from 'lodash';
import { FilterQuery, Model, Types } from 'mongoose';
import { nanoid } from 'nanoid';
import {
  S3_PROFILE_PHOTO_DEFAULT_PATH,
  S3_PROFILE_PHOTO_FOLDER,
  S3_USERS_FOLDER
} from 'src/commons/const/project.constants';
import { RoleCategories } from 'src/commons/enum/role-categories.enum';
import { UserStatus } from 'src/commons/enum/user-status.enum';
import {
  hasAdminRoleAccess,
  hasSuperAdminRoleAccess
} from 'src/commons/lib/role-functions';
import { cleanEmail } from 'src/commons/lib/string-functions';
import {
  generateRandomPassword,
  generateRandomToken
} from 'src/commons/lib/user-functions';
import { Company } from 'src/database/entities/company.schema';
import { Location } from 'src/database/entities/location.schema';
import { EmployeeInfo } from 'src/database/entities/nested/employee-info.schema';
import { Role, RoleTypes } from 'src/database/entities/role.schema';
import { User } from 'src/database/entities/user.schema';
import { UpdatePasswordDto } from 'src/dto/user/update-password.dto';
import { UpdateProfileDto } from 'src/dto/user/update-profile.dto';
import { UpdateUserPreferencesDto } from 'src/dto/user/update-user-preferences.dto';
import { UserRepository } from 'src/repositories/user.repository';
import { BulkUpdateUserDto } from '../../dto/user/bulk-update-user.dto';
import { CreateUserDto } from '../../dto/user/create-user.dto';
import { UpdateUserDto } from '../../dto/user/update-user.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { FileService } from '../file/file.service';
import {
  NotificationTypes,
  SendgridService
} from '../sendgrid/sendgrid.service';

export interface UserPopulateOptions {
  // #region Properties (6)

  withCompany?: boolean;
  withLocation?: boolean;
  withPassword?: boolean;
  withRole?: boolean;
  withSales?: boolean;
  withSupervisor?: boolean;

  // #endregion Properties (6)
}

@Injectable({ scope: Scope.REQUEST })
export class UserService {
  // #region Properties (1)

  private sanitizeProfileAttributes = (user: UpdateUserDto) => {
    const userToSanitize = { ...user };
    if (user.firstName && user.lastName) {
      userToSanitize.name = `${user.firstName} ${user.lastName}`;
    }

    if (user.mobilePhone) {
      userToSanitize.mobilePhone = user.mobilePhone.replace(/\D/g, '');
    }

    if (userToSanitize.phone) {
      userToSanitize.phone = user.phone.replace(/\D/g, '');
    }

    if (user.email) {
      userToSanitize.email = cleanEmail(user.email);
    }

    return userToSanitize;
  };

  // #endregion Properties (1)

  // #region Constructors (1)

  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(Role.name) private roleModel: Model<Role>,
    @InjectModel(Company.name) private companyModel: Model<Company>,
    @InjectModel(Location.name) private locationModel: Model<Location>,
    private sendgridService: SendgridService,
    private fileService: FileService,
    @Inject(REQUEST) private req: AuthenticatedRequest,
    private readonly userRepository: UserRepository,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (15)

  public authUserWithRole(userId: string): Promise<User> {
    const options = {
      populate: {
        role: 'id hierarchy name',
      },
    };
    return this.userRepository.findById(userId, options);
  }

  /* public async batchCreate(usersDto: UserDto[], reqUser: Partial<User>) {
    const authUser = await this.userModel
      .findOne({ _id: new Types.ObjectId(reqUser.id) })
      .exec();
    const locations: Partial<Location>[] = await this.locationModel
      .find({})
      .exec();

    const users: any = usersDto.map(async (user: UserDto) => {
      user = this.sanitizeProfileAttributes(user);

      if (user.code) {
        user.updatedBy = user.updatedBy ? user.updatedBy : authUser.id;
      } else {
        user.username = user.email.split('@')[0] + nanoid(3);
        user.createdBy = authUser.id;
        user.company = authUser.company;

        //TODO: Replace with line below once we implement permissions edition in frontend
        const filterQuery: FilterQuery<Role> = !user.role
          ? { key: RoleCategories.SELLER }
          : { id: user.role };
        const role: Partial<Role> = await this.roleModel.findOne(filterQuery);

        if (!user.role) {
          user.role = role.id;
        }

        user.employeeInfo.category = role.key;
        user.permissions = role.permissions;
      }

      user.country = null;

      if (user.location) {
        const location = locations.find(
          (location: Location) => location.id === user.location
        );
        user.country = location.business?.address?.country;
      }

      user.employeeInfo.hourlyRate = this.calculateHourlyRate(
        user.employeeInfo
      );

      return user;
    });

    const bulkPayload = users.map((user: Partial<User>) => ({
      updateOne: {
        filter: { code: user.code },
        update: { $set: user },
        upsert: true,
      },
    }));

    this.userModel
      .bulkWrite(bulkPayload)
      .then((bulkWriteOpResult) => {
        return bulkWriteOpResult;
      })
      .catch((err) => {
        console.log('BULK update error');
        console.log(JSON.stringify(err, null, 2));
        throw new BadRequestException(
          'Cannot upsert customers. ' + JSON.stringify(err, null, 2),
        );
      });
  } */
  public async createUser(
    userDto: CreateUserDto,
    requestUser: Partial<User>,
  ): Promise<User> {
    const token = await generateRandomToken();

    const userToCreate: any = {
      ...userDto,
      username: userDto.email.split('@')[0] + nanoid(3),
      status: UserStatus.UNCONFIRMED,
      confirmToken: token,
      password: generateRandomPassword(8),
    };

    const authenticatedUser: Partial<User> = await this.userModel
      .findOne({ _id: requestUser.id })
      .populate('company');

    let userLocationAttributes = {
      country: null,
      authorizedIpAddresses: [],
    };

    if (userToCreate.location) {
      const location: Partial<Location> = await this.locationModel
        .findById(userToCreate.location)
        .exec();
      userLocationAttributes = {
        country: location.business?.address?.country,
        authorizedIpAddresses: [location.ipAddress],
      };
    }

    const companyId: string = userToCreate.company
      ? userToCreate.company
      : authenticatedUser.company.id;

    const company = await this.companyModel.findOne({ _id: companyId }).exec();

    await this.setRoleAndRelatedAttributes(userToCreate, requestUser, company);

    const cleanUser: any = this.sanitizeProfileAttributes(userToCreate);

    const created: User = await this.userModel.create({
      ...cleanUser,
      createdBy: authenticatedUser.id,
      company: company.id,
      ...userLocationAttributes,
    });

    await this.sendgridService.sendNotification(
      NotificationTypes.EMPLOYEE_NEW_CONFIRM_EMAIL,
      created,
      { company: company },
    );

    return created;
  }

  public async delete(id: string): Promise<User> {
    const userToDelete: User = await this.userModel.findOne({ _id: id }).exec();

    if (!userToDelete) {
      throw new NotFoundException(`User:${id} was not found`);
    }

    userToDelete.status = UserStatus.ARCHIVED;

    await userToDelete.save();

    await userToDelete.delete();

    return userToDelete;
  }

  public async exists(field: string, value: any): Promise<boolean> {
    const filter: any = {};
    if (!(field in ['id', 'email', 'username'])) {
      return false;
    }
    filter[field] = value;
    const userFound: User = await this.userModel.findOne({ filter }).exec();
    return !!userFound;
  }

  public async findAll(skip = 0, limit = 0): Promise<User[]> {
    return await this.userModel.find().skip(skip).limit(limit).exec();
  }

  public async findByEmail(email: string): Promise<User> {
    return await this.userModel.findOne({ email }).exec();
  }

  public async findById(
    id: string,
    populateOptions: UserPopulateOptions = {},
  ): Promise<User> {
    const userQuery = this.userModel.findOne({ _id: id });

    if (populateOptions) {
      if (populateOptions.withRole) {
        userQuery.populate('role', 'id hierarchy name');
      }

      if (populateOptions.withSales) {
        userQuery.populate('sales');
      }
      if (populateOptions.withCompany) {
        userQuery.populate('company');
      }

      if (populateOptions.withLocation) {
        userQuery.populate('location');
      }

      if (populateOptions.withSupervisor) {
        userQuery.populate('supervisor');
      }

      if (!populateOptions.withPassword) {
        userQuery.select('-password');
      }
    }

    const userFound: User = await userQuery.exec();
    if (!userFound) {
      throw new NotFoundException(`User:${id} was not found`);
    }

    return userFound;
  }

  public async findByLocation(location: Partial<Location>): Promise<User[]> {
    return await this.userModel.find({ location: location }).exec();
  }

  public async findByUsername(username: string): Promise<User> {
    return await this.userModel.findOne({ username }).exec();
  }

  public async getCatalog(
    filterCriteria?: any,
    user?: Partial<User>,
  ): Promise<any> {
    let role = null;
    if ('role' in filterCriteria) {
      role = filterCriteria.role;
      delete filterCriteria['role'];
    }

    let country = null;
    if ('country' in filterCriteria) {
      country = filterCriteria.country;
      delete filterCriteria['country'];
    }

    let location = null;
    if ('location' in filterCriteria) {
      location = filterCriteria.location;
      delete filterCriteria['location'];
    }

    let company = null;
    if ('company' in filterCriteria) {
      company = filterCriteria.company;
      delete filterCriteria['company'];
    }

    const conditions = {};
    const fixedQueries = [];
    let filterQueries = [];

    conditions['$and'] = [{ deleted: false }];
    conditions['$and'].push({ status: { $ne: UserStatus.ARCHIVED } });

    if (user) {
      const foundUser: Partial<User> = await this.findById(user._id);

      if (!hasAdminRoleAccess(foundUser.role)) {
        conditions['$and'].push({
          'employeeInfo.category': {
            $nin: [RoleCategories.SUPERADMIN, RoleCategories.ADMIN],
          },
        });
      } else if (!hasSuperAdminRoleAccess(user.role)) {
        conditions['$and'].push({
          'employeeInfo.category': { $nin: [RoleCategories.SUPERADMIN] },
        });
      }
    }

    if (
      role ||
      country ||
      location ||
      company ||
      (filterCriteria && Object.keys(filterCriteria).length > 0)
    ) {
      if (role) {
        conditions['$and'].push({ 'role.key': role });
      }

      if (country) {
        conditions['$and'].push({ country: country });
      }

      if (location) {
        conditions['$and'].push({
          'location._id': new Types.ObjectId(location),
        });
      }

      if (company) {
        conditions['$and'].push({
          company: new Types.ObjectId(company),
        });
      }

      if (filterCriteria && Object.keys(filterCriteria).length > 0) {
        filterQueries = Object.keys(filterCriteria).map((key) => {
          return {
            [key]: filterCriteria[key],
          };
        });
      }
    }

    if (filterQueries.length || fixedQueries.length) {
      conditions['$and'] = [...filterQueries, ...fixedQueries];
    }

    const query = this.userModel.aggregate();

    query
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'locations',
        localField: 'location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true });

    query
      .unwind({ path: '$role', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'roles',
        localField: 'role',
        foreignField: '_id',
        as: 'role',
      })
      .unwind({ path: '$role', preserveNullAndEmptyArrays: true });

    if (conditions) {
      query.match(conditions);
    }

    query.append({
      $project: {
        id: '$_id',
        role: '$role.name',
        type: '$type',
        category: '$employeeInfo.category',
        location: {
          $cond: {
            if: { $eq: ['$location', null] },
            then: '',
            else: '$location.business.name',
          },
        },
        name: '$name',
        position: {
          $cond: {
            if: { $eq: ['$employeeInfo', null] },
            then: '',
            else: '$employeeInfo.position',
          },
        },
        code: '$code',
        country: '$country',
        status: '$status',
      },
    });


    return await query.sort({ name: 1 }).exec();
  }

  public async getEmployees(filterCriteria?: any): Promise<any> {
    return await this.userModel
      .find(filterCriteria || {})
      .sort({ name: 1 })
      .exec();
  }

  public async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let role = null;

    if ('role' in queryParams.filter) {
      role = queryParams.filter.role;
      delete queryParams.filter['role'];
    }

    let country = null;
    if ('country' in queryParams.filter) {
      country = queryParams.filter.country;
      delete queryParams.filter['country'];
    }

    let location = null;
    if ('location' in queryParams.filter) {
      location = queryParams.filter.location;
      delete queryParams.filter['location'];
    }

    let conditions = {};
    const fixedQueries = [];
    let filterQueries = [];

    if (
      !queryParams.filter.includeDeleted ||
      queryParams.filter.includeDeleted === false
    ) {
      conditions = {
        $and: [
          { deleted: false },
          { status: { $ne: UserStatus.ARCHIVED } },
          { type: RoleTypes.EMPLOYEE },
        ],
      };
    }

    if (
      role ||
      country ||
      location ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      if (role) {
        conditions['$and'].push({ 'role.key': role });
      }

      if (country) {
        conditions['$and'].push({ country: country });
      }

      if (location) {
        conditions['$and'].push({
          'location._id': new Types.ObjectId(location),
        });
      }

      if (queryParams.filter && Object.keys(queryParams.filter).length > 0) {
        filterQueries = Object.keys(queryParams.filter).map((key) => {
          return {
            [key]: {
              $regex: new RegExp(
                '.*' + escapeRegExp(queryParams.filter[key]) + '.*',
                'i',
              ),
            },
          };
        });
      }
    }

    if (filterQueries.length || fixedQueries.length) {
      conditions['$or'] = [...filterQueries, ...fixedQueries];
    }

    const query = this.userModel.aggregate();

    query
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'locations',
        localField: 'location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true });

    query
      .unwind({ path: '$role', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'roles',
        localField: 'role',
        foreignField: '_id',
        as: 'role',
      })
      .unwind({ path: '$role', preserveNullAndEmptyArrays: true });

    if (conditions) {
      query.match(conditions);
    }

    query.append({
      $project: {
        id: '$_id',
        role: '$role._id',
        roleName: '$role.name',
        employeeCategory: '$employeeInfo.category',
        status: '$status',
        deleted: '$deleted',
        type: '$type',
        locationName: {
          $ifNull: ['$location.business.name', '']
        },
        locationId: '$location._id',
        locationCode: '$location.code',
        name: '$name',
        position: {
          $ifNull: ['$employeeInfo.position', '']
        },
        code: '$code',
        country: '$country',
        createdAt: '$createdAt',
        createdByName: '$createdBy.name',
        createdByCode: '$createdBy._id',
        updatedAt: '$updatedAt',
        updatedByName: '$updatedBy.name',
        updatedByCode: '$updatedBy._id',
      },
    });

    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.skip(skipCriteria).limit(limitCriteria).sort(sortCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  public async setRandomPasswordByUserId(id: string): Promise<string> {
    const user: User = await this.userModel
      .findOne({ _id: new Types.ObjectId(id) })
      .exec();

    if (!user) {
      throw new NotFoundException(`User (id: ${id}) not found.`);
    }

    const password: string = generateRandomPassword(8);

    const userToUpdate: User = {
      ...user['_doc'],
      password: password,
    };

    await this.userModel.findOneAndUpdate(
      { _id: new Types.ObjectId(id) },
      userToUpdate,
      { new: true },
    );

    return password;
  }

  public async updateAllUsers(
    payload: BulkUpdateUserDto,
    conditions: any = {},
  ): Promise<any> {
    return await this.userModel.updateMany(conditions, payload);
  }

  public async updateUser(
    id: string,
    userDto: UpdateUserDto,
    requestUser: Partial<User>,
  ): Promise<User> {
    const userFound: User = await this.userModel
      .findOne({ _id: new Types.ObjectId(id) })
      .exec();

    if (!userFound) {
      throw new NotFoundException(`User (id: ${id}) not found.`);
    }

    if (userDto.role) {
      const newUserRole = await this.roleModel.findOne({ role: userDto.role });
      userFound.permissions = newUserRole.permissions;
    }

    const user: UpdateUserDto = {
      ...userFound['_doc'],
      ...userDto,
      updatedBy: requestUser.id,
    };

    let userLocationAttributes = {
      country: null,
      authorizedIpAddresses: [],
    };

    if (user.location) {
      const location: Location = await this.locationModel.findById(
        user.location,
      );
      userLocationAttributes = {
        country: location.business?.address?.country,
        authorizedIpAddresses: [location.ipAddress],
      };
    }

    if (
      user.employeeInfo &&
      user.employeeInfo.payRate &&
      user.employeeInfo.payFrequency
    ) {
      user.employeeInfo.hourlyRate = this.calculateHourlyRate(
        user.employeeInfo,
      );
    }

    //TODO: Replace with line below once we implement permissions edition in frontend
    const filterQuery: FilterQuery<Role> = !user.role
      ? { key: RoleCategories.SELLER }
      : { _id: user.role };
    const role: Role = await this.roleModel.findOne(filterQuery);

    if (!user.role || !user.permissions) {
      user.role = role.id;
      user.type = role.type;
      user.permissions = role.permissions;

      if (role.type === RoleTypes.EMPLOYEE) {
        user.employeeInfo.category = role.key;
      }
    }

    const cleanUser = {
      ...this.sanitizeProfileAttributes(user),
      updatedBy: requestUser.id,
      company: requestUser.company,
      ...userLocationAttributes,
    };

    return await this.userModel.findOneAndUpdate(
      { _id: new Types.ObjectId(userFound._id) },
      cleanUser,
      { new: true },
    );
  }

  public async updateUserPreferences(
    authUser: Partial<User>,
    updateUserPreferencesDto: UpdateUserPreferencesDto,
  ): Promise<User> {
    const userFound: User = await this.userModel
      .findOne({ _id: authUser.id })
      .exec();

    if (!userFound) {
      throw new NotFoundException(`User (id: ${authUser.id}) not found.`);
    }

    const user = {
      ...updateUserPreferencesDto,
      updatedBy: authUser.id,
    };

    return await this.userModel.findOneAndUpdate({ _id: userFound._id }, user, {
      new: true,
    });
  }

  public async updateUserProfile(
    authUser: Partial<User>,
    updateProfileDto: UpdateProfileDto,
  ): Promise<User> {
    const userFound: User = await this.userModel
      .findOne({ _id: authUser.id })
      .exec();

    if (!userFound) {
      throw new NotFoundException(`User (id: ${authUser.id}) not found.`);
    }

    const user = {
      ...updateProfileDto,
    };

    const userProfile: UpdateProfileDto = {
      ...this.sanitizeProfileAttributes(user),
    };

    return await this.userModel.findOneAndUpdate(
      { _id: userFound._id },
      { ...userProfile, updatedBy: authUser.id },
      { new: true },
    );
  }

  public async updateUserPassword(
    authUser: Partial<User>,
    updatePasswordDto: UpdatePasswordDto,
  ) {
    const userFound: User = await this.userModel
      .findOne({ _id: authUser.id })
      .exec();

    if (!userFound) {
      throw new NotFoundException(`User (id: ${authUser.id}) not found.`);
    }

    const matchesPassword = await compare(
      updatePasswordDto.oldPassword,
      userFound.password,
    );

    if (!matchesPassword) {
      throw new UnauthorizedException(`Password didn't matched`);
    }

    return await this.userModel.findOneAndUpdate(
      { _id: userFound._id },
      { password: updatePasswordDto.newPassword },
      { new: true },
    );
  }

  async uploadProfilePhoto(file: Express.Multer.File, authUser: Partial<User>) {
    const extension: string = this.getFileExtension(file);
    const profilePhoto: string = this.generateProfilePhotoPath(
      authUser.id.toString(),
      extension,
    );

    await this.fileService.uploadFileDriver(file.buffer, profilePhoto);

    return await this.userModel.findOneAndUpdate(
      { _id: authUser.id },
      { profilePhoto: profilePhoto },
      { new: true },
    );
  }

  async getProfilePhoto(authUser: Partial<User>) {
    const user: User = await this.userModel
      .findOne({ _id: authUser.id })
      .exec();

    if (!user) {
      throw new NotFoundException(`User (id: ${authUser.id}) not found.`);
    }

    if (!user.profilePhoto) {
      return await this.fileService.getFile(
        S3_PROFILE_PHOTO_DEFAULT_PATH
      );
    }

    return await this.fileService.getFile(user.profilePhoto);
  }

  async deleteProfilePhoto(authUser: Partial<User>) {
    const user: User = await this.userModel
      .findOne({ _id: authUser.id })
      .exec();

    if (user.profilePhoto?.length) {
      await this.fileService.deleteFileDriver(user.profilePhoto);
    }

    return await this.userModel.findOneAndUpdate(
      { _id: authUser.id },
      { profilePhoto: null },
      { new: true },
    );
  }

  private getFileExtension(file: Express.Multer.File): string {
    return file.originalname.slice(file.originalname.lastIndexOf('.') + 1);
  }

  private generateProfilePhotoPath(userId: string, extension): string {
    return `${S3_USERS_FOLDER}/${userId}/${S3_PROFILE_PHOTO_FOLDER}/${nanoid(
      10,
    )}.${extension}`;
  }

  /* @Delete(':file')
  async deleteFile(@Body() deleteFileDto: DeleteFileDto) {
    return this.fileService.deleteFile(deleteFileDto.filename);
  }
  */

  // #endregion Public Methods (15)

  // #region Private Methods (3)

  private calculateHourlyRate(employeeInfo: EmployeeInfo) {
    if (employeeInfo && employeeInfo.payRate && employeeInfo.payFrequency) {
      switch (employeeInfo?.payFrequency) {
        case 'WEEKLY':
          return employeeInfo.payRate / 40;

        case 'BI-WEEKLY':
          return employeeInfo.payRate / 80;

        case 'TWICE-MONTH':
          return employeeInfo.payRate / 160;

        case 'MONTHLY':
          return employeeInfo.payRate / 160;

        case 'YEARLY':
          return employeeInfo.payRate / 2080;

        case 'HOURLY':
          return employeeInfo.payRate;
      }
    }
    return 0;
  }

  private setEmployeeInfo(user: Partial<User>, category: string) {
    if (user.type === RoleTypes.EMPLOYEE) {
      if (!user.employeeInfo) {
        user.employeeInfo = new EmployeeInfo();
      } else {
        if (user.employeeInfo.payRate && user.employeeInfo.payFrequency) {
          user.employeeInfo.hourlyRate = this.calculateHourlyRate(
            user.employeeInfo,
          );
        }
      }
      user.employeeInfo.category = category;
    }
  }

  private async setRoleAndRelatedAttributes(
    user: Partial<User>,
    requestUser: Partial<User>,
    company: Partial<Company>,
  ) {
    const userRole: Partial<Role> = user.role;
    let newUserRole: Partial<Role>;
    if (userRole) {
      newUserRole = userRole._id;
    }

    if (!newUserRole) {
      newUserRole = company.settings.defaults.role;

      if (!newUserRole) {
        throw new NotFoundException('Role not found');
      }
    }

    const filterQuery: FilterQuery<Role> = { _id: newUserRole };
    const role: Role = await this.roleModel.findOne(filterQuery);
    if (!role) {
      throw new NotFoundException('Role not found.');
    }

    user.role = role.id;
    user.type = role.type;

    this.setEmployeeInfo(user, role.key);

    user.permissions = role.permissions;
  }

  // #endregion Private Methods (3)
}
