
import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  Scope
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp, toLower, trim } from 'lodash';
import { Model, Types } from 'mongoose';
import { SearchResult } from 'src/commons/interfaces/search-result.interface';
import { executeAggregatorWithPagination } from 'src/commons/queries/mongo/helpers/query';
import { Customer } from 'src/database/entities/customer.schema';
import { CreateCustomerDto } from 'src/dto/customer/create-customer.dto';
import { UpdateCustomerDto } from 'src/dto/customer/update-customer.dto';
import { AuthenticatedRequest } from 'src/modules/auth/interface/authenticated-request.interface';
import { buildSearchConditions } from '../sale/queries/mongo/helpers/search.conditions';
import { buildSearchAggregator } from './queries/mongo/aggregators/search.aggregator';

@Injectable({ scope: Scope.REQUEST })
export class CustomerService {
  constructor(
    @InjectModel(Customer.name) private customerModel: Model<Customer>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) { }

  /**
   * @returns Promise
   */
  async findAll(): Promise<any> {
    return await this.customerModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async findByCode(code: string): Promise<Customer> {
    const customer: Customer = await this.customerModel
      .findOne({ code: code })
      .exec();

    if (!customer) {
      throw new NotFoundException(`Customer (code: ${code}) not found.`);
    }

    return customer;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  async findById(id: string): Promise<Customer> {
    const customer: Customer = await this.customerModel
      .findOne({ _id: id })
      .exec();

    if (!customer) {
      throw new NotFoundException(`Customer (id: ${id}) was not found`);
    }

    return customer;
  }

  /**
   * @param  {CreateCustomerDto} customerDto
   * @returns Promise
   */
  async save(customerDto: CreateCustomerDto): Promise<Customer> {
    const customer: CreateCustomerDto = {
      ...customerDto,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    };

    this.sanitizeProfileAttributes(customer);

    return await this.customerModel.create(customer);
  }

  /**
   * @param  {string} code
   * @param  {UpdateCustomerDto} data
   * @returns Promise
   */
  async update(
    code: string,
    customerDto: UpdateCustomerDto,
  ): Promise<Customer> {
    const customerFound: Customer = await this.customerModel
      .findOne({ code: code })
      .exec();

    if (!customerFound) {
      throw new NotFoundException(`Customer (code: ${code}) not found.`);
    }

    const customer: UpdateCustomerDto = {
      ...customerFound['_doc'],
      ...customerDto,
      updatedBy: this.req.user.id,
    };

    this.sanitizeProfileAttributes(customer);

    return await this.customerModel.findOneAndUpdate(
      { _id: new Types.ObjectId(customerFound._id) },
      customer,
      { new: true },
    );
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async deleteByCode(code: string): Promise<Customer> {
    const customer: Customer = await this.customerModel
      .findOne({ code: code })
      .exec();

    if (!customer) {
      throw new NotFoundException(`Customer (code: ${code}) not found.`);
    }

    return await this.customerModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */

  async deleteAll(): Promise<any> {
    return await this.customerModel.deleteMany({}).exec();
  }

  /**
   * @returns Promise
   */

  async batchDelete(codes: string[]): Promise<any> {
    return await this.customerModel.deleteMany({ code: { $in: codes } }).exec();
  }

  async search(queryParams?: SearchQueryParams): Promise<SearchResult> {

    const conditions = buildSearchConditions(queryParams)

    const aggregator = buildSearchAggregator(conditions);
    const result = await executeAggregatorWithPagination(aggregator, queryParams, this.customerModel);
    return result;
  }

  async getCatalog(filterCriteria: any): Promise<any> {
    const query: any = this.customerModel.aggregate();

    if (filterCriteria) {
      query.match({
        $and: [
          { 'company': new Types.ObjectId(filterCriteria.company) },
          { 'deleted': false }
        ]
      });
    }

    query.append({
      $project: {
        id: '$_id',
        company: '$company',
        deleted: '$deleted',
        code: '$code',
        name: '$name',
        type: '$type',
        usDOT: '$business.usDOT',
        state: '$business.address.state'
      }
    });
    query.sort({ name: 1 });

    return await query.exec();

  }

  private sanitizeProfileAttributes = (customer: UpdateCustomerDto) => {
    if (customer.contact) {
      customer.contact.firstName = trim(
        customer.contact ? customer.contact.firstName || '' : '',
      );
      customer.contact.lastName = trim(
        customer.contact ? customer.contact.lastName || '' : '',
      );

      customer.contact.mobilePhone = (
        customer.contact.mobilePhone || ''
      ).replace(/\D/g, '');
      customer.contact.phone = (customer.contact.phone || '').replace(
        /\D/g,
        '',
      );
      customer.contact.email = trim(
        toLower(trim(customer.contact.email || '')),
      );
    }

    if (customer.type === 'BUSINESS') {
      customer.business.name = trim(
        customer.business ? customer.business.name || '' : '',
      );
      customer.business.email = trim(toLower(customer.business.email || ''));
      customer.business.primaryPhone = (
        customer.business.primaryPhone || ''
      ).replace(/\D/g, '');

      customer.name = trim(customer.business.name || '');
      customer.email = customer.business.email || '';
      customer.phone = (customer.business.primaryPhone || '').replace(
        /\D/g,
        '',
      );
    } else {
      customer.name = `${customer.contact.firstName || ''}  ${customer.contact.lastName || ''
        }`;
      customer.email = customer.contact.email || '';
      customer.phone = trim(
        customer.contact.phone
          ? customer.contact.phone
          : customer.contact.mobilePhone || '',
      );
    }
    return customer;
  };

  async batchCreate(customers: UpdateCustomerDto[]) {
    const user = this.req.user.id;
    const company = this.req.user.company;

    customers = customers.map((customer: UpdateCustomerDto) => {
      customer = this.sanitizeProfileAttributes(customer);
      if (customer.code) {
        customer = {
          ...customer,
          updatedBy: customer.updatedBy ? customer.updatedBy : user,
        };
      } else {
        customer = {
          ...customer,
          createdBy: customer.createdBy ? customer.createdBy : user,
          company: company,
        };
      }
      return customer;
    });

    const bulkPayload = customers.map((customer: Partial<Customer>) => ({
      updateOne: {
        filter: { code: customer.code },
        update: { $set: customer },
        upsert: true,
      },
    }));

    this.customerModel
      .bulkWrite(bulkPayload)
      .then((bulkWriteOpResult) => {
        console.log('BULK update OK');
        //console.log(JSON.stringify(bulkWriteOpResult, null, 2));
        return bulkWriteOpResult;
      })
      .catch((err) => {
        console.log('BULK update error');
        //console.log(JSON.stringify(err, null, 2));
        throw new BadRequestException(
          'Cannot upsert customers. ' + JSON.stringify(err, null, 2),
        );
      });
  }
}
