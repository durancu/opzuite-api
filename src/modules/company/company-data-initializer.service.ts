import {
    Injectable,
    OnModuleInit
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Company } from '../../database/entities/company.schema';
import { CreateCompanyDto } from '../../dto/company/create-company.dto';

@Injectable()
export class CompanyDataInitializerService
  implements OnModuleInit {

  private companyData: CreateCompanyDto[] = [
  ];

  constructor(
    @InjectModel(Company.name) private companyModel: Model<Company>,
  ) { }

  async onModuleInit(): Promise<void> {
    console.log('(CompanyModule) is initialized...');
  }
}
