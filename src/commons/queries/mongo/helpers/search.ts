
export function calculateSortCriteria(queryParams: SearchQueryParams): Record<string, any> {

  let sortCriteria: any = { createdAt: -1 };

  if (queryParams) {
    const { sortField, sortOrder } = queryParams;
    if (sortField) {
      sortCriteria = {};
      sortCriteria[sortField] =
        sortOrder === 'desc' ? -1 : 1;
    }
  }
  return sortCriteria;
}

export function calculateSkipCriteria(queryParams?: SearchQueryParams): number {
  if (queryParams && queryParams.pageNumber && queryParams.pageSize) {
    return (queryParams.pageNumber - 1) * queryParams.pageSize;
  }
  return 0;
}

export function calculateLimitCriteria(queryParams: SearchQueryParams): number {
  if (queryParams && queryParams.pageSize) {
    return queryParams.pageSize;
  }
  return 0;
}

