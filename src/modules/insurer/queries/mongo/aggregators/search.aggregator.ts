
export const buildSearchAggregator = (conditions: any) => {
    const aggregator = [];

    aggregator.push(
        ...buildSearchUnwindReferenceFieldsAggregator()
    );

    if (conditions) {
        aggregator.push({ $match: conditions });
    }
    aggregator.push(
        {
            $project: {
                id: '$_id',
                type: '$type',
                usDOT: '$business.usDOT',
                name: '$name',
                createdAt: '$createdAt',
                createdByName: '$createdBy.name',
                createdByCode: '$createdBy._id',
                updatedAt: '$updatedAt',
                updatedByName: '$updatedBy.name',
                updatedByCode: '$updatedBy._id',
                locationName: { $arrayElemAt: ['$location.business.name', 0] },
                locationCode: { $arrayElemAt: ['$location.code', 0] },
                state: {
                    $function: {
                        body: function (business: any, contact: any, type: any) {
                            return type === 'BUSINESS'
                                ? business.address.state
                                : contact.address.state;
                        },
                        args: ['$business', '$contact', '$type'],
                        lang: 'js',
                    },
                },
                code: '$code',
                deleted: '$deleted',
            }
        }
    );

    return aggregator;

}

export const buildSearchUnwindReferenceFieldsAggregator = () => {
    return [
        {
            $unwind: {
                path: '$createdBy',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'createdBy',
                foreignField: '_id',
                as: 'createdBy'
            }
        },
        {
            $unwind: {
                path: '$createdBy',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$createdBy.location',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'locations',
                localField: 'createdBy.location',
                foreignField: '_id',
                as: 'location'
            }
        },
        {
            $unwind: {
                path: '$createdBy.location',
                preserveNullAndEmptyArrays: true
            }
        }
    ]
};