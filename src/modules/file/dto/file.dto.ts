import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class BasicFileDto {
  @ApiProperty({ example: 'test.pdf', required: true })
  @IsString()
  @IsNotEmpty()
  filename: string;
}

export class GetFileDto extends BasicFileDto {}

export class DeleteFileDto extends BasicFileDto {}
export class MoveFileDto extends BasicFileDto {}
export class RenameFileDto {
  @ApiProperty({ example: 'old.pdf', required: true })
  @IsString()
  @IsNotEmpty()
  oldFileName: string;

  @ApiProperty({ example: 'new.pdf', required: true })
  @IsString()
  @IsNotEmpty()
  newFileName: string;
}

export class CopyFileDto extends RenameFileDto {}
export class FileStorageDto {
  @ApiProperty({ type: 'string', format: 'binary', required: true })
  file: Express.Multer.File;
}
