export enum PdfAnswer {
    YES = 'Y',
    NO = 'N',
    EMPTY = ''
}