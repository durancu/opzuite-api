import { IsAlphanumeric, IsEmail, IsNotEmpty, IsObject, IsOptional } from 'class-validator';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { Company } from 'src/database/entities/company.schema';
import { Location } from 'src/database/entities/location.schema';
import { Address } from 'src/database/entities/nested/address.schema';
import { Communication } from 'src/database/entities/nested/communication.schema';
import { EmailSettings } from 'src/database/entities/nested/email-settings.schema';
import { EmployeeInfo } from 'src/database/entities/nested/employee-info.schema';
import { UserPreferences } from 'src/database/entities/nested/user-preferences.schema';
import { Role } from 'src/database/entities/role.schema';
import { User } from 'src/database/entities/user.schema';

export class CreateUserDto {
  // #region Properties (26)

  @IsOptional()
  public address?: Address;
  @IsOptional()
  public authorizedIpAddresses?: string[];
  @IsOptional()
  @IsObject()
  public communication?: Communication;
  //EMPLOYEE DATA (DEPENDS ON BUSINESS MODEL)
  @IsOptional()
  @IsNotEmpty()
  public company?: Partial<Company>;
  @IsOptional()
  public country?: string;
  @IsOptional()
  public dob?: string;
  @IsNotEmpty()
  @IsEmail()
  public email?: string;
  @IsOptional()
  @IsObject()
  public emailSettings?: EmailSettings;
  @IsOptional()
  @IsObject()
  public employeeInfo?: EmployeeInfo;
  @IsNotEmpty()
  public firstName?: string;
  @IsOptional()
  public gender?: string;
  @IsOptional()
  public language?: string;
  @IsOptional()
  public lastName?: string;
  @IsOptional()
  public name?: string;
  @IsOptional()
  @IsNotEmpty()
  public location?: Partial<Location>;
  @IsOptional()
  public mobilePhone?: string;
  @IsOptional()
  permissions?: PermissionType[];
  /* @IsNotEmpty()
  @MinLength(8, { message?: ' The min length of password is 8 ' })
  @MaxLength(20, {
    message?: " The password can't accept more than 20 characters ",
  }) */
  /* @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,20}$/, {
    message:
      ' A password at least contains one numeric digit, one supercase char and one lowercase char',
  }) */
  /* password?: string; */
  @IsOptional()
  public phone?: string;
  @IsOptional()
  preferences?: Partial<UserPreferences>;
  @IsOptional()
  role?: Partial<Role>;
  @IsOptional()
  public startedAt?: Date;
  @IsOptional()
  public status?: string;
  @IsOptional()
  @IsNotEmpty()
  public supervisor?: Partial<User>;
  @IsOptional()
  public timezone?: string;
  @IsOptional()
  public type?: string;
  @IsOptional()
  @IsNotEmpty()
  @IsAlphanumeric()
  public username?: string;
  @IsOptional()
  public website?: string;

  // #endregion Properties (26)
}
