import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Company, CompanySchema } from 'src/database/entities/company.schema';
import { File, FileSchema } from 'src/database/entities/file.schema';
import { Location, LocationSchema } from 'src/database/entities/location.schema';
import { Role, RoleSchema } from 'src/database/entities/role.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { SendgridService } from 'src/modules/sendgrid/sendgrid.service';
import { UserRepository } from 'src/repositories/user.repository';
import { FileModule } from '../file/file.module';
import { FileService } from '../file/file.service';
import { LocationModule } from '../location/location.module';
import { SendgridModule } from '../sendgrid/sendgrid.module';
import { ProfileController } from './profile.controller';
import { RoleController } from './role.controller';
import { RoleService } from './role.service';
import { UserModuleInitializerService } from './user-module-initializer.service';
import { UserController } from './user.controller';
import { UserService } from './user.service';
@Module({
  imports: [
    FileModule,
    SendgridModule,
    LocationModule,
    MongooseModule.forFeature([
      { name: File.name, schema: FileSchema },
      { name: User.name, schema: UserSchema },
      { name: Role.name, schema: RoleSchema },
      { name: Location.name, schema: LocationSchema },
      { name: Company.name, schema: CompanySchema },
    ]),
  ],
  providers: [
    FileService,

    SendgridService,
    UserService,
    RoleService,
    UserRepository,
    UserModuleInitializerService,
  ],
  controllers: [
    ProfileController,
    UserController,
    RoleController,
  ],
  exports: [
    UserService,
    UserRepository,
    RoleService,
    SendgridModule,
    FileModule
  ],
})
export class UserModule { }
