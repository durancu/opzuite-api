import { BadRequestException, Body, Controller, Delete, Get, Headers, HttpCode, Param, Post, Put, Response, Scope, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { nanoid } from 'nanoid';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { Certificate } from 'src/database/entities/certificate.schema';
import { CreateCertificateDto } from 'src/dto/certificates/create-certificate.dto';
import { UpdateCertificateDto } from 'src/dto/certificates/update-certificate.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { CertificateService } from './certificate.service';


@ApiTags('Certificate')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'certificates', scope: Scope.REQUEST })
export class CertificateController {
  constructor(private certificateService: CertificateService) { }

  @Get()
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_READ)
  async getAllCertificates(): Promise<any> {
    const res = await this.certificateService.findAll();
    return {
      data: res,
    };
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_READ)
  async searchCertificates(@Body() query: any): Promise<any> {
    return await this.certificateService.search(query.queryParams);
  }

  @Get(':id')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_READ)
  async findCertificate(
    @Response() res,
    @Param('id') id: string,
    @Headers('id-field') idField: string,
  ): Promise<Certificate> {
    let certificate: Certificate;
    if (idField && idField === 'id') {
      certificate = await this.certificateService.findById(id);
    } else {
      certificate = await this.certificateService.findByCode(id);
    }

    return res.json(certificate);
  }

  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_CREATE)
  async createCertificate(
    @Body() certificate: CreateCertificateDto,
  ): Promise<Certificate> {
    return this.certificateService.create(certificate);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_UPDATE)
  async updateCertificate(
    @Param('code') code: string,
    @Body() certificate: UpdateCertificateDto,
  ): Promise<Certificate> {
    return await this.certificateService.update(code, certificate);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_DELETE)
  async deleteCertificateById(@Param('code') code: string): Promise<Certificate> {
    return await this.certificateService.deleteByCode(code);
  }

  @Post('init')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_CREATE)
  async initCertificate(
    @Response() res,
    @Body() payload: any,
  ): Promise<Certificate> {

    const { customer } = payload;

    if (!customer) {
      throw new BadRequestException('Missing required request attribute `customer`.');
    }

    const certificateLayout = await this.certificateService.initCertificate(customer);

    return res.json(certificateLayout);
  }

  @Post('generate')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_CREATE)
  async generate(
    @Response() res,
    @Body() certificate: Partial<Certificate>,
  ): Promise<any> {

    //try {
    const previewFile = await this.certificateService.generateCertificate(certificate);

    res.set('Content-Type', 'application/pdf');
    res.set('Content-Disposition', `inline; filename=${nanoid(6)}.pdf`);
    res.status(200).end(previewFile);
  }

  @Get(':code/preview')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CERTIFICATES_READ)
  async preview(
    @Response() res,
    @Param('code') code: string,
  ): Promise<any> {

    const certificate: Partial<Certificate> = await this.certificateService.findByCode(code);

    //try {
    const previewFile = await this.certificateService.previewCertificate(certificate);

    res.set('Content-Type', 'application/pdf');
    res.set('Content-Disposition', `inline; filename=${nanoid(6)}.pdf`);
    res.status(200).end(previewFile);
  }

  /* catch (error) {

    console.error(`Error downloading file: ${error.message}`);
    res.status(500).send('Error downloading file');
  } 

}*/


}
