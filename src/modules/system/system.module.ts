import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SystemSetting, SystemSettingSchema } from 'src/database/entities/system-setting.schema';
import { SettingService } from './settings.service';
import { SystemModuleInitializerService } from './system-module-initializer.service';
import { SystemController } from './system.controller';
import { SystemService } from './system.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: SystemSetting.name, schema: SystemSettingSchema },
    ]),
  ],
  controllers: [
    SystemController
  ],
  providers: [
    SystemModuleInitializerService,
    SystemService,
    SettingService
  ],
  exports: [
    SystemService,
    SettingService
  ]
})
export class SystemModule { }
