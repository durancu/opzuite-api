import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { EndorsementItem, EndorsementItemSchema } from './nested/endorsement-item.schema';
import { Sale } from './sale.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Endorsement extends Document {
    // #region Properties (30)

    @Prop()
    public accountingClass: string;
    @Prop({ default: 0, required: false })
    public amount: number;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company', required: true })
    public company: Company;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
    public createdBy: User;
    @Prop()
    public description: string;
    @Prop()
    public endorsedAt: Date;
    @Prop()
    public followUpDate: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public followUpPerson: User;
    @Prop({ type: [EndorsementItemSchema], required: false })
    public items: EndorsementItem[];
    @Prop({ required: false })
    public name: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Sale', required: true })
    public sale: Sale;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public seller: User;
    @Prop()
    public status: string;
    @Prop({ default: 0, required: false })
    public totalAgencyCommission: number;
    @Prop({ default: 0, required: false })
    public totalAgentCommission: number;
    @Prop({ default: 0, required: false })
    public totalFinanced: number;
    @Prop({ default: 0, required: false })
    public totalFinancedPaid: number;
    @Prop({ default: 0, required: false })
    public totalNonCommissionablePremium: number;
    @Prop({ default: 0, required: false })
    public totalNonPremium: number;
    @Prop({ default: 0, required: false })
    public totalPaid: number;
    @Prop({ default: 0, required: false })
    public totalPayables: number;
    @Prop({ default: 0, required: false })
    public totalPremium: number;
    @Prop({ default: 0, required: false })
    public totalReceivables: number;
    @Prop({ default: 0, required: false })
    public totalReceived: number;
    @Prop({ default: 0, required: false })
    public totalTaxesAndFees: number;
    @Prop()
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;

    // #endregion Properties (30)
}

export const EndorsementSchema = SchemaFactory.createForClass(Endorsement);

EndorsementSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
