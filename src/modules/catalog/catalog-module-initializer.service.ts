import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class CatalogModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Catalog Module) is initialized...');
  }
}
