import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { DashboardLayout, DashboardLayoutSchema } from './dashboard-layout.schema';

@Schema()
export class DashboardPreferences extends Document {
  // #region Properties (1)

  @Prop({ type: DashboardLayoutSchema })
  public layout: DashboardLayout;

  // #endregion Properties (1)
}

export const DashboardPreferencesSchema = SchemaFactory.createForClass(DashboardPreferences);