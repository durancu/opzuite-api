export enum GroupingCriteria {
  FULL = 'full',
  SELLER = 'seller',
  CUSTOMER = 'customer',
  CARRIER = 'carrier',
  BROKER = 'broker',
  INSURER = 'insurer',
  LOCATION = 'location',
  TYPE = 'type',
  COUNTRY = 'country',
  STATUS = 'status',
  YEAR = 'year',
  MONTH = 'month',
  DAY = 'day'
}