import {
  CallHandler,
  ExecutionContext,
  ForbiddenException, Injectable,
  NestInterceptor
} from '@nestjs/common';
import { Observable, throwError } from 'rxjs';
import { UserType } from 'src/commons/enum/user-type.enum';
import { User } from 'src/database/entities/user.schema';

@Injectable()
export class IpControlInterceptor implements NestInterceptor {
  // #region Public Methods (1)

  public async intercept(
    context: ExecutionContext,
    next: CallHandler,
  ): Promise<Observable<any>> {
    const request = context.switchToHttp().getRequest();

    if (!request.user) {
      return next.handle();
    }

    const user: Partial<User> = request.user;

    const userIPAddress: string =
      request.header('x-forwarded-for') || request.connection.remoteAddress;

    if (userIPAddress !== '::1' &&
      user.type === UserType.EMPLOYEE &&
      user.authorizedIpAddresses.length > 0 &&
      !user.authorizedIpAddresses.includes(userIPAddress)
    ) {
      return throwError(
        new ForbiddenException('UNAUTHORIZED_IP_ADDRESS', `${userIPAddress}`),
      );
    }
    return next.handle();
  }

  // #endregion Public Methods (1)
}
