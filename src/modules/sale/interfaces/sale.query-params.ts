interface SaleQueryParams {
    filter?: SaleQueryParamFilter
}

interface SaleQueryParamFilter {
    saleDateFrom?: string;
    saleDateTo?: string;
    effectiveDateFrom?: string;
    effectiveDateTo?: string;
    expirationDateFrom?: string;
    expirationDateTo?: string;
    lineOfBusiness?: string;
    type?: string;
    customer?: string;
    seller?: string;
    carrier?: string;
    broker?: string;
    status?: string;
    location?: string;
    country?: string;
    usDOT?: string;
    dot?: string;
}
