import { Injectable, OnModuleInit } from '@nestjs/common';

@Injectable()
export class SendgridModuleInitializerService implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Sendgrid Module) is initialized...');
  }
}
