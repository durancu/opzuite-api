import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';
@Injectable()
export class DashboardModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Dashboard Module) is initialized...');
  }
}
