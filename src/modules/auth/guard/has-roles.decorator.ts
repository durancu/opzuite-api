import { SetMetadata } from '@nestjs/common';
import { Role } from 'src/database/entities/role.schema';
import { HAS_ROLES_KEY } from '../../../commons/const/auth.constants';

export const HasRoles = (...args: Role[]) => SetMetadata(HAS_ROLES_KEY, args);
