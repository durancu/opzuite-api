import { CertificatePdfTemplate } from "src/commons/certificates/certificate-pdf-template";

export interface PdfInput {
    inputPdfBuffer: Buffer;
    template: CertificatePdfTemplate;
}