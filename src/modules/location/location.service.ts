import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp } from 'lodash';
import { Model, Types } from 'mongoose';
import { RoleHierarchyEdges } from 'src/commons/enum/role-categories.enum';
import { Location } from 'src/database/entities/location.schema';
import { Role } from 'src/database/entities/role.schema';
import { User } from 'src/database/entities/user.schema';
import { AuthenticatedRequest } from 'src/modules/auth/interface/authenticated-request.interface';
import { CreateLocationDto } from '../../dto/location/create-location.dto';
import { UpdateLocationDto } from '../../dto/location/update-location.dto';

@Injectable({ scope: Scope.REQUEST })
export class LocationService {
  // #region Constructors (1)

  constructor(
    @InjectModel(Location.name) private locationModel: Model<Location>,
    @InjectModel(Role.name) private roleModel: Model<Role>,
    @InjectModel(User.name) private userModel: Model<User>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (11)

  /**
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.locationModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @returns Promise
   */
  public async deleteAll(): Promise<any> {
    return await this.locationModel.deleteMany({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async deleteByCode(code: string): Promise<Location> {
    const location: Location = await this.locationModel
      .findOne({ code: code })
      .exec();

    if (!location) {
      throw new NotFoundException(`Location (code: ${code}) not found.`);
    }

    return await this.locationModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.locationModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<Location> {
    const location: Location = await this.locationModel
      .findOne({ code: code })
      .exec();

    if (!location) {
      throw new NotFoundException(`Location (code: ${code}) not found.`);
    }

    return location;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<Location> {
    const location: Location = await this.locationModel
      .findOne({ _id: id })
      .exec();

    if (!location) {
      throw new NotFoundException(`Location (id: ${id}) was not found`);
    }

    return location;
  }

  public async getCatalog(filterCriteria: any): Promise<any> {
    return await this.locationModel
      .find(filterCriteria)
      .select('business.name _id name country')
      .sort({ name: 1 })
      .exec();
  }

  /**
   * @param  {CreateLocationDto} locationDto
   * @returns Promise
   */
  public async save(locationDto: CreateLocationDto): Promise<Location> {
    return await this.locationModel.create({
      ...locationDto,
      country: locationDto.business?.address?.country,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    });
  }

  public async search(searchQueryParams?: any): Promise<any> {
    const sortCriteria = {};
    const queryParams: any = { ...searchQueryParams };
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    const user: Partial<User> = this.req.user;
    const { hierarchy }: Role = await this.roleModel.findById(
      user.role.toString(),
    );

    /* if (hierarchy >= RoleHierarchyEdges.INTERNATIONAL && hierarchy < RoleHierarchyEdges.COUNTRY) {
      queryParams['country']= user.employeeInfo.allowedCountries?.length ? user.employeeInfo.allowedCountries : user.country;
    } */

    if (hierarchy === RoleHierarchyEdges.COUNTRY) {
      queryParams.filter['country'] = user.country;
    }

    if (hierarchy > RoleHierarchyEdges.COUNTRY) {
      queryParams.filter['location'] = user.location;
    }

    let country = null;
    if ('country' in queryParams.filter) {
      country = queryParams.filter.country;
      delete queryParams.filter['country'];
    }

    let location = null;
    if ('location' in queryParams.filter) {
      location = queryParams.filter.location;
      delete queryParams.filter['location'];
    }

    let conditions = {};
    const fixedQueries = [];
    let filterQueries = [];

    conditions = {
      $and: [{ deleted: false }],
    };
    if (
      country ||
      location ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      if (location) {
        conditions['$and'].push({ _id: new Types.ObjectId(location) });
      } else {
        if (country) {
          conditions['$and'].push({ 'business.address.country': country });
        }
      }

      if (queryParams.filter && Object.keys(queryParams.filter).length > 0) {
        filterQueries = Object.keys(queryParams.filter).map((key) => {
          return {
            [key]: {
              $regex: new RegExp(
                '.*' + escapeRegExp(queryParams.filter[key]) + '.*',
                'i',
              ),
            },
          };
        });
      }
    }

    if (filterQueries.length || fixedQueries.length) {
      conditions['$or'] = [...filterQueries, ...fixedQueries];
    }

    const query = this.locationModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query.append({
      $project: {
        id: '$_id',
        name: '$business.name',
        code: '$code',
        alias: '$alias',
        email: '$business.email',
        city: '$business.address.city',
        country: '$business.address.country',
        phone: '$business.primaryPhone',
        createdAt: '$createdAt',
        createdByName: '$createdBy.name',
        createdByCode: '$createdBy._id',
        updatedAt: '$updatedAt',
        updatedByName: '$updatedBy.name',
        updatedByCode: '$updatedBy._id',
        locationName: { $arrayElemAt: ['$location.business.name', 0] },
        locationCode: { $arrayElemAt: ['$location.code', 0] },
      }
    });


    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.skip(skipCriteria).limit(limitCriteria).sort(sortCriteria);
    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  /**
   * @param  {string} code
   * @param  {UpdateLocationDto} data
   * @returns Promise
   */
  public async update(code: string, location: UpdateLocationDto): Promise<Location> {
    const locationFound: Partial<Location> = await this.locationModel
      .findOne({ code: code })
      .exec();

    if (!locationFound) {
      throw new NotFoundException(`Location (code: ${code}) not found.`);
    }

    if (location.ipAddress) {
      await this.userModel.updateMany(
        { location: locationFound._id },
        { authorizedIpAddresses: [location.ipAddress] }
      );
    }

    const locationData: UpdateLocationDto = {
      ...locationFound['_doc'],
      ...location,
      country: location.business
        ? location.business?.address?.country
        : locationFound['_doc'].business?.address?.country || null,
      updatedBy: this.req.user.id,
    };

    return await this.locationModel.findOneAndUpdate(
      { _id: new Types.ObjectId(locationFound._id) },
      locationData,
      { new: true },
    );
  }

  // #endregion Public Methods (11)
}
