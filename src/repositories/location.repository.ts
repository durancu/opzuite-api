import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Location } from 'src/database/entities/location.schema';

export class LocationRepository {
  // #region Constructors (1)

  constructor(@InjectModel(Location.name) private readonly locationModel: Model<Location>) { }

  // #endregion Constructors (1)

  // #region Public Methods (6)
  public async findById(id: string, options?: any): Promise<Location> {

    const query = this.locationModel.findOne({ _id: id });

    if (options) {
      if ('populate' in options) {
        const { populate } = options;
        populate.map((field: any) => {
          query.populate(field, populate['field'])
        });
      }
    }

    return await query.exec();
  }

  public async findByCompany(company: string, options?: any): Promise<Location[]> {

    const query = this.locationModel.find({ company: company });

    if (options) {
      if ('populate' in options) {
        const { populate } = options;
        populate.map((field: any) => {
          query.populate(field, populate['field'])
        });
      }
    }

    return await query.exec();
  }

  

  // #endregion Public Methods (6)
}