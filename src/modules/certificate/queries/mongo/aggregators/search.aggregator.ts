
export const buildSearchAggregator = (conditions: any) => {
    const aggregator = [];

    aggregator.push(
        ...buildSearchUnwindReferenceFieldsAggregator()
    );

    if (conditions) {
        aggregator.push({ $match: conditions });
    }
    aggregator.push(
        {
            $project: {
                id: '$_id',
                customerName: '$customer.name',
                customerId: '$customer._id',
                customerCode: '$customer.code',
                usDOT: {
                    $cond: [
                        { $eq: ['$customer.type', 'BUSINESS'] },
                        '$customer.business.usDOT',
                        '',
                    ],
                },
                holderCode: '$holder.code',
                holderId: '$holder._id',
                holderName: '$holder.name',
                holderCity: '$holder.business.address.city',
                holderState: '$holder.business.address.state',
                name: '$name',
                createdAt: '$createdAt',
                issuedAt: '$issuedAt',
                createdByName: '$createdBy.name',
                createdBy: '$createdBy._id',
                code: '$code',
                deleted: '$deleted',
                masterTemplateName: '$masterTemplate.name',
                locationName: { $ifNull: ['$location.business.name', ''] },
                locationCode: { $ifNull: ['$location.code', ''] }
            }
        }
    );

    return aggregator;

}

export const buildSearchUnwindReferenceFieldsAggregator = () => {
    return [
        {
            $unwind: {
                path: '$createdBy',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'users',
                localField: 'createdBy',
                foreignField: '_id',
                as: 'createdBy'
            }
        },
        {
            $unwind: {
                path: '$createdBy',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$createdBy.location',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: 'locations',
                localField: 'createdBy.location',
                foreignField: '_id',
                as: 'location',
            },
        },
        {
            $unwind: {
                path: '$location',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $unwind: {
                path: '$holder',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'insurers',
                localField: 'holder',
                foreignField: '_id',
                as: 'holder'
            }
        },
        {
            $unwind: {
                path: '$holder',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$customer',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'customers',
                localField: 'customer',
                foreignField: '_id',
                as: 'customer'
            }
        },
        {
            $unwind: {
                path: '$customer',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $unwind: {
                path: '$masterTemplate',
                preserveNullAndEmptyArrays: true
            }
        },
        {
            $lookup: {
                from: 'certificatemastertemplates',
                localField: 'masterTemplate',
                foreignField: '_id',
                as: 'masterTemplate'
            }
        },
        {
            $unwind: {
                path: '$masterTemplate',
                preserveNullAndEmptyArrays: true
            }
        },
    ]
};