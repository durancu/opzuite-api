export enum LimitPer {
    LOCATION = 'LOCATION',
    POLICY = 'POLICY',
    PROJECT = 'PROJECT',
    OTHER = 'OTHER'
}