export const getPoliciesByProduct = (carriersWithCoverages: any[]) => {
    const policiesByProduct: any = {}
    carriersWithCoverages.forEach((carrierWithCoverages) => {
        carrierWithCoverages.coverages.forEach((coverage) => {
            const product = coverage.product;
            if (!policiesByProduct[product]) {
                policiesByProduct[product] = {
                    name: carrierWithCoverages.name,
                    letter: carrierWithCoverages.letter,
                    policyNumber: carrierWithCoverages.policyNumber,
                    effectiveAt: carrierWithCoverages.effectiveAt,
                    expiresAt: carrierWithCoverages.expiresAt,
                    coverage: coverage,
                };
            }
        });
    });
    return policiesByProduct;
}