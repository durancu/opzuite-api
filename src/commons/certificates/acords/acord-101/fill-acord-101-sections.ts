import { Certificate } from 'src/database/entities/certificate.schema';
import { Company } from 'src/database/entities/company.schema';
import { Sale } from 'src/database/entities/sale.schema';
import { fillRemarksByPolicyFields } from './helpers/fill-remarks-by-policy-fields';
import { fillRemarksHeaderFields } from './helpers/fill-remarks-header-fields';
import { Acord101PdfTemplateV2008 } from './models/acord-101-pdf-template-v-2018';

export const fillAcord101Sections = (certificate: Partial<Certificate>, policy: Sale, company: Company, pageNumber: number): Acord101PdfTemplateV2008 => {

    const template = new Acord101PdfTemplateV2008();

    fillRemarksHeaderFields(certificate, policy, company, template, pageNumber);
    fillRemarksByPolicyFields(certificate, policy, template);

    return template;
}



