import {
  Body,
  Controller, Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put, Req,
  Response,
  Scope, UseGuards, UseInterceptors
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { CreateFinancingDto } from 'src/dto/financing/create-financing.dto';
import { UpdateFinancingDto } from 'src/dto/financing/update-financing.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { Financing } from '../../database/entities/financing.schema';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { FinancingService } from './financing.service';

@ApiTags('Financing')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'financing', scope: Scope.REQUEST })
export class FinancingController {
  constructor(private financingService: FinancingService) { }

  @Get('')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_READ, PermissionType.MGAS_READ, PermissionType.FINANCERS_READ)
  //@UseFilters(MongoFilter)
  async findAll(@Req() req: Request, @Response() res): Promise<any> {
    return res.json(await this.financingService.findAll());
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_READ, PermissionType.MGAS_READ, PermissionType.FINANCERS_READ)
  //@UseFilters(MongoFilter)
  async getFinancingByCode(
    @Param('code') code: string,
  ): Promise<Financing> {
    return await this.financingService.findByCode(code);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_READ, PermissionType.MGAS_READ, PermissionType.FINANCERS_READ)
  async searchFinancing(@Body() query: any): Promise<any> {
    return await this.financingService.search(query.queryParams);
  }

  @Post('')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_CREATE, PermissionType.MGAS_CREATE, PermissionType.FINANCERS_CREATE)
  async createFinancing(
    @Req() req: Request,
    @Body() financing: any,
  ): Promise<CreateFinancingDto> {
    return await this.financingService.create(financing);
  }

  @Post('/batch-create')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.CARRIERS,
    PermissionType.MGAS,
  )
  async batchCreateFinancing(
    @Req() req: Request,
    @Body() financing: CreateFinancingDto[],
  ): Promise<any> {
    return await this.financingService.batchCreate(financing);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_UPDATE, PermissionType.MGAS_UPDATE, PermissionType.FINANCERS_UPDATE)
  //@UseFilters(MongoFilter)
  async updateFinancing(
    @Req() req: Request,
    @Param('code') code: string,
    @Body() financing: UpdateFinancingDto,
  ): Promise<Financing> {
    return await this.financingService.update(code, financing);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_DELETE, PermissionType.MGAS_DELETE, PermissionType.FINANCERS_DELETE)
  async deleteFinancing(@Param('code') code: string): Promise<Financing> {
    return await this.financingService.deleteByCode(code);
  }

  @Post('/delete')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_DELETE, PermissionType.MGAS_DELETE, PermissionType.FINANCERS_DELETE)
  //@UseFilters(MongoFilter)
  async deleteFinancings(@Req() req: Request, @Body() body: any): Promise<any> {
    const codes: string[] = body['codes'];
    return await this.financingService.batchDelete(codes);
  }
}
