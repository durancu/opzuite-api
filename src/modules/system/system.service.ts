import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { SystemSetting } from 'src/database/entities/system-setting.schema';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';

@Injectable({ scope: Scope.REQUEST })
export class SystemService {
  constructor(
    @InjectModel(SystemSetting.name) private settingModel: Model<SystemSetting>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) {}

  
}
