import { CoverageBasedForm } from "src/commons/enum/coverage-based-form";
import { PdfAnswer } from "src/commons/enum/pdf-answer.enum";
import { PdfCheckBox } from "src/commons/enum/pdf-checkbox.enum";
import { PolicyBasedForm } from "src/commons/enum/policy-based-form";
import { CoverageDetails } from "src/database/entities/nested/coverage-details.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillExcessUmbrellaLiabilityFields = (policiesByProduct: any, template: Acord25PdfTemplateV2014NowCerts) => {

    if (policiesByProduct['EXCESS_UMBRELLA_LIABILITY']) {

        const policy = policiesByProduct['EXCESS_UMBRELLA_LIABILITY'];
        template.setField('ExcessUmbrella_InsurerLetterCode', policy.letter);
        template.setField('Policy_ExcessLiability_PolicyNumberIdentifier', policy.policyNumber);
        template.setField('Policy_ExcessLiability_EffectiveDate', policy.effectiveAt?.toLocaleDateString());
        template.setField('Policy_ExcessLiability_ExpirationDate', policy.expiresAt?.toLocaleDateString());

        const coverageDetails: CoverageDetails = policy.coverage.details;
        if (coverageDetails) {
            template.setField('Policy_PolicyType_UmbrellaIndicator', coverageDetails.coverageFormUsed === CoverageBasedForm.UMBRELLA ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('Policy_PolicyType_ExcessIndicator', coverageDetails.coverageFormUsed === CoverageBasedForm.EXCESS ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('ExcessUmbrella_OccurrenceIndicator', coverageDetails.policyBasedForm === PolicyBasedForm.OCCUR ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('ExcessUmbrella_ClaimsMadeIndicator', coverageDetails.policyBasedForm === PolicyBasedForm.CLAIMS ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('ExcessUmbrella_DeductibleIndicator', coverageDetails.hasDeductible ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('ExcessUmbrella_RetentionIndicator', coverageDetails.hasRetention ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('ExcessUmbrella_Umbrella_DeductibleOrRetentionAmount', coverageDetails.hasDeductible || coverageDetails.hasRetention ? coverageDetails.deductibleOrRetentionAmount : '');
            template.setField('CertificateOfInsurance_ExcessLiability_AdditionalInsuredCode', coverageDetails.additionalInsured ? PdfAnswer.YES : PdfAnswer.EMPTY);
            template.setField('Policy_ExcessLiability_SubrogationWaivedCode', coverageDetails.waiverOfSubrogation ? PdfAnswer.YES : PdfAnswer.EMPTY);
            template.setField('ExcessUmbrella_Umbrella_EachOccurrenceAmount', coverageDetails.eachOccurrenceLimit);
            template.setField('ExcessUmbrella_Umbrella_AggregateAmount', coverageDetails.generalAggregateLimit);
            template.setField('ExcessUmbrella_OtherCoverageDescription', coverageDetails.other1LimitName);
            template.setField('ExcessUmbrella_OtherCoverageLimitAmount', coverageDetails.other1LimitAmount);
        }

        delete policiesByProduct['EXCESS_UMBRELLA_LIABILITY'];
    }
}
