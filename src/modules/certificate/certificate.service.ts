import {
  Inject,
  Injectable,
  NotFoundException,
  Scope
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { isEmpty } from 'lodash';
import { Model, Types } from 'mongoose';
import { nanoid } from 'nanoid';
import { fillAcord101Sections } from 'src/commons/certificates/acords/acord-101/fill-acord-101-sections';
import { fillAcord25Sections } from 'src/commons/certificates/acords/acord-25/fill-acord-25-sections';
import { CertificatePdfTemplate } from 'src/commons/certificates/certificate-pdf-template';
import { ADDITIONAL_REMARKS_SCHEDULE_TEMPLATE_CODE } from 'src/commons/const/project.constants';
import { SearchResult } from 'src/commons/interfaces/search-result.interface';
import { executeAggregatorWithPagination } from 'src/commons/queries/mongo/helpers/query';
import { CertificateCoverTemplate } from 'src/database/entities/certificate-cover-template.schema';
import { CertificateMasterTemplate } from 'src/database/entities/certificate-master-template.schema';
import { Certificate } from 'src/database/entities/certificate.schema';
import { Company } from 'src/database/entities/company.schema';
import { Customer } from 'src/database/entities/customer.schema';
import { File } from 'src/database/entities/file.schema';
import { Insurer } from 'src/database/entities/insurer.schema';
import { SaleItem } from 'src/database/entities/nested/sale-item.schema';
import { Sale } from 'src/database/entities/sale.schema';
import { User } from 'src/database/entities/user.schema';
import { CreateCertificateDto } from '../../dto/certificates/create-certificate.dto';
import { UpdateCertificateDto } from '../../dto/certificates/update-certificate.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { FileService } from '../file/file.service';
import { buildQueryConditions, extractParamFilters } from './certificate.utils';
import { PdfFormFiller } from './helpers/fill-pdf-form';
import { PdfInput } from './interfaces/pdf-input.interface';
import { buildSearchAggregator } from './queries/mongo/aggregators/search.aggregator';

export interface CarrierSaleItems {
  // #region Public Indexers (1)

  [carrierName: string]: SaleItem[];

  // #endregion Public Indexers (1)
}

@Injectable({ scope: Scope.REQUEST })
export class CertificateService {
  // #region Constructors (1)

  constructor(

    @InjectModel(Certificate.name) private certificateModel: Model<Certificate>,
    @InjectModel(CertificateMasterTemplate.name) private masterTemplateModel: Model<CertificateMasterTemplate>,
    @InjectModel(CertificateCoverTemplate.name) private coverTemplateModel: Model<CertificateCoverTemplate>,
    @InjectModel(Customer.name) private customerModel: Model<Customer>,
    @InjectModel(Company.name) private companyModel: Model<Company>,
    @InjectModel(Sale.name) private saleModel: Model<Sale>,
    @InjectModel(Insurer.name) private insurerModel: Model<Insurer>,
    @InjectModel(User.name) private userModel: Model<User>,
    @Inject(REQUEST) private request: AuthenticatedRequest,
    private readonly fileService: FileService,
    private configService: ConfigService,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (13)

  /**
   * @param {string[]} codes 
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.certificateModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @param  {CreateCertificateDto} certificateDto
   * @returns Promise
   */
  public async create(
    certificateDto: CreateCertificateDto
  ): Promise<Certificate> {
    const newCertificate: Certificate = await new this.certificateModel({
      ...certificateDto,
      createdBy: { _id: this.request.user.id },
      company: { _id: this.request.user.company },
    });

    return newCertificate.save();
  }

  /**
   * @returns Promise
   */
  public async deleteAll(): Promise<any> {
    return await this.certificateModel.deleteMany({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async deleteByCode(code: string): Promise<Certificate> {
    const certificate = await this.certificateModel
      .findOne({ code: code })
      .exec();

    if (!certificate) {
      throw new NotFoundException(`Certificate (code: ${code}) not found.`);
    }

    return certificate.delete().exec();

    //return await this.certificateModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.certificateModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<Certificate> {
    const certificate: Certificate = await this.certificateModel
      .findOne({ code: code }).populate('masterTemplate holder')
      .exec();

    if (!certificate) {
      throw new NotFoundException(`Certificate (code: ${code}) not found.`);
    }

    return certificate;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<Certificate> {
    const certificate: Certificate = await this.certificateModel
      .findOne({ _id: id })
      .exec();

    if (!certificate) {
      throw new NotFoundException(`Certificate (id: ${id}) was not found`);
    }

    return certificate;
  }

  /**
   * @param  {string} code
   * @param  {UpdateCertificateDto} certificateDto
   * @returns Promise
   */
  public async generateCertificate(certificate: Partial<Certificate>): Promise<any> {
    const company = await this.getCompany();
    const producerAgents = await this.getCompanyProducerAgents(company);

    const holder = await this.getHolder(certificate.holder.toString());
    const policies = await this.getPolicies(certificate);
    const uniqueCarrierIds = this.getUniqueCarrierIds(policies);
    const carriers = await this.getCarriers(uniqueCarrierIds);
    const carrierWithCoverages = this.buildCarrierWithCoverages(carriers, policies);

    const masterTemplate = await this.getMasterTemplate(certificate.masterTemplate.toString());
    const file = await this.getMasterTemplateFile(masterTemplate.file.toString());
    const masterTemplateBuffer = await this.downloadMasterTemplatePdf(file);

    const pdfInputs: PdfInput[] = [];

    let template: CertificatePdfTemplate;

    switch (masterTemplate?.code) {
      case "25":
      default:
        template = fillAcord25Sections(certificate, company, producerAgents, holder, carrierWithCoverages);
        pdfInputs.push({
          inputPdfBuffer: masterTemplateBuffer,
          template: template
        });
    }

    if (template.hasAdditionalRemarksSchedule || !isEmpty(certificate.vehiclesIncluded) || !isEmpty(certificate.cohortsIncluded)) {

      const additionalRemarksScheduleTemplate = await this.getAdditionalRemarksScheduleTemplate();
      const file = await this.getMasterTemplateFile(additionalRemarksScheduleTemplate.file.toString());
      const additionalRemarksScheduleTemplateBuffer = await this.downloadMasterTemplatePdf(file);

      if (!isEmpty(policies)) {
        for (let i = 0; i < policies.length; i++) {

          const additionalRemarksScheduleTemplate = fillAcord101Sections(certificate, policies[i], company, i + 2);

          pdfInputs.push({
            inputPdfBuffer: additionalRemarksScheduleTemplateBuffer,
            template: additionalRemarksScheduleTemplate
          });
        }
      }
    }

    const pdfFormFiller = new PdfFormFiller();
    const combinedPdfBuffer = await pdfFormFiller.processAndCombinePdfForms(pdfInputs);

    return combinedPdfBuffer;
  }

  /**
   * @param  {string} code
   * @param  {UpdateCertificateDto} certificateDto
   * @returns Promise
   */
  public async previewCertificate(certificate: Partial<Certificate>): Promise<any> {
    const company = await this.getCompany();
    const producerAgents = await this.getCompanyProducerAgents(company);

    const holder = await this.getHolder(certificate.holder._id.toString());
    const policies = await this.getPolicies(certificate);
    const uniqueCarrierIds = this.getUniqueCarrierIds(policies);
    const carriers = await this.getCarriers(uniqueCarrierIds);
    const carrierWithCoverages = this.buildCarrierWithCoverages(carriers, policies);

    const masterTemplate = await this.getMasterTemplate(certificate.masterTemplate._id.toString());
    const file = await this.getMasterTemplateFile(masterTemplate.file.toString());
    const masterTemplateBuffer = await this.downloadMasterTemplatePdf(file);

    const pdfInputs: PdfInput[] = [];

    let template: CertificatePdfTemplate;

    switch (masterTemplate?.code) {
      case "25":
      default:
        template = fillAcord25Sections(certificate, company, producerAgents, holder, carrierWithCoverages);
        pdfInputs.push({
          inputPdfBuffer: masterTemplateBuffer,
          template: template
        });
    }

    if (template.hasAdditionalRemarksSchedule || !isEmpty(certificate.vehiclesIncluded) || !isEmpty(certificate.cohortsIncluded)) {

      const additionalRemarksScheduleTemplate = await this.getAdditionalRemarksScheduleTemplate();
      const file = await this.getMasterTemplateFile(additionalRemarksScheduleTemplate.file.toString());
      const additionalRemarksScheduleTemplateBuffer = await this.downloadMasterTemplatePdf(file);

      if (!isEmpty(policies)) {
        for (let i = 0; i < policies.length; i++) {

          const additionalRemarksScheduleTemplate = fillAcord101Sections(certificate, policies[i], company, i + 2);

          pdfInputs.push({
            inputPdfBuffer: additionalRemarksScheduleTemplateBuffer,
            template: additionalRemarksScheduleTemplate
          });
        }
      }
    }

    const pdfFormFiller = new PdfFormFiller();
    const combinedPdfBuffer = await pdfFormFiller.processAndCombinePdfForms(pdfInputs);

    return combinedPdfBuffer;
  }

  public async getCatalog(filterCriteria: any): Promise<any> {
    return await this.certificateModel
      .find(filterCriteria)
      .select('code name type _id')
      .sort({ name: 1 })
      .exec();
  }

  /**
   * @param  {string} code
   * @param  {UpdateCertificateDto} certificateDto
   * @returns Promise
   */
  public async initCertificate(
    customerId: string
  ): Promise<any> {
    const customer = await this.customerModel
      .findOne({ _id: customerId }).populate([
        { path: 'vehicles', select: '_id code color licensePlate make model vinNumber year unitNumber' },
        { path: 'cohorts', select: '_id code email firstName lastName phone relationship' }
      ])
      .select('code email name phone type contact business')
      .exec();

    if (!customer) {
      throw new NotFoundException(`Customer (id: ${customerId}) was not found`);
    }

    const policies: Sale[] = await this.saleModel.find({ customer: customer.id, deleted: false }).select('_id code number').exec();

    const certificate = new this.certificateModel();
    certificate.code = nanoid(6);
    certificate.ccEmails = [];
    certificate.ccFaxNumbers = [];
    certificate.coverPage = "";
    certificate.customer = customer;
    certificate.holder = null;
    certificate.insurers = [];
    certificate.name = "";
    certificate.policies = policies;
    certificate.policiesIncluded = [];
    certificate.cohortsIncluded = [];
    certificate.vehiclesIncluded = []

    return certificate;
  }

  public async search(queryParams?: SearchQueryParams): Promise<SearchResult> {
    /* const { pageNumber = 1, pageSize = 10, searchText, searchColumns } = queryParams || {};

    const sortCriteria = calculateSortCriteria(queryParams);

    const skipCriteria = (pageNumber - 1) * pageSize;
    const limitCriteria = pageSize;

    const matchDeleted = { $match: { deleted: false } };
    const matchSearchText = searchText
      ? {
        $match: {
          $or: buildSearchTextQueries(
            searchText,
            searchColumns && searchColumns.length ? searchColumns : ['name']
          ),
        },
      }
      : null;

    const lookupCreatedBy = {
      $lookup: {
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy',
      },
    };
    const unwindCreatedBy = { $unwind: { path: '$createdBy', preserveNullAndEmptyArrays: true } };
    const projectFields = {
      $project: {
        id: '$_id',
        customer: '$customer',
        holder: '$holder',
        name: '$name',
        createdAt: '$createdAt',
        createdBy: '$createdBy.name',
        state: '$insured.address.state',
        code: '$code',
        deleted: '$deleted',
      },
    };

    const sortEntities = { $sort: sortCriteria };
    const skipEntities = { $skip: skipCriteria };
    const limitEntities = { $limit: limitCriteria };

    const basePipeline: any[] = [
      matchDeleted,
      matchSearchText,
      lookupCreatedBy,
      unwindCreatedBy,
      projectFields,
    ].filter(Boolean);

    const countPipeline = basePipeline.concat([{ $count: 'totalCount' }]);
    const resultPipeline = basePipeline.concat([sortEntities, skipEntities, limitEntities]);

    const [[{ totalCount } = { totalCount: 0 }], entities] = await Promise.all([
      this.certificateModel.aggregate(countPipeline).exec(),
      this.certificateModel.aggregate(resultPipeline).exec(),
    ]);

    return {
      entities,
      totalCount,
      page: pageNumber,
      limit: pageSize,
    }; */

    const queryFilters: any = extractParamFilters(queryParams);
    const conditions = buildQueryConditions(queryParams, queryFilters);

    const aggregator = buildSearchAggregator(conditions);

    const result = await executeAggregatorWithPagination(aggregator, queryParams, this.certificateModel);
    return result;
  }


  /**
   * @param  {string} code
   * @param  {UpdateCertificateDto} certificateDto
   * @returns Promise
   */
  public async update(
    code: string,
    certificateDto: UpdateCertificateDto,
  ): Promise<Certificate> {
    const existingCertificate = await this.certificateModel
      .findOne({ code: code })
      .exec();

    if (!existingCertificate) {
      throw new NotFoundException(`Certificate (code: ${code}) not found.`);
    }

    return await this.certificateModel.findByIdAndUpdate(
      existingCertificate.id,
      {
        ...existingCertificate['_doc'],
        ...certificateDto,
        updatedBy: this.request.user.id,
      });

    //this.sanitizeProfileAttributes(certificate);

    //    return certificate.save();

    /* return await this.certificateModel.findOneAndUpdate(
      { _id: new Types.ObjectId(existingCertificate._id) },
      certificate,
      { new: true },
    ); */
  }

  // #endregion Public Methods (13)

  // #region Private Methods (12)

  private buildCarrierWithCoverages(carriers: Insurer[], policies: Sale[]): { name: string, naicCode: string, coverages: SaleItem[] }[] {
    const coverageListByCarrier: { name: string, naicCode: string, coverages: SaleItem[] }[] = [];
    this.buildCoveragesByCarrierList(carriers, policies, coverageListByCarrier);
    return Object.values(coverageListByCarrier);
  }

  private buildCoveragesByCarrierList(carriers: Insurer[], policies: (Sale)[], coverageListByCarrier: { name: string; naicCode: string; coverages: SaleItem[]; }[]) {
    carriers.forEach((carrier) => {
      const coverageList: { name: string; naicCode: string; coverages: SaleItem[]; policyNumber: string; effectiveAt?: Date; expiresAt?: Date; letter?: string; } = {
        name: carrier.name,
        naicCode: carrier.business?.naicCode ?? '',
        coverages: [],
        policyNumber: '',
      };

      policies.forEach((policy) => {
        policy.items.forEach((item) => {
          if (item.carrier && item.carrier.toString() === carrier._id.toString()) {
            coverageList.coverages.push(item);
            coverageList.policyNumber = policy.number;
            coverageList.effectiveAt = policy.effectiveAt;
            coverageList.expiresAt = policy.expiresAt;
          }
        });
      });
      coverageListByCarrier[carrier._id.toString()] = { ...coverageList };
    });
  }

  private async downloadMasterTemplatePdf(file: File): Promise<Buffer> {
    const s3Filename = `${file.path}/${file.name}`;
    return this.fileService.getFileDriver(s3Filename, this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PRIVATE'));
  }

  private async getAdditionalRemarksScheduleTemplate(): Promise<CertificateMasterTemplate> {
    return this.masterTemplateModel.findOne({ code: ADDITIONAL_REMARKS_SCHEDULE_TEMPLATE_CODE }).exec();
  }

  private async getCarriers(uniqueCarrierIds: Set<string>): Promise<Insurer[]> {
    return this.insurerModel.find({ _id: { $in: Array.from(uniqueCarrierIds) } }).select('_id name business.naicCode').exec();
  }

  private async getCompany(): Promise<Company> {
    return this.companyModel.findOne({ _id: this.request.user.company }).populate('settings').exec();
  }

  private async getCompanyProducerAgents(company: Company): Promise<User[]> {
    if (company.settings?.certificateProducerAgents) {
      return this.userModel.find({ _id: { $in: company.settings.certificateProducerAgents } }).exec();
    }
    return [];
  }

  private async getHolder(holderId: string): Promise<Insurer> {
    return await this.insurerModel.findOne({ _id: new Types.ObjectId(holderId.toString()) }).exec();
  }

  private async getMasterTemplate(masterTemplateId: string): Promise<CertificateMasterTemplate> {
    return await this.masterTemplateModel.findById(masterTemplateId).exec();
  }

  private async getMasterTemplateFile(fileId: string): Promise<File> {
    return await this.fileService.findById(fileId);
  }

  private async getPolicies(certificate: Partial<Certificate>): Promise<Sale[]> {
    const policyIds: string[] = certificate.policiesIncluded.length ? certificate.policiesIncluded : certificate.policies.map(policy => policy._id);
    return this.saleModel.find({ _id: { $in: policyIds.map(id => new Types.ObjectId(id)) } });
  }

  private getUniqueCarrierIds(policies: Sale[]): Set<string> {
    const carrierIds = policies.flatMap((policy) => policy.items.map((item) => item.carrier.toString()));
    return new Set(carrierIds);
  }

  // #endregion Private Methods (12)
}
