import { PdfAnswer } from "src/commons/enum/pdf-answer.enum";
import { PdfCheckBox } from "src/commons/enum/pdf-checkbox.enum";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillWorkersCompensationFields = (policiesByProduct: any, template: Acord25PdfTemplateV2014NowCerts) => {
    if (policiesByProduct['WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY']) {

        const policy: any = policiesByProduct['WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY'];

        template.setField('WorkersCompensationEmployersLiability_InsurerLetterCode', policy.letter ?? '');
        template.setField('Policy_WorkersCompensationAndEmployersLiability_PolicyNumberIdentifier', policy.policyNumber ?? '');
        template.setField('Policy_WorkersCompensationAndEmployersLiability_EffectiveDate', policy.effectiveAt?.toLocaleDateString() ?? '');
        template.setField('Policy_WorkersCompensationAndEmployersLiability_ExpirationDate', policy.expiresAt?.toLocaleDateString() ?? '');

        const coverageDetails = policy.coverage.details;

        if (coverageDetails) {
            template.setField('WorkersCompensationEmployersLiability_AnyPersonsExcludedIndicator', coverageDetails.hasExcludedPeople ? PdfAnswer.YES : PdfAnswer.NO);
            template.setField('Policy_WorkersCompensation_SubrogationWaivedCode', coverageDetails.waiverOfSubrogation ? PdfAnswer.YES : PdfAnswer.EMPTY);
            template.setField('WorkersCompensationEmployersLiability_WorkersCompensationStatutoryLimitIndicator', coverageDetails.perStatute ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('WorkersCompensationEmployersLiability_OtherCoverageIndicator', coverageDetails.other ? PdfCheckBox.ON : PdfCheckBox.OFF);
            //template.setField('WorkersCompensationEmployersLiability_OtherCoverageDescription', coverageDetails.other && coverageDetails.);
            template.setField('WorkersCompensationEmployersLiability_EmployersLiability_EachAccidentLimitAmount', coverageDetails.employerLiabilityEachAccidentLimit);
            template.setField('WorkersCompensationEmployersLiability_EmployersLiability_DiseaseEachEmployeeLimitAmount', coverageDetails.employerLiabilityEachEmployeeLimit);
            template.setField('WorkersCompensationEmployersLiability_EmployersLiability_DiseasePolicyLimitAmount', coverageDetails.employerLiabilityDiseasePolicyLimit);
        }

        delete policiesByProduct['WORKERS_COMPENSATION_AND_EMPLOYERS_LIABILITY'];
    }
}