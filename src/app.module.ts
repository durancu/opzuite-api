import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DevtoolsModule } from '@nestjs/devtools-integration';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './modules/auth/auth.module';
import { CatalogModule } from './modules/catalog/catalog.module';
import { CertificateModule } from './modules/certificate/certificate.module';
import { CompanyModule } from './modules/company/company.module';
import { CustomerModule } from './modules/customer/customer.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { FileModule } from './modules/file/file.module';
import { FinancingModule } from './modules/financing/financing.module';
import { InsurerModule } from './modules/insurer/insurer.module';
import { LocationModule } from './modules/location/location.module';
import { LoggerModule } from './modules/logger/logger.module';
import { PayrollModule } from './modules/payroll/payroll.module';
import { ReportModule } from './modules/report/report.module';
import { SaleModule } from './modules/sale/sale.module';
import { SendgridModule } from './modules/sendgrid/sendgrid.module';
import { SystemModule } from './modules/system/system.module';
import { UserModule } from './modules/user/user.module';

@Module({
  imports: [
    ConfigModule.forRoot({ cache: true, isGlobal: true }),
    DevtoolsModule.register({ http: process.env.NODE_ENV !== 'production' }),
    MongooseModule.forRoot(process.env.MONGODB_URI),
    ScheduleModule.forRoot(),
    LoggerModule.forRoot(),

    //Independent modules
    SystemModule,
    SendgridModule,
    InsurerModule,
    CustomerModule,
    FinancingModule,
    CertificateModule,
    FileModule,
    CompanyModule,

    //Depends on Location, Sendgrid
    UserModule,

    //Depends on User
    AuthModule,
    LocationModule,

    //Depends on Insurer, Location, User, Company
    SaleModule,

    //Depends on Sale, User
    ReportModule,

    //Depends on Report
    DashboardModule,

    //Depends on Location, Metrics, User
    PayrollModule,

    //Depends on  UserService, LocationService, CustomerService, InsurerService, ReportService, RoleService, PayrollService,
    CatalogModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [AppService],
})
export class AppModule { }
