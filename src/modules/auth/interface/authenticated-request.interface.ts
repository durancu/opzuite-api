import { Request } from 'express';
import { User } from 'src/database/entities/user.schema';

export interface AuthenticatedRequest extends Request {
  readonly user: Partial<User>;
}
