import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { Customer } from './customer.schema';
import { Location } from './location.schema';
import { SaleItem, SaleItemSchema } from './nested/sale-item.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Sale extends Document {
    // #region Properties (44)

    @Prop({ type: Boolean, default: false })
    public autoRenew: boolean;
    @Prop({ type: Date, required: false })
    public cancelledAt: Date;
    @Prop({ default: 0, required: false })
    public checksumSanity: number;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company', required: true })
    public company: Company;
    @Prop({ default: 'USA' })
    public country: string;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
    public createdBy: User;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Customer', required: true })
    public customer: Customer;
    @Prop({ type: SchemaTypes.Map, default: {} })
    public details: any;
    @Prop({ type: Date, required: false })
    public effectiveAt: Date;
    @Prop({ type: Date, required: false })
    public expiresAt: Date;
    @Prop({ type: Boolean, default: true })
    public isChargeItemized: boolean;
    @Prop({ type: [SaleItemSchema], required: false, default: [] })
    public items: SaleItem[];
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Location', required: false })
    public location: Location;
    @Prop({ required: false, default: "" })
    public number: string;
    @Prop({ type: [SchemaTypes.Map], default: [] })
    public products: any
    @Prop({ required: false, default: "ANNUAL" })
    public renewalFrequency: string;
    @Prop({ type: [String], required: false/* , ref: 'Sale', required: false  */ })
    public renewalReferences: string[];
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public renewedBy: User;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Sale', required: false })
    public renewedWith: Sale;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
    public seller: User;
    @Prop({ type: Date, required: false, default: new Date() })
    public soldAt: Date;
    @Prop({ default: 'NONE' })
    public states: string;
    @Prop({ default: 'ACTIVE' })
    public status: string;
    @Prop({ default: 0, required: false })
    public totalAgencyCommission: number;
    @Prop({ default: 0, required: false })
    public totalAgentCommission: number;
    @Prop({ default: 0, required: false })
    public totalCoveragesDownPayment: number;
    @Prop({ default: 0, required: false })
    public totalCoveragesPremium: number;
    @Prop({ default: 0, required: false })
    public totalFinanced: number;
    @Prop({ default: 0, required: false })
    public totalFinancedPaid: number;
    @Prop({ default: 0, required: false })
    public totalNonPremium: number;
    @Prop({ default: 0, required: false })
    public totalPaid: number;
    @Prop({ default: 0, required: false })
    public totalPayables: number;
    @Prop({ default: 0, required: false })
    public totalPermits: number;
    @Prop({ default: 0, required: false })
    public totalPremium: number;
    @Prop({ default: 0, required: false })
    public totalReceivables: number;
    @Prop({ default: 0, required: false })
    public totalReceived: number;
    @Prop({ default: 0, required: false })
    public totalTaxesAndFees: number;
    @Prop()
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;
    @Prop()
    public version: string;
    @Prop({ type: Boolean, default: false })
    public wasRenewed: boolean;

    // #endregion Properties (44)
}

export const SaleSchema = SchemaFactory.createForClass(Sale);
SaleSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
