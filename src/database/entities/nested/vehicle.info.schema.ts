import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { nanoid } from 'nanoid';

@Schema()
export class VehicleInfo extends Document {
  // #region Properties (18)

  @Prop()
  public annualMileage: string;
  @Prop()
  public autoInsuranceCompany: string;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop()
  public color: string;
  @Prop()
  public condition: string;
  @Prop()
  public drivingDaysPerWeek: string;
  @Prop()
  public drivingMilesPerWeek: string;
  @Prop()
  public hasAutoInsurance: boolean;
  @Prop()
  public licensePlate: string;
  @Prop()
  public make: string;
  @Prop()
  public model: string;
  @Prop()
  public notes: string;
  //Owned, Financed, Leased
  @Prop()
  public ownershipStatus: string;
  @Prop()
  public primaryUse: string;
  @Prop()
  public trim: string;
  //Commuting, Pleasure, Business
  @Prop()
  public type: string;
  @Prop()
  public vinNumber: string;
  @Prop()
  public year: string;

  // #endregion Properties (18)
}

export const VehicleInfoSchema = SchemaFactory.createForClass(VehicleInfo);