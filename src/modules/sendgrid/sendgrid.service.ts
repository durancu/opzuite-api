
import { Inject, Injectable } from '@nestjs/common';
import { MailDataRequired, MailService } from '@sendgrid/mail';
import { from, Observable, of } from 'rxjs';
import { catchError, mergeMap, tap } from 'rxjs/operators';
import { Company } from 'src/database/entities/company.schema';
import { User } from 'src/database/entities/user.schema';
import { buildNotificationMessage } from './message.builder';
import { SENDGRID_MAIL } from './sendgrid.constants';


export enum NotificationTypes {
    EMPLOYEE_WELCOME = 'employee-welcome',
    EMPLOYEE_NEW_CONFIRM_EMAIL = 'employee-new-confirm-email',
    RESET_PASSWORD = 'reset-password',
    EMPLOYEE_FORGOT_PASSWORD_CONFIRM_EMAIL = 'employee-forgot-password-confirm-email',
}

@Injectable()
export class SendgridService {
    // #region Constructors (1)

    constructor(
        @Inject(SENDGRID_MAIL) private mailService: MailService,
    ) { }

    // #endregion Constructors (1)

    // #region Public Methods (3)

    public build(notificationType:string, user: Partial<User>, company: Partial<Company>, data: any = {}): MailDataRequired {
        let additionalData = {};
        let templateId = '';

        switch (notificationType) {
            case NotificationTypes.EMPLOYEE_WELCOME:
                additionalData = {
                    first_name: user.firstName,
                    login_url: `${process.env.APP_HOST_URL+'/login'}`,
                    password: data.password
                }
                templateId = company.settings.defaults.email.templates.employeeNewWelcomeToUser;
                break;
            case NotificationTypes.EMPLOYEE_NEW_CONFIRM_EMAIL:
                additionalData = {
                    first_name: user.firstName,
                    user_confirmation_url: `${process.env.APP_HOST_URL}/auth/confirm-email/${user.confirmToken}`,
                }
                templateId = company.settings.defaults.email.templates.employeeNewConfirmToUser;
                break;
            case NotificationTypes.EMPLOYEE_FORGOT_PASSWORD_CONFIRM_EMAIL:
                additionalData = {
                    first_name: user.firstName,
                    user_confirmation_url: `${process.env.APP_HOST_URL}/auth/reset-password/${data.token}`,
                }
                templateId = company.settings.defaults.email.templates.employeeForgotPasswordConfirmToUser;
                break;
        }

        return buildNotificationMessage(templateId, user, company, additionalData);
    }

    public send(data: MailDataRequired): Observable<any> {
        return from(this.mailService.send(data, false))
    }

    public async sendNotification(notificationType:string, user: Partial<User>, data: any = {}): Promise<any> {
        const company: Partial<Company> = data.company ? data.company : user.company;

        const mailData: MailDataRequired = this.build(notificationType, user, company, data);
        return this.send(mailData).pipe(
            catchError(err => of(`sending email failed: ${err}`)),
            tap(user => console.log(user)),
            mergeMap(user => from(user)),
        );
    }

    // #endregion Public Methods (3)
}
