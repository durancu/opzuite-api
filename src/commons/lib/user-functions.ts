/* eslint-disable prefer-spread */
/* eslint-disable no-useless-escape */

import { nanoid } from 'nanoid';
import { User } from 'src/database/entities/user.schema';
import { PermissionType } from '../enum/permission-type.enum';

export function can(permissions: any, user: Partial<User>): boolean {
  const permissionsToCheck = Array.isArray(permissions)
    ? permissions
    : [permissions];

  const userPermissions: any[] = user?.permissions;
  if (!permissionsToCheck || permissionsToCheck.length == 0) return true;
  if (!userPermissions || userPermissions.length == 0) return false;

  return (
    userPermissions &&
    userPermissions.some((userPermission) => {
      return includesPermission(userPermission, permissionsToCheck);
    })
  );
}

export function includesPermission(
  userPermission: string,
  permissionsToCheck: string[],
): boolean {
  if (permissionsToCheck.includes(userPermission)) return true;

  return permissionsToCheck.some(
    (permissionToCheck: PermissionType): boolean => {
      return permissionToCheck.includes(userPermission);
    },
  );
}

export function generateRandomToken() { return nanoid(32) }

export function generateRandomPassword(length: any): string {
  const Password = {
    _pattern: /[a-zA-Z0-9_\-\+\.]/,

    _getRandomByte: function () {
      return Math.floor(Math.random() * 256);
    },

    generate: function (length: any) {
      return Array.apply(null, { 'length': length })
        .map(function () {
          let result: string;
          while (result) {
            result = String.fromCharCode(this._getRandomByte());
            if (this._pattern.test(result)) {
              return result;
            }
          }
        }, this)
        .join('');
    }

  };

  return Password.generate(length || 8);
}
