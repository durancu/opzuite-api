import { PartialType } from '@nestjs/mapped-types';
import {
  IsOptional
} from 'class-validator';
import { CreateFinancingDto } from './create-financing.dto';
export class UpdateFinancingDto extends PartialType(CreateFinancingDto) {
  // #region Properties (2)

  @IsOptional()
  public _id?: string;
  @IsOptional()
  public id?: string;

  // #endregion Properties (2)
}
