import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  Param,
  Post,
  Put,
  Response,
  Scope,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { File } from 'src/database/entities/file.schema';
import { CreateFileDto } from 'src/dto/file/create-file.dto';
import { UpdateFileDto } from 'src/dto/file/update-file.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import {
  CopyFileDto,
  DeleteFileDto,
  FileStorageDto,
  MoveFileDto,
  RenameFileDto
} from './dto/file.dto';
import { FileService } from './file.service';

@ApiTags('File')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'files', scope: Scope.REQUEST })
export class FileController {
  constructor(private fileService: FileService) { }

  @Get()
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.FILES_READ)
  async getAllFiles(): Promise<any> {
    const res = await this.fileService.findAll();
    return {
      data: res,
    };
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.FILES_READ)
  async searchFiles(@Body() query: any): Promise<any> {
    return await this.fileService.search(query.queryParams);
  }

  @Get(':id')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.FILES_READ)
  async findFile(
    @Response() res,
    @Param('id') id: string,
    @Headers('id-field') idField: string,
  ): Promise<File> {
    let file: File;
    if (idField && idField === 'id') {
      file = await this.fileService.findById(id);
    } else {
      file = await this.fileService.findByCode(id);
    }

    return res.json(file);
  }

  @ApiBearerAuth()
  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard /*, PermissionsGuard */)
  //@HasPermissions(PermissionType.FILES_CREATE)
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('file'))
  async createFile(
    @Body() fileDto: CreateFileDto,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<File> {
    return this.fileService.create(fileDto, file.buffer, file.originalname);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.FILES_UPDATE)
  async updateFile(
    @Param('code') code: string,
    @Body() file: UpdateFileDto,
  ): Promise<File> {
    return await this.fileService.update(code, file);
  }

  @ApiBearerAuth()
  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.FILES_DELETE)
  async deleteFileById(@Param('code') code: string): Promise<File> {
    return await this.fileService.deleteByCode(code);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: FileStorageDto,
  })
  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file: Express.Multer.File) {
    return this.fileService.uploadFileDriver(file.buffer, file.originalname);
  }

  @Delete(':file')
  async deleteFile(@Body() deleteFileDto: DeleteFileDto) {
    return this.fileService.deleteFileDriver(deleteFileDto.filename);
  }

  @Get('get/:file')
  async getFile(@Param('file') fileName: string) {
    return this.fileService.getFileDriver(fileName);
  }

  @Post('file/rename')
  async renameFile(@Body() renameFileDto: RenameFileDto) {
    return this.fileService.renameFileDriver(
      renameFileDto.oldFileName,
      renameFileDto.newFileName,
    );
  }

  @Post('file/move')
  async moveFile(@Body() moveFileDto: MoveFileDto) {
    return this.fileService.moveFileDriver(moveFileDto.filename);
  }

  @Post('file/copy')
  async copyFile(@Body() copyFileDto: CopyFileDto) {
    return this.fileService.copyFileDriver(
      copyFileDto.oldFileName,
      copyFileDto.newFileName,
    );
  }
}
