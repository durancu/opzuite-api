// string-to-boolean.pipe.ts
import { BadRequestException, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class StringToBooleanPipe implements PipeTransform {
    transform(value: string): boolean {
        if (value) {
            if (value.toLowerCase() === 'true') {
                return true;
            } else if (value.toLowerCase() === 'false') {
                return false;
            } else {
                throw new BadRequestException('Invalid Value. It must be "true" or "false".');
            }
        }
    }
}
