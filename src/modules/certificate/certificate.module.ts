import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CertificateCoverTemplate, CertificateCoverTemplateSchema } from 'src/database/entities/certificate-cover-template.schema';
import { CertificateMasterTemplate, CertificateMasterTemplateSchema } from 'src/database/entities/certificate-master-template.schema';
import { Certificate, CertificateSchema } from 'src/database/entities/certificate.schema';
import { Company, CompanySchema } from 'src/database/entities/company.schema';
import { Customer, CustomerSchema } from 'src/database/entities/customer.schema';
import { Endorsement, EndorsementSchema } from 'src/database/entities/endorsement.schema';
import { File, FileSchema } from 'src/database/entities/file.schema';
import { Insurer, InsurerSchema } from 'src/database/entities/insurer.schema';
import { Sale, SaleSchema } from 'src/database/entities/sale.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { InsurerRepository } from 'src/repositories/insurer.repository';
import { CompanyModule } from '../company/company.module';
import { CustomerModule } from '../customer/customer.module';
import { CustomerService } from '../customer/customer.service';
import { FileModule } from '../file/file.module';
import { FileService } from '../file/file.service';
import { SaleModule } from '../sale/sale.module';
import { SaleService } from '../sale/sale.service';
import { UserModule } from '../user/user.module';
import { CertificateCoverService } from './certificate-cover.service';
import { CertificateModuleInitializerService } from './certificate-data-initializer.service';
import { CertificateTemplateService } from './certificate-template.service';
import { CertificateController } from './certificate.controller';
import { CertificateService } from './certificate.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Certificate.name, schema: CertificateSchema },
      { name: CertificateMasterTemplate.name, schema: CertificateMasterTemplateSchema },
      { name: CertificateCoverTemplate.name, schema: CertificateCoverTemplateSchema },
      { name: Customer.name, schema: CustomerSchema },
      { name: Sale.name, schema: SaleSchema },
      { name: Endorsement.name, schema: EndorsementSchema },
      { name: Insurer.name, schema: InsurerSchema },
      { name: File.name, schema: FileSchema },
      { name: Company.name, schema: CompanySchema },
      { name: User.name, schema: UserSchema }

    ]),
    CustomerModule,
    SaleModule,
    UserModule,
    FileModule,
    CompanyModule,
  ],
  controllers: [
    CertificateController
  ],
  providers: [
    CertificateModuleInitializerService,
    CertificateService,
    CertificateCoverService,
    CertificateTemplateService,
    CustomerService,
    SaleService,
    InsurerRepository,
    FileService
  ],
  exports: [
    CertificateService,
    CertificateCoverService,
    CertificateTemplateService,
  ]
})
export class CertificateModule { }
