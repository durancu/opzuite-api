import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { nanoid } from 'nanoid';

@Schema()
export class PayAddon extends Document {
  // #region Properties (9)

  @Prop()
  public amount?: number;
  @Prop()
  public category?: string;
  @Prop({ default: () => nanoid(6), required: false })
  public code?: string;
  @Prop()
  public description?: string;
  @Prop()
  public endedAt?: Date;
  @Prop()
  public frequency?: number;
  @Prop()
  public normalHoursWorked?: number;
  @Prop()
  public startedAt?: Date;
  @Prop()
  public type?: string;

  // #endregion Properties (9)
}

export const PayAddonSchema = SchemaFactory.createForClass(PayAddon);