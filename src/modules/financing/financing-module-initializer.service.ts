import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';
@Injectable()
export class FinancingDataInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Financing Module) is initialized...');
  }
}
