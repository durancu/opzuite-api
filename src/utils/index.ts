import { nanoid } from 'nanoid';

export const getNanoId = () => {
  return nanoid(6);
};

export const getFileName = (code: string, fileName: string) => {
  return `${code}-${fileName}`;
};
