import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Report } from 'src/database/entities/report.schema';

export class ReportRepository {
  // #region Constructors (1)

  constructor(@InjectModel(Report.name) private readonly reportModel: Model<Report>) { }

  // #endregion Constructors (1)

  // #region Public Methods (6)
  public async findById(id: string, options?: any): Promise<Report> {

    const query = this.reportModel.findOne({ _id: id });

    if (options) {
      if ('populate' in options) {
        const { populate } = options;
        populate.map((field: any) => {
          query.populate(field, populate['field'])
        });
      }
    }

    return await query.exec();
  }

  // #endregion Public Methods (6)
}