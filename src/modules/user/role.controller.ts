import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Req, Response, Scope, UseGuards, UseInterceptors } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { Role } from '../../database/entities/role.schema';
import { CreateRoleDto } from '../../dto/user/create-role.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { RoleService } from './role.service';
@ApiTags('Role')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'roles', scope: Scope.REQUEST })
export class RoleController {
  constructor(private roleService: RoleService) { }

  @Get('')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_READ)
  //@UseFilters(MongoFilter)
  async findAll(@Req() req: Request, @Response() res): Promise<any> {
    return res.json(await this.roleService.findAll());
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_READ)
  //@UseFilters(MongoFilter)
  async getRoleByCode(
    @Param('code') code: string,
  ): Promise<Role> {
    return await this.roleService.findByCode(code);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_READ)
  async searchRoles(@Body() query: any): Promise<any> {
    return await this.roleService.search(query.queryParams);
  }

  @Post('')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS)
  async createRole(
    @Req() req: Request,
    @Body() role: CreateRoleDto,
  ): Promise<Role> {
    return await this.roleService.save(role);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS)
  //@UseFilters(MongoFilter)
  async updateRole(
    @Req() req: Request,
    @Param('code') code: string,
    @Body() role: Role,
  ): Promise<Role> {
    return await this.roleService.update(code, role);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS)
  async deleteRole(@Param('code') code: string): Promise<Role> {
    return await this.roleService.deleteByCode(code);
  }

  @Post('/delete')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS)
  //@UseFilters(MongoFilter)
  async deleteRoles(@Req() req: Request, @Body() body: any): Promise<any> {
    const codes: string[] = body['codes'];
    return await this.roleService.batchDelete(codes);
  }
}
