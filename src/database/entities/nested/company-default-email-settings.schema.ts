import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';
import { CompanyDefaultEmailTemplatesSettings, CompanyDefaultEmailTemplatesSettingsSchema } from "./company-default-email-templates-settings.schema";

@Schema()
export class CompanyDefaultEmailSettings extends Document {
    // #region Properties (1)

    @Prop({ type: CompanyDefaultEmailTemplatesSettingsSchema })
    public templates: CompanyDefaultEmailTemplatesSettings;

    // #endregion Properties (1)
}

export const CompanyDefaultEmailSettingsSchema = SchemaFactory.createForClass(CompanyDefaultEmailSettings);