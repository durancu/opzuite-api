import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class UserModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(User Module) is initialized...');
  }

}
