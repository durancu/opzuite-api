import {
  CopyObjectCommand,
  CopyObjectCommandInput,
  DeleteObjectCommand,
  DeleteObjectCommandInput,
  GetObjectCommand,
  GetObjectCommandInput,
  PutObjectCommand,
  PutObjectCommandInput,
  S3
} from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { HttpException, HttpStatus, Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { buildSearchTextQueries } from 'src/commons/queries/mongo/helpers/query';
import { File } from 'src/database/entities/file.schema';
import { getFileName, getNanoId } from 'src/utils';
import { Readable } from 'stream';
import { CreateFileDto } from '../../dto/file/create-file.dto';
import { UpdateFileDto } from '../../dto/file/update-file.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { Logger } from '../logger/logger.decorator';
import { LoggerService } from '../logger/logger.service';

@Injectable({ scope: Scope.REQUEST })
export class FileService {
  // #region Constructors (1)

  constructor(
    @InjectModel(File.name) private fileModel: Model<File>,
    @Inject(REQUEST) private request: AuthenticatedRequest,
    private s3Service: S3,
    @Logger('FileService') private logger: LoggerService,
    private configService: ConfigService,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (11)

  /**
   * @param {string[]} codes
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.fileModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @param  {CreateFileDto} fileDto
   * @returns Promise
   */
  public async create(
    fileDto: CreateFileDto,
    file: Buffer,
    fileName: string,
  ): Promise<File> {
    const fileCode = getNanoId();
    const newFile: File = new this.fileModel({
      name: fileName,
      ...fileDto,
      code: fileCode,
      createdBy: { _id: this.request.user.id },
      company: { _id: this.request.user.company },
    });

    const fullFilename = fileDto.path ? fileDto.path.concat(`/${fileName}`) : fileName
    const uploadResponse: any = await this.uploadFileDriver(file, fullFilename);

    if (uploadResponse['$metadata'].httpStatusCode === 200) {
      return await newFile.save();
    }

    throw new HttpException('Failed uploading file', HttpStatus.BAD_REQUEST);

  }

  /**
   * @returns Promise
   */
  public async deleteAll(): Promise<any> {
    return await this.fileModel.deleteMany({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async deleteByCode(code: string): Promise<File> {
    const file = await this.fileModel.findOne({ code: code }).exec();

    if (!file) {
      throw new NotFoundException(`File (code: ${code}) not found.`);
    }

    const fileFullName = getFileName(file.code, file.name);
    return this.deleteFileDriver(fileFullName).then(() => {
      return file.delete();
    });
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.fileModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<File> {
    const file: File = await this.fileModel.findOne({ code: code }).exec();

    if (!file) {
      throw new NotFoundException(`File (code: ${code}) not found.`);
    }

    return file;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<File> {
    const file: File = await this.fileModel.findOne({ _id: id }).exec();

    if (!file) {
      throw new NotFoundException(`File (id: ${id}) was not found`);
    }

    return file;
  }

  public async getCatalog(filterCriteria: any): Promise<any> {
    return await this.fileModel
      .find(filterCriteria)
      .select('code name type _id')
      .sort({ name: 1 })
      .exec();
  }

  public async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let type = null;
    if (queryParams.filter) {
      if (
        'type' in queryParams.filter &&
        queryParams.filter.type.trim() !== ''
      ) {
        type = queryParams.filter.type;
        delete queryParams.filter['type'];
      }
    }

    let conditions = {};

    conditions = {
      $and: [{ deleted: false }],
    };

    if (
      type ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      if (type) {
        conditions['$and'].push({ type: type });
      }
    }

    if (queryParams.searchText) {
      let searchColumns: string[] = ['name'];

      if (queryParams.searchColumns && queryParams.searchColumns.length) {
        searchColumns = queryParams.searchColumns;
      }
      const searchTextQueries = buildSearchTextQueries(
        queryParams.searchText,
        searchColumns,
      );

      if (searchTextQueries.length > 0) {
        conditions['$or'] = searchTextQueries;
      }
    }

    const query = this.fileModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query
      .unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy',
      })
      .unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true });

    query.append({
      $project: {
        id: '$_id',
        type: '$type',
        name: '$name',
        createdAt: '$createdAt',
        createdBy: '$createdBy.name',
        code: '$code',
        deleted: '$deleted',
      },
    });

    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.sort(sortCriteria).skip(skipCriteria).limit(limitCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  /**
   * @param  {string} code
   * @param  {UpdateFileDto} fileDto
   * @returns Promise
   */
  public async update(code: string, fileDto: UpdateFileDto): Promise<File> {
    const existingFile = await this.fileModel.findOne({ code: code }).exec();

    if (!existingFile) {
      throw new NotFoundException(`File (code: ${code}) not found.`);
    }

    return await this.fileModel.findByIdAndUpdate(existingFile.id, {
      ...existingFile['_doc'],
      ...fileDto,
      updatedBy: this.request.user.id,
    });

    //this.sanitizeProfileAttributes(file);

    //    return file.save();

    /* return await this.fileModel.findOneAndUpdate(
      { _id: new Types.ObjectId(existingFile._id) },
      file,
      { new: true },
    ); */
  }

  /**
   * @param {Buffer} fileBuffer
   * @param {string} fileName
   * @param {string} bucket
   * @returns Promise
   */
  public async uploadFile(
    fileBuffer: Buffer,
    fileName: string,
    bucket: string,
  ): Promise<any> {
    const params: PutObjectCommandInput = {
      Bucket: bucket,
      Key: fileName,
      Body: fileBuffer,
    };
    return await this.s3Service.send(new PutObjectCommand(params));
  }

  public async uploadFileDriver(
    fileBuffer: Buffer,
    fileName: string,
    bucket?: string
  ): Promise<any> {
    try {
      const response = await this.uploadFile(
        fileBuffer,
        fileName,
        bucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
      );
      this.logger.log(`uploaded file ${fileName}`);
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async deleteFile(fileName: string, bucket: string): Promise<any> {
    const params: DeleteObjectCommandInput = {
      Bucket: bucket,
      Key: fileName,
    };

    return this.s3Service.send(new DeleteObjectCommand(params));
  }

  public async deleteFileDriver(fileName: string, bucket?: string): Promise<any> {
    try {
      const response = await this.deleteFile(
        fileName,
        bucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
      );
      this.logger.log(`deleted file ${fileName}`);
      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async getFile(fileName: string, bucket?: string): Promise<any> {
    const params: GetObjectCommandInput = {
      Bucket: bucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
      Key: fileName,
    };

    const command = new GetObjectCommand(params);
    return getSignedUrl(this.s3Service, command, {
      expiresIn: 3600,
    });
  }

  public async getFileDriver(fileName: string, bucket?: string): Promise<any> {
    try {

      const command = new GetObjectCommand({
        Bucket: bucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
        Key: fileName,
      });

      const response = await this.s3Service.send(command);

      if (!response.Body) {
        throw new Error('Failed to get file from S3');
      }

      return new Promise((resolve, reject) => {
        const chunks: Uint8Array[] = [];
        const readableStream = response.Body as Readable;

        readableStream.on('data', (chunk: Uint8Array) => {
          chunks.push(chunk);
        }).on('end', () => {
          resolve(Buffer.concat(chunks));
        }).on('error', (error) => {
          reject(error);
        });
      });
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async renameFile(
    oldFileName: string,
    newFileName: string,
    bucket: string,
  ): Promise<any> {
    return this.copyFile(oldFileName, newFileName, bucket, bucket).then(() => {
      return this.deleteFile(oldFileName, bucket);
    });
  }

  public async renameFileDriver(
    oldFileName: string,
    newFileName: string,
    bucket?: string
  ): Promise<any> {
    try {
      const response = await this.renameFile(
        oldFileName,
        newFileName,
        bucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
      );
      this.logger.log(`renamed file ${oldFileName} to ${newFileName}`);

      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async copyFile(
    sourceFileName: string,
    newFileName: string,
    sourceBucket: string,
    targetBucket: string,
  ): Promise<any> {
    const params: CopyObjectCommandInput = {
      Bucket: targetBucket,
      CopySource: `${sourceBucket}/${sourceFileName}`,
      Key: newFileName,
    };

    const copyCommand = new CopyObjectCommand(params);
    return this.s3Service.send(copyCommand);
  }

  public async copyFileDriver(
    sourceFileName: string,
    newFileName: string,
    sourceBucket?: string,
    targetBucket?: string,
  ): Promise<any> {
    try {
      const response = await this.copyFile(
        sourceFileName,
        newFileName,
        sourceBucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
        targetBucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
      );
      this.logger.log(
        `Copied file ${sourceFileName} successfully`,
      );

      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  public async moveFile(
    fileName: string,
    sourceBucket?: string,
    targetBucket?: string,
  ): Promise<any> {
    return this.copyFile(
      fileName,
      fileName,
      sourceBucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
      targetBucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC')
    ).then(
      () => {
        return this.deleteFile(fileName, sourceBucket);
      },
    );
  }

  public async moveFileDriver(fileName: string, sourceBucket?: string, targetBucket?: string): Promise<any> {
    try {
      const response = await this.moveFile(
        fileName,
        sourceBucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC'),
        targetBucket ?? this.configService.get<string>('AWS_S3_CLIENT_BUCKET_PUBLIC')
      );
      this.logger.log(
        `Moved file sucessfully`,
      );

      return response;
    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }
  // #endregion Public Methods (11)
}
