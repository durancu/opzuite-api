import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp, kebabCase } from 'lodash';
import { Model, Types, UpdateQuery } from 'mongoose';
import { Role } from 'src/database/entities/role.schema';
import { CreateRoleDto } from '../../dto/user/create-role.dto';
import { UpdateRoleDto } from '../../dto/user/update-role.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';

@Injectable({ scope: Scope.REQUEST })
export class RoleService {
  constructor(
    @InjectModel(Role.name) private roleModel: Model<Role>,
    @Inject(REQUEST) private req: AuthenticatedRequest
  ) { }

  /**
   * @returns Promise<Role>
   */
  async findAll(): Promise<Role[]> {
    return await this.roleModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise<Role>
   */
  async findByCode(code: string): Promise<Role> {
    const role: Role = await this.roleModel.findOne({ code: code }).exec();

    if (!role) {
      throw new NotFoundException(`Role (code: ${code}) not found.`);
    }

    return role;
  }

  /**
   * @param  {string} id
   * @returns Promise<Role>
   */
  async findById(id: string): Promise<Role> {
    const role: Role = await this.roleModel.findOne({ _id: id }).exec();

    if (!role) {
      throw new NotFoundException(`Role (id: ${id}) was not found`);
    }

    return role;
  }

  /**
   * @param  {CreateRoleDto} roleDto
   * @returns Promise
   */
  async save(roleDto: CreateRoleDto): Promise<Role> {
    return await this.roleModel.create({
      ...roleDto,
      key: kebabCase(roleDto.name),
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    });
  }

  /**
   * @param  {string} code
   * @param  {UpdateRoleDto} data
   * @returns Promise
   */
  async update(code: string, role: UpdateRoleDto): Promise<Role> {
    const roleFound: Role = await this.roleModel.findOne({ code: code }).exec();

    if (!roleFound) {
      throw new NotFoundException(`Role (code: ${code}) not found.`);
    }

    const roleData: UpdateQuery<Role> = {
      ...roleFound['_doc'],
      ...role,
      updatedBy: this.req.user.id,
    };

    return await this.roleModel.findOneAndUpdate(
      { _id: new Types.ObjectId(roleFound._id) },
      roleData,
      { new: true },
    );
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async deleteByCode(code: string): Promise<Role> {
    const role: Role = await this.roleModel.findOne({ code: code }).exec();

    if (!role) {
      throw new NotFoundException(`Role (code: ${code}) not found.`);
    }

    return await this.roleModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */

  async deleteAll(): Promise<any> {
    return await this.roleModel.deleteMany({}).exec();
  }

  /**
   * @returns Promise
   */

  async batchDelete(codes: string[]): Promise<any> {
    return await this.roleModel.deleteMany({ code: { $in: codes } }).exec();
  }

  async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField || 'hierarchy'] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let conditions = {};

    conditions = {
      $and: [{ deleted: false }],
    };

    if (queryParams.filter && Object.keys(queryParams.filter).length > 0) {
      const filterQueries = Object.keys(queryParams.filter).map((key) => {
        return {
          [key]: {
            $regex: new RegExp(
              '.*' + escapeRegExp(queryParams.filter[key]) + '.*',
              'i',
            ),
          },
        };
      });
      conditions['$or'] = filterQueries;
    }

    const query = this.roleModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query.append(
      {
        $project: {
          id: '$_id',
          name: '$name',
          permissions: '$permissions',
          deleted: '$deleted',
          code: '$code',
          hierarchy: '$hierarchy',
        },
      }
    );

    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.skip(skipCriteria).limit(limitCriteria).sort(sortCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  async getCatalog(filterCriteria: any): Promise<any> {
    return await this.roleModel
      .find(filterCriteria)
      .select('name description code key _id hierarchy')
      .sort({ name: 1 })
      .exec();
  }
}
