import { PartialType } from "@nestjs/mapped-types";
import { IsOptional } from "class-validator";
import { CreateSystemSettingDto } from "./create-system-setting.dto";

export class UpdateSystemSettingDto extends PartialType(CreateSystemSettingDto) {
  // #region Properties (2)

  @IsOptional()
  public _id?: string;
  @IsOptional()
  public id?: string;

  // #endregion Properties (2)
}
