import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Product extends Document {
  // #region Properties (6)

  @Prop()
  public amount: number;
  @Prop()
  public applicationType: string;
  @Prop()
  public description: string;
  @Prop()
  public iconClass: string;
  @Prop()
  public iconLabel: string;
  @Prop()
  public name: string;

  // #endregion Properties (6)
}

export const ProductSchema = SchemaFactory.createForClass(Product);
