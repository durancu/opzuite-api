import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class InsurerModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Insurer Module) is initialized...');
  }
}
