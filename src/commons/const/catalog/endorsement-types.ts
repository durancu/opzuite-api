export const ENDORSEMENT_TYPES = [
  {
    name: 'Initial Base Premium',
    id: 'INITIAL_BASE_PREMIUM',
    type: 'PREMIUM',
  },
  {
    name: 'Positive Endorsement Premium',
    id: 'POSITIVE_ENDORSEMENT_PREMIUM',
    type: 'PREMIUM',
  },
  {
    name: 'Negative Endorsement Premium',
    id: 'NEGATIVE_ENDORSEMENT_PREMIUM',
    type: 'NEGATIVE',
  },
  {
    name: 'Cancel',
    id: 'CANCEL',
    type: 'NEGATIVE',
  },
  {
    name: 'Renewal Premium',
    id: 'RENEWAL_PREMIUM',
    type: 'PREMIUM',
  },
  {
    name: 'Rewrite Premium',
    id: 'REWRITE_PREMIUM',
    type: 'PREMIUM',
  },
  {
    name: 'Reinstate',
    id: 'REINSTATE',
    type: 'PREMIUM',
  },
  {
    name: 'General Discount',
    id: 'GENERAL_DISCOUNT',
    type: 'NEGATIVE',
  },
  {
    name: 'Govt Discount',
    id: 'GOVT_DISCOUNT',
    type: 'NEGATIVE',
  },
  {
    name: 'Premium Audit',
    id: 'PREMIUM_AUDIT',
    type: 'PREMIUM',
  },
  {
    name: 'Credit',
    id: 'CREDIT',
    type: 'PREMIUM',
  },
  {
    name: 'Installment Premium',
    id: 'INSTALLMENT_PREMIUM',
    type: 'PREMIUM',
  },
  {
    name: 'New Policy Premium',
    id: 'NEW_POLICY_PREMIUM',
    type: 'PREMIUM',
  },
  {
    name: 'No Premium Impact',
    id: 'NO_PREMIUM_IMPACT',
    type: 'PREMIUM',
  },
  {
    name: 'Other',
    id: 'OTHER',
    type: 'PREMIUM',
  },
];
