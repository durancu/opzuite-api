import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { User, UserSchema } from '../user.schema';
import { CompanyDefaults, CompanyDefaultsSchema } from './company-defaults.schema';
import { InsurerCommissionsPlan, InsurerCommissionsPlanSchema } from './insurer-commissions-plan.schema';

@Schema()
export class CompanySettings extends Document {
  // #region Properties (2)

  @Prop({ type: CompanyDefaultsSchema })
  public defaults: CompanyDefaults;
  @Prop({ type: [InsurerCommissionsPlanSchema] })
  public insurersCommissionPlans: InsurerCommissionsPlan[];
  @Prop({ type: [UserSchema], required: false, default: [] })
  public certificateAgents: User[];
  @Prop({ type: [UserSchema], required: false, default: [] })
  public certificateProducerAgents: User[];
  @Prop({ type: [UserSchema], required: false, default: [] })
  public renewalAgents: User[];

  // #endregion Properties (2)
}

export const CompanySettingsSchema = SchemaFactory.createForClass(CompanySettings);