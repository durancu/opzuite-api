export const SALE_COMMISSION_PLANS = [
  {
    id: 'PERCENT_OF_SALES_FIXED',
    name: 'Fixed Percent of Sales as Commission',
    percentCommission: 1,
  },
   {
    id: 'CASH_AFTER_MEETING_PERSONAL_SALES_TARGET',
    name: 'Cash commission after meeting personal sales target',
    target: 50000,
    cashCommission: 100,
  },
   {
    id: 'CASH_BY_COUNTRY_EMPLOYEES_COUNT',
    name: 'Cash commission depending on Country employees count',
    cashCommission: 400,
    target: 20,
  },
   {
    id: 'CASH_AFTER_COUNTRY_EMPLOYEES_SALES',
    name: 'Cash commission depending on Country employees sales by employee',
    cashCommission: 100,
    targetByEmployee: 50000,
  },
  {
    id: 'PERCENT_AFTER_OFFICE_EMPLOYEES_SALES',
    name: 'Cash commission depending on office employees sales by employee',
    percentCommission: 0.5,
    targetByEmployee: 50000,
  },
   {
    id: 'PERCENT_OF_SALES_BY_RANGE',
    name: 'Variable Percent of Sales as commission depending on total sales',
    ranges: [
      {
        min: 0,
        max: 25000,
        percentCommission: 0,
      },
      {
        min: 25000,
        max: 50000,
        percentCommission: 0.5,
      },
      {
        min: 50000,
        percentCommissionRanges: [1, 1.5],
      },
    ],
  },
   {
    id: 'PERCENT_AFTER_MEETING_PERSONAL_SALES_TARGET',
    name: 'Fixed Percent of Sales as commission after meeting a sales target',
    targetByEmployee: 50000,
    percentCommission: 1,
  },
];

//TEMPORARY SOLUTION
export const SALE_COMMISSION_PLANS_BY_ROLE = {
  MEX: {
    'country-manager': [
      {
        id: 'CASH_BY_COUNTRY_EMPLOYEES_COUNT',
      },
      {
        id: 'CASH_AFTER_COUNTRY_EMPLOYEES_SALES',
      },
    ],
    'location-manager': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
      {
        id: 'PERCENT_AFTER_OFFICE_EMPLOYEES_SALES',
      },
    ],
    csr: [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
    certificates: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
      {
        id: 'CASH_AFTER_MEETING_PERSONAL_SALES_TARGET',
      },
    ],
    endorsements: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
    ],
    'csr-in-training': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
  },
  COL: {
    'country-manager': [
      {
        id: 'CASH_BY_COUNTRY_EMPLOYEES_COUNT',
      },
      {
        id: 'CASH_AFTER_COUNTRY_EMPLOYEES_SALES',
      },
    ],
    'location-manager': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
      {
        id: 'PERCENT_AFTER_OFFICE_EMPLOYEES_SALES',
      },
    ],
    csr: [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
    certificates: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
      {
        id: 'CASH_AFTER_MEETING_PERSONAL_SALES_TARGET',
      },
    ],
    endorsements: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
    ],
    'csr-in-training': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
  },
  USA: {
    'super-admin': [],
    admin: [],
    'country-manager': [
      {
        id: 'CASH_BY_COUNTRY_EMPLOYEES_COUNT',
      },
      {
        id: 'CASH_AFTER_COUNTRY_EMPLOYEES_SALES',
      },
    ],
    'location-manager': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
      {
        id: 'PERCENT_AFTER_OFFICE_EMPLOYEES_SALES',
      },
    ],
    csr: [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
    certificates: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
      {
        id: 'CASH_AFTER_MEETING_PERSONAL_SALES_TARGET',
      },
    ],
    endorsements: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
    ],
    'csr-in-training': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
    'company-manager': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
  },
  SUR: {
    'country-manager': [
      {
        id: 'CASH_BY_COUNTRY_EMPLOYEES_COUNT',
      },
      {
        id: 'CASH_AFTER_COUNTRY_EMPLOYEES_SALES',
      },
    ],
    'location-manager': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
      {
        id: 'PERCENT_AFTER_OFFICE_EMPLOYEES_SALES',
      },
    ],
    csr: [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
    certificates: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
      {
        id: 'CASH_AFTER_MEETING_PERSONAL_SALES_TARGET',
      },
    ],
    endorsements: [
      {
        id: 'PERCENT_OF_SALES_FIXED',
      },
    ],
    'csr-in-training': [
      {
        id: 'PERCENT_OF_SALES_BY_RANGE',
      },
    ],
  },
};

export interface CommissionPlan {
  id: string;
  percentCommission?: number;
  cashCommission?: number;
  target?: number;
  ranges?: any;
  targetByEmployee?: number;
}
