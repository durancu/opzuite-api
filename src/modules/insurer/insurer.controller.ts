import {
  Body,
  Controller, Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put, Req,
  Response,
  Scope, UseGuards, UseInterceptors
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { CreateInsurerDto } from 'src/dto/insurer/create-insurer.dto';
import { UpdateInsurerDto } from 'src/dto/insurer/update-insurer.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { Insurer } from '../../database/entities/insurer.schema';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { InsurerService } from './insurer.service';

@ApiTags('Insurer')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'insurers', scope: Scope.REQUEST })
export class InsurerController {
  constructor(private insurerService: InsurerService) { }

  @Get('')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_READ, PermissionType.MGAS_READ, PermissionType.FINANCERS_READ)
  //@UseFilters(MongoFilter)
  async findAll(@Req() req: Request, @Response() res): Promise<any> {
    return res.json(await this.insurerService.findAll());
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_READ, PermissionType.MGAS_READ, PermissionType.FINANCERS_READ)
  //@UseFilters(MongoFilter)
  async getInsurerByCode(
    @Param('code') code: string,
  ): Promise<Insurer> {
    return await this.insurerService.findByCode(code);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_READ, PermissionType.MGAS_READ, PermissionType.FINANCERS_READ)
  async searchInsurers(@Body() query: any): Promise<any> {
    return await this.insurerService.search(query.queryParams);
  }

  @Post('')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_CREATE, PermissionType.MGAS_CREATE, PermissionType.FINANCERS_CREATE)
  async createInsurer(
    @Req() req: Request,
    @Body() insurer: any,
  ): Promise<CreateInsurerDto> {
    return await this.insurerService.save(insurer);
  }

  @Post('/batch-create')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(
    PermissionType.CARRIERS,
    PermissionType.MGAS,
  )
  async batchCreateInsurers(
    @Req() req: Request,
    @Body() insurers: CreateInsurerDto[],
  ): Promise<any> {
    return await this.insurerService.batchCreate(insurers);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_UPDATE, PermissionType.MGAS_UPDATE, PermissionType.FINANCERS_UPDATE)
  //@UseFilters(MongoFilter)
  async updateInsurer(
    @Req() req: Request,
    @Param('code') code: string,
    @Body() insurer: UpdateInsurerDto,
  ): Promise<Insurer> {
    return await this.insurerService.update(code, insurer);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_DELETE, PermissionType.MGAS_DELETE, PermissionType.FINANCERS_DELETE)
  async deleteInsurer(@Param('code') code: string): Promise<Insurer> {
    return await this.insurerService.deleteByCode(code);
  }

  @Post('/delete')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.CARRIERS_DELETE, PermissionType.MGAS_DELETE, PermissionType.FINANCERS_DELETE)
  async deleteInsurers(@Req() req: Request, @Body() body: any): Promise<any> {
    const codes: string[] = body['codes'];
    return await this.insurerService.batchDelete(codes);
  }
}
