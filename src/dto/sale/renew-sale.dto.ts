import { IsBoolean, IsDateString, IsMongoId, IsOptional } from "class-validator";
import { Company } from "src/database/entities/company.schema";
import { Customer } from "src/database/entities/customer.schema";
import { Endorsement } from "src/database/entities/endorsement.schema";
import { Location } from "src/database/entities/location.schema";
import { Product } from "src/database/entities/nested/product.schema";
import { SaleItemVersion } from "src/database/entities/nested/sale-item-version.schema";
import { SaleItem } from "src/database/entities/nested/sale-item.schema";
import { User } from "src/database/entities/user.schema";

export class RenewSaleDto {
  // #region Properties (42)

  @IsOptional()
  @IsBoolean()
  public autoRenew?: boolean;
  @IsOptional()
  @IsDateString()
  public cancelledAt?: Date;
  @IsOptional()
  public checksumSanity?: number;
  @IsMongoId()
  public company?: Partial<Company>;
  @IsOptional()
  public country?: string;
  @IsOptional()
  public createdAt?: Date;
  @IsOptional()
  public createdBy?: Partial<User>;
  @IsMongoId()
  public customer?: Partial<Customer>;
  @IsOptional()
  public details?: any;
  @IsDateString()
  @IsOptional()
  public effectiveAt?: Date;
  @IsOptional()
  public endorsements?: Partial<Endorsement>[];
  @IsOptional()
  public expiresAt?: Date;
  public isChargeItemized?: boolean;
  @IsOptional()
  public itemVersions?: SaleItemVersion[];
  @IsOptional()
  public items?: SaleItem[];
  public lineOfBusiness?: string;
  @IsOptional()
  public location?: Partial<Location>;
  @IsOptional()
  public monthlyPayment?: number;
  @IsOptional()
  public number?: string;
  @IsOptional()
  public products?: Partial<Product>[];
  public renewalFrequency?: string;
  @IsOptional()
  public seller?: Partial<User>;
  @IsOptional()
  public soldAt?: Date;
  @IsOptional()
  public states?: string;
  @IsOptional()
  public status?: string;
  @IsOptional()
  public totalAgencyCommission?: number;
  @IsOptional()
  public totalAgentCommission?: number;
  @IsOptional()
  public totalCoveragesDownPayment?: number;
  @IsOptional()
  public totalCoveragesPremium?: number;
  @IsOptional()
  public totalFinanced?: number;
  @IsOptional()
  public totalFinancedPaid?: number;
  @IsOptional()
  public totalNonPremium?: number;
  @IsOptional()
  public totalPaid?: number;
  @IsOptional()
  public totalPayables?: number;
  @IsOptional()
  public totalPermits?: number;
  @IsOptional()
  public totalPremium?: number;
  @IsOptional()
  public totalReceivables?: number;
  @IsOptional()
  public totalReceived?: number;
  @IsOptional()
  public totalTaxesAndFees?: number;
  public type?: string;
  @IsOptional()
  public updatedAt?: Date;
  @IsOptional()
  public updatedBy?: Partial<User>;

  //renewedWith, renewalReferences, wasRenewed, renewedBy

  // #endregion Properties (42)
}
