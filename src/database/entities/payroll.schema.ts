import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { Location } from './location.schema';
import { PayStub, PayStubSchema } from './nested/pay-stub.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Payroll extends Document {
  // #region Properties (27)

  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company', required: true })
  public company: Partial<Company>;
  @Prop()
  public country: string;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
  public createdBy: User;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public executedBy: User;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Location', required: false })
  public location: Partial<Location>;
  @Prop()
  public payExecutedAt: Date;
  @Prop()
  public payPeriodEndedAt: Date;
  @Prop()
  public payPeriodStartedAt: Date;
  @Prop({ type: [PayStubSchema], required: false, default: [] })
  public payStubs: PayStub[];
  @Prop({ required: true })
  public periodCode: string;
  @Prop()
  public scope: string;
  @Prop({ default: 'IN-PROGRESS' })
  public status: string;
  @Prop({ default: 0 })
  public totalDiscount: number;
  @Prop({ default: 0 })
  public totalExtraBonus: number;
  @Prop({ default: 0 })
  public totalFees: number;
  @Prop({ default: 0 })
  public totalNetSalary: number;
  @Prop({ default: 0 })
  public totalPermits: number;
  @Prop({ default: 0 })
  public totalPremium: number;
  @Prop({ default: 0 })
  public totalRegularSalary: number;
  @Prop({ default: 0 })
  public totalReimbursement: number;
  @Prop({ default: 0 })
  public totalSaleBonus: number;
  @Prop({ default: 0 })
  public totalSales: number;
  @Prop({ default: 0 })
  public totalTips: number;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public updatedBy: Partial<User>;

  // #endregion Properties (27)
}

export const PayrollSchema = SchemaFactory.createForClass(Payroll);

PayrollSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
