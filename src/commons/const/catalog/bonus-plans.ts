export const BONUS_PLANS = [
  
  //MEXICO BONUS PLANS
  {
    id: 'NONE',
    name: 'NONE',
    country: 'MEX',
    location: null,
    frequency: 'NONE',
    description: 'This plan does not calculate any bonus.',
  },
  {
    id: 'VL17_MEX_CERTIFICATES',
    name: 'Bonus plan for Certificates Assistant in Mexico country',
    country: 'MEX',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_MEX_ENDORSEMENTS',
    name: 'Bonus plan for Endorsements Assistant in Mexico country',
    country: 'MEX',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_MEX_LEGAL',
    name: 'Bonus plan for Country Manager in Mexico country',
    country: 'MEX',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_MEX_OFFICE_MANAGER',
    name: 'Bonus plan for Office Manager in Mexico country',
    country: 'MEX',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_MEX_SELLER',
    name: 'Bonus plan for Sales Agent in Mexico country',
    country: 'MEX',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_MEX_TRAINING',
    name: 'Bonus plan for Sales Agent in training in Mexico country',
    country: 'MEX',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },

  //USA BONUS PLANS
  {
    id: 'VL17_COMPANY_MANAGER',
    name: 'Bonus plan for Company Manager',
    country: null,
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_USA_ENDORSEMENTS',
    name: 'Bonus plan for Endorsements Assistant in USA country',
    country: 'USA',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_USA_CERTIFICATES',
    name: 'Bonus plan for Certificates Assistant in USA country',
    country: 'USA',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_USA_SELLER',
    name: 'Bonus plan for Sales Agent in USA country',
    country: 'USA',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_USA_TRAINING',
    name: 'Bonus plan for Sales Agent in training in USA country',
    country: 'USA',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_USA_OFFICE_MANAGER',
    name: 'Bonus plan for Office Manager in USA country',
    country: 'USA',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },

  //COLOMBIA
  {
    id: 'NONE',
    name: 'NONE',
    country: 'COL',
    location: null,
    frequency: 'NONE',
    description: 'This plan does not calculate any bonus.',
  },
  {
    id: 'VL17_COL_CERTIFICATES',
    name: 'Bonus plan for Certificates Assistant in Colombia country',
    country: 'COL',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_COL_ENDORSEMENTS',
    name: 'Bonus plan for Endorsements Assistant in Colombia country',
    country: 'COL',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_COL_COUNTRY_MANAGER',
    name: 'Bonus plan for Country Manager in Colombia country',
    country: 'COL',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_COL_OFFICE_MANAGER',
    name: 'Bonus plan for Office Manager in Colombia country',
    country: 'COL',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_COL_SELLER',
    name: 'Bonus plan for Sales Agent in Colombia country',
    country: 'COL',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_COL_TRAINING',
    name: 'Bonus plan for Sales Agent in training in Colombia country',
    country: 'COL',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },

  //SURINAM
  {
    id: 'NONE',
    name: 'NONE',
    country: 'SUR',
    location: null,
    frequency: 'NONE',
    description: 'This plan does not calculate any bonus.',
  },
  {
    id: 'VL17_SUR_CERTIFICATES',
    name: 'Bonus plan for Certificates Assistant in Suriname country',
    country: 'SUR',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_SUR_ENDORSEMENTS',
    name: 'Bonus plan for Endorsements Assistant in Suriname country',
    country: 'SUR',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_SUR_COUNTRY_MANAGER',
    name: 'Bonus plan for Country Manager in Suriname country',
    country: 'SUR',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_SUR_OFFICE_MANAGER',
    name: 'Bonus plan for Office Manager in Suriname country',
    country: 'SUR',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_SUR_SELLER',
    name: 'Bonus plan for Sales Agent in Suriname country',
    country: 'SUR',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
  {
    id: 'VL17_SUR_TRAINING',
    name: 'Bonus plan for Sales Agent in training in Suriname country',
    country: 'SUR',
    location: null,
    frequency: 'MONTHLY',
    description: 'TBD',
  },
];
