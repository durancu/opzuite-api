import { Certificate } from "src/database/entities/certificate.schema";
import { Company } from "src/database/entities/company.schema";
import { Sale } from "src/database/entities/sale.schema";
import { Acord101PdfTemplateV2008 } from "../models/acord-101-pdf-template-v-2018";


export const fillRemarksHeaderFields = (certificate: Partial<Certificate>, policy: Sale, company: Company, template: Acord101PdfTemplateV2008, pageNumber: number) => {

    template.setField('Producer_CustomerIdentifier_A', certificate.customer.code);
    template.setField('Location_ProducerIdentifier_A', '');

    template.setField('Form_CurrentPageNumber_A', pageNumber);
    template.setField('Form_TotalPageNumber_A', certificate.policies.length + 1);

    template.setField('Producer_FullName_A', company.name);
    template.setField('Policy_PolicyNumberIdentifier_A', policy.number);

    template.setField('Insurer_FullName_A', '');
    template.setField('Insurer_NAICCode_A', '');
    template.setField('NamedInsured_FullName_A', certificate.customer?.name);
    template.setField('NamedInsured_FullName_B', '');
    template.setField('NamedInsured_FullName_C', '');
    template.setField('NamedInsured_FullName_D', '');
    template.setField('Policy_EffectiveDate_A', policy.effectiveAt?.toLocaleDateString());






}