import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class SystemModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(System Module) is initialized...');
  }
}
