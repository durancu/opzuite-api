import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { Company } from './company.schema';
import { Report } from './report.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Chart extends Document {
  // #region Properties (14)

  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
  public company: Company;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public createdBy: User;
  @Prop({ type: SchemaTypes.Map })
  public dataset: any;
  @Prop()
  public description: string;
  @Prop({ default: 'apex' })
  public library: string;
  @Prop()
  public name: string;
  @Prop({ type: SchemaTypes.Map, default: {} })
  public options: any;
  @Prop({ type: [SchemaTypes.String], default: [] })
  public permissions: PermissionType[];
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Report' })
  public report: Report;
  @Prop({ default: 'public' })
  public scope: string;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public updatedBy: User;

  // #endregion Properties (14)
}

export const ChartSchema = SchemaFactory.createForClass(Chart);

ChartSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
