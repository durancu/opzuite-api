import { Types } from "mongoose";
import { CertificateCoverTemplate } from "src/database/entities/certificate-cover-template.schema";
import { CertificateMasterTemplate } from "src/database/entities/certificate-master-template.schema";
import { Company } from "src/database/entities/company.schema";
import { Customer } from "src/database/entities/customer.schema";
import { Insurer } from "src/database/entities/insurer.schema";
import { Cohort } from "src/database/entities/nested/cohort.schema";
import { VehicleInfo } from "src/database/entities/nested/vehicle.info.schema";
import { Sale } from "src/database/entities/sale.schema";
import { User } from "src/database/entities/user.schema";

export class PreviewCertificateDto {
    ccEmails?: string[];
    ccFaxNumbers?: string[];
    code?: string;
    company?: Types.ObjectId | Company;
    coverPage?: string;
    coverTemplate?: Types.ObjectId | CertificateCoverTemplate;
    createdAt?: Date;
    createdBy?: Types.ObjectId | User;
    customer?: Types.ObjectId | Customer;
    holder?: Types.ObjectId | Insurer;
    insurers?: (Types.ObjectId | Insurer)[];
    issuedAt?: Date;
    masterTemplate?: Types.ObjectId | CertificateMasterTemplate;
    name?: string;
    number?: string;
    revisionNumber?: string;
    policies?: (Types.ObjectId | Sale)[];
    policiesIncluded?: (Types.ObjectId | Sale)[];
    cohortsIncluded?: (Types.ObjectId | Cohort)[];
    producer?: Types.ObjectId | Company;
    updatedAt?: Date;
    updatedBy?: Types.ObjectId | User;
    vehiclesIncluded?: VehicleInfo[];
}