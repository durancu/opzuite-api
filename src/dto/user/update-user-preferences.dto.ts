import { IsObject, IsOptional } from 'class-validator';
import { Communication } from 'src/database/entities/nested/communication.schema';
import { EmailSettings } from 'src/database/entities/nested/email-settings.schema';
import { UserPreferences } from 'src/database/entities/nested/user-preferences.schema';

export class UpdateUserPreferencesDto {
  // #region Properties (26)
  @IsOptional()
  @IsObject()
  public communication?: Communication;
  @IsOptional()
  @IsObject()
  public emailSettings?: EmailSettings;
  @IsOptional()
  public language?: string;
  @IsOptional()
  @IsObject()
  public preferences?: Partial<UserPreferences>;
  @IsOptional()
  public timezone?: string;
  // #endregion Properties (26)
}
