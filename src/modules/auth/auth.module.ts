import { Module } from '@nestjs/common';
import { ConfigModule, ConfigType } from '@nestjs/config';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';
import { PassportModule } from '@nestjs/passport';
import { User, UserSchema } from 'src/database/entities/user.schema';
import jwtConfig from '../../config/jwt.config';
import { SendgridModule } from '../sendgrid/sendgrid.module';
import { SendgridService } from '../sendgrid/sendgrid.service';
import { UserModule } from '../user/user.module';
import { AuthModuleInitializerService } from './auth-module-initializer.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt.strategy';
import { LocalStrategy } from './strategy/local.strategy';

@Module({
  imports: [
    UserModule,
    SendgridModule,
    MongooseModule.forFeature([
      { name: User.name, schema: UserSchema }
    ]),
    ConfigModule.forFeature(jwtConfig),
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule.forFeature(jwtConfig)],
      useFactory: (config: ConfigType<typeof jwtConfig>) => {
        return {
          secret: config.secretKey,
          signOptions: { expiresIn: config.expiresIn },
        } as JwtModuleOptions;
      },
      inject: [jwtConfig.KEY],
    }),
  ],
  providers: [
    AuthModuleInitializerService,
    AuthService, 
    LocalStrategy, 
    JwtStrategy,
    SendgridService,
  ],
  controllers: [
    AuthController
  ],
  exports: [
    AuthService,
    SendgridModule,
  ]
})
export class AuthModule { }
