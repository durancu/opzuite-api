import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class CertificateModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Certificate Module) is initialized...');
  }
}
