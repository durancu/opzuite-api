import {
  IsOptional
} from 'class-validator';
import { Company } from 'src/database/entities/company.schema';
import { BusinessInfo } from 'src/database/entities/nested/business-info.schema';
import { Commission } from 'src/database/entities/nested/commission.schema';
import { ContactInfo } from 'src/database/entities/nested/contact-info.schema';
import { User } from 'src/database/entities/user.schema';

export class CreateInsurerDto {
  // #region Properties (13)

  @IsOptional()
  public business?: Partial<BusinessInfo>;
  @IsOptional()
  public code?: string;
  @IsOptional()
  public commissions?: Partial<Commission>[];
  @IsOptional()
  public company?: Partial<Company>;
  @IsOptional()
  public contact?: Partial<ContactInfo>;
  @IsOptional()
  public createdAt?: Date;
  @IsOptional()
  public createdBy?: Partial<User>;
  @IsOptional()
  public email?: string;
  @IsOptional()
  public name?: string;
  @IsOptional()
  public phone?: string;
  @IsOptional()
  public type?: string;
  @IsOptional()
  public updatedAt?: Date;
  @IsOptional()
  public updatedBy?: Partial<User>;

  // #endregion Properties (13)
}
