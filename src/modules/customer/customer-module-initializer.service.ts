import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class CustomerModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Customer Module) is initialized...');
  }
}
