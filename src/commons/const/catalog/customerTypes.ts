export const CUSTOMER_TYPES = [
  {
    id: 'BUSINESS',
    name: 'Business',
  },
  {
    id: 'INDIVIDUAL',
    name: 'Individual',
  },
];
