import { PartialType } from "@nestjs/mapped-types";
import { IsOptional } from "class-validator";
import { CreateCertificateDto } from "./create-certificate.dto";

export class UpdateCertificateDto extends PartialType(CreateCertificateDto) {
    // #region Properties (2)

    @IsOptional()
    public _id?: string;
    @IsOptional()
    public id?: string;

    // #endregion Properties (2)
}
