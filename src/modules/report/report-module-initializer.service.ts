import { Injectable, OnModuleInit } from '@nestjs/common';

@Injectable()
export class ReportModuleInitializerService implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Report Module) is initialized...');
  }
}
