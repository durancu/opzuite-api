import { Inject, Injectable } from '@nestjs/common';
import { ConfigType } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UpdateUserDto } from 'src/dto/user/update-user.dto';
import jwtConfig from '../../../config/jwt.config';
import { JwtPayload } from '../interface/jwt-payload.interface';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(@Inject(jwtConfig.KEY) config: ConfigType<typeof jwtConfig>) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: true,
      secretOrKey: config.secretKey,
    });
  }

  //payload is the decoded jwt clmais.
  validate(payload: JwtPayload): UpdateUserDto {

    return {
      email: payload.upn,
      username: payload.username,
      id: payload.sub,
      firstName: payload.firstName,
      lastName: payload.lastName,
      mobilePhone: payload.phone,
      company: payload.company,
      location: payload.location,
      permissions: payload.permissions,
      country: payload.country,
      preferences: payload.preferences, 
      role: payload.role,
      type: payload.type,
      authorizedIpAddresses: payload.authorizedIpAddresses,
      status: payload.status
    };
  }
}