import { Acord101PDFTemplate } from "./acord-101-pdf-template";


export class Acord101PdfTemplateV2008 extends Acord101PDFTemplate {
    // #region Constructors (1)

    constructor() {
        super();
        this.fieldPrefix = 'F[0].P1[0].';
        this.fieldSuffix = '[0]';
    }

    // #endregion Constructors (1)
}