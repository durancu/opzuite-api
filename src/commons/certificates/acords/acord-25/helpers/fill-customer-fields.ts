import { Certificate } from "src/database/entities/certificate.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillInsuredFields = (certificate: Partial<Certificate>, template: Acord25PdfTemplateV2014NowCerts) => {
    const customer = certificate.customer;
    const businessAddress = customer?.business?.address;

    if (customer) {
        template.setField('NamedInsured_FullName', customer.name || '');

        if (businessAddress) {
            template.setField('NamedInsured_MailingAddress_LineOne', businessAddress.address1);
            template.setField('NamedInsured_MailingAddress_LineTwo', businessAddress.address2);
            template.setField('NamedInsured_MailingAddress_CityName', businessAddress.city);
            template.setField('NamedInsured_MailingAddress_StateOrProvinceCode', businessAddress.state);
            template.setField('NamedInsured_MailingAddress_PostalCode', businessAddress.zip);
        }
    }

}