import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { nanoid } from 'nanoid';

@Schema()
export class Tag extends Document {
  // #region Properties (3)

  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop()
  public text: string;
  // #endregion Properties (3)
}

export const TagSchema = SchemaFactory.createForClass(Tag);