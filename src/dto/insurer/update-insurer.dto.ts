import { PartialType } from '@nestjs/mapped-types';
import { IsOptional } from 'class-validator';
import { CreateInsurerDto } from './create-insurer.dto';

export class UpdateInsurerDto extends PartialType(CreateInsurerDto) {
  // #region Properties (2)

  @IsOptional()
  public _id?: string;
  @IsOptional()
  public id?: string;

  // #endregion Properties (2)
}