import {
  Body,
  Controller, Delete, Get, Post,
  Put, Res,
  UploadedFile,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { User } from 'src/database/entities/user.schema';
import { UpdatePasswordDto } from 'src/dto/user/update-password.dto';
import { UpdateProfileDto } from 'src/dto/user/update-profile.dto';
import { UpdateUserPreferencesDto } from 'src/dto/user/update-user-preferences.dto';
import { CurrentUser } from 'src/modules/auth/decorators/current-user.decorator';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { FileStorageDto } from '../file/dto/file.dto';
import { UserPopulateOptions, UserService } from './user.service';


@ApiTags('profile')
@ApiBearerAuth()
@UseGuards(JwtAuthGuard)
@Controller('profile')
export class ProfileController {
  constructor(private userService: UserService) { }


  @Get()
  @ApiOkResponse({ description: 'Returns the user profile.' })
  async getProfile(
    @CurrentUser() authUser: User,
  ): Promise<Partial<User>> {
    const populateOptions: UserPopulateOptions = { withPassword: false, withCompany: true }
    const profilePhoto: string = await this.userService.getProfilePhoto(authUser);
    const userWithoutPhoto: Partial<User> = await this.userService.findById(authUser.id, populateOptions);
    const user = userWithoutPhoto.toJSON();
    user.profilePhoto = profilePhoto;
    return { ...user };
  }

  @Put('preferences')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Update user preferences' })
  async updatePreferences(
    @Body() updateUserPreferencesDto: UpdateUserPreferencesDto,
    @CurrentUser() user: User
  ): Promise<User> {
    return await this.userService.updateUserPreferences(
      user, updateUserPreferencesDto);
  }

  @Put('')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Update user profile' })
  async update(
    @CurrentUser() user: User,
    @Body() updateProfileDto: UpdateProfileDto,
  ): Promise<User> {
    return await this.userService.updateUserProfile(
      user, updateProfileDto);
  }

  @Put('password')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Update user password' })
  async updatePassword(
    @CurrentUser() user: User,
    @Body() updatePasswordDto: UpdatePasswordDto,
  ): Promise<User> {
    return await this.userService.updateUserPassword(
      user, updatePasswordDto);
  }

  @ApiConsumes('multipart/form-data')
  @ApiBody({ type: FileStorageDto })
  @Post('photo')
  @UseGuards(JwtAuthGuard)
  @UseInterceptors(FileInterceptor('file'))
  @ApiOperation({ summary: 'Upload user profile photo' })
  async uploadProfilePhoto(
    @CurrentUser() user: User,
    @UploadedFile() file: Express.Multer.File,
  ) {
    return await this.userService.uploadProfilePhoto(file, user);
  }

  @Get('photo')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Get user profile photo' })
  async getFile(
    @CurrentUser() user: User,
    @Res() res: Response
  ) {
    const profilePhoto: any = await this.userService.getProfilePhoto(user);
    return res.status(200).json(profilePhoto);
  }

  @Delete('photo')
  @UseGuards(JwtAuthGuard)
  @ApiOperation({ summary: 'Delete user profile photo' })
  async deleteFile(
    @CurrentUser() user: User
  ) {
    return await this.userService.deleteProfilePhoto(user);
  }

}
