import { CertificatePdfTemplate } from "../../../certificate-pdf-template";
import { acord101GenericFields } from "./acord-101-pdf-generic-fields";

export class Acord101PDFTemplate extends CertificatePdfTemplate {
    // #region Constructors (1)

    constructor() {
        super();
        this.fieldNames = acord101GenericFields;
    }

    // #endregion Constructors (1)
}