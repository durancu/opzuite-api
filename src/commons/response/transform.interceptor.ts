import {
  CallHandler, ExecutionContext, Injectable,
  NestInterceptor
} from '@nestjs/common';

  import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { ApiResponse } from './api-response.model';
  
  export interface Response<ApiResponse> {
    message: string;
    data: ApiResponse;
  }
  
  @Injectable()
  export class TransformInterceptor<T>
    implements NestInterceptor<ApiResponse, Response<ApiResponse>> {
    intercept(
      context: ExecutionContext,
      next: CallHandler,
    ): Observable<Response<ApiResponse>> {
      return next
        .handle()
        .pipe(
          map((data) => ({
            message: data.message,
            data: data.data,
            meta: {}
          })),
        );
    }
  }