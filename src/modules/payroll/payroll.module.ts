import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Company, CompanySchema } from 'src/database/entities/company.schema';
import { Endorsement, EndorsementSchema } from 'src/database/entities/endorsement.schema';
import { Location, LocationSchema } from 'src/database/entities/location.schema';
import { Payroll, PayrollSchema } from 'src/database/entities/payroll.schema';
import { Role, RoleSchema } from 'src/database/entities/role.schema';
import { Sale, SaleSchema } from 'src/database/entities/sale.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { UserService } from 'src/modules/user/user.service';
import { LocationModule } from '../location/location.module';
import { MetricsService } from '../sale/metrics.service';
import { SaleModule } from '../sale/sale.module';
import { UserModule } from '../user/user.module';
import { PayrollModuleInitializerService } from './payroll-module-initializer.service';
import { PayrollController } from './payroll.controller';
import { PayrollService } from './payroll.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Payroll.name, schema: PayrollSchema },
      { name: Location.name, schema: LocationSchema },
      { name: User.name, schema: UserSchema },
      { name: Sale.name, schema: SaleSchema },
      { name: Endorsement.name, schema: EndorsementSchema },
      { name: Role.name, schema: RoleSchema },
      { name: Company.name, schema: CompanySchema },
    ]),
    LocationModule,
    SaleModule,
    UserModule,
  ],
  controllers: [
    PayrollController
  ],
  providers: [
    PayrollService,
    MetricsService,
    UserService,
    PayrollModuleInitializerService,
  ],
  exports: [
    PayrollService,
  ]
})
export class PayrollModule { }
