import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { Role } from '../role.schema';
import { User, UserSchema } from '../user.schema';
import { CompanyDefaultEmailSettings, CompanyDefaultEmailSettingsSchema } from './company-default-email-settings.schema';

@Schema()
export class CompanyDefaults extends Document {
  // #region Properties (6)
  @Prop({ type: SchemaTypes.Map })
  public dashboard: any;
  @Prop({ type: CompanyDefaultEmailSettingsSchema })
  public email: CompanyDefaultEmailSettings;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Role', required: true })
  public role: Role;

  // #endregion Properties (6)
}

export const CompanyDefaultsSchema = SchemaFactory.createForClass(CompanyDefaults);