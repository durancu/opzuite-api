import { IsNotEmpty, IsOptional } from 'class-validator';
import { PayStub } from 'src/database/entities/nested/pay-stub.schema';
export class UpdatePayrollDto {
  @IsOptional()
  @IsNotEmpty()
  payPeriodEndedAt?: Date;

  @IsOptional()
  @IsNotEmpty()
  payPeriodStartedAt?: Date;

  @IsOptional()
  @IsNotEmpty()
  periodCode?: string;

  @IsOptional()
  @IsNotEmpty()
  scope?: string; //can be Company (C), Payroll (L), Individual (I)

  @IsOptional()
  payStubs?: PayStub[];
}
