import { COVERAGES_TYPES } from "src/commons/const/catalog/coverages-types";

export const buildSearchAggregator = (conditions: any) => {
    const aggregator = [];

    aggregator.push(
        ...buildSearchUnwindReferenceFieldsAggregator()
    );

    if (conditions) {
        aggregator.push({ $match: conditions });
    }

    aggregator.push(
        {
            $project: {
                id: '$_id',
                code: '$code',
                createdAt: '$createdAt',
                customerName: '$customer.name',
                customerId: '$customer._id',
                customerCode: '$customer.code',
                usDOT: {
                    $cond: [
                        { $eq: ['$customer.type', 'BUSINESS'] },
                        '$customer.business.usDOT',
                        '',
                    ],
                },
                carriers: '$carriers',
                brokers: '$brokers',
                items: {
                    $map: {
                        input: {
                            $filter: {
                                input: '$items',
                                as: 'item',
                                cond: { $not: ['$$item.deleted'] },
                            },
                        },
                        as: 'item',
                        in: {
                            amount: '$$item.amount',
                            brokerName: {
                                $let: {
                                    vars: {
                                        broker: {
                                            $arrayElemAt: [
                                                {
                                                    $filter: {
                                                        input: '$brokers',
                                                        as: 'broker',
                                                        cond: { $eq: ['$$broker._id', '$$item.broker'] },
                                                    },
                                                },
                                                0,
                                            ],
                                        },
                                    },
                                    in: { $ifNull: ['$$broker.name', ''] },
                                },
                            },
                            brokerCode: {
                                $let: {
                                    vars: {
                                        broker: {
                                            $arrayElemAt: [
                                                {
                                                    $filter: {
                                                        input: '$brokers',
                                                        as: 'broker',
                                                        cond: { $eq: ['$$broker._id', '$$item.broker'] },
                                                    },
                                                },
                                                0,
                                            ],
                                        },
                                    },
                                    in: { $ifNull: ['$$broker.code', ''] },
                                },
                            },
                            carrierName: {
                                $let: {
                                    vars: {
                                        carrier: {
                                            $arrayElemAt: [
                                                {
                                                    $filter: {
                                                        input: '$carriers',
                                                        as: 'carrier',
                                                        cond: { $eq: ['$$carrier._id', '$$item.carrier'] },
                                                    },
                                                },
                                                0,
                                            ],
                                        },
                                    },
                                    in: { $ifNull: ['$$carrier.name', ''] },
                                },
                            },
                            carrierCode: {
                                $let: {
                                    vars: {
                                        carrier: {
                                            $arrayElemAt: [
                                                {
                                                    $filter: {
                                                        input: '$carriers',
                                                        as: 'carrier',
                                                        cond: { $eq: ['$$carrier._id', '$$item.carrier'] },
                                                    },
                                                },
                                                0,
                                            ],
                                        },
                                    },
                                    in: { $ifNull: ['$$carrier.code', ''] },
                                },
                            },
                            description: '$$item.description',
                            effectiveAt: '$$item.effectiveAt',
                            expiresAt: '$$item.expiresAt',
                            name: '$$item.name',
                            number: '$$item.number',
                            premium: '$$item.premium',
                            product: {
                                $arrayElemAt: [
                                    {
                                        $filter: {
                                            input: COVERAGES_TYPES,
                                            as: 'type',
                                            cond: { $eq: ['$$type.id', '$$item.product'] },
                                        },
                                    },
                                    0,
                                ],
                            },
                            status: '$$item.status',
                        },
                    },
                },
                deleted: '$deleted',
                locationName: { $ifNull: ['$location.business.name', ''] },
                locationCode: { $ifNull: ['$location.code', ''] },
                sellerName: { $ifNull: ['$seller.name', ''] },
                sellerCode: { $ifNull: ['$seller.code', ''] },
                number: '$number',
                effectiveAt: '$effectiveAt',
                expiresAt: '$expiresAt',
                totalPremium: { $round: ['$totalPremium', 2] },
                products: '$products',
                seller: { $ifNull: ['$seller._id', ''] },
                soldAt: '$soldAt',
                status: '$status',
                type: '$type',
            },
        }
    );

    aggregator.push({
        $unset: ['carriers', 'brokers'],
    });

    return aggregator;
}

export function buildSearchUnwindReferenceFieldsAggregator() {
    return [
        {
            $unwind: {
                path: '$seller',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: 'users',
                localField: 'seller',
                foreignField: '_id',
                as: 'seller',
            },
        },
        {
            $unwind: {
                path: '$seller',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $unwind: {
                path: '$customer',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: 'customers',
                localField: 'customer',
                foreignField: '_id',
                as: 'customer',
            },
        },
        {
            $unwind: {
                path: '$customer',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $unwind: {
                path: '$location',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: 'locations',
                localField: 'location',
                foreignField: '_id',
                as: 'location',
            },
        },
        {
            $unwind: {
                path: '$location',
                preserveNullAndEmptyArrays: true,
            },
        },
        {
            $lookup: {
                from: 'insurers',
                localField: 'items.carrier',
                foreignField: '_id',
                as: 'carriers',
            },
        },
        {
            $lookup: {
                from: 'insurers',
                localField: 'items.broker',
                foreignField: '_id',
                as: 'brokers',
            },
        },
        
    ];
}