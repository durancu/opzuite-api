import { Model, Schema, SchemaTypes } from "mongoose";

interface PremiumByCarrier extends DataView {
  readonly id: any;
  totalPremium: number;
}

type PremiumByCarrierModel = Model<PremiumByCarrier>;

const PremiumByCarrierSchema = new Schema<any>(
  {
    _id: { type: SchemaTypes.Mixed},
    totalPremium: { type: SchemaTypes.Number, default: 0, required: false },
  }
);

export { PremiumByCarrier, PremiumByCarrierSchema, PremiumByCarrierModel };
