import { Controller, HttpCode, Post, Req, Response, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { Company } from 'src/database/entities/company.schema';
import { User } from 'src/database/entities/user.schema';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { CatalogService } from './catalog.service';

@ApiTags('Catalog')
@Controller('catalogs')
export class CatalogController {
    // #region Constructors (1)

    constructor(
        private catalogService: CatalogService,
    ) { }

    // #endregion Constructors (1)

    // #region Public Methods (1)

    @Post('search')
    @HttpCode(200)
    @UseGuards(JwtAuthGuard)
    public async getCatalog(@Req() req: Request, @Response() res: any): Promise<any> {
        const user: Partial<User> = req.user;
        const company: Partial<Company> = user.company;

        const catalog: any = await this.catalogService.getCatalog(company);
        return res.json(catalog);
    }

    // #endregion Public Methods (1)
}
