import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp } from 'lodash';
import { Model, Types } from 'mongoose';
import { COMPANY } from 'src/commons/const/project.constants';
import { SaleCommissionsCalculator } from 'src/commons/vl17-specific/sale-commissions-calculator';
import { Location } from 'src/database/entities/location.schema';
import { PayStub } from 'src/database/entities/nested/pay-stub.schema';
import { Payroll } from 'src/database/entities/payroll.schema';
import { User } from 'src/database/entities/user.schema';
import { CreatePayrollDto } from 'src/dto/payroll/create-payroll.dto';
import { PayrollDto } from 'src/dto/payroll/payroll.dto';
import { UpdatePayrollDto } from 'src/dto/payroll/update-payroll.dto';
import { InitPayrollDto } from '../../dto/payroll/init-payroll.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { MetricsService } from '../sale/metrics.service';
import {
  calculateTotalNetSalary,
  generateDefaultPayStubs,
  getLastPayPeriod,
  getPayDayPeriods,
  runPayrollCalculations
} from './payroll.utils';

@Injectable({ scope: Scope.REQUEST })
export class PayrollService {
  // #region Properties (2)

  // #endregion Properties (2)

  // #region Constructors (1)

  constructor(
    @InjectModel(Payroll.name) private payrollModel: Model<Payroll>,
    @InjectModel(Location.name) private locationModel: Model<Location>,
    @InjectModel(User.name) private userModel: Model<User>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
    private metricsService: MetricsService,
  ) { }


  public async generatePayroll(force: boolean, all: boolean): Promise<any> {
    const locations: Location[] = await this.locationModel.find({});

    if (locations) {
      return locations.map(async (location: Location) => {
        const initialPayroll: InitPayrollDto[] =
          await this.getAvailablePayPeriod(location);

        let payrollDto: PayrollDto;
        const payrolls: any[] = [];

        if (initialPayroll.length > 0) {
          if (all) {
            payrollDto = await this.initPayroll(
              initialPayroll[0],
            );
            payrolls.push(await this.save(payrollDto as CreatePayrollDto));
          } else {
            initialPayroll.map(async (payroll: any) => {
              payrollDto = await this.initPayroll(
                initialPayroll[0],
              );
              payrolls.push(await this.save(payrollDto as CreatePayrollDto));
            });
          }
          return payrolls;
        }
      });
    }
    return [];
  }

  // #endregion Constructors (1)

  // #region Public Methods (13)

  /**
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.payrollModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @returns Promise
   */
  public async deleteAll(): Promise<any> {
    return await this.payrollModel.deleteMany({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async deleteByCode(code: string): Promise<Payroll> {
    const payroll: Payroll = await this.payrollModel
      .findOne({ code: code })
      .exec();

    if (!payroll) {
      throw new NotFoundException(`Payroll (code: ${code}) not found.`);
    }

    return await this.payrollModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @param  {InitPayrollDto} initPayroll
   * @returns Promise
   */
  public async exists(initPayroll: InitPayrollDto): Promise<Payroll> {
    const payroll: Payroll = await this.payrollModel
      .findOne({
        location: initPayroll.location,
        payPeriodStartedAt: initPayroll.payPeriodStartedAt,
        payPeriodEndedAt: initPayroll.payPeriodEndedAt,
      })
      .exec();

    return payroll;
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.payrollModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<Payroll> {
    const payroll: Payroll = await this.payrollModel
      .findOne({ code: code })
      .exec();

    if (!payroll) {
      throw new NotFoundException(`Payroll (code: ${code}) not found.`);
    }

    return payroll;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<Payroll> {
    const payroll: Payroll = await this.payrollModel
      .findOne({ _id: id })
      .exec();

    if (!payroll) {
      throw new NotFoundException(`Payroll (id: ${id}) was not found`);
    }

    return payroll;
  }

  /*
   * Get Available Pay Period for next Payroll
   */
  public async getAvailablePayPeriod(
    location: Partial<Location> = null,
  ): Promise<InitPayrollDto[]> {
    const payPeriod = getLastPayPeriod(COMPANY.payrollDay);

    const payrollDto: InitPayrollDto = {
      payPeriodStartedAt: payPeriod.start,
      payPeriodEndedAt: payPeriod.end,
      periodCode: payPeriod.code,
      location: location,
    };

    const existentPayroll = await this.exists(payrollDto);

    return !existentPayroll ? [payrollDto] : [];
  }

  /*
   * Get Available Pay Period for next Payroll
   */
  public async getAvailablePayPeriods(): Promise<any[]> {
    return getPayDayPeriods(COMPANY.payrollDay, 6);
  }

  /*
   * Generate initial Payroll with Sales bonus calculated but without: regular salary, extra-bonus, discounts, reimbursements,
   * to be set up in frontend by payroll creator user.
   */
  public async initPayroll(initPayroll: InitPayrollDto): Promise<PayrollDto> {
    // initPayroll contains location and date range
    let payrollDto: PayrollDto = {
      company: this.req.user.company,
      ...initPayroll,
    };

    const users: Partial<User>[] = await this.userModel.find({ location: initPayroll.location }).exec();

    // generate initial paystubs by employee with metrics set to zero (0)
    payrollDto.payStubs = await generateDefaultPayStubs(
      payrollDto,
      users
    );

    // calculate sale metrics by employee (policy and permit sales, etc)
    payrollDto = await runPayrollCalculations(
      payrollDto,
      this.metricsService,
    );

    const stubs = [...payrollDto.payStubs];

    const st: PayStub[] = stubs.map((stub: any) => {
      const commission = SaleCommissionsCalculator.calculateEmployeeCommissions(
        stub,
        payrollDto,
      );
      stub.totalSaleBonus = commission;
      stub.totalNetSalary = calculateTotalNetSalary(stub);
      return stub;
    });

    payrollDto.payStubs = st;

    payrollDto.totalSaleBonus = payrollDto.payStubs.reduce(
      (accumulator: any, { totalSaleBonus }: PayStub) =>
        accumulator + totalSaleBonus,
      0,
    );

    payrollDto.totalNetSalary = payrollDto.payStubs.reduce(
      (accumulator: any, { totalNetSalary }: PayStub) =>
        accumulator + totalNetSalary,
      0,
    );

    return payrollDto;
  }

  /**
   * @param  {CreatePayrollDto} payrollDto
   * @returns Promise
   */
  public async save(payroll: CreatePayrollDto): Promise<PayrollDto> {
    const location: Partial<Location> = await this.locationModel
      .findOne({ _id: payroll.location })
      .exec();
    let payrollDto: PayrollDto = {
      company: this.req.user.company,
      country: location.country,
      ...payroll,
    };

    // calculate sale metrics by employee (policy and permit sales, etc)
    payrollDto = await runPayrollCalculations(
      payrollDto,
      this.metricsService,
    );

    const stubs = [...payrollDto.payStubs];

    const st: PayStub[] = stubs.map((stub: any) => {
      const commission = SaleCommissionsCalculator.calculateEmployeeCommissions(
        stub,
        payrollDto,
      );
      stub.totalSaleBonus = commission;
      stub.totalNetSalary = calculateTotalNetSalary(stub);
      return stub;
    });

    payrollDto.payStubs = st;

    payrollDto.totalSaleBonus = payrollDto.payStubs.reduce(
      (accumulator: any, { totalSaleBonus }: PayStub) =>
        accumulator + totalSaleBonus,
      0,
    );

    payrollDto.totalNetSalary = payrollDto.payStubs.reduce(
      (accumulator: any, { totalNetSalary }: PayStub) =>
        accumulator + totalNetSalary,
      0,
    );

    return await this.payrollModel.create({
      ...payrollDto,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    });
  }

  public async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;

    const limitCriteria = queryParams.pageSize;

    let location = null;

    if ('location' in queryParams.filter) {
      location = queryParams.filter.location;
      delete queryParams.filter['location'];
    }

    let country = null;

    if ('country' in queryParams.filter) {
      country = queryParams.filter.country;
      delete queryParams.filter['country'];
    }

    let conditions = {};
    const fixedQueries = [];
    let filterQueries = [];

    conditions = {
      $and: [{ deleted: false }],
    };
    if (
      location || country ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      if (location) {
        conditions['$and'].push({ location: new Types.ObjectId(location) });
      }

      if (country) {
        conditions['$and'].push({ country: country });
      }

      if (queryParams.filter && Object.keys(queryParams.filter).length > 0) {
        filterQueries = Object.keys(queryParams.filter).map((key) => {
          return {
            [key]: {
              $regex: new RegExp('.*' + escapeRegExp(queryParams.filter[key]) + '.*', 'i'),
            },
          };
        });
      }
    }

    if (filterQueries.length || fixedQueries.length) {
      conditions['$or'] = [...filterQueries, ...fixedQueries];
    }

    const query = this.payrollModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'locations',
        localField: 'location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true });

    query.append({
      $project: {
        id: '$_id',
        payPeriodStartedAt: '$payPeriodStartedAt',
        payPeriodEndedAt: '$payPeriodEndedAt',
        locationName: { $ifNull: ['$location.business.name', ''] },
        code: '$code',
        status: '$status',
        totalSaleBonus: '$totalSaleBonus',
        totalNetSalary: '$totalNetSalary',
        createdAt: '$createdAt',
        scope: '$scope',
      }
    });

    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.skip(skipCriteria).limit(limitCriteria).sort(sortCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  /**
   * @param  {string} code
   * @param  {UpdatePayrollDto} data
   * @returns Promise
   */
  public async update(code: string, payroll: UpdatePayrollDto): Promise<Payroll> {
    const payrollFound: Payroll = await this.payrollModel
      .findOne({ code: code })
      .exec();

    if (!payrollFound) {
      throw new NotFoundException(`Payroll (code: ${code}) not found.`);
    }

    let payrollDto: PayrollDto = {
      company: this.req.user.company,
      ...payroll,
    };

    // calculate sale metrics by employee (policy and permit sales, etc)
    payrollDto = await runPayrollCalculations(
      payrollDto,
      this.metricsService,
    );

    const stubs = [...payrollDto.payStubs];

    const st: PayStub[] = stubs.map((stub: any) => {
      const commission = SaleCommissionsCalculator.calculateEmployeeCommissions(
        stub,
        payrollDto,
      );
      stub.totalSaleBonus = commission;
      stub.totalNetSalary = calculateTotalNetSalary(stub);
      return stub;
    });

    payrollDto.payStubs = st;

    payrollDto.totalSaleBonus = payrollDto.payStubs.reduce(
      (accumulator: any, { totalSaleBonus }: PayStub) =>
        accumulator + totalSaleBonus,
      0,
    );

    payrollDto.totalNetSalary = payrollDto.payStubs.reduce(
      (accumulator: any, { totalNetSalary }: PayStub) =>
        accumulator + totalNetSalary,
      0,
    );

    const payrollData: PayrollDto = {
      ...payrollFound['_doc'],
      ...payrollDto,
      updatedBy: this.req.user.id,
    };

    return await this.payrollModel.findOneAndUpdate(
      { _id: new Types.ObjectId(payrollFound._id) },
      payrollData,
      { new: true },
    );
  }

  // #endregion Public Methods (13)
}
