/* eslint-disable @typescript-eslint/no-explicit-any */
import {
  Body,
  Controller,
  HttpCode,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { EMPTY, from, Observable, of } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { User } from 'src/database/entities/user.schema';
import { ForgotPasswordRequest } from '../../dto/auth/forgot-password.request';
import { ResetPasswordRequest } from '../../dto/auth/reset-password.request';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { LocalAuthGuard } from './guard/local-auth.guard';
import { AuthenticatedRequest } from './interface/authenticated-request.interface';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('login')
  @ApiBody({
    type: LoginDto,
  })
  login(
    @Req() req: AuthenticatedRequest,
    @Res() res: Response,
  ): Observable<Response> {
    return this.authService.login(req.user).pipe(
      map((token) => {
        return res.json(token).send();
      }),
    );
  }

  //@UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('confirm-email')
  async confirmUserEmail(
    @Body() payload: any,
    @Res() res: Response,
  ): Promise<any> {
    const { confirmToken } = payload;

    return from(this.authService.confirmEmailAddressByToken(confirmToken)).pipe(
      mergeMap((p) => (p ? of(p) : EMPTY)),
      map((user) => res.json(user.passwordToken).send()),
    );
  }

  //@UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('forgot-password')
  async forgotPassword(
    @Body() request: ForgotPasswordRequest,
    @Res() res: Response,
  ): Promise<any> {
    let { email } = request;

    email = email.toLowerCase().trim();

    const reset: User = await this.authService.forgotPassword(email);
    return res.json(reset !== null).send();
  }

  //@UseGuards(LocalAuthGuard)
  @HttpCode(200)
  @Post('reset-password')
  async resetPassword(
    @Body() request: ResetPasswordRequest,
    @Res() res: Response,
  ): Promise<any> {
    const { token, password } = request;

    return from(this.authService.resetPassword(token, password)).pipe(
      mergeMap((p) => (p ? of(p) : EMPTY)),
      map((user) => {
        return res.json(user).send();
      }),
    );
  }
}
