import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @ApiProperty({ example: 'john.lee@inzuite.com', required: true })
  @IsString()
  @IsNotEmpty()
  email: string;

  @ApiProperty({ example: 'Manager23*', required: true })
  @IsString()
  @IsNotEmpty()
  password: string;
}
