import { IsNotEmpty } from "class-validator";

export class ForgotPasswordRequest {
    // #region Properties (1)

    @IsNotEmpty()
    public email?: string;

    // #endregion Properties (1)
}