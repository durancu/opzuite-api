
import { lowerCase } from "lodash";
import { Types } from "mongoose";
import { SALE_FIELD_CUSTOMER, SALE_FIELD_LOCATION, SALE_FIELD_SELLER } from "src/commons/const/project.constants";
import { GroupingCriteria } from "src/commons/enum/grouping-criteria.enum";
import { MetricsCalMethod } from "src/commons/enum/metrics-calc-method.enum";
import { createDateRangeFilterExpressions } from "src/commons/lib/aggregator-functions";
import { MetricsParamsFilterDto } from "src/dto/metrics/metrics-params-filter.dto";
import { MetricsParamsDto } from "src/dto/metrics/metrics-params.dto";

export async function calculateSaleMetricsByEndorsements(params: MetricsParamsDto): Promise<any> {
    let conditions = {};
    conditions = {
      $and: [{ deleted: false }],
    };

    if (params.dateFrom || params.dateTo) {
      conditions['$and'].push({
        endorsedAt: createDateRangeFilterExpressions(
          params.dateFrom,
          params.dateTo,
        ),
      });
    }

    const filters: MetricsParamsFilterDto[] = params.filters || [];

    filters.forEach(({ field, value }: MetricsParamsFilterDto) => {
      const filterExpression = {};
      if (
        [SALE_FIELD_SELLER, SALE_FIELD_CUSTOMER, SALE_FIELD_LOCATION].includes(
          lowerCase(field),
        )
      ) {
        //TODO: evaluate operator to set the proper expression, only using EQUAL for now
        filterExpression[`${field}._id`] = new Types.ObjectId(value);
      } else if (['broker', 'carrier'].includes(lowerCase(field))) {
        filterExpression[`items.${field}._id`] = new Types.ObjectId(value);
      } else {
        filterExpression['sale.' + field] = value;
      }
      conditions[`$and`].push(filterExpression);
    });

    const groupingId = this.setGroupingIdExpression(
      params.groupByModel,
      params.groupByFields,
      MetricsCalMethod.ENDORSEMENTS
    );

    const query = this.endorsementModel.aggregate();

    query
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'sales',
        localField: 'sale',
        foreignField: '_id',
        as: 'sale',
      })
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .addFields({
        followUpPerson: { $ifNull: ['$followUpPerson', '$sale.seller'] },
      })
      .addFields({
        seller: { $ifNull: ['$seller', '$sale.seller'] },
      })
      .addFields({
        endorsedAt: { $ifNull: ['$endorsedAt', '$sale.soldAt'] },
      });

    if (
      params.groupByModel === GroupingCriteria.BROKER ||
      params.groupByModel === GroupingCriteria.CARRIER ||
      params.filters.find(
        (param: MetricsParamsFilterDto) =>
          param.field === GroupingCriteria.BROKER.toString() ||
          param.field === GroupingCriteria.CARRIER.toString(),
      )
    ) {
      query
        .unwind({ path: '$sale.items', preserveNullAndEmptyArrays: true })
        .lookup({
          from: 'insurers',
          localField: 'sales.items.carrier',
          foreignField: '_id',
          as: 'items.carrier',
        })
        .unwind({ path: '$items.carrier', preserveNullAndEmptyArrays: true })
        .lookup({
          from: 'insurers',
          localField: 'sales.items.broker',
          foreignField: '_id',
          as: 'items.broker',
        })
        .unwind({ path: '$items.broker', preserveNullAndEmptyArrays: true });
    }

    query
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'users',
        localField: 'seller',
        foreignField: '_id',
        as: 'seller',
      })
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false });

    query
      .unwind({ path: '$sale.location', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'locations',
        localField: 'sale.location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: false })

      .unwind({ path: '$sale.customer', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'customers',
        localField: 'sale.customer',
        foreignField: '_id',
        as: 'customer',
      })
      .unwind({ path: '$customer', preserveNullAndEmptyArrays: false });

    if (conditions) {
      query.match(conditions);
    }

    let groupExpression = {};
    (params.fields || []).forEach((field) => {
      const fieldName =
        field.trim() === 'items.premium'
          ? 'totalPremium'
          : field.trim() === 'items.amount'
            ? 'totalReceivables'
            : field.trim();
      return (groupExpression[''.concat(fieldName)] = {
        $sum: '$'.concat(field),
      });
    });

    groupExpression['_id'] = groupingId;
    if (params.withCount) {
      groupExpression['count'] = { $sum: 1 };
    }

    groupExpression = {
      ...groupExpression,
      ...{
        seller: { $first: '$seller.name' },
        location: { $first: '$location.business.name' },
        role: { $first: '$seller.role' },
        employeeCategory: { $first: '$seller.employeeInfo.category' },
        country: { $first: '$seller.country' },
        customer: { $first: '$customer.name' },
        totalPremium: {
          $sum: '$totalPremium',
        },
        totalAgentCommission: {
          $sum: '$totalAgentCommission',
        },
        totalTaxesAndFees: {
          $sum: '$totalTaxesAndFees',
        },
        totalNonPremium: {
          $sum: '$totalNonPremium',
        },
        totalAgencyCommission: {
          $sum: '$totalAgencyCommission',
        },
        year: { $first: { $year: '$sale.soldAt' } },
        month: { $first: { $month: '$sale.soldAt' } },
        day: { $first: { $dayOfMonth: '$sale.soldAt' } },
      },
    };

    query.group(groupExpression);

    let sortCriteria = {};

    if ([GroupingCriteria.YEAR.toString(), GroupingCriteria.MONTH.toString(), GroupingCriteria.DAY.toString()].includes(params.groupByModel)) {
      sortCriteria['year'] = -1;
      sortCriteria['month'] = -1;
      sortCriteria['day'] = -1;
    } else if (params.sortField && params.sortField.length > 0) {
      const order = params.sortOrder && params.sortOrder === 'asc' ? 1 : -1;
      sortCriteria = {};
      sortCriteria[params.sortField] = order;
    } else {
      sortCriteria['totalPremium'] = -1;
    }

    query.sort(sortCriteria);

    return query.exec();
  }