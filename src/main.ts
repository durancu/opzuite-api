import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import * as bodyParser from 'body-parser';
import { AppModule } from './app.module';
import { AllExceptionsFilter } from './commons/filter/all-exceptions.filter';

declare const module: any;

async function bootstrap() {

  const app = await NestFactory.create(AppModule, {
    cors: true,
    snapshot: true
  });
  app.setGlobalPrefix('api/v2');
  //app.useGlobalInterceptors(new NewrelicInterceptor());

  // enable shutdown hooks explicitly.
  app.enableShutdownHooks();

  app.useGlobalPipes(new ValidationPipe());
  app.useGlobalFilters(new AllExceptionsFilter());

  app.use(bodyParser.json({ limit: '50mb' }));
  app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  app.enableCors();
  const config = new DocumentBuilder()
    .setTitle('Inzuite API')
    .setDescription(
      'The project is a CRM for small insurance agencies which currently comprehends an app developed with MERN stack',
    )
    .setVersion('1.0')
    .addTag('API')
    .addBearerAuth()
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document, {
    swaggerOptions: { persistAuthorization: true },
  });
  const port: number = parseInt(`${process.env.PORT}`) || 3000;


  await app.listen(port);

  if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => app.close());
  }
}
bootstrap();
