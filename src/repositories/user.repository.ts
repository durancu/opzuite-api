import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User } from 'src/database/entities/user.schema';

export class UserRepository {
  // #region Constructors (1)

  constructor(@InjectModel(User.name) private readonly userModel: Model<User>) { }

  // #endregion Constructors (1)

  // #region Public Methods (6)

  //getUserWithRole
  public async findById(id: string, options?: any): Promise<User> {

    const query = this.userModel.findOne({ _id: id });

    if (options && options.populate) {
      const populate: any = options.populate;
      if (populate) {

        for (const field in populate) {
          if (field in populate) {
            if (populate[field].length) {
              query.populate(field, populate[field])
            } else {
              query.populate(field);
            }
          }
        }
      }
    }

    return await query.exec();
  }

  public async findAllActiveWithLocation(options?: any): Promise<User[]> {

    const query = this.userModel.find({
      $and: [{ deleted: false }, { location: { $exists: true } }],
    });

    if (options) {
      if ('populate' in options) {
        const { populate } = options;
        populate.map((field: any) => {
          if (populate['field'].length) {
            query.populate(field, populate['field']);
          } else {
            query.populate(field);
          }
        });
      }
    }

    return await query.exec();
  }





  // #endregion Public Methods (6)
}