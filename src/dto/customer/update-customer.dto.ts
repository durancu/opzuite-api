import { PartialType } from '@nestjs/mapped-types';
import { IsOptional } from 'class-validator';
import { User } from 'src/database/entities/user.schema';
import { CreateCustomerDto } from './create-customer.dto';
export class UpdateCustomerDto extends PartialType(CreateCustomerDto) {
  // #region Properties (4)

  @IsOptional()
  public _id?: string;
  @IsOptional()
  public code?: string;
  @IsOptional()
  public id?: string;
  @IsOptional()
  public updatedBy?: Partial<User>;

  // #endregion Properties (4)
}
