export const DEFAULT_EMAIL_SETTINGS = {
    emailNotification: true,
    sendCopyToPersonalEmail: false,
    activityRelatesEmail: {
        youHaveNewNotifications: false,
        youAreSentADirectMessage: false,
        locationTargetReached: false,
        newTeamMember: false,
        employeeTargetReached: true,
    },
}