import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Endorsement, EndorsementSchema } from 'src/database/entities/endorsement.schema';
import { Insurer, InsurerSchema } from 'src/database/entities/insurer.schema';
import { Report, ReportSchema } from 'src/database/entities/report.schema';
import { Sale, SaleSchema } from 'src/database/entities/sale.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { InsurerRepository } from 'src/repositories/insurer.repository';
import { ReportRepository } from 'src/repositories/report.repository';
import { SaleRepository } from 'src/repositories/sale.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { MetricsService } from '../sale/metrics.service';
import { SaleModule } from '../sale/sale.module';
import { SaleService } from '../sale/sale.service';
import { UserModule } from '../user/user.module';
import { ReportModuleInitializerService } from './report-module-initializer.service';
import { ReportController } from './report.controller';
import { ReportService } from './report.service';

@Module({
  imports: [
    UserModule,
    SaleModule,
    MongooseModule.forFeature([
      { name: Report.name, schema: ReportSchema },
      { name: Sale.name, schema: SaleSchema },
      { name: Endorsement.name, schema: EndorsementSchema },
      { name: User.name, schema: UserSchema },
      { name: Insurer.name, schema: InsurerSchema },
    ]),
  ],
  controllers: [
    ReportController
  ],
  providers: [
    ReportService,
    ReportRepository,
    MetricsService,
    UserRepository,
    SaleRepository,
    InsurerRepository,
    SaleService,
    ReportModuleInitializerService,
  ],
  exports: [
    ReportService,
    ReportRepository,
    MetricsService,
    SaleService,
    InsurerRepository
  ]
})
export class ReportModule { }
