import { Certificate } from "src/database/entities/certificate.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillCertificateGeneralFields = (certificate: Partial<Certificate>, template: Acord25PdfTemplateV2014NowCerts) => {
    template.setField('Form_CompletionDate', (new Date()).toLocaleDateString());
    template.setField('CertificateOfInsurance_CertificateNumberIdentifier', certificate.number);
    template.setField('CertificateOfInsurance_RevisionNumberIdentifier', certificate.revisionNumber);
}