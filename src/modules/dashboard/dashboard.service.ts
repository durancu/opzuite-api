import { Injectable, NotFoundException, Scope } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Chart } from 'src/database/entities/chart.schema';
import { Report } from 'src/database/entities/report.schema';
import { ReportParamsDto } from 'src/dto/report/report-params.dto';
import { ReportRepository } from 'src/repositories/report.repository';
import { ReportService } from '../report/report.service';

@Injectable({ scope: Scope.REQUEST })
export class DashboardService {
  constructor(
    @InjectModel(Chart.name) private chartModel: Model<Chart>,
    private readonly reportRepository: ReportRepository,
    private reportService: ReportService,
  ) {}

  async getChartByCode(
    chartCode: string,
    params: ReportParamsDto,
  ): Promise<Partial<Chart>> {
    const chart: Partial<Chart> = await this.chartModel
      .findOne({ code: chartCode })
      .exec();

    if (!chart) {
      throw new NotFoundException(`Chart ${chartCode} not found`);
    }

    try {
      const report: Partial<Report> = await this.reportRepository.findById(chart.report._id);

      if (!report) {
        throw new NotFoundException(`Chart report ${chart.report} not found`);
      }

      const dataset = await this.reportService.buildReportChartData(report.code, params);

      chart.dataset = dataset;
      return chart;
    } catch (e) {
      return e;
    }
  }
}
