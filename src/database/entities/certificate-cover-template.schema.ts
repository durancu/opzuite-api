//import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
//import * as patchHistory from 'mongoose-patch-history';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true }, discriminatorKey: 'kind' })
export class CertificateCoverTemplate extends Document {
    // #region Properties (9)

    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public company: Company;
    @Prop()
    public content: string;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public createdBy: User;
    @Prop()
    public description: string;
    @Prop()
    public name: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public updatedBy: User;

    // #endregion Properties (9)
}

export const CertificateCoverTemplateSchema = SchemaFactory.createForClass(CertificateCoverTemplate);

CertificateCoverTemplateSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
