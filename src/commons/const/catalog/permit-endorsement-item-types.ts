export const PERMIT_ENDORSEMENT_ITEM_TYPES = {
  PAYABLE: [
    { name: 'Down Payment', id: 'DOWN_PAYMENT' },
    { name: 'Paid in Full', id: 'PAID_IN_FULL' },
    { name: 'Installment', id: 'INSTALLMENT' },
    { name: 'Other', id: 'OTHER' },
  ],
  RECEIVABLE: [
    { name: 'Down Payment', id: 'DOWN_PAYMENT' },
    { name: 'Paid in Full', id: 'PAID_IN_FULL' },
    { name: 'Installment', id: 'INSTALLMENT' },
    { name: 'Other', id: 'OTHER' },
  ],
  FINANCING_DIRECT_BILL: [
    { name: 'Direct Bill', id: 'DIRECT_BILL' },
    { name: 'Inside Financing', id: 'INSIDE_FINANCING' },
    { name: 'Other', id: 'OTHER' },
  ],
  FEE_TAX: [
    { name: 'Agency Fee', id: 'AGENCY_FEE' },
    { name: 'Inspection Fee', id: 'INSPECTION_FEE' },
    { name: 'Processing Fee', id: 'PROCESSING_FEE' },
    { name: 'Other Fees', id: 'OTHER_FEES' },
    { name: 'Taxes', id: 'TAXES' },
    { name: 'State Tax', id: 'STATE_TAX' },
    { name: 'Stamping Fee', id: 'STAMPING_FEE' },
    { name: 'Other Tax', id: 'OTHER_TAX' },
    { name: 'Payment Processing Fee', id: 'PAYMENT_PROCESSING_FEE' },
    { name: 'Risk Management Fee', id: 'RISKMENT_FEE' },
    { name: 'Filing Fee', id: 'FILING_FEE' },
    { name: 'Other', id: 'OTHER' },
  ],
  AGENCY_COMMISSION: [
    { name: 'Gross', id: 'GROSS' },
    { name: 'Monthly - Net', id: 'MONTHLY_NET' },
    { name: 'Net', id: 'NET' },
    { name: 'Month Earned', id: 'MONTH_EARNED' },
  ],
  AGENT_COMMISSION: [
    { name: 'From Agency Commission', id: 'FROM_AGENCY_COMMISSION' },
    { name: 'From Base Premium', id: 'FROM_BASE_PREMIUM' },
    {
      name: 'From Finance Company Incentive',
      id: 'FROM_FINANCE_COMPANY_INCENTIVE',
    },
    { name: 'Fee', id: 'FEE' },
    { name: 'Tip', id: 'TIP' },
  ],
};
