import { Global, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import sendgridConfig from '../../config/sendgrid.config';
import { SendgridModuleInitializerService } from './sendgrid-module-initializer.service';
import { sendgridProviders } from './sendgrid.providers';
import { SendgridService } from './sendgrid.service';

@Global()
@Module({
  imports: [
    ConfigModule.forFeature(sendgridConfig),
  ],
  providers: [
    ...sendgridProviders,
    SendgridService,
    SendgridModuleInitializerService
  ],
  exports: [
    ...sendgridProviders,
    SendgridService
  ]
})
export class SendgridModule { }
