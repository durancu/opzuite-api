import { Insurer } from "src/database/entities/insurer.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillHolderFields = (holder: Insurer, template: Acord25PdfTemplateV2014NowCerts) => {
    if (holder) {
        template.setField('CertificateHolder_FullName', holder ? holder.name : '');

        if (holder.business && holder.business.address) {
            template.setField('CertificateHolder_MailingAddress_LineOne', holder.business.address?.address1);
            template.setField('CertificateHolder_MailingAddress_LineTwo', holder.business.address?.address2);
            template.setField('CertificateHolder_MailingAddress_CityName', holder.business.address?.city);
            template.setField('CertificateHolder_MailingAddress_StateOrProvinceCode', holder.business.address?.state);
            template.setField('CertificateHolder_MailingAddress_PostalCode', holder.business.address?.zip);
        }
    }
}