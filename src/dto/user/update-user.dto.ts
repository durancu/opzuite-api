import { PartialType } from '@nestjs/mapped-types';
import { IsNotEmpty, IsOptional, Matches, MaxLength, MinLength } from 'class-validator';
import { CreateUserDto } from './create-user.dto';

export class UpdateUserDto extends PartialType(CreateUserDto) {
    @IsOptional()
    _id?: string;
    @IsOptional()
    id?: string;
    @IsNotEmpty()
    @IsOptional()
    @MinLength(8, { message: ' The min length of password is 8 ' })
    @MaxLength(20, {
        message: " The password can't accept more than 20 characters ",
    })
    @Matches(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]).{8,20}$/, {
        message:
            ' A password at least contains one numeric digit, one supercase char and one lowercase char',
    })
    password?: string;
}
