import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class ActivityRelatesEmail extends Document {
  // #region Properties (5)

  @Prop()
  public employeeTargetReached: boolean;
  @Prop()
  public locationTargetReached: boolean;
  @Prop()
  public newTeamMember: boolean;
  @Prop()
  public youAreSentADirectMessage: boolean;
  @Prop()
  public youHaveNewNotifications: boolean;

  // #endregion Properties (5)
}

export const ActivityRelatesEmailSchema = SchemaFactory.createForClass(ActivityRelatesEmail);