import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { Customer } from '../customer.schema';
import { User } from '../user.schema';

@Schema()
export class SaleItemVersion extends Document {
  // #region Properties (6)

  @Prop()
  public action: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Customer' })
  public customer: Customer;
  @Prop()
  public deleted: boolean;
  @Prop({ type: SchemaTypes.Map, required: false })
  public policy: any;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public updatedBy: User;

  // #endregion Properties (6)
}

export const SaleItemVersionSchema = SchemaFactory.createForClass(SaleItemVersion);