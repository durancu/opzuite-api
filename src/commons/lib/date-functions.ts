import moment, { Moment } from 'moment-timezone';
import { DateRangeEnum } from '../enum/date-range.enum';

export interface DateRange {
    start: Date;
    end: Date;
}

export interface DateRangeStr {
    start: string;
    end: string;
}

export interface DateRangeMoment {
    start: Moment;
    end: Moment;
}

export const DEFAULT_FORMAT = 'MM-DD-YYYYTHH:mm:ssZ';
export const DEFAULT_TIMEZONE = 'America/Chicago';

export function dateRangeByName(rangeName?: string, format?: string): DateRange {

    let range: DateRangeMoment;
    format = format ? format : DEFAULT_FORMAT;

    switch (rangeName) {

        case DateRangeEnum.YESTERDAY:
            range = this.yesterday();
            break;
        case DateRangeEnum.WEEK_TO_DATE:
            range = this.weekToDate();
            break;
        case DateRangeEnum.LAST_WEEK:
            range = this.lastWeek();
            break;
        case DateRangeEnum.MONTH_TO_DATE:
            range = this.monthToDate();
            break;
        case DateRangeEnum.LAST_MONTH:
            range = this.lastMonth();
            break;
        case DateRangeEnum.QUARTER_TO_DATE:
            range = this.quarterToDate();
            break;
        case DateRangeEnum.LAST_QUARTER:
            range = this.lastQuarter();
            break;
        case DateRangeEnum.YEAR_TO_DATE:
            range = this.yearToDate();
            break;
        case DateRangeEnum.LAST_YEAR:
            range = this.lastYear();
            break;
        case DateRangeEnum.TODAY:
        default:
            range = this.today();
    }

    return {
        start: new Date(range.start.format(format)),
        end: new Date(range.end.format(format))
    }
}

export function todayTz(timezone = DEFAULT_TIMEZONE): Moment {
    return moment().tz(timezone);
}

export function today(): DateRangeMoment {
    return {
        start: todayTz().startOf('day'),
        end: todayTz().endOf('day')
    };
}

export function yesterday(): DateRangeMoment {
    return {
        start: todayTz().startOf('day').subtract(1, 'day'),
        end: todayTz().endOf('day').subtract(1, 'day')
    };
}

export function weekToDate(): DateRangeMoment {
    return {
        start: todayTz().startOf('week'),
        end: todayTz().endOf('day')
    };
}

export function lastWeek(): DateRangeMoment {
    return {
        start: todayTz().startOf('week').subtract(1, 'week'),
        end: todayTz().endOf('week').endOf('day').subtract(1, 'week').endOf('week')
    };
}

export function monthToDate(): DateRangeMoment {
    return {
        start: todayTz().startOf('month'),
        end: todayTz().endOf('day')
    };
}

export function lastMonth(): DateRangeMoment {
    return {
        start: todayTz().startOf('month').subtract(1, 'month'),
        end: todayTz().subtract(1, 'month').endOf('month').endOf('day')
    };
}

export function quarterToDate(): DateRangeMoment {
    return {
        start: todayTz().startOf('quarter'),
        end: todayTz().endOf('day')
    };
}

export function lastQuarter(): DateRangeMoment {
    return {
        start: todayTz().startOf('quarter').subtract(1, 'quarter'),
        end: todayTz().subtract(1, 'quarter').endOf('quarter')
    };
}

export function yearToDate(): DateRangeMoment {
    return {
        start: todayTz().startOf('year'),
        end: todayTz().endOf('day')
    };
}

export function lastYear(): DateRangeMoment {
    return {
        start: todayTz().subtract(1, 'year').startOf('year'),
        end: todayTz().subtract(1, 'year').endOf('year'),
    };
}

export function monthDifference(oldDate: Date, recentDate: Date) {
    let months: number;
    months = (recentDate.getFullYear() - oldDate.getFullYear()) * 12;
    months -= oldDate.getMonth();
    months += recentDate.getMonth();
    return months <= 0 ? 0 : months;
}

