export class CertificatePdfTemplate {
    // #region Properties (4)

    fieldNames?: string[];
    fieldPrefix?: string;
    fieldSuffix?: string;
    public fields: any;
    public hasAdditionalRemarksSchedule?: boolean;

    // #endregion Properties (4)

    // #region Constructors (1)

    constructor() {
        this.fieldSuffix = "";
        this.fieldPrefix = "";
        this.fieldNames = [];
        this.fields = {};
        this.hasAdditionalRemarksSchedule = false;
    }

    // #endregion Constructors (1)

    // #region Public Methods (4)

    public getField(fieldName: string) {
        return this.fields[fieldName];
    }

    public getFieldNames() {
        return this.fieldNames.map((field) => {
            return this.fieldPrefix + field + this.fieldSuffix;
        });
    }

    public getPdfFieldsObject() {
        return this.fieldNames.reduce((acc, field) => {
            acc[this.fieldPrefix + field + this.fieldSuffix] = this.fields[field] ?? "";
            return acc;
        }, {});
    }

    public setField(fieldName: string, value: any) {
        const valueStr: any = value ? value.toString() : '';
        this.fields[this.fieldPrefix + fieldName + this.fieldSuffix] = valueStr ?? '';
    }

    // #endregion Public Methods (4)
}