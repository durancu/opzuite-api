import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { DashboardPreferences, DashboardPreferencesSchema } from './dashboard-preferences.schema';

@Schema()
export class UserPreferences extends Document {
  // #region Properties (1)

  @Prop({ type: DashboardPreferencesSchema })
  public dashboard: DashboardPreferences;

  // #endregion Properties (1)
}

export const UserPreferencesSchema = SchemaFactory.createForClass(UserPreferences);