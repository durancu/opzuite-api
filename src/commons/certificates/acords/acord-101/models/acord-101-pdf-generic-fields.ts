export const acord101GenericFields = [
        "Producer_CustomerIdentifier_A",
        "Location_ProducerIdentifier_A",

        "Form_CurrentPageNumber_A",
        "Form_TotalPageNumber_A",
        
        "Producer_FullName_A",
        "Policy_PolicyNumberIdentifier_A",
        
        "Insurer_FullName_A",
        "Insurer_NAICCode_A",
        "NamedInsured_FullName_A",
        "NamedInsured_FullName_B",
        "NamedInsured_FullName_C",
        "NamedInsured_FullName_D",
        "Policy_EffectiveDate_A",
        
        "AdditionalRemark_FormIdentifier_A",
        "AdditionalRemark_FormName_A",
        "AdditionalRemark_RemarkText_A"
]

