import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class AuthModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Auth Module) is initialized...');
  }
}
