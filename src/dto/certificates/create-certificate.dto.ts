import { IsArray, IsMongoId, IsNotEmpty, IsOptional, IsString } from "class-validator";
import { CertificateCoverTemplate } from "src/database/entities/certificate-cover-template.schema";
import { CertificateMasterTemplate } from "src/database/entities/certificate-master-template.schema";
import { Company } from "src/database/entities/company.schema";
import { Customer } from "src/database/entities/customer.schema";
import { Insurer } from "src/database/entities/insurer.schema";
import { VehicleInfo } from "src/database/entities/nested/vehicle.info.schema";
import { Sale } from "src/database/entities/sale.schema";
import { User } from "src/database/entities/user.schema";

export class CreateCertificateDto {

    @IsOptional()
    code?: string;

    @IsString()
    name?: string;

    @IsOptional()
    coverPage?: string

    @IsOptional()
    masterTemplate: Partial<CertificateMasterTemplate>;

    @IsOptional()
    coverTemplate: Partial<CertificateCoverTemplate>;

    @IsNotEmpty()
    @IsMongoId()
    customer?: Partial<Customer>;

    @IsNotEmpty()
    @IsMongoId()
    holder?: Partial<Insurer>;

    @IsOptional()
    issuedAt?: Date;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    number?: string;

    @IsArray()
    @IsOptional()
    policies?: Partial<Sale>[];

    @IsMongoId()
    @IsOptional()
    producer?: Partial<Company>;

    @IsString()
    @IsOptional()
    @IsNotEmpty()
    revisionNumber?: string;

    @IsOptional()
    ccEmails?: string[];

    @IsOptional()
    ccFaxNumbers?: string[];

    @IsOptional()
    policiesIncluded?: Sale[];

    @IsOptional()
    vehiclesIncluded?: VehicleInfo[]

    @IsOptional()
    insurers?: Insurer[]

    @IsOptional()
    updatedAt?: Date;

    @IsOptional()
    updatedBy?: Partial<User>;
}
