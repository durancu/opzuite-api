import { S3 } from '@aws-sdk/client-s3';
import { Module } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { File, FileSchema } from 'src/database/entities/file.schema';
import { FileModuleInitializerService } from './file-data-initializer.service';
import { FileController } from './file.controller';
import { FileService } from './file.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: File.name, schema: FileSchema }]),
  ],
  controllers: [FileController],
  providers: [
    FileModuleInitializerService,
    FileService,
    {
      provide: S3,
      useFactory: (configService: ConfigService) => {
        return new S3({
          region: configService.get<string>('AWS_DEFAULT_REGION'),
          credentials: {
            accessKeyId: configService.get<string>('AWS_ACCESS_KEY_ID'),
            secretAccessKey: configService.get<string>('AWS_SECRET_ACCESS_KEY'),
          },
        });
      },
      inject: [ConfigService],
    },
  ],
  exports: [FileService, S3],
})
export class FileModule { }
