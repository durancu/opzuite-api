import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { DEFAULT_COMMUNICATION } from 'src/commons/const/defaults/communication.default';
import { Company } from './company.schema';
import { BusinessInfo, BusinessInfoSchema } from './nested/business-info.schema';
import { Cohort, CohortSchema } from './nested/cohort.schema';
import { Communication, CommunicationSchema } from './nested/communication.schema';
import { ContactInfo, ContactInfoSchema } from './nested/contact-info.schema';
import { VehicleInfo, VehicleInfoSchema } from './nested/vehicle.info.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Customer extends Document {
  // #region Properties (13)

  @Prop({ type: BusinessInfoSchema })
  public business: BusinessInfo;
  @Prop({ default: () => nanoid(6), required: false, })
  public code: string;
  @Prop({ type: CommunicationSchema, default: DEFAULT_COMMUNICATION })
  public communication: Communication;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
  public company: Company;
  @Prop({ type: ContactInfoSchema })
  public contact: ContactInfo;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public createdBy: User;
  @Prop()
  public email: string;
  @Prop()
  public name: string;
  @Prop()
  public phone: string;
  @Prop({ default: 'BUSINESS' })
  public type: string;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public updatedBy: User;

  @Prop({ type: [CohortSchema], required: false, default: [] })
  public cohorts: Cohort[];
  @Prop({ type: [ContactInfoSchema], required: false, default: [] })
  public contacts: ContactInfo[];
  @Prop({ type: [VehicleInfoSchema], required: false, default: [] })
  public vehicles: VehicleInfo[];

  // #endregion Properties (13)
}

export function primaryStateGetHook(): boolean {
  return this.type === 'BUSINESS'
    ? `${this.business && this.business.address
      ? this.business.address.state
      : ''
    }`
    : this.contact && this.contact.address
      ? this.contact.address.state
      : '';
}

export const CustomerSchema = SchemaFactory.createForClass(Customer);

CustomerSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
CustomerSchema.virtual('state').get(primaryStateGetHook);
