

import * as moment from 'moment';
import { SaleCommissionsCalculator } from 'src/commons/vl17-specific/sale-commissions-calculator';
import { PayStub } from 'src/database/entities/nested/pay-stub.schema';
import { User } from 'src/database/entities/user.schema';
import { PayrollDto } from 'src/dto/payroll/payroll.dto';
import { MetricsService } from '../sale/metrics.service';


const ADDON_TYPE_DISCOUNT = 'DISCOUNT';
const ADDON_TYPE_BONUS = 'BONUS';
const ADDON_TYPE_REIMBURSEMENT = 'REIMBURSEMENT';
const ADDON_CATEGORY_SALES_BONUS = 'BONUS_SALE';
const DEFAULT_PAYROLL_WORKED_HOURS = 160;

/* 
  const DEFAULT_SALE_BONUS_ADDON = {
  amount: 0,
  category: 'BONUS_SALE',
  description: 'Employee Monthly Sale Bonus',
  type: 'BONUS',
}; 
*/

const DEFAULT_STUB_METRICS = {
  totalPremium: 0,
  totalNonPremium: 0,
  totalTaxesAndFees: 0,
  totalSaleBonus: 0,
  totalSales: 0,
  totalPermits: 0,
  totalFees: 0,
  totalTips: 0,
  totalExtraBonus: 0,
  totalDiscount: 0,
  totalDownPayment: 0,
  totalNetSalary: 0,
  totalReimbursement: 0,
  totalRegularSalary: 0,
};

/*
 * Receives:
 *   payrollDto: a payroll object to push final result
 *   metricsService: injected Service to calculate bonus
 *   currentUser: current authenticated user
 *
 * Description: calculates salary bonus for each employee based in the data range coming in the payrollDto object,
 *              adding
 *
 */
export async function runPayrollCalculations(
  payroll: PayrollDto,
  metricsService: MetricsService,
): Promise<any> {
  const payrollDto = { ...payroll };
  resetPayrollAggregators(payrollDto);

  const salaryMetrics: any[] = await metricsService.getEmployeesSalaryMetrics(
    payrollDto.payPeriodStartedAt,
    payrollDto.payPeriodEndedAt,
  );

  removeInvalidPayStubs();

  //remove all employee metrics == null
  const reports = salaryMetrics.filter(
    (employeeMetrics) => employeeMetrics !== null,
  );

  //find if exists, for each paystub, its matching employee metrics
  payrollDto.payStubs = payrollDto.payStubs.map((payStub) => {
    const employeeMetrics = reports.find(
      ({ id }: any) => id._id.toString() === payStub.employee,
    );

    const stub: Partial<PayStub> = {
      ...DEFAULT_STUB_METRICS,
      ...payStub,
    };

    if (employeeMetrics) {
      stub.totalPremium = employeeMetrics['totalPremium'] || 0;
      stub.totalPermits = employeeMetrics['totalPermits'] || 0;
      stub.totalTaxesAndFees = employeeMetrics['totalTaxesAndFees'] || 0;
      //totalFees is used in Bonus calculation. Using totalTaxesAndFees until new decision is taken
      stub.totalFees = stub.totalTaxesAndFees;
      stub.totalTips = employeeMetrics['totalTips'] || 0;
      //totalTips is used in Bonus calculation. Using totalExtraBonus until new decision is taken
      stub.totalExtraBonus = 0;
      stub.totalNonPremium = employeeMetrics['totalNonPremium'] || 0;
      //totalDownPayment is used in Bonus calculation. Using totalNonPremium until new decision is taken
      stub.totalDownPayment = stub.totalNonPremium;
    }

    stub.totalSales = (stub.totalPremium || 0) + (stub.totalPermits || 0);

    //DISCARD THIS SOLUTION UNTIL WE IMPLEMENT MULTIPLE BONUS|DISCOUNT|REIMBURSEMENT INPUT IN FRONTEND
    /* stub.totalTips = calculatePayStubTotalExtraBonus(payStub);
    stub.totalDiscount = calculatePayStubTotalDiscount(payStub);
    stub.totalReimbursement = calculatePayStubTotalReimbursement(payStub); */

    stub.totalRegularSalary = calculateTotalRegularSalary(stub);
    updatePayrollAggregators(payrollDto, stub);
    return stub;
  });

  payrollDto.payStubs = payrollDto.payStubs.map((stub: PayStub) => {
    return stub;
  });

  return payrollDto;

  function removeInvalidPayStubs() {
    if (payrollDto.payStubs) {
      const stubs = payrollDto.payStubs.filter((payStub) => payStub);
      payrollDto.payStubs = stubs;
    } else {
      payrollDto.payStubs = [];
    }
  }
}

function resetPayrollAggregators(payrollDto: PayrollDto) {
  payrollDto.totalSaleBonus = 0;
  payrollDto.totalSales = 0;
  payrollDto.totalPermits = 0;
  payrollDto.totalFees = 0;
  payrollDto.totalTips = 0;
  payrollDto.totalExtraBonus = 0;
  payrollDto.totalDiscount = 0;
  payrollDto.totalReimbursement = 0;
  payrollDto.totalRegularSalary = 0;
  payrollDto.totalNetSalary = 0;
  payrollDto.totalPremium = 0;
  payrollDto.totalNonPremium = 0;
  payrollDto.totalTaxesAndFees = 0;
  payrollDto.totalDownPayment = 0;
}

function updatePayrollAggregators(
  payrollDto: PayrollDto,
  stub: Partial<PayStub>,
) {
  payrollDto.totalPremium += stub.totalPremium || 0;
  payrollDto.totalNonPremium += stub.totalNonPremium || 0;
  payrollDto.totalTaxesAndFees += stub.totalTaxesAndFees || 0;
  payrollDto.totalSaleBonus += stub.totalSaleBonus || 0;
  payrollDto.totalSales += stub.totalSales || 0;
  payrollDto.totalPermits += stub.totalPermits || 0;
  payrollDto.totalFees += stub.totalFees || 0;
  payrollDto.totalTips += stub.totalTips || 0;
  payrollDto.totalExtraBonus += stub.totalExtraBonus || 0;
  payrollDto.totalDiscount += stub.totalDiscount || 0;
  payrollDto.totalReimbursement += stub.totalReimbursement || 0;
  payrollDto.totalRegularSalary += stub.totalRegularSalary || 0;
  payrollDto.totalNetSalary += stub.totalNetSalary || 0;
  payrollDto.totalDownPayment += stub.totalDownPayment || 0;
}

export function calculateTotalNetSalary(stub: Partial<PayStub>): number {
  return (
    (stub.totalRegularSalary || 0) +
    (stub.totalSaleBonus || 0) +
    SaleCommissionsCalculator.percentBonusPerFees(stub.totalFees) +
    SaleCommissionsCalculator.percentBonusPerPermits(stub.totalPermits) +
    (stub.totalExtraBonus || 0) +
    (stub.totalReimbursement || 0) -
    (stub.totalDiscount || 0) +
    (stub.totalTips || 0)
  );
}

function calculateTotalRegularSalary(stub: Partial<PayStub>): number {
  return (
    stub.hourlyRate * (stub.normalHoursWorked || 0) +
    (stub.overtimeHoursWorked || 0) * stub.hourlyRate * 1.5
  );
}

function calculatePayStubTotalReimbursement(payStub: Partial<PayStub>): number {
  return payStub.addons.reduce(function (prev, cur) {
    return prev + (cur.type === ADDON_TYPE_REIMBURSEMENT ? cur.amount : 0);
  }, 0);
}

function calculatePayStubTotalDiscount(payStub: Partial<PayStub>): number {
  return payStub.addons.reduce(function (prev, cur) {
    return prev + (cur.type === ADDON_TYPE_DISCOUNT ? cur.amount : 0);
  }, 0);
}

function calculatePayStubTotalExtraBonus(payStub: Partial<PayStub>): number {
  return payStub.addons.reduce(function (prev, cur) {
    return (
      prev +
      (cur.type === ADDON_TYPE_BONUS &&
        cur.category !== ADDON_CATEGORY_SALES_BONUS
        ? cur.amount
        : 0)
    );
  }, 0);
}

export function getLastPayPeriod(payPeriodStartDay = 1) {
  const monthDiff = moment().date() >= payPeriodStartDay ? 0 : 1;

  const startDate = moment()
    .subtract(monthDiff + 1, 'month')
    .date(payPeriodStartDay)
    .startOf('day');
  const endDate = moment()
    .subtract(monthDiff, 'month')
    .date(payPeriodStartDay - 1)
    .endOf('day');

  const code = startDate.format('yyyyMMDD').concat(endDate.format('yyyyMMDD'));

  return {
    code: code,
    start: new Date(startDate.toISOString()),
    end: new Date(endDate.toISOString()),
  };
}

export function getPayDayPeriods(
  payPeriodStartDay = 1,
  monthsBefore: number,
): any[] {
  const periods = [];

  for (let i = 0; i < monthsBefore; i++) {
    const monthDiff = moment().date() >= payPeriodStartDay ? 0 : 1;

    const startDate = moment()
      .subtract(monthDiff + i + 1, 'month')
      .date(payPeriodStartDay)
      .startOf('day');
    const endDate = moment()
      .subtract(monthDiff + i, 'month')
      .date(payPeriodStartDay - 1)
      .endOf('day');

    const code = startDate
      .format('yyyyMMDD')
      .concat(endDate.format('yyyyMMDD'));

    const start: Date = new Date(startDate.toISOString());
    const end: Date = new Date(endDate.toISOString());

    periods.push({
      id: code,
      start: start.toISOString(),
      end: end.toISOString(),
      name: `${start.toLocaleString('default', {
        month: 'short',
      })}/${start.getFullYear()} - ${end.toLocaleString('default', {
        month: 'short',
      })}/${end.getFullYear()}`,
    });
  }
  return periods;
}

export async function generateDefaultPayStubs(
  payroll: PayrollDto,
  employees: Partial<User>[],
): Promise<Partial<PayStub>[]> {

  let paystubs: Partial<PayStub>[] = [];

  if (employees.length) {
    paystubs = employees.map((employee) => {
      const paystub: Partial<PayStub> = {
        employeeName: (({ firstName, lastName }) => `${firstName} ${lastName}`)(
          employee,
        ),
        employee: employee.id,
        normalHoursWorked: DEFAULT_PAYROLL_WORKED_HOURS,
        overtimeHoursWorked: 0,
        hourlyRate: employee.employeeInfo.hourlyRate,
        saleCommissionPlans: employee.employeeInfo.saleCommissionPlans,
        bonusFrequency: employee.employeeInfo.bonusFrequency,
        employeeCategory: employee.employeeInfo.category,
        employeeCountry: employee.country,
        employeeLocation: employee.location,
      };

      paystub.addons = [
        {
          amount: 0,
          category: 'EMPLOYER_BONUS',
          description: '',
          type: 'BONUS',
        },
        {
          amount: 0,
          category: 'EMPLOYER_REIMBURSEMENT',
          description: '',
          type: 'REIMBURSEMENT',
        },
        {
          amount: 0,
          category: 'EMPLOYER_DISCOUNT',
          description: '',
          type: 'DISCOUNT',
        },
      ];

      return paystub;
    });
  }

  return paystubs;
}
