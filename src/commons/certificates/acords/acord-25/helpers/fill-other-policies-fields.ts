import { PdfAnswer } from "src/commons/enum/pdf-answer.enum";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillOtherPoliciesFields = (policiesByProduct: any, template: Acord25PdfTemplateV2014NowCerts, includeRemarks: boolean) => {
    let index = 1;
    const otherPolicyProducts = Object.keys(policiesByProduct);

    otherPolicyProducts.forEach((product: any) => {
        if (index <= 2) {
            fillOtherPolicyLine(policiesByProduct[product], template, index);
        } else {
            // TODO: policy in description line
        }
        index++;
    });

    if (includeRemarks) {
        template.setField("CertificateOfLiabilityInsurance_ACORDForm_RemarkText", "Vehicles: [See Attached]; Drivers: [See Attached]")
    }
}

function fillOtherPolicyLine(policyDetails: any, template: Acord25PdfTemplateV2014NowCerts, index) {
    const suffix = index === 2 ? '2' : '';
    template.setField(`OtherPolicy_InsurerLetterCode${suffix}`, policyDetails.letter);
    template.setField(`OtherPolicy_OtherPolicyDescription${suffix}`, policyDetails.coverage.product);
    template.setField(`CertificateOfInsurance_OtherPolicy_AdditionalInsuredCode${suffix}`, PdfAnswer.YES);
    template.setField(`OtherPolicy_SubrogationWaivedCode${suffix}`, '');
    template.setField(`OtherPolicy_PolicyNumberIdentifier${suffix}`, policyDetails.policyNumber ?? '');
    template.setField(`OtherPolicy_PolicyEffectiveDate${suffix}`, policyDetails.effectiveAt ? policyDetails.effectiveAt.toLocaleDateString() : '');
    template.setField(`OtherPolicy_PolicyExpirationDate${suffix}`, policyDetails.expiresAt ? policyDetails.expiresAt.toLocaleDateString() : '');
    //template.setField(`OtherPolicy_CoverageCode${suffix}`, '');

    let limitsText = '';
    const { coverage } = policyDetails;
    switch (coverage.product) {
        case "CARGO":
            limitsText = `Limit: $${coverage.limit || 0}. Deductible: $${coverage.deductible || 0}`;
            break;
        case "PHYSICAL_DAMAGE":
            limitsText = `Limit: $${coverage.limit || 0}. Comp:$${coverage.compensation || 0}. Deductible: $${coverage.deductible || 0}`;
            break;
    }

    template.setField(`OtherPolicy_CoverageLimitAmount${suffix}`, limitsText);

}