import {
  BadRequestException,
  ConflictException,
  Inject,
  Injectable,
  NotFoundException,
  Scope
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { differenceBy, intersectionBy, map } from 'lodash';
import { Model, Types } from 'mongoose';
import { COVERAGES_TYPES } from 'src/commons/const/catalog/coverages-types';
import { PERMIT_TYPES } from 'src/commons/const/catalog/permits-types';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { hasAdminRoleAccess } from 'src/commons/lib/role-functions';
import { can } from 'src/commons/lib/user-functions';
import { Company } from 'src/database/entities/company.schema';
import { Endorsement } from 'src/database/entities/endorsement.schema';
import { Insurer } from 'src/database/entities/insurer.schema';
import { Location } from 'src/database/entities/location.schema';
import { Sale } from 'src/database/entities/sale.schema';
import { User } from 'src/database/entities/user.schema';
import { CreateEndorsementDto } from 'src/dto/endorsement/create-endorsement.dto';
import { UpdateEndorsementDto } from 'src/dto/endorsement/update-endorsement.dto';
import { CreateSaleDto } from 'src/dto/sale/create-sale.dto';
import { ExtendedSaleDto } from 'src/dto/sale/extended-sale.dto';
import { EndorsementRepository } from 'src/repositories/endorsement.repository';
import { InsurerRepository } from 'src/repositories/insurer.repository';
import { LocationRepository } from 'src/repositories/location.repository';
import { SaleRepository } from 'src/repositories/sale.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { UpdateSaleDto } from '../../dto/sale/update-sale.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { UserService } from '../user/user.service';
import {
  calculateMetrics
} from './sale.utils';

@Injectable({ scope: Scope.REQUEST })
export class SaleService {
  // #region Constructors (1)

  constructor(
    @InjectModel(Endorsement.name) private endorsementModel: Model<Endorsement>,
    @InjectModel(Sale.name) private saleModel: Model<Sale>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
    private readonly saleRepository: SaleRepository,
    private readonly endorsementRepository: EndorsementRepository,
    private readonly insurerRepository: InsurerRepository,
    private readonly userRepository: UserRepository,
    private readonly locationRepository: LocationRepository,
    private readonly userService: UserService,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (11)

  /*   public async batchCreate(sales: CreateSaleDto[]): Promise<any> {
      return sales.map(async (sale: CreateSaleDto) => {
        const saleDto: ExtendedSaleDto = { ...sale } as ExtendedSaleDto;
  
        let company = null;
  
        const user: Partial<User> = await this.userModel.findById(this.req.user.id).populate('role').select('-password').exec();
        company = user.company;
  
        if (
          !saleDto.seller ||
          (saleDto.seller && !hasAdminRoleAccess(user.role)) ||
          !can(PermissionType.POLICIES, user)
        ) {
          saleDto.seller = user.id;
          await this.setGeoMetaByUser(saleDto, user);
        } else {
          let seller: Partial<User> = await this.userModel.findById(saleDto.seller).populate('role').select('-password').exec();
  
          company = user.company;
  
          if (!seller) {
            const location: Partial<Location> = await this.locationModel.findById(saleDto.location).exec();
            if (location) {
              seller = location.manager;
            } else {
              throw new ConflictException(
                `Sale coordinator (code:${saleDto.seller}) not found.`,
              );
            }
          }
          saleDto['seller'] = seller._id;
          await this.setGeoMetaByUser(saleDto, seller);
        }
  
        saleDto.isChargeItemized = true && saleDto.isChargeItemized;
        const insurers = await this.insurerModel.find({}).exec();
  
        const saleData: ExtendedSaleDto = await calculateMetrics(
          saleDto as CreateSaleDto,
          insurers,
        );
  
        const endorsements: UpdateEndorsementDto[] = saleData.endorsements as UpdateEndorsementDto[];
  
        saleData.products = [];
        this.setProducts(saleData, endorsements);
  
        if (endorsements) {
          delete saleData['endorsements'];
        }
  
        const created: Sale = await this.saleModel.findOneAndUpdate({ _id: new Types.ObjectId(saleData['_id']) }, {
          ...saleData,
          createdBy: { _id: saleData['seller'] },
          company: { _id: company },
        }, {
          new: true, // Always returning updated work experiences.
          upsert: true, // By setting this true, it will create if it doesn't exist
        }
        );
  
        const createdSaleDto: ExtendedSaleDto = this.saleToSaleDto(created);
  
        if (endorsements.length > 0) {
          await this.processEndorsements(endorsements, created, createdSaleDto);
        } 
  
        return created;
      });
    } */

  /**
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    //TODO: Delete all endorsements with (get all policies by codes, then delete all endorsement by sale Id)

    return this.saleRepository.batchDeleteSales({ code: { $in: codes } });
  }

  /**
   * @param  {CreateSaleDto} createSaleDto
   * @returns Promise
   */
  public async create(createSaleDto: CreateSaleDto): Promise<ExtendedSaleDto> {
    let sale: ExtendedSaleDto = {
      ...createSaleDto,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    };

    sale = await this.setSellerAndGeo(sale);

    const createdSaleDto: ExtendedSaleDto =
      await this.createSaleAndEndorsements(sale);
    return createdSaleDto;
  }

  /**
  * @returns Promise
  */
  public async deleteAll(): Promise<any> {
    await this.endorsementRepository.deleteAll();
    return await this.saleRepository.deleteAll();
  }

  /**
   * @param  {string} code
   * @returns Observable
   */
  public async deleteByCode(code: string): Promise<Sale> {
    const deleted: Sale = await this.saleRepository.deleteByCode(code);

    if (deleted) {
      this.endorsementRepository.deleteSaleEndorsements(deleted.id);
    }
    return deleted;
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.saleRepository.findAll();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string, layout = 'SIMPLE'): Promise<any> {
    const sale: Sale = await this.saleRepository.findByCode(code, layout);

    const saleDto: ExtendedSaleDto = this.saleToSaleDto(sale);

    saleDto.endorsements = await this.endorsementRepository.getSaleEndorsements(sale.id);

    const items: any = saleDto.items.map((item: any) => {
      const productItem: any = COVERAGES_TYPES.find((type: any) => type.id === item.product);
      item = { ...item['_doc'], productItem };
      return item;
    })

    saleDto.items = [...items];

    return saleDto;
  }

  /**
   * @param  {string} id
   * @returns Observable
   */
  public async findById(id: string): Promise<Partial<any>> {
    const options: any = {
      populate: {
        seller: 'role name',
        customer: 'type name'
      }
    }
    const sale: ExtendedSaleDto = await this.saleRepository.findById(id, options);

    if (!sale) {
      throw new NotFoundException(`Sale (id: ${id}) was not found`);
    }

    sale.endorsements = await this.endorsementRepository.getSaleEndorsements(id);

    return sale;
  }

  public async getPoliciesReadyToRenew(
    queryParams?: any
  ): Promise<any> {
    return await this.saleRepository.getPoliciesReadyToRenew(queryParams);
  }

  /**
   * @param  {ExtendedSaleDto} renewSaleDto
   * @returns Promise
   */
  public async renew(renewSaleDto: ExtendedSaleDto): Promise<ExtendedSaleDto> {
    const foundSales: Partial<Sale>[] = await this.saleModel.find({ delete: false, code: { $in: renewSaleDto.renewalReferenceCodes } }).exec();
    if (foundSales.length < renewSaleDto.renewalReferenceCodes.length) {
      throw new NotFoundException(
        `Could not complete renewal: with codes ${JSON.stringify(renewSaleDto.renewalReferenceCodes)}) not found.`,
      );
    }

    delete renewSaleDto.code;

    await this.setSellerAndGeo(renewSaleDto);

    const sale: ExtendedSaleDto = { ...renewSaleDto };

    const renewedSaleDto: ExtendedSaleDto = await this.createSaleAndEndorsements({ ...sale });

    const saleResult: any = await this.archiveSales(sale, renewedSaleDto);

    await this.saleModel.updateOne({ id: renewedSaleDto.id }, { renewalReferences: renewedSaleDto.renewalReferenceCodes }).exec();
    return saleResult;
  }

  public async search(queryParams: any): Promise<any> {
    return this.saleRepository.search(queryParams);
  }

  /**
   * @param  {string} code
   * @param  {UpdateSaleDto} data
   * @returns Promise
   */
  public async update(
    code: string,
    updateSaleDto: UpdateSaleDto,
  ): Promise<ExtendedSaleDto> {
    const sale: Sale = await this.saleRepository.findByCode(code);
    if (!sale) {
      throw new NotFoundException(`Sale (code: ${code}) not found.`);
    }

    let saleDto: ExtendedSaleDto = { ...updateSaleDto };
    saleDto = {
      ...this.saleToSaleDto(sale),
      ...saleDto,
      updatedBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    };

    await this.setSellerAndGeo(saleDto);

    const insurers: Insurer[] = await this.insurerRepository.findAll();

    const saleData: ExtendedSaleDto = await calculateMetrics(saleDto, insurers);

    const endorsements: UpdateEndorsementDto[] = saleData.endorsements;
    this.setProducts(saleData, endorsements);

    if (endorsements) {
      delete saleData['endorsements'];
    }

    const updated = await this.saleRepository.update(sale.id, { ...saleData });

    const updatedSaleDto: ExtendedSaleDto = this.saleToSaleDto(updated);
    if (endorsements) {
      await this.processEndorsements(endorsements, updated, updatedSaleDto);
    }
    return updatedSaleDto;
  }

  // #endregion Public Methods (11)

  // #region Private Methods (9)

  private async archiveSales(sale: ExtendedSaleDto, renewalSale: ExtendedSaleDto) {
    let renewalReferences: any[] = [];
    if (sale.renewalReferenceCodes) {
      //sale.renewalReferenceCodes.map(async (referenceCode: string): Promise<Partial<Sale>> => {
      const salesToUpdate: Partial<Sale>[] = await this.saleModel.find({ code: { $in: sale.renewalReferenceCodes }, deleted: false }).exec();
      if (!salesToUpdate || salesToUpdate.length == 0) {
        throw new NotFoundException(
          `Could not complete renewal: sale (codes: ${sale.renewalReferenceCodes}) not found.`,
        );
      }

      await this.saleModel.updateMany(
        {
          code: { $in: sale.renewalReferenceCodes }, deleted: false
        }, {
        renewedWith: sale.id,
        renewedBy: this.req.user.id,
        wasRenewed: true
      }/* ,
        async function (err: any, docs: any) {
          if (err) {
            throw new NotFoundException(
              `Could not update all sales to renew.`,
            );
          }
        } */
      );

      renewalReferences = map(salesToUpdate, 'id');

      await this.endorsementModel.updateMany({ sale: { $in: renewalReferences }, delete: false }, { delete: true });
      await this.saleModel.updateMany({ _id: { $in: renewalReferences } }, { delete: true });

      //delete sale['renewalReferenceCodes'];
    }
    renewalSale.renewalReferences = renewalReferences;
    return renewalSale;
  }

  private async createSaleAndEndorsements(
    saleDto: ExtendedSaleDto,
    //session: ClientSession
  ) {
    const insurers: Insurer[] = await this.insurerRepository.findAll();

    const createSaleDto: ExtendedSaleDto = await calculateMetrics(saleDto, insurers);

    const endorsements: CreateEndorsementDto[] = createSaleDto.endorsements;

    endorsements.map((endorsement: any) => {
      delete endorsement['_id'];
      delete endorsement['id'];
      delete endorsement['code'];
    })

    this.setProducts(createSaleDto, endorsements);

    if (endorsements) {
      delete createSaleDto['endorsements'];
    }

    const authUser = await this.userService.authUserWithRole(this.req.user.id)

    createSaleDto.createdBy = { _id: authUser.id.toString() };
    createSaleDto.company = { _id: authUser.company.toString() };

    const created: Sale = await this.saleRepository.createSale(createSaleDto);

    const createdSaleDto: ExtendedSaleDto = this.saleToSaleDto(created);

    if (endorsements) {
      endorsements.map((endorsement: CreateEndorsementDto) => {
        endorsement.company = createSaleDto.company;
        return endorsement;
      })
      await this.processEndorsements(endorsements, created, createdSaleDto);
    }
    return createdSaleDto;
  }

  private async findUserDefaultLocation(user: Partial<User>) {
    let defaultLocation: Partial<Location>;
    const company: Partial<Company> = user.company;

    if (company.defaultLocation) {
      defaultLocation = company.defaultLocation;
    } else {
      const companyLocations: Partial<Location>[] = await this.locationRepository.findByCompany(user.company.toString());
      if (companyLocations) {
        defaultLocation = companyLocations[0].id;
      } else {
        throw new BadRequestException(`Could not determine seller location.`);
      }
    }
    return defaultLocation;
  }

  private async processEndorsements(
    endorsements: UpdateEndorsementDto[],
    sale: Sale,
    saleDto: ExtendedSaleDto,
  ) {
    if (endorsements) {
      await this.upsertAndDeleteEndorsements(endorsements, sale);

      const saleId = sale['_doc']._id.toString();

      saleDto.endorsements = await this.endorsementModel
        .find({ sale: saleId })
        .exec();

      return saleDto;
    }
  }

  private saleToSaleDto(sale: Sale): ExtendedSaleDto {
    return { ...sale['_doc'] };
  }

  private async setGeoMetaByUser(
    saleDto: ExtendedSaleDto,
    user: Partial<User>,
  ) {
    if (user.location) {
      saleDto.location = user.location;
      const location: Partial<Location> = await this.locationRepository.findById(user.location._id);
      saleDto.country = location.business?.address?.country;
    } else {
      const defaultLocation: Partial<Location> = await this.findUserDefaultLocation(this.req.user);
      saleDto.location = defaultLocation.id;
      saleDto.country = defaultLocation.business?.address?.country;
    }
    return saleDto;
  }

  private setProducts(
    saleData: ExtendedSaleDto,
    endorsements: UpdateEndorsementDto[],
  ) {
    switch (saleData.type) {
      case 'POLICY':
        if (saleData.items) {
          saleData.products = saleData.items.map((item: any) => {
            return COVERAGES_TYPES.find((type: any) => {
              return type['id'] === item.product;
            });
          });
        }
        break;
      case 'PERMIT':
        if (endorsements) {
          saleData.products = endorsements.map((endorsement: any) => {
            return (
              PERMIT_TYPES.find((permitType: any) => {
                return permitType.type === endorsement.type;
              }) || {
                id: 'CUSTOM',
                iconLabel: 'CST',
                name: 'Custom',
                description: 'Custom Application',
              }
            );
          });
        }

        break;
      default:
        saleData.products = [];
    }
  }

  private async setSellerAndGeo(sale: ExtendedSaleDto) {
    const options = {
      populate: {
        role: 'id hierarchy name'
      }
    }
    const user: Partial<User> = await this.userRepository.findById(this.req.user.id, options);

    if (
      !sale.seller ||
      (sale.seller &&
        !hasAdminRoleAccess(user.role) &&
        !can(PermissionType.POLICIES, user))
    ) {
      sale.seller = user.id;
      sale = await this.setGeoMetaByUser(sale, user);
    } else {
      const seller = await this.userRepository.findById(sale.seller.toString(), options);
      if (!seller) {
        throw new ConflictException(
          `Agent (code:${sale.seller}) not found.`,
        );
      }
      sale.seller = seller._id;
      sale = await this.setGeoMetaByUser(sale, seller);
    }
    return sale;
  }

  private async upsertAndDeleteEndorsements(
    endorsements: UpdateEndorsementDto[],
    sale: Sale,
  ) {
    const endorsementDocs: any[] = [];

    endorsements = endorsements.map((endorsement: UpdateEndorsementDto) => {
      if (!endorsement.followUpPerson || !endorsement.followUpPerson) {
        delete endorsement['followUpPerson'];
      }

      if (endorsement.items) {
        endorsement.items = endorsement.items.map((item) => {
          if (!item.followUpPerson || !item.followUpPerson) {
            delete item['followUpPerson'];
          }
          return item;
        });
      }

      if (endorsement.code) {
        endorsement = {
          ...endorsement,
          updatedBy: this.req.user.id,
        };
      } else {
        endorsement = {
          ...endorsement,
          createdBy: this.req.user.id,
          company: this.req.user.company,
          sale: sale.id,
        };
      }
      return endorsement;
    });

    const oldEndorsements = await this.endorsementModel
      .find({ sale: sale.id })
      .exec();

    const endorsementsToCreate = endorsements.filter(
      (endorsement: any) => !endorsement.code,
    );

    endorsementsToCreate.forEach((endorsement: any) => {
      endorsementDocs.push({
        insertOne: {
          document: endorsement,
        },
      });
    });

    const endorsementsToUpdate = intersectionBy(
      endorsements,
      oldEndorsements,
      'code',
    );

    endorsementsToUpdate.forEach((endorsement: UpdateEndorsementDto) => {
      if (endorsement._id) {
        endorsementDocs.push({
          updateOne: {
            filter: { _id: new Types.ObjectId(endorsement._id) },
            update: { $set: endorsement },
            new: true,
            upsert: true,
          },
        });
      } else if (endorsement.code) {
        endorsementDocs.push({
          updateOne: {
            filter: { code: endorsement.code },
            update: { $set: endorsement },
            new: true,
            upsert: true,
          },
        });
      }
    });

    const endorsementsToDelete = differenceBy(
      oldEndorsements,
      endorsements,
      'code',
    );

    endorsementsToDelete.forEach((endorsement: any) => {
      if (endorsement.code) {
        endorsementDocs.push({
          deleteOne: {
            filter: { code: endorsement.code },
          },
        });
      }
    });

    this.endorsementModel
      .bulkWrite(endorsementDocs)
      .then((bulkWriteOpResult) => {
        console.log('BULK update OK');
        console.log(JSON.stringify(bulkWriteOpResult, null, 2));
        return bulkWriteOpResult;
      })
      .catch((err) => {
        console.log('BULK update error');
        console.log(JSON.stringify(err, null, 2));
      });
  }

  // #endregion Private Methods (9)
}
