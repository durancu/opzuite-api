import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { nanoid } from 'nanoid';

@Schema()
export class Commission extends Document {
  // #region Properties (3)

  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop()
  public coverage: string;
  @Prop()
  public percent: number;

  // #endregion Properties (3)
}

export const CommissionSchema = SchemaFactory.createForClass(Commission);