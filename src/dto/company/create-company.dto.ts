import { IsEmail, IsNotEmpty, IsOptional } from 'class-validator';
import { Location } from 'src/database/entities/location.schema';
import { Address } from 'src/database/entities/nested/address.schema';
import { User } from 'src/database/entities/user.schema';
export class CreateCompanyDto {

  @IsOptional()
  @IsNotEmpty()
  address?: Address;

  @IsOptional()
  defaultLocation?: Partial<Location>;

  @IsOptional()
  certificateAgents?: Partial<User>[];

  @IsOptional()
  certificateProducerAgents?: Partial<User>[];

  @IsNotEmpty()
  @IsEmail()
  email?: string;


  @IsOptional()
  fax?: string;

  @IsOptional()
  industry?: string; //can be?: Auto Parts, Entertainment, Chemical, Engineering, etc

  @IsOptional()
  logo?: string;

  @IsNotEmpty()
  name?: string;

  @IsOptional()
  otherPhones?: string[]; // delimited by-comma string

  @IsOptional()
  primaryPhone?: string;

  @IsOptional()
  primaryPhoneExtension?: string;

  @IsOptional()
  secondaryPhone?: string;

  @IsOptional()
  secondaryPhoneExtension?: string;

  @IsOptional()
  sector?: string; // can be?: Financial, Technology, Healthcare, etc

  @IsOptional()
  startedAt?: Date;

  @IsOptional()
  type?: string; // LLC, S Corp, C Corp

  @IsOptional()
  website?: string;

  @IsOptional()
  restrictedAccessByIP?: boolean;
}
