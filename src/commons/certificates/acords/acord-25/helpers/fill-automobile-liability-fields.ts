import { AutoCoverageScope } from "src/commons/enum/auto-coverage-scope";
import { PdfAnswer } from "src/commons/enum/pdf-answer.enum";
import { PdfCheckBox } from "src/commons/enum/pdf-checkbox.enum";
import { CoverageDetails } from "src/database/entities/nested/coverage-details.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillAutomobileLiabilityFields = (policiesByProduct: any, template: Acord25PdfTemplateV2014NowCerts) => {
    if (policiesByProduct['AUTOMOBILE_LIABILITY']) {

        const policy = policiesByProduct['AUTOMOBILE_LIABILITY']

        template.setField('Vehicle_InsurerLetterCode', policy.letter);
        template.setField('Policy_AutomobileLiability_PolicyNumberIdentifier', policy.policyNumber);
        template.setField('Policy_AutomobileLiability_EffectiveDate', policy.effectiveAt?.toLocaleDateString());
        template.setField('Policy_AutomobileLiability_ExpirationDate', policy.expiresAt?.toLocaleDateString());

        const coverageDetails: CoverageDetails = policy.coverage.details;

        if (coverageDetails && coverageDetails.autoCoverageScope) {
            if (coverageDetails.autoCoverageScope === AutoCoverageScope.ANY) {
                template.setField('Vehicle_AnyAutoIndicator', PdfCheckBox.ON);
            } else if (coverageDetails.autoCoverageScope === AutoCoverageScope.SCHEDULED) {
                template.setField('Vehicle_ScheduledAutosIndicator', PdfCheckBox.ON);
                template.setField('Vehicle_AllOwnedAutosIndicator', coverageDetails.includeOwnedAutos ? PdfCheckBox.ON : PdfCheckBox.OFF);
                template.setField('Vehicle_HiredAutosIndicator', coverageDetails.includeHiredAutos ? PdfCheckBox.ON : PdfCheckBox.OFF);
                template.setField('Vehicle_NonOwnedAutosIndicator', coverageDetails.includeNonOwnedAutos ? PdfCheckBox.ON : PdfCheckBox.OFF);
            }
        }

        if (coverageDetails) {
            template.setField('Vehicle_OtherCoveredAutoIndicator', coverageDetails.includeOtherCoveredAutoA ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('Vehicle_OtherCoveredAutoDescription', coverageDetails.otherCoveredAutoADescription);
            template.setField('Vehicle_OtherCoveredAutoIndicator_B', coverageDetails.includeOtherCoveredAutoB ? PdfCheckBox.ON : PdfCheckBox.OFF);
            template.setField('Vehicle_OtherCoveredAutoDescription_B', coverageDetails.otherCoveredAutoBDescription);

            template.setField('CertificateOfInsurance_AutomobileLiability_AdditionalInsuredCode', coverageDetails.additionalInsured ? PdfAnswer.YES : PdfAnswer.EMPTY);
            template.setField('Policy_AutomobileLiability_SubrogationWaivedCode', coverageDetails.waiverOfSubrogation ? PdfAnswer.YES : PdfAnswer.EMPTY);

            template.setField('Vehicle_CombinedSingleLimit_EachAccidentAmount', coverageDetails.combinedSingleLimit);
            template.setField('Vehicle_BodilyInjury_PerPersonLimitAmount', coverageDetails.bodilyInjuryPerPersonLimit);
            template.setField('Vehicle_BodilyInjury_PerAccidentLimitAmount', coverageDetails.bodilyInjuryPerAccidentLimit);
            template.setField('Vehicle_PropertyDamage_PerAccidentLimitAmount', coverageDetails.propertyDamagePerAccidentLimit);
            template.setField('Vehicle_OtherCoverage_CoverageDescription', coverageDetails.other1LimitName);
            template.setField('Vehicle_OtherCoverage_LimitAmount', coverageDetails.other1LimitAmount);
        }

        delete policiesByProduct['AUTOMOBILE_LIABILITY'];
    }
}