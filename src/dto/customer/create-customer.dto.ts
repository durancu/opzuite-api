import { IsNotEmpty, IsNotEmptyObject, IsOptional, IsString } from 'class-validator';
import { Company } from 'src/database/entities/company.schema';
import { BusinessInfo } from 'src/database/entities/nested/business-info.schema';
import { Cohort } from 'src/database/entities/nested/cohort.schema';
import { Communication } from 'src/database/entities/nested/communication.schema';
import { ContactInfo } from 'src/database/entities/nested/contact-info.schema';
import { VehicleInfo } from 'src/database/entities/nested/vehicle.info.schema';
import { User } from 'src/database/entities/user.schema';

export class CreateCustomerDto {
  // #region Properties (9)

  @IsOptional()
  @IsNotEmptyObject()
  business?: Partial<BusinessInfo>;
  @IsOptional()
  @IsNotEmptyObject()
  communication?: Partial<Communication>;
  @IsOptional()
  company?: Partial<Company>;
  @IsOptional()
  @IsNotEmptyObject()
  contact?: Partial<ContactInfo>;
  @IsOptional()
  createdBy?: Partial<User>
  @IsOptional()
  email?: string;
  @IsOptional()
  name?: string;
  @IsOptional()
  phone?: string;
  @IsOptional()
  @IsNotEmpty()
  @IsString()
  type?: string;
  @IsOptional()
  cohorts?: Partial<Cohort>[];
  @IsOptional()
  vehicles?: Partial<VehicleInfo>[];

  // #endregion Properties (9)
}