import { BadRequestException, Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { buildSearchTextQueries } from 'src/commons/queries/mongo/helpers/query';
import { Financing } from 'src/database/entities/financing.schema';
import { CreateFinancingDto } from 'src/dto/financing/create-financing.dto';
import { UpdateFinancingDto } from 'src/dto/financing/update-financing.dto';
import { AuthenticatedRequest } from 'src/modules/auth/interface/authenticated-request.interface';

@Injectable({ scope: Scope.REQUEST })
export class FinancingService {
  constructor(
    @InjectModel(Financing.name) private financingModel: Model<Financing>,
    @Inject(REQUEST) private req: AuthenticatedRequest
  ) { }

  /**
   * @returns Promise
   */
  async findAll(): Promise<any> {
    return await this.financingModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async findByCode(code: string): Promise<Financing> {
    const financing: Financing = await this.financingModel
      .findOne({ code: code })
      .exec();

    if (!financing) {
      throw new NotFoundException(`Financing (code: ${code}) not found.`);
    }

    return financing;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  async findById(id: string): Promise<Financing> {
    const financing: Financing = await this.financingModel
      .findOne({ _id: id })
      .exec();

    if (!financing) {
      throw new NotFoundException(`Financing (id: ${id}) was not found`);
    }

    return financing;
  }

  /**
   * @param  {CreateFinancingDto} financingDto
   * @returns Promise
   */
  async create(financingDto: CreateFinancingDto): Promise<Financing> {

    const financing: CreateFinancingDto = {
      ...financingDto,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    };

    return await this.financingModel.create(financing);
  }

  /**
   * @param  {string} code
   * @param  {UpdateFinancingDto} data
   * @returns Promise
   */
  async update(code: string, financingDto: UpdateFinancingDto): Promise<Financing> {
    const financingFound: Financing = await this.financingModel
      .findOne({ code: code })
      .exec();

    if (!financingFound) {
      throw new NotFoundException(`Financing (code: ${code}) not found.`);
    }

    const financing: UpdateFinancingDto = {
      ...financingFound['_doc'],
      ...financingDto,
      updatedBy: this.req.user.id,
    };

    return await this.financingModel.findOneAndUpdate(
      { _id: new Types.ObjectId(financingFound._id) },
      financing,
      { new: true },
    );
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  async deleteByCode(code: string): Promise<Financing> {
    const financing: Financing = await this.financingModel
      .findOne({ code: code })
      .exec();

    if (!financing) {
      throw new NotFoundException(`Financing (code: ${code}) not found.`);
    }

    return await this.financingModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */

  async deleteAll(): Promise<any> {
    return await this.financingModel.deleteMany({}).exec();
  }

  /**
   * @returns Promise
   */

  async batchDelete(codes: string[]): Promise<any> {
    return await this.financingModel.deleteMany({ code: { $in: codes } }).exec();
  }

  async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let type = null;
    if ('type' in queryParams.filter) {
      type = queryParams.filter.type;
      delete queryParams.filter['type'];
    }

    let conditions = {};

    conditions = {
      $and: [{ deleted: false }],
    };

    if (
      type ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      if (type) {
        conditions['$and'].push({ type: type });
      }
    }

    if (queryParams.searchText) {
      let searchColumns: string[] = [
        'email',
        'name',
        'phone',
      ];

      if (queryParams.searchColumns && queryParams.searchColumns.length) {
        searchColumns = queryParams.searchColumns;
      }
      const searchTextQueries = buildSearchTextQueries(
        queryParams.searchText,
        searchColumns,
      );

      if (searchTextQueries.length > 0) {
        conditions['$or'] = searchTextQueries;
      }
    }

    const query = this.financingModel.aggregate();

    if (conditions) {
      query.match(conditions);
    }

    query.unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'createdBy',
        foreignField: '_id',
        as: 'createdBy',
      })
      .unwind({ path: '$createdBy', preserveNullAndEmptyArrays: true });

    query.unwind({ path: '$updatedBy', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'updatedBy',
        foreignField: '_id',
        as: 'updatedBy',
      })
      .unwind({ path: '$updatedBy', preserveNullAndEmptyArrays: true });

    query.append({
      $project: {
        id: '$_id',
        type: '$type',
        name: '$name',
        createdAt: "$createdAt",
        createdByName: '$createdBy.name',
        createdByCode: '$createdBy._id',
        updatedAt: '$updatedAt',
        updatedByName: '$updatedBy.name',
        updatedByCode: '$updatedBy._id',
        locationName: { $arrayElemAt: ['$location.business.name', 0] },
        locationCode: { $arrayElemAt: ['$location.code', 0] },
        deleted: '$deleted',
        code: '$code',
      }
    });

    const allEntities = await query.exec();
    const totalCount = allEntities.length;

    query.skip(skipCriteria).limit(limitCriteria).sort(sortCriteria);

    const entities = await query.exec();

    return {
      entities: entities,
      totalCount: totalCount,
      page: queryParams.pageNumber,
      limit: queryParams.pageSize,
    };
  }

  async getCatalog(filterCriteria: any): Promise<any> {
    const financinglist = await this.financingModel
      .find(filterCriteria)
      .select('name email phone type _id')
      .sort({ 'name': 1 })
      .exec();

    return {
      carriers: financinglist.filter((financing) => financing.type === 'CARRIER'),
      brokers: financinglist.filter((financing) => financing.type === 'BROKER'),
      financers: financinglist.filter((financing) => financing.type === 'FINANCER'),
    };
  }

  async batchCreate(financinglist: UpdateFinancingDto[]) {
    const user = this.req.user.id;
    const company = this.req.user.company;

    financinglist = financinglist.map((financing: UpdateFinancingDto) => {
      if (financing.code) {
        financing = {
          ...financing,
          updatedBy: financing.updatedBy ? financing.updatedBy : user,
        };
      } else {
        financing = {
          ...financing,
          createdBy: financing.createdBy ? financing.createdBy : user,
          company: company,
        };
      }
      return financing;
    });

    const bulkPayload = financinglist.map((financing: Partial<Financing>) => ({
      updateOne: {
        filter: { code: financing.code },
        update: { $set: financing },
        upsert: true,
      },
    }));

    this.financingModel
      .bulkWrite(bulkPayload)
      .then((bulkWriteOpResult) => {
        console.log('BULK update OK');
        //console.log(JSON.stringify(bulkWriteOpResult, null, 2));
        return bulkWriteOpResult;
      })
      .catch((err) => {
        console.log('BULK update error');
        console.log(JSON.stringify(err, null, 2));
        throw new BadRequestException(
          'Cannot upsert financing list. ' + JSON.stringify(err, null, 2),
        );
      });
  }
}
