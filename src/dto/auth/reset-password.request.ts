import { IsNotEmpty } from "class-validator";

export class ResetPasswordRequest {
    // #region Properties (2)

    @IsNotEmpty()
    password?: string;
    @IsNotEmpty()
    token?: string;

    // #endregion Properties (2)
}