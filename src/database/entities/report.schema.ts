import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { Company } from './company.schema';
import { ReportParams, ReportParamsSchema } from './nested/report-params.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Report extends Document {
    // #region Properties (13)

    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public company: Company;
    @Prop({ type: SchemaTypes.Map, default: {} })
    public config: any;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public createdBy: User;
    @Prop()
    public description: string;
    @Prop()
    public key: string;
    @Prop()
    public name: string;
    @Prop({ type: ReportParamsSchema })
    public params: ReportParams;
    @Prop({ type: [SchemaTypes.String], default: [] })
    public permissions: PermissionType[];
    @Prop({ default: 'public' })
    public scope: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public updatedBy: User;

    // #endregion Properties (13)
}

export const ReportSchema = SchemaFactory.createForClass(Report);
ReportSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
