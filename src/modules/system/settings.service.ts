import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp } from 'lodash';
import { Model, Types } from 'mongoose';
import { SystemSetting } from 'src/database/entities/system-setting.schema';
import { CreateSystemSettingDto } from 'src/dto/system-setting/create-system-setting.dto';
import { UpdateSystemSettingDto } from 'src/dto/system-setting/update-system-setting.dto';
import { AuthenticatedRequest } from 'src/modules/auth/interface/authenticated-request.interface';

@Injectable({ scope: Scope.REQUEST })
export class SettingService {
  constructor(
    @InjectModel(SystemSetting.name) private settingModel: Model<SystemSetting>,
    @Inject(REQUEST) private req: AuthenticatedRequest
  ) { }

  /**
   * @returns Promise
   */
  async findAll(): Promise<any> {
    return await this.settingModel.find({}).exec();
  }

  /**
   * @returns Promise
   */
  async catalog(): Promise<any> {
    const settings: SystemSetting[] = await this.settingModel.find({}).sort({ key: 1 }).exec();

    const catalog: any = {};

    settings.map((setting: any) => {
      catalog[setting.key] = setting.value;
    });
    return catalog;
  }

  /**
   * @param  {string} key
   * @returns Promise
   */
  async findByKey(key: string): Promise<SystemSetting> {
    const setting: SystemSetting = await this.settingModel
      .findOne({ key: key })
      .exec();

    if (!setting) {
      throw new NotFoundException(`System Setting (key: ${key}) not found.`);
    }

    return setting;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  async findById(id: string): Promise<SystemSetting> {
    const setting: SystemSetting = await this.settingModel
      .findOne({ _id: id })
      .exec();

    if (!setting) {
      throw new NotFoundException(`System Setting (id: ${id}) was not found`);
    }

    return setting;
  }

  /**
   * @param  {CreateSystemDto} settingDto
   * @returns Promise
   */
  async save(settingDto: CreateSystemSettingDto): Promise<SystemSetting> {
    return await this.settingModel.create({
      ...settingDto,
      createdBy: { _id: this.req.user.id },
    });
  }

  /**
   * @param  {string} key
   * @param  {UpdateSystemDto} data
   * @returns Promise
   */
  async update(
    key: string,
    setting: UpdateSystemSettingDto,
  ): Promise<SystemSetting> {
    const settingFound: SystemSetting = await this.settingModel
      .findOne({ key: key })
      .exec();

    if (!settingFound) {
      throw new NotFoundException(`System Setting (key: ${key}) not found.`);
    }

    const settingData: Partial<SystemSetting> = {
      ...settingFound['_doc'],
      ...setting,
      updatedBy: this.req.user.id,
    };

    return await this.settingModel.findOneAndUpdate(
      { _id: new Types.ObjectId(settingFound._id) },
      settingData,
      { new: true },
    );
  }

  /**
   * @param  {string} key
   * @returns Promise
   */
  async deleteByKey(key: string): Promise<SystemSetting> {
    const setting: SystemSetting = await this.settingModel
      .findOne({ key: key })
      .exec();

    if (!setting) {
      throw new NotFoundException(`System Setting (key: ${key}) not found.`);
    }

    return await this.settingModel.findOneAndDelete({ key: key }).exec();
  }

  /**
   * @returns Promise
   */

  async deleteAll(): Promise<any> {
    return await this.settingModel.deleteMany({}).exec();
  }

  /**
   * @returns Promise
   */

  async batchDelete(keys: string[]): Promise<any> {
    return await this.settingModel.deleteMany({ key: { $in: keys } }).exec();
  }

  async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let filterCriteria = {};

    if (
      queryParams.filter &&
      Object.keys(queryParams.filter).length > 0 &&
      queryParams.filter.constructor === Object
    ) {
      filterCriteria = {
        $or: Object.keys(queryParams.filter).map((key) => {
          return {
            [key]: {
              $regex: new RegExp('.*' + escapeRegExp(queryParams.filter[key]) + '.*', 'i'),
            },
          };
        }),
      };
    }

    return {
      totalCount: await this.settingModel
        .find(filterCriteria)
        .countDocuments()
        .exec(),
      entities: await this.settingModel
        .find(filterCriteria)
        .select('name')
        .skip(skipCriteria)
        .limit(limitCriteria)
        .sort(sortCriteria)
        .exec(),
    };
  }
}
