import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { nanoid } from 'nanoid';
import { Insurer } from '../insurer.schema';
import { User } from '../user.schema';
import { Cohort, CohortSchema } from './cohort.schema';
import { ContactInfo, ContactInfoSchema } from './contact-info.schema';
import { CoverageDetails, CoverageDetailsSchema } from './coverage-details.schema';
import { SaleItemVersion, SaleItemVersionSchema } from './sale-item-version.schema';
import { VehicleInfo, VehicleInfoSchema } from './vehicle.info.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } })
export class SaleItem extends Document {
    // #region Properties (21)

    @Prop()
    public agencyCommission: number;
    @Prop()
    public amount: number;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Insurer', required: false, nullable: true })
    public broker: Insurer;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Insurer', required: false, nullable: true })
    public carrier: Insurer;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public createdBy: User;
    @Prop({ type: Boolean, default: false })
    public deleted: boolean;
    @Prop()
    public description: string;
    @Prop({ type: CoverageDetailsSchema })
    public details: CoverageDetails;
    @Prop({ type: [SaleItemVersionSchema], required: false })
    public history: SaleItemVersion[]
    @Prop({ type: Boolean, default: true })
    public includeInCommissions: boolean;
    @Prop({ type: Boolean, required: false })
    public isNonCommissionablePremium: boolean;
    @Prop()
    public lineOfBusiness: string;
    @Prop()
    public name: string;
    @Prop({ required: false, default: () => nanoid(6) })
    public nonCommissionableReferenceId: string;
    @Prop()
    public premium: number;
    @Prop()
    public deductible: number;
    @Prop()
    public compensation: number;
    @Prop()
    public limit: number;
    @Prop()
    public product: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public seller: User;
    @Prop()
    public status: string;
    @Prop()
    public type: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;

    @Prop({ type: [CohortSchema] })
    public cohorts: Cohort[];
    @Prop({ type: [ContactInfoSchema], required: false, default: [] })
    public contacts: ContactInfo[];
    @Prop({ type: [VehicleInfoSchema], required: false, default: [] })
    public vehicles: VehicleInfo[];

    // #endregion Properties (21)
}

export const SaleItemSchema = SchemaFactory.createForClass(SaleItem);