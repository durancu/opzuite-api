import { split } from "lodash";

export const SystemSettingsDefaults =
{
  "security.ip_addresses.whitelist": [
    process.env.IP_WHITELIST ? split(process.env.IP_WHITELIST, ',') : []
  ],
  "security.ip_addresses.blacklist": [
    process.env.IP_BLACKLIST ? split(process.env.IP_BLACKLIST, ',') : []
  ]
}

export enum SystemSettingsEnum {
  SECURITY_IP_ADDRESSES_WHITELIST = "security.ip_addresses.whitelist",
}

export const getPropertyByPath = function (obj, path) {
  const splittedPath = path.split('.');
  const len = splittedPath.length;
  for (let i = 0; i < len; i++) {
    obj = obj[path[i]];
  }
  return obj;
};
