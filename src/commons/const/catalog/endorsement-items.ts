export const ENDORSEMENT_ITEMS = [
  {
    name: 'Fee / Tax',
    id: 'FEE_TAX',
    description: '',
    permissions: [],
    color: '#EF7A1A',
    defaultInitialStatus: {
      type: "TIP",
      amount: 0,
      endorsedAt: new Date(),
      description: "",
      seller: null,
      status: "COMPLETE",
    }
  },
  {
    name: 'Payable',
    id: 'PAYABLE',
    description: '',
    permissions: [],
    color: '#0054A3',
    defaultInitialStatus: {
      type: "TIP",
      amount: 0,
      endorsedAt: new Date(),
      description: "",
      seller: null,
      status: "COMPLETE",
    }
  },
  {
    name: 'Receivable',
    id: 'RECEIVABLE',
    description: '',
    permissions: [],
    color: '#741FEA',
    defaultInitialStatus: {
      type: "TIP",
      amount: 0,
      endorsedAt: new Date(),
      description: "",
      seller: null,
      status: "COMPLETE",
    }
  },
  {
    name: 'Agent Commission',
    id: 'AGENT_COMMISSION',
    description: '',
    permissions: [],
    color: '#D4A121',
    defaultInitialStatus: {
      type: "TIP",
      amount: 0,
      endorsedAt: new Date(),
      description: "",
      seller: null,
      status: "COMPLETE",
    }
  },
  {
    name: 'Agency Commission',
    id: 'AGENCY_COMMISSION',
    description: '',
    permissions: [],
    color: '#1A9E2B',
    defaultInitialStatus: {
      type: "TIP",
      amount: 0,
      endorsedAt: new Date(),
      description: "",
      seller: null,
      status: "COMPLETE",
    }
  },
  {
    name: 'Financing / Direct Bill',
    id: 'FINANCING_DIRECT_BILL',
    description: '',
    permissions: [],
    color: '#2BA193',
    defaultInitialStatus: {
      type: "TIP",
      amount: 0,
      endorsedAt: new Date(),
      description: "",
      seller: null,
      status: "COMPLETE",
    }
  },
];
