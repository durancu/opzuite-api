import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { BusinessInfo, BusinessInfoSchema } from './nested/business-info.schema';
import { Commission, CommissionSchema } from './nested/commission.schema';
import { ContactInfo, ContactInfoSchema } from './nested/contact-info.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Insurer extends Document {
    // #region Properties (13)

    @Prop({ type: BusinessInfoSchema })
    public business: BusinessInfo;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: [CommissionSchema] })
    public commissions: Commission[];
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public company: Company;
    @Prop({ type: ContactInfoSchema })
    public contact: ContactInfo;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public createdBy: User;
    @Prop()
    public email: string;
    @Prop()
    public name: string;
    @Prop()
    public phone: string;
    @Prop({ default: 'CARRIER' })
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;

    // #endregion Properties (13)
}

export const InsurerSchema = SchemaFactory.createForClass(Insurer);
InsurerSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });

