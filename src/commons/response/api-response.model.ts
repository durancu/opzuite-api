
interface ApiResponse {
  message: string,
  data: any,
  meta: any
}

export { ApiResponse };

