
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { DEFAULT_BUSINESS } from 'src/commons/const/defaults/business.default';
import { Company } from './company.schema';
import { BusinessInfo, BusinessInfoSchema } from './nested/business-info.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Location extends Document {
  // #region Properties (12)

  @Prop()
  public alias: string;
  @Prop({ type: BusinessInfoSchema, default: DEFAULT_BUSINESS })
  public business: BusinessInfo;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company', required: true })
  public company: Company;
  @Prop({ default: "USA" })
  public country: string;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public createdBy: User;
  @Prop({ required: false })
  public ipAddress: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
  public manager: User;
  @Prop()
  public payFrequency: string;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
  public updatedBy: User;

  // #endregion Properties (12)
}

export const LocationSchema = SchemaFactory.createForClass(Location);

function nameGetHook(): string {
  return `${this.business.name}`;
}
LocationSchema.virtual('name').get(nameGetHook);

LocationSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });

export { nameGetHook };
