//import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
//import * as patchHistory from 'mongoose-patch-history';
import { nanoid } from 'nanoid';
import { Company } from './company.schema';
import { File } from './file.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true }, discriminatorKey: 'kind' })
export class CertificateMasterTemplate extends Document {
    // #region Properties (11)

    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public company: Company;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public createdBy: User;
    @Prop()
    public description: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'File' })
    public file: File;
    @Prop()
    public name: string;
    @Prop()
    public revisionNumber: string;
    @Prop()
    public shortName: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public updatedBy: User;

    // #endregion Properties (11)
}

export const CertificateMasterTemplateSchema = SchemaFactory.createForClass(CertificateMasterTemplate);

CertificateMasterTemplateSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
