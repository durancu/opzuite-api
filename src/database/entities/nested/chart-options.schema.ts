import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class ChartOptions extends Document {
  // #region Properties (7)

  @Prop()
  public cssClasses: string;
  @Prop()
  public height: number;
  @Prop({  default: 'success' })
  public primaryColor: string;
  @Prop({  default: 'info' })
  public secondaryColor: string;
  @Prop({  default: 'circle' })
  public symbolShape: string;
  @Prop({  default: 'bar' })
  public type: string;
  @Prop()
  public width: number;

  // #endregion Properties (7)
}

export const ChartOptionsSchema = SchemaFactory.createForClass(ChartOptions);
