export const PERMIT_ENDORSEMENT_TYPES = [
  {
    name: 'Base Incorporation/Application Fees',
    id: 'INITIAL_BASE_PROCESSING_FEES',
    type: 'PREMIUM',
  },
  {
    name: 'Other Application Fees',
    id: 'OTHER',
    type: 'PREMIUM',
  },
];
