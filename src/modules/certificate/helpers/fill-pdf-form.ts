import { PDFCheckBox, PDFDocument, PDFField, PDFTextField } from 'pdf-lib';

import { PdfCheckBox } from 'src/commons/enum/pdf-checkbox.enum';
import { PdfInput } from '../interfaces/pdf-input.interface';

export class PdfFormFiller {
    // #region Public Methods (1)

    public async processAndCombinePdfForms(pdfInputs: PdfInput[]): Promise<Buffer> {
        const filledPdfBuffers: Buffer[] = [];

        for (const pdfInput of pdfInputs) {
            const filledPdfBuffer = await this.fillPdfForm(pdfInput);
            filledPdfBuffers.push(filledPdfBuffer);
        }

        return this.combinePdfBuffers(filledPdfBuffers);
    }

    // #endregion Public Methods (1)

    // #region Private Methods (5)

    private async combinePdfBuffers(pdfBuffers: Buffer[]): Promise<Buffer> {
        const combinedPdfDoc = await PDFDocument.create();

        for (const pdfBuffer of pdfBuffers) {
            const pdfDoc = await PDFDocument.load(pdfBuffer);
            const pageCount = pdfDoc.getPageCount();
            const copiedPages = await combinedPdfDoc.copyPages(pdfDoc, Array.from({ length: pageCount }, (_, i) => i));
            copiedPages.forEach((page) => combinedPdfDoc.addPage(page));
        }

        const combinedPdfBytes = await combinedPdfDoc.save();
        return Buffer.from(combinedPdfBytes);
    }

    private async fillCheckBox(field: PDFCheckBox, value: string): Promise<void> {
        if (value === PdfCheckBox.ON) {
            field.check();
        }
    }

    private async fillFormField(field: PDFField, value: any): Promise<void> {
        if (field instanceof PDFTextField) {
            await this.fillTextField(field, value);
        } else if (field instanceof PDFCheckBox) {
            await this.fillCheckBox(field, value);
        } else {
            console.log('Error: Non Defined PDF Field ', field, value);
        }
    }

    private async fillPdfForm(pdfInput: PdfInput): Promise<Buffer> {
        const { inputPdfBuffer, template } = pdfInput;

        // Cargar el PDF
        const pdfDoc = await PDFDocument.load(inputPdfBuffer);

        // Obtener el formulario
        const form = pdfDoc.getForm();

        const fieldNames: string[] = template.getFieldNames();

        // Rellenar los campos del formulario
        for (const fieldName of fieldNames) {
            const field = form.getField(fieldName);

            if (field) {
                await this.fillFormField(field, template.getField(fieldName));
            } else {
                console.log('Invalid field:', field);
            }
        }

        // Finalizar el formulario
        form.flatten();

        const outputPdfBytes = await pdfDoc.save();

        return Buffer.from(outputPdfBytes);
    }

    private async fillTextField(field: PDFTextField, value: string): Promise<void> {
        field.enableMultiline();
        field.setText(value);
        field.setFontSize(7); // Tamaño de letra en puntos
    }

    // #endregion Private Methods (5)
}