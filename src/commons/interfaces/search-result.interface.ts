export interface SearchResult {
    entities: any[];
    totalCount: number;
    page?: number;
    limit?: number;
}