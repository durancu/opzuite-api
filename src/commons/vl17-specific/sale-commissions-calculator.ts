import { PayStub } from 'src/database/entities/nested/pay-stub.schema';
import { Payroll } from 'src/database/entities/payroll.schema';
import { PayrollDto } from 'src/dto/payroll/payroll.dto';
import { RoleCategories } from '../enum/role-categories.enum';
import {
  CommissionPlan,
  SALE_COMMISSION_PLANS,
  SALE_COMMISSION_PLANS_BY_ROLE
} from './sale-commission-plans';

export const PERCENT_OF_SALES_FIXED = 'PERCENT_OF_SALES_FIXED';
export const CASH_AFTER_MEETING_PERSONAL_SALES_TARGET =
  'CASH_AFTER_MEETING_PERSONAL_SALES_TARGET';
export const CASH_BY_COUNTRY_EMPLOYEES_COUNT =
  'CASH_BY_COUNTRY_EMPLOYEES_COUNT';
export const CASH_AFTER_COUNTRY_EMPLOYEES_SALES =
  'CASH_AFTER_COUNTRY_EMPLOYEES_SALES';
export const PERCENT_AFTER_OFFICE_EMPLOYEES_SALES =
  'PERCENT_AFTER_OFFICE_EMPLOYEES_SALES';
export const PERCENT_OF_SALES_BY_RANGE = 'PERCENT_OF_SALES_BY_RANGE';
export const PERCENT_AFTER_MEETING_PERSONAL_SALES_TARGET =
  'PERCENT_AFTER_MEETING_PERSONAL_SALES_TARGET';

export const DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE = 50000;
export const DEFAULT_FEE_BONUS_PERCENT = 0.3;
export const DEFAULT_TIP_BONUS_PERCENT = 1;
export const DEFAULT_PERMIT_BONUS_PERCENT = 0.2;
export const DEFAULT_SALES_COMMISION_PERCENT = 0.01;
export const DEFAULT_AFTER_TARGET_COMMISSION = 100;
export const DEFAULT_EMPLOYEES_COUNT_TARGET = 20;
export const DEFAULT_EMPLOYEES_COUNT_COMMISSION = 400;
export const DEFAULT_LOCATION_TOTAL_SALES_BONUS_PERCENT = 0.005;
export const DEFAULT_25K_BONUS_PERCENT = 0.005;
export const DEFAULT_50K_PLUS_BONUS_PERCENT = 0.01;
export const DEFAULT_TRAINEE_CASH_BONUS_AFTER_SALES = 50;
export const DEFAULT_PERCENT_SCALES = [
  {
    min: 0,
    max: 25000,
    percentCommission: 0,
  },
  {
    min: 25000,
    max: 50000,
    percentCommission: 0.5,
  },
  {
    min: 50000,
    percentCommissionRanges: [1, 1.5],
  },
];

export class SaleCommissionsCalculator {
  public static calculateEmployeeCommissions = (
    stub: Partial<PayStub>,
    payroll: PayrollDto,
  ) => {
    let plans: any[] = [];
    if (
      SALE_COMMISSION_PLANS_BY_ROLE[stub.employeeCountry] &&
      SALE_COMMISSION_PLANS_BY_ROLE[stub.employeeCountry][stub.employeeCategory]
    ) {
      plans =
        SALE_COMMISSION_PLANS_BY_ROLE[stub.employeeCountry][
        stub.employeeCategory
        ];
    }

    const commissionsArray: any[] = plans.map((plan: CommissionPlan) => {
      const stubPlan = SALE_COMMISSION_PLANS.find(
        (commissionPlan: CommissionPlan) => commissionPlan.id === plan.id,
      );
      return (
        SaleCommissionsCalculator.calculateCommissionByPlan(
          stub,
          stubPlan,
          payroll,
        ) || 0
      );
    });

    return commissionsArray.reduce((a, b) => a + b, 0);
  };

  public static calculateCommissionByPlan = (
    stub: Partial<PayStub>,
    plan: CommissionPlan,
    payroll: PayrollDto,
  ) => {
    let percent = null;
    let cash = null;
    let target = null;
    let targetByEmployee = null;
    const ranges = null;

    switch (plan.id) {
      case PERCENT_OF_SALES_FIXED:
        percent = plan.percentCommission;
        return SaleCommissionsCalculator.percentOfSalesFixed(
          plan.percentCommission,
          stub.totalPremium,
        );

      case CASH_AFTER_MEETING_PERSONAL_SALES_TARGET:
        target = plan.target;
        cash = plan.cashCommission;
        return SaleCommissionsCalculator.cashBonusAfterMeetSalesTarget(
          stub.totalPremium,
          plan.cashCommission,
          plan.target,
        );

      case CASH_BY_COUNTRY_EMPLOYEES_COUNT:
        cash = plan.cashCommission;
        target = plan.target;
        return SaleCommissionsCalculator.cashBonusAfterEmployeesCount(
          payroll.payStubs.length || 0,
          plan.target,
          plan.cashCommission,
        );

      case CASH_AFTER_COUNTRY_EMPLOYEES_SALES:
        cash = plan.cashCommission;
        targetByEmployee = plan.targetByEmployee;
        return SaleCommissionsCalculator.cashBonusAfterEmployeesTotalSales(
          payroll.totalPremium,
          (plan.targetByEmployee || 0) * (payroll.payStubs.length || 0),
          plan.cashCommission,
        );

      case PERCENT_AFTER_OFFICE_EMPLOYEES_SALES:
        targetByEmployee = plan.targetByEmployee;
        percent = plan.percentCommission;
        return SaleCommissionsCalculator.locationSalesBonusAfterTarget(
          payroll.totalPremium,
          payroll.payStubs.length,
          stub.totalPremium,
          plan.targetByEmployee,
          plan.percentCommission,
        );

      case PERCENT_OF_SALES_BY_RANGE:
        return SaleCommissionsCalculator.variablePercentCommissionByThreeScales(
          stub.totalPremium,
          plan.ranges,
        );
      case PERCENT_AFTER_MEETING_PERSONAL_SALES_TARGET:
        return SaleCommissionsCalculator.percentBonusAfterMeetSalesTarget(
          stub.totalPremium,
          plan.percentCommission,
          plan.targetByEmployee,
        );
      default:
        return 0;
    }
  };

  /**
   * Calculates total bonus as sum of Fees + Permits + Tips
   *
   * @param  {number} feesAmount
   * @param  {number} permitsAmount
   * @param  {number} tipsAmount
   */
  public static totalNonPremiumBonus = (
    feesAmount: number,
    permitsAmount: number,
  ) => {
    return (
      SaleCommissionsCalculator.percentBonusPerFees(feesAmount) +
      SaleCommissionsCalculator.percentBonusPerPermits(permitsAmount)
    );
  };

  /**
   * Calculates bonus according to Fee sales
   *
   * @param  {number} feesAmount
   * @param  {number=DEFAULT_FEE_BONUS_PERCENT} commisionPercent
   */
  public static percentBonusPerFees = (
    feesAmount: number,
    commisionPercent: number = DEFAULT_FEE_BONUS_PERCENT,
  ) => {
    return feesAmount * commisionPercent;
  };

  /**
   * Calculates bonus according to Permit sales
   *
   * @param  {number} permitsAmount
   * @param  {number=DEFAULT_PERMIT_BONUS_PERCENT} commisionPercent
   */
  public static percentBonusPerPermits = (
    permitsAmount: number,
    commisionPercent: number = DEFAULT_PERMIT_BONUS_PERCENT,
  ) => {
    return permitsAmount * commisionPercent;
  };

  /**
   * Calculates bonus according to Tips sales
   *
   * @param  {number} tipsAmount
   * @param  {number=DEFAULT_TIP_BONUS_PERCENT} commisionPercent
   * @returns number
   */
  public static percentBonusPerTips = (
    tipsAmount: number,
    commisionPercent: number = DEFAULT_TIP_BONUS_PERCENT,
  ) => {
    return tipsAmount * commisionPercent;
  };

  //PERCENTAGE OF SALES
  /**
   * Calculates bonus based on a fixed percentage of total sales
   *
   * @param  {number} totalSales
   * @param  {number=DEFAULT_SALES_COMMISION_PERCENT} percent
   * @returns number
   */
  public static percentOfSalesFixed = (
    totalSales: number,
    percent: number = DEFAULT_SALES_COMMISION_PERCENT,
  ) => {
    return totalSales * percent;
  };

  //COMMISSION AFTER TARGET
  /**
   * Calculates
   *
   * @param  {number} totalSales
   * @param  {number=DEFAULT_AFTER_TARGET_COMMISSION} commission
   * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} target
   */
  public static cashBonusAfterMeetSalesTarget = (
    totalSales: number,
    commission: number = DEFAULT_AFTER_TARGET_COMMISSION,
    monthSalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
  ) => {
    return totalSales > monthSalesTarget ? commission : 0;
  };

  //COMMISSION AFTER TARGET
  /**
   * Calculates
   *
   * @param  {number} totalSales
   * @param  {number=DEFAULT_AFTER_TARGET_COMMISSION} commission
   * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} target
   */
  public static percentBonusAfterMeetSalesTarget = (
    totalSales: number,
    percent: number = DEFAULT_SALES_COMMISION_PERCENT,
    monthSalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
  ) => {
    return totalSales > monthSalesTarget ? totalSales * percent : 0;
  };

  public static cashBonusAfterEmployeesCount = (
    employeesCount: number,
    employeesTarget: number = DEFAULT_EMPLOYEES_COUNT_TARGET,
    employeesTargetCommission: number = DEFAULT_EMPLOYEES_COUNT_COMMISSION,
  ) => {
    return employeesCount >= employeesTarget ? employeesTargetCommission : 0;
  };

  public static cashBonusAfterEmployeesTotalSales = (
    officeTotalSales: number,
    officeTotalSalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
    cashCommission: number = DEFAULT_AFTER_TARGET_COMMISSION,
  ) => {
    return officeTotalSales >= officeTotalSalesTarget ? cashCommission : 0;
  };

  /**
   * Calculates a bonus paid to location managers, according to the formula: 
   * TOTAL OFFICE SALES > GOAL (ej: 50K) * EMPLOYEES COUNT - 
  
   * @param  {number} locationTotalSales
   * @param  {number} locationEmployeesCount
   * @param  {number} managerTotalSales
   * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} expectedMonthSalesBySeller
   * @param  {number=DEFAULT_LOCATION_TOTAL_SALES_BONUS_PERCENT} salesBonusPercent
   * @returns number
   */

  static locationSalesBonusAfterTarget = (
    locationTotalSales: number,
    locationEmployeesCount: number,
    managerTotalSales: number,
    expectedMonthSalesBySeller: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
    salesBonusPercent: number = DEFAULT_LOCATION_TOTAL_SALES_BONUS_PERCENT,
  ) => {
    return locationTotalSales >
      expectedMonthSalesBySeller * locationEmployeesCount
      ? ((locationTotalSales - managerTotalSales) * salesBonusPercent) / 100
      : 0;
  };

  /**

  RANGES EXAMPLE:

  ranges: [
      {
        min: 0,
        max: 25000,
        percentCommission: 0, //0% as sales commission
      },
      {
        min: 25000,
        max: 50000,
        percentCommission: 0.5, //0.5% for all sales
      },
      {
        min: 50000,
        percentCommissionRanges: [1, 1.5], //1.5% for over 50K sales, 1% for sales below 50K
      },
    ],

   *
   * @param  {number} employeeTotalSales
   * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} employeeMonthlySalesTarget
   */

  static variablePercentCommissionByThreeScales = (
    employeeMonthTotalSales = 0,
    scaleRanges: any[] = DEFAULT_PERCENT_SCALES,
  ) => {
    //Ej: Si totalVentas >= 50K -> 1.5% de 30K + 1% de 50K
    if (employeeMonthTotalSales >= (scaleRanges[2].min || 0)) {
      const commissionScale3 =
        ((employeeMonthTotalSales - scaleRanges[2].min) *
          scaleRanges[2].percentCommissionRanges[1]) /
        100;
      const commissionScale2 =
        (scaleRanges[2].min * scaleRanges[2].percentCommissionRanges[0]) / 100;
      return commissionScale3 + commissionScale2;
    }
    //Ej: Si totalVentas >= 25K (pero no mayor que 50K)
    else if (employeeMonthTotalSales >= (scaleRanges[1].min || 0)) {
      return (employeeMonthTotalSales * scaleRanges[1].percentCommission) / 100;
    } else {
      //Ej: Si vende menos de 25K, no se lleva bonus
      return 0;
    }
  };

  /**
   * Calculates bonus according to employee total sales in last month.
   * Scales are described as the following table:
   *
   * Start   End     %
   * 0       25      0
   * 25+     50      0.005
   * 50+     100     0.01
   * 100+    150     0.01
   * 150+    200     0.01
   * 200+    250     0.01
   *
   * @param  {number} employeeTotalSales
   * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} employeeMonthlySalesTarget
   */

  static variablePercentSalesBonus = (
    employeeTotalSales: number,
    employeeMonthlySalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
  ) => {
    let percent = DEFAULT_SALES_COMMISION_PERCENT;

    if (
      employeeTotalSales > employeeMonthlySalesTarget / 2 &&
      employeeTotalSales < employeeMonthlySalesTarget
    ) {
      percent = DEFAULT_25K_BONUS_PERCENT;
    }

    return employeeTotalSales * percent;
  };

  public static saleCommissionByEmployeeCategory = (
    stub: PayStub,
    payroll: Partial<Payroll>,
  ) => {
    if (stub.employeeCountry && stub.employeeCountry !== 'USA') {
      return SaleCommissionsCalculator.salesBonusByEmployeeCategoryMexico(
        stub.employeeCategory,
        stub.totalPremium,
        0,
        payroll.payStubs.length,
        payroll.totalPremium,
      );
    }
  };

  /**
   * Calculates Salary Bonus by Role according to Mexico bonus calculation rules
   *
   * @param  {string} role
   * @param  {number} employeeTotalSales
   * @param  {number} extraBonus
   * @param  {number} officeEmployees
   * @param  {number} officeTotalSales
   */
  public static salesBonusByEmployeeCategoryMexico = (
    employeeCategory: string,
    employeeTotalSales: number,
    nonPremiumBonus: number,
    officeEmployees: number,
    officeTotalSales: number,
  ) => {
    let premiumBonus = 0;
    switch (employeeCategory) {
      case RoleCategories.CERTIFICATES:
        premiumBonus =
          SaleCommissionsCalculator.percentOfSalesFixed(employeeTotalSales);
        +SaleCommissionsCalculator.cashBonusAfterMeetSalesTarget(
          employeeTotalSales,
        );
        break;

      case RoleCategories.ENDORSEMENTS:
        premiumBonus =
          SaleCommissionsCalculator.percentOfSalesFixed(employeeTotalSales);
        break;

      case RoleCategories.COUNTRY_MANAGER:
        premiumBonus =
          SaleCommissionsCalculator.cashBonusAfterEmployeesCount(
            officeEmployees,
          ) +
          SaleCommissionsCalculator.cashBonusAfterEmployeesTotalSales(
            officeTotalSales,
          );
        break;

      case RoleCategories.LOCATION_MANAGER:
        premiumBonus =
          SaleCommissionsCalculator.variablePercentSalesBonus(
            employeeTotalSales,
          ) +
          SaleCommissionsCalculator.locationSalesBonusAfterTarget(
            officeTotalSales,
            officeEmployees,
            employeeTotalSales,
          );
        break;

      case RoleCategories.SELLER:
      case RoleCategories.COMPANY_MANAGER:
        premiumBonus =
          SaleCommissionsCalculator.variablePercentSalesBonus(
            employeeTotalSales,
          );
        break;

      case RoleCategories.TRAINEE:
        premiumBonus =
          SaleCommissionsCalculator.percentBonusAfterMeetSalesTarget(
            DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
          ) + DEFAULT_TRAINEE_CASH_BONUS_AFTER_SALES;

        break;
      case RoleCategories.ADMIN:
      default:
        premiumBonus = 0;
    }

    return premiumBonus + nonPremiumBonus;
  };
}
