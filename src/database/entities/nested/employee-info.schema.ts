import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { nanoid } from 'nanoid';
import { Location, LocationSchema } from '../location.schema';

@Schema()
export class EmployeeInfo extends Document {
  // #region Properties (17)

  @Prop({ type: [SchemaTypes.String], default: [] })
  public allowedCountries: string[];
  @Prop({ type: [LocationSchema], default: [] })
  public allowedLocations: Location[];
  @Prop({ default: 'MONTHLY' })
  public bonusFrequency: string;
  @Prop()
  public category: string;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop()
  public endedAt: string;
  @Prop()
  public hourlyRate: number;
  @Prop()
  public overtimeAuthorized: boolean;
  @Prop()
  public overtimePayRate: number;
  @Prop({ default: 'MONTHLY' })
  public payFrequency: string;
  @Prop({ default: 0 })
  public payRate: number;
  @Prop()
  public position: string;
  @Prop()
  public salaryFormula: string;
  @Prop({ type: [SchemaTypes.Map], default: [] })
  public saleCommissionPlans: any;
  @Prop()
  public startedAt: Date;
  @Prop()
  public workPrimaryPhone: string;
  @Prop()
  public workPrimaryPhoneExtension: string;

  // #endregion Properties (17)
}

export const EmployeeInfoSchema = SchemaFactory.createForClass(EmployeeInfo);