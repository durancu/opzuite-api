import { PartialType } from '@nestjs/mapped-types';
import { IsOptional } from 'class-validator';
import { CreateReportDto } from './create-report.dto';

export class UpdateReportDto extends PartialType(CreateReportDto) {
  // #region Properties (2)

  @IsOptional()
  public _id?: string;
  @IsOptional()
  public id?: string;

  // #endregion Properties (2)
}
