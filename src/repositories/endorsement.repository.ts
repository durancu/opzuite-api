import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Endorsement } from 'src/database/entities/endorsement.schema';

export class EndorsementRepository {
  // #region Constructors (1)

  constructor(@InjectModel(Endorsement.name) private readonly endorsementModel: Model<Endorsement>) { }

  // #endregion Constructors (1)

  // #region Public Methods (6)

  public async deleteSaleEndorsements(saleId: string): Promise<any> {
    return await this.endorsementModel
      .deleteMany({ sale: saleId })
      .exec();
  }

  public async getSaleEndorsements(saleId: string): Promise<Partial<Endorsement>[]> {
    return await this.endorsementModel
      .find({ sale: saleId })
      .exec();
  }

  public async deleteAll(): Promise<any> {
    return await this.endorsementModel.deleteMany({}).exec();
  }


  // #endregion Public Methods (6)
}