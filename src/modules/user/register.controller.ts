
/* @Controller('register')
export class RegisterController {
    constructor(private userService: UserService) { }

    @Post()
    @UseGuards(JwtAuthGuard, RolesGuard)
    @HttpCode(201)
    @UseFilters( MongoFilter)
    register(
        @Body() createUserDto: CreateUserDto,
        @Res() res: Response): Observable<Response> {
        const email = createUserDto.email;

        return this.userService.exists('email',email).pipe(
            mergeMap(exists => {
                if (exists) {
                    throw new ConflictException(`email:${email} exists already`)
                }
                else {
                    const email = createUserDto.email;
                    return this.userService..exists('email',email).pipe(
                        mergeMap(exists => {
                            if (exists) {
                                throw new ConflictException(`email:${email} exists already`)
                            }
                            else {
                                return this.userService.createUser(createUserDto).pipe(
                                    map(user =>
                                        res.location('/users/' + user.id)
                                            .status(201)
                                            .send()
                                    )
                                );
                            }
                        })
                    );
                }
            })
        );
    }
} */
