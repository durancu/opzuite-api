import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Scope,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { Location } from '../../database/entities/location.schema';
import { CreateLocationDto } from '../../dto/location/create-location.dto';
import { UpdateLocationDto } from '../../dto/location/update-location.dto';
import { JwtAuthGuard } from '../auth/guard/jwt-auth.guard';
import { LocationService } from './location.service';

@ApiTags('Location')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'locations', scope: Scope.REQUEST })
export class LocationController {
  constructor(
    private locationService: LocationService,
  ) { }

  @Get()
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.LOCATIONS_READ)
  async getAllLocations(): Promise<Location[]> {
    return await this.locationService.findAll();
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.LOCATIONS_READ)
  async getLocationByCode(@Param('code') code: string): Promise<Location> {
    return await this.locationService.findByCode(code);
  }

  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.LOCATIONS_CREATE)
  async createLocation(
    @Body() location: CreateLocationDto,
  ): Promise<Location> {
    return await this.locationService.save(location);
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.LOCATIONS_UPDATE)
  async updateLocation(
    @Param('code') code: string,
    @Body() location: UpdateLocationDto,
  ): Promise<Location> {
    return await this.locationService.update(code, location);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.LOCATIONS_DELETE)
  async deleteLocationByCode(@Param('code') code: string): Promise<Location> {
    return await this.locationService.deleteByCode(code);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.LOCATIONS_READ)
  async searchLocations(@Body() query: any): Promise<any> {
    return await this.locationService.search(query.queryParams);
  }
}
