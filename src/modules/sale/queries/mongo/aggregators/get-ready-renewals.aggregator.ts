import { COVERAGES_TYPES } from "src/commons/const/catalog/coverages-types";

export const MILLISECONDS_PER_MONTH = 2592000000;

export const buildPoliciesReadyToRenewAggregator = (monthsToExpiration = 3, conditions: any) => {

    let aggregator = [];

    aggregator = aggregator.concat({
        '$match': {
            'type': 'POLICY'
        }
    })

    aggregator = aggregator.concat(unwrapTableRelationship('customers', 'customer'));

    aggregator = aggregator.concat({
        '$unwind': {
            'path': '$items',
            'preserveNullAndEmptyArrays': false
        }
    });

    aggregator = aggregator.concat(unwrapTableRelationship('users', 'seller'));
    aggregator = aggregator.concat(unwrapTableRelationship('users', 'items.seller'));
    aggregator = aggregator.concat(unwrapTableRelationship('insurers', 'items.broker'));
    aggregator = aggregator.concat(unwrapTableRelationship('insurers', 'items.carrier'));
    aggregator = aggregator.concat(unwrapTableRelationship('locations', 'location'));

    if (conditions) {
        aggregator = aggregator.concat({
            '$match': conditions
        })
    }

    const otherClauses: any[] = [
        {
            '$project': {
                'id': {
                    id: '$_id',
                    code: '$code',
                    deadline: {
                        '$add': ['$$NOW', parseInt(monthsToExpiration.toString()) * MILLISECONDS_PER_MONTH]
                    },
                    customerId: '$customer._id',
                    customerCode: '$customer.code',
                    customerName: '$customer.name',
                    customerEmail: '$customer.email',
                    customerDOT: '$customer.business.usDOT',
                    customerPhone: {
                        '$ifNull': [
                            '$customer.phone', '$customer.mobilePhone'
                        ]
                    },
                    totalPremium: '$totalPremium',
                    locationName: { $ifNull: ['$location.business.name', ''] },
                    locationCode: { $ifNull: ['$location.code', ''] },
                    sellerId: '$seller._id',
                    sellerName: { $ifNull: ['$seller.name', ''] },
                    sellerCode: { $ifNull: ['$seller.code', ''] },
                    agentEmail: '$seller.email',
                    agentPhone: {
                        '$ifNull': [
                            '$seller.phone', '$seller.mobilePhone'
                        ]
                    },
                    status: '$status',
                    number: '$number',
                    effectiveAt: '$effectiveAt',
                    expiresAt: '$expiresAt',
                    wasRenewed: {
                        '$ifNull': [
                            '$wasRenewed', false
                        ]
                    },
                    seller: '$seller',
                },
                wasRenewed: {
                    '$ifNull': [
                        '$wasRenewed', false
                    ]
                },
                seller: {
                    '$ifNull': [
                        '$items.seller', '$seller'
                    ]
                },
                lineOfBusiness: '$items.lineOfBusiness',
                product: {
                    $function: {
                        body: function (product: string, coverages: any[]): any {
                            let foundProduct = {};
                            if (product && product.length > 0) {
                                foundProduct = coverages.find(
                                    (type: any) => type['id'] === product,
                                );
                            }
                            return foundProduct;
                        },
                        args: ['$items.product', COVERAGES_TYPES],
                        lang: 'js',
                    },
                },
                description: '$items.description',
                brokerName: '$items.broker.name',
                carrierName: '$items.carrier.name',
                provider: {
                    '$cond': [
                        {
                            '$eq': [
                                '$items.broker.name', '$items.carrier.name'
                            ]
                        }, '$items.broker.name', {
                            '$concat': [
                                '$items.broker.name', '/', '$items.carrier.name'
                            ]
                        }
                    ]
                },
                items: '$items'
            }
        },
        //TODO: Add aggregator to set expiresAt = effectiveAt + 1 year (if expiresAt is null, and effectiveAt is not null)
        {
            '$addFields': {
                'aboutToExpire': {
                    '$and': [
                        {
                            '$gte': [
                                '$id.expiresAt', '$$NOW'
                            ]
                        }, {
                            '$lte': [
                                '$id.expiresAt', '$id.deadline'
                            ]
                        }
                    ]
                },
            }
        }, {
            '$match': {
                'aboutToExpire': true,
                'wasRenewed': false,
                'id.status': { $in: ["ACTIVE", "PENDING"] }
            }
        }, {
            '$group': {
                '_id': '$id',
                'policies': {
                    '$push': {
                        items: '$items',
                        agentId: '$seller._id',
                        agentName: '$seller.name',
                        agentPhone: {
                            '$ifNull': [
                                '$items.phone', '$seller.mobilePhone'
                            ]
                        },
                        product: '$product',
                        agentEmail: '$seller.email',
                        description: '$description',
                        lineOfBusiness: '$lineOfBusiness',
                        brokerName: '$brokerName',
                        carrierName: '$carrierName',
                        provider: '$provider',
                    }
                }
            }
        }, {
            '$unset': 'policies.items'
        }, {
            '$project': {
                _id: "$_id.id",
                code: "$_id.code",
                deadline: "$_id.deadline",
                customerId: "$_id.customerId",
                customerCode: "$_id.customerCode",
                customerName: "$_id.customerName",
                customerEmail: "$_id.customerEmail",
                customerDOT: "$_id.customerDOT",
                customerPhone: "$_id.customerPhone",
                agentId: "$_id.agentId",
                agentName: "$_id.agentName",
                agentEmail: "$_id.agentEmail",
                agentPhone: "$_id.agentPhone",
                items: "$policies",
                expiresAt: "$_id.expiresAt",
                status: "$_id.status",
                sellerId: "$_id.sellerId",
                locationName: { $ifNull: ['$_id.locationName', ''] },
                locationCode: { $ifNull: ['$_id.locationCode', ''] },
                sellerName: { $ifNull: ['$_id.sellerName', ''] },
                sellerCode: { $ifNull: ['$_id.sellerCode', ''] },
                number: "$_id.number",
                totalPremium: '$_id.totalPremium',
            }
        }
    ];

    return [...aggregator, ...otherClauses];
}

export const unwrapTableRelationship = (table: string, localKey: string, foreignKey = '_id', preserveNullAndEmptyArrays = true) => (
    [
        {
            '$unwind': {
                'path': `$${localKey}`,
                'preserveNullAndEmptyArrays': preserveNullAndEmptyArrays
            }
        }, {
            '$lookup': {
                'from': table,
                'localField': `${localKey}`,
                'foreignField': foreignKey,
                'as': `${localKey}`
            }
        }, {
            '$unwind': {
                'path': `$${localKey}`,
                'preserveNullAndEmptyArrays': preserveNullAndEmptyArrays
            }
        }
    ]
)