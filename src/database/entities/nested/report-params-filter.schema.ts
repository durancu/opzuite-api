import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';

@Schema()
export class ReportParamsFilter extends Document {
    // #region Properties (3)

    @Prop()
    public field: string;
    @Prop()
    public operator: string;
    @Prop({ type: SchemaTypes.Mixed })
    public value: any;

    // #endregion Properties (3)
}

export const ReportParamsFilterSchema = SchemaFactory.createForClass(ReportParamsFilter);