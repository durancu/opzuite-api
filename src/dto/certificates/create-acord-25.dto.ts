import { CreateCertificateDto } from "./create-certificate.dto";

class CreateAcord25Dto extends CreateCertificateDto {
    blanketAdditionalInterest?: any[];
    blanketWaiverOfSubrogation?: any[];
    toUseInHOAUnit?: boolean;
}

export { CreateAcord25Dto };
