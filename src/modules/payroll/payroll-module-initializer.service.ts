import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class PayrollModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Payroll Module) is initialized...');
  }
}
