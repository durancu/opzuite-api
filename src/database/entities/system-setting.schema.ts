import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class SystemSetting extends Document {
  // #region Properties (7)

  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
public createdBy: User;
  @Prop()
  public description: string;
  @Prop({ unique: true, required: true, dropDups: true })
  public key: string;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
public updatedBy: User;
  @Prop({ type: SchemaTypes.Mixed })
  public value: any;

  // #endregion Properties (7)
}

export const SystemSettingSchema = SchemaFactory.createForClass(SystemSetting);

SystemSettingSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
