import {
  BadRequestException,
  Body,
  ConflictException,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  HttpCode,
  Param,
  Post,
  Put,
  Query,
  Req,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { Request } from 'express';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { RoleCategories } from 'src/commons/enum/role-categories.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { hasSuperAdminRoleAccess } from 'src/commons/lib/role-functions';
import { ParseObjectIdPipe } from 'src/commons/pipe/parse-object-id.pipe';
import { Company } from 'src/database/entities/company.schema';
import { Role } from 'src/database/entities/role.schema';
import { User } from 'src/database/entities/user.schema';
import { CreateUserDto } from 'src/dto/user/create-user.dto';
import { UpdateUserDto } from 'src/dto/user/update-user.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { JwtAuthGuard } from 'src/modules/auth/guard/jwt-auth.guard';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { FileService } from '../file/file.service';
import { RoleService } from './role.service';
import { UserPopulateOptions, UserService } from './user.service';

@ApiTags('User')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: '/users' })
export class UserController {
  // #region Constructors (1)

  constructor(
    private userService: UserService,
    private roleService: RoleService,
    private fileService: FileService
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (8)

  /* @Post('/batch-create')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS)
  public async batchCreateUsers(
    @Req() req: Request,
    @Body() users: UserDto[],
  ): Promise<any> {
    const user: Partial<User> = req.user;
    return await this.userService.batchCreate(users, user);
  } */

  @Post()
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_CREATE)
  public async createUser(
    @Req() req: Request,
    @Body() createUserDto: CreateUserDto,
  ): Promise<User> {
    const email = createUserDto.email;

    const exists: boolean = await this.userService.exists('email', email);

    if (exists) {
      throw new ConflictException(`User with email:${email} exists already`);
    }

    if (createUserDto.role) {
      const newRole = await this.roleService.findById(createUserDto.role.toString());
      createUserDto['permissions'] = newRole.permissions;
    }
    return await this.userService.createUser(createUserDto, req.user);
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_DELETE)
  public async deleteUser(@Param('id', ParseObjectIdPipe) id: string): Promise<User> {
    return await this.userService.delete(id);
  }

  @Get()
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_READ)
  public async getAllUsers(): Promise<any> {
    const res = await this.userService.findAll();
    return {
      data: res,
    };
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_READ)
  public async getUser(
    @Param('id', ParseObjectIdPipe) id: string,
    @Query('withPassword', new DefaultValuePipe(false)) withPassword?: boolean,
    @Query('withSales', new DefaultValuePipe(false)) withSales?: boolean,
    @Query('withCompany', new DefaultValuePipe(false)) withCompany?: boolean,
    @Query('withLocation', new DefaultValuePipe(false)) withLocation?: boolean,
    @Query('withSupervisor', new DefaultValuePipe(false))
    withSupervisor?: boolean,
    @Query('withRole', new DefaultValuePipe(false)) withRole?: boolean,
  ): Promise<Partial<User>> {
    const populateOptions: UserPopulateOptions = {
      withPassword: false,
      withSales: !!withSales,
      withCompany: !!withCompany,
      withLocation: !!withLocation,
      withSupervisor: !!withSupervisor,
      withRole: !!withRole,
    }

    return await this.userService.findById(id, populateOptions);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_READ)
  public async searchUsers(@Body() query: any): Promise<any> {
    return await this.userService.search(query.queryParams);
  }

  @Put(':id')
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.USERS_UPDATE)
  public async updateUser(
    @Req() req: Request,
    @Param('id', ParseObjectIdPipe) id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<User> {
    return this.userService.updateUser(id, updateUserDto, req.user);
  }

  @Get('/employees')
  @HttpCode(200)
  @HasPermissions(PermissionType.MY_COMPANY, PermissionType.MY_COMPANY_READ)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  async getMyCompanyEmployees(
    @Req() req: Request,
  ): Promise<Company> {
    const user: any = req.user;
    const role: Partial<Role> = await this.roleService.findById(
      user.role,
    );

    if (!user.company && !hasSuperAdminRoleAccess(role))
      throw new BadRequestException(user.company);
    const entitiesFilter = { company: user.company, deleted: false };

    const employeesFilter = entitiesFilter;
    employeesFilter['employeeInfo.category'] = {
      $nin: [RoleCategories.SUPERADMIN, RoleCategories.ADMIN],
    };
    return await this.userService.getEmployees(employeesFilter);
  }

  // #endregion Public Methods (8)
}
