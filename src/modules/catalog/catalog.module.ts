import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { CertificateCoverTemplate, CertificateCoverTemplateSchema } from 'src/database/entities/certificate-cover-template.schema';
import { CertificateMasterTemplate, CertificateMasterTemplateSchema } from 'src/database/entities/certificate-master-template.schema';
import { Company, CompanySchema } from 'src/database/entities/company.schema';
import { Customer, CustomerSchema } from 'src/database/entities/customer.schema';
import { Insurer, InsurerSchema } from 'src/database/entities/insurer.schema';
import { Location, LocationSchema } from 'src/database/entities/location.schema';
import { Payroll, PayrollSchema } from 'src/database/entities/payroll.schema';
import { Report, ReportSchema } from 'src/database/entities/report.schema';
import { Role, RoleSchema } from 'src/database/entities/role.schema';
import { User, UserSchema } from 'src/database/entities/user.schema';
import { CertificateCoverService } from '../certificate/certificate-cover.service';
import { CertificateTemplateService } from '../certificate/certificate-template.service';
import { CertificateModule } from '../certificate/certificate.module';
import { CustomerModule } from '../customer/customer.module';
import { CustomerService } from '../customer/customer.service';
import { InsurerModule } from '../insurer/insurer.module';
import { InsurerService } from '../insurer/insurer.service';
import { LocationModule } from '../location/location.module';
import { LocationService } from '../location/location.service';
import { PayrollModule } from '../payroll/payroll.module';
import { PayrollService } from '../payroll/payroll.service';
import { ReportModule } from '../report/report.module';
import { ReportService } from '../report/report.service';
import { RoleService } from '../user/role.service';
import { UserModule } from '../user/user.module';
import { UserService } from '../user/user.service';
import { CatalogModuleInitializerService } from './catalog-module-initializer.service';
import { CatalogController } from './catalog.controller';
import { CatalogService } from './catalog.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Payroll.name, schema: PayrollSchema },
      { name: Location.name, schema: LocationSchema },
      { name: User.name, schema: UserSchema },
      { name: Role.name, schema: RoleSchema },
      { name: Insurer.name, schema: InsurerSchema },
      { name: Customer.name, schema: CustomerSchema },
      { name: Report.name, schema: ReportSchema },
      { name: Company.name, schema: CompanySchema },
      { name: CertificateMasterTemplate.name, schema: CertificateMasterTemplateSchema },
      { name: CertificateCoverTemplate.name, schema: CertificateCoverTemplateSchema },
    ]),
    UserModule,
    LocationModule,
    CustomerModule,
    InsurerModule,
    ReportModule,
    PayrollModule,
    CertificateModule
  ],
  controllers: [CatalogController],
  providers: [
    CatalogService,
    UserService,
    LocationService,
    CustomerService,
    InsurerService,
    ReportService,
    RoleService,
    CertificateTemplateService,
    CertificateCoverService,
    PayrollService,
    CatalogModuleInitializerService
  ],
  exports: [
    CatalogService
  ]
})
export class CatalogModule { }
