
import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from 'mongoose';

@Schema()
export class Address extends Document {
  // #region Properties (7)

  @Prop({ default: '' })
  public address1: string;
  @Prop({ default: '' })
  public address2: string;
  @Prop({ default: '' })
  public city: string;
  @Prop({ default: '' })
  public country: string;
  @Prop({ default: '' })
  public county: string;
  @Prop({ default: '' })
  public state: string;
  @Prop({ default: '' })
  public zip: string;

  // #endregion Properties (7)
}

export const AddressSchema = SchemaFactory.createForClass(Address);
