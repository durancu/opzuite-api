import { PartialType } from "@nestjs/mapped-types";
import { IsOptional } from "class-validator";
import { CreateFileDto } from "./create-file.dto";

export class UpdateFileDto extends PartialType(CreateFileDto) {
    // #region Properties (2)

    @IsOptional()
    public _id?: string;
    @IsOptional()
    public id?: string;

    // #endregion Properties (2)
}
