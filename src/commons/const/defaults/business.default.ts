export const DEFAULT_BUSINESS = {
    address: {
        address1: '',
        address2: '',
        city: '',
        state: '',
        country: 'USA',
        zip: '',
    },
    email: '',
    fax: '',
    industry: '',
    logo: '',
    name: '',
    otherPhones: [],
    primaryPhone: '',
    primaryPhoneExtension: '',
    secondaryPhone: '',
    secondaryPhoneExtension: '',
    sector: '', // can be: Financial, Technology, Healthcare, etc
    startedAt: '',
    type: '',
    website: '',
}