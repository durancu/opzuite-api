import { buildSearchTextQueries } from "src/commons/queries/mongo/helpers/query";

export function buildSearchConditions(queryParams: SearchQueryParams): Record<string, any> {
  const conditions: Record<string, any> = {
    $and: [{ deleted: false }],
  };

  if (queryParams && queryParams.filter) {
    const { filter } = queryParams;

    if (filter.type && filter.type.trim() !== '') {
      conditions.$and.push({ type: filter.type });
      delete filter['type'];
    }

    if (Object.keys(filter).length > 0) {
      // Add any other necessary filter conditions here
    }
  }

  if (queryParams && queryParams.searchText) {
    const searchColumns = queryParams.searchColumns && queryParams.searchColumns.length
      ? queryParams.searchColumns
      : ['email', 'name', 'phone'];

    const searchTextQueries = buildSearchTextQueries(
      queryParams.searchText,
      searchColumns,
    );

    if (searchTextQueries.length > 0) {
      conditions['$or'] = searchTextQueries;
    }
  }

  return conditions;
}