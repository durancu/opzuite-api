import { Injectable, OnModuleInit } from '@nestjs/common';

@Injectable()
export class SaleModuleInitializerService implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Sale Module) is initialized...');
  }
}
