import { CertificatePdfTemplate } from "../../../certificate-pdf-template";
import { acord25GenericFields } from "./acord-25-pdf-generic-fields";

export class Acord25PDFTemplate extends CertificatePdfTemplate {
    // #region Constructors (1)

    constructor() {
        super();
        this.fieldNames = acord25GenericFields;
    }

    // #endregion Constructors (1)
}