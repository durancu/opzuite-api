
// using Twilio SendGrid's v3 Node.js Library
// https://github.com/sendgrid/sendgrid-nodejs

import { Company } from "src/database/entities/company.schema";
import { User } from "src/database/entities/user.schema";

export const buildNotificationMessage = (templateId: string, user: Partial<User>, company: Partial<Company>, additionalData: any) => {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const sgMail = require('@sendgrid/mail');
  sgMail.setApiKey(process.env.SENDGRID_API_KEY);
  const msg = {
    to: user.email,
    from: process.env.EMAIL_FROM_ADDRESS,
    templateId: templateId,
    personalizations: [
      {
        to: user.email,
        dynamicTemplateData: {
          client_contact_url: `${process.env.APP_HOST_URL}/contact`,
          client_name: company.name,
          client_address: company.address.address1,
          client_city: company.address.city,
          client_state: company.address.state,
          client_zip: company.address.zip,
          client_url: company.website,
          developer_name: process.env.APP_TEAM_NAME,
          developer_url: process.env.APP_HOST_URL,
          developer_phone: process.env.APP_TEAM_PHONE,
          ...additionalData,
        },
      }
    ]
  };
  return msg;
}
