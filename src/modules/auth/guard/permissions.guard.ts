import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { can } from 'src/commons/lib/user-functions';
import { HAS_PERMISSIONS_KEY } from '../../../commons/const/auth.constants';
import { AuthenticatedRequest } from '../interface/authenticated-request.interface';

@Injectable()
export class PermissionsGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const permissionsToCheck = this.reflector.get<PermissionType[]>(
      HAS_PERMISSIONS_KEY,
      context.getHandler(),
    );

    const { user } = context
      .switchToHttp()
      .getRequest() as AuthenticatedRequest;

    return can(permissionsToCheck, user);
  }
}
