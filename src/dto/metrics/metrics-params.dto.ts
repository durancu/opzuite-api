import { IsOptional } from "class-validator";
import { MetricsParamsFilterDto } from "./metrics-params-filter.dto";

export class MetricsParamsDto{
    @IsOptional()
    model?: string;
    @IsOptional()
    filters?: MetricsParamsFilterDto[];
    @IsOptional()
    groupByModel?: string;
    @IsOptional()
    groupByFields?: string[];
    @IsOptional()
    dateFrom?: Date;
    @IsOptional()
    dateTo?: Date;
    @IsOptional()
    fields?: string[];
    @IsOptional()
    withCount?: boolean;
    @IsOptional()
    withRecords?: boolean;
    @IsOptional()
    sortOrder?: string;
    @IsOptional()
    sortField?: string;
}