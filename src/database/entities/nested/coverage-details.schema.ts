import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { nanoid } from 'nanoid';

@Schema({ timestamps: true, toJSON: { virtuals: true }, toObject: { virtuals: true } })
export class CoverageDetails extends Document {
    // #region Properties (40)

    @Prop({ type: Boolean, required: false })
    additionalInsured?: boolean;
    @Prop({ type: String, enum: ['ANY', 'SCHEDULED', 'OWNED', 'HIRED', 'NON-OWNED'], required: false })
    autoCoverageScope?: string;
    @Prop({ type: Number, required: false })
    bodilyInjuryPerAccidentLimit?: number;
    @Prop({ type: Number, required: false })
    bodilyInjuryPerPersonLimit?: number;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: Number, required: false })
    combinedSingleLimit?: number;
    @Prop({ type: String, enum: ['UMBRELLA', 'EXCESS'], required: false })
    coverageFormUsed?: string;
    @Prop({ type: Number, required: false })
    damageToRentedPremisesLimit?: number;
    @Prop({ type: Number, required: false })
    deductibleOrRetentionAmount?: number;
    @Prop({ type: Number, required: false })
    eachOccurrenceLimit?: number;
    @Prop({ type: Number, required: false })
    employerLiabilityDiseasePolicyLimit?: number;
    @Prop({ type: Number, required: false })
    employerLiabilityEachAccidentLimit?: number;
    @Prop({ type: Number, required: false })
    employerLiabilityEachEmployeeLimit?: number;
    @Prop({ required: false })
    excludedPeopleDescription?: string;
    @Prop({ type: Number, required: false })
    generalAggregateLimit?: number;
    @Prop({ type: String, enum: ['PROJECT', 'POLICY', 'LOCATION', 'OTHER'], required: false })
    generalAggregateLimitAppliesPer?: string;
    @Prop({ required: false })
    generalAggregateLimitAppliesPerOther?: string;
    @Prop({ type: Boolean, required: false })
    hasDeductible?: boolean;
    @Prop({ type: Boolean, required: false })
    hasExcludedPeople?: boolean;
    @Prop({ type: Boolean, required: false })
    hasRetention?: boolean;
    @Prop({ type: Boolean, required: false })
    includeHiredAutos?: boolean;
    @Prop({ type: Boolean, required: false })
    includeNonOwnedAutos?: boolean;
    @Prop({ type: Boolean, required: false })
    includeOtherCoveredAutoA?: boolean;
    @Prop({ type: Boolean, required: false })
    includeOtherCoveredAutoB?: boolean;
    @Prop({ type: Boolean, required: false })
    includeOwnedAutos?: boolean;
    @Prop({ type: Number, required: false })
    medicalExpensesLimit?: number;
    @Prop({ type: Boolean, required: false })
    other?: boolean;
    @Prop({ type: Number, required: false })
    other1LimitAmount?: number;
    @Prop({ required: false })
    other1LimitName?: string;
    @Prop({ type: Number, required: false })
    other2LimitAmount?: number;
    @Prop({ required: false })
    other2LimitName?: string;
    @Prop({ required: false })
    otherCoveredAutoADescription?: string;
    @Prop({ required: false })
    otherCoveredAutoBDescription?: string;
    @Prop({ type: Boolean, required: false })
    perStatute?: boolean;
    @Prop({ type: Number, required: false })
    personalAndAdvertisingInjuryLimit?: number;
    @Prop({ type: String, enum: ['CLAIMS', 'OCCUR'], required: false })
    policyBasedForm?: string;
    @Prop({ type: Number, required: false })
    productAndCompletedOperationsAggregateLimit?: number;
    @Prop({ type: Number, required: false })
    productCompletedOperationsAggregateLimit?: number;
    @Prop({ type: Number, required: false })
    propertyDamagePerAccidentLimit?: number;
    @Prop({ type: Boolean, required: false })
    waiverOfSubrogation?: boolean;



    @Prop({ type: Boolean, required: false })
    includeOtherCoverageA?: boolean;
    @Prop({ type: Boolean, required: false })
    includeOtherCoverageB?: boolean;
    @Prop({ required: false })
    otherCoverageADescription?: string;
    @Prop({ required: false })
    otherCoverageBDescription?: string;

    // #endregion Properties (40)
}

export const CoverageDetailsSchema = SchemaFactory.createForClass(CoverageDetails);