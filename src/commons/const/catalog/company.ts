export const CompanyCatalog = {
  employeeRateFrequencies: [
    { id: 'HOURLY', name: 'Per Hour' },
    { id: 'DAILY', name: 'Per Day' },
    { id: 'WEEKLY', name: 'Per Week' },
    { id: 'BI-WEEKLY', name: 'Each Two Weeks' },
    { id: 'TWICE-MONTH', name: 'Twice a Month' },
    { id: 'MONTHLY', name: 'Per Month' },
    { id: 'YEARLY', name: 'Per Year' },
  ],
  sales: {
    renewalFrequencies: [
      { id: 'ANNUAL', name: 'Annual' },
      { id: 'SEMI-ANNUAL', name: 'Semi-Annual' },
      { id: 'QUARTERLY', name: 'Quarterly' },
      { id: 'MONTHLY', name: 'Monthly' },
      { id: 'VARIABLE', name: 'Variable' },
    ],
  },
  locations: {
    payrollFrequencies: [
      { id: 'WEEKLY', name: 'Per Week' },
      { id: 'BI-WEEKLY', name: 'Each Two Weeks' },
      { id: 'TWICE-MONTH', name: 'Twice a Month' },
      { id: 'MONTHLY', name: 'Per Month' },
    ],
    availableCountries: [
      { id: 'USA', name: 'USA' },
      { id: 'MEX', name: 'Mexico' },
      { id: 'COL', name: 'Colombia' },
      { id: 'SUR', name: 'Suriname' },
    ]
  },
};
