
import { Types } from 'mongoose';
import { createDateRangeFilterExpressions } from 'src/commons/lib/aggregator-functions';
import { buildSearchTextQueries } from 'src/commons/queries/mongo/helpers/query';

export function extractParamFilters(queryParams?: SaleQueryParams): any {
  const queryFilters: any = {};

  if (!queryParams || !queryParams.filter) {
    return queryFilters;
  }

  const { filter } = queryParams;

  const keysToExtract = [
    'holder',
    'issuedAt',
    'masterTemplate'
  ];

  for (const key of keysToExtract) {
    if (key in filter) {
      queryFilters[key] = filter[key];
      delete filter[key];
    }
  }

  return queryFilters;
}

export function buildQueryConditions(queryParams: SearchQueryParams | null | undefined, queryFilters: any) {
  const conditions = {
    $and: [{ deleted: false }],
  };

  if (queryParams) {
    if (
      Object.keys(queryFilters).length > 0 ||
      (queryParams.filter && Object.keys(queryParams.filter).length > 0)
    ) {
      setFiltersByParams(conditions, queryFilters);
    }

    let searchColumns: string[] = [
      'holder.name',
      'customer.name',
      'masterTemplate.name',
      'issuedAt',
      'code',
    ];

    if (queryParams.searchText) {
      if (queryParams.searchColumns && queryParams.searchColumns.length) {
        searchColumns = queryParams.searchColumns;
      }
      const searchTextQueries = buildSearchTextQueries(
        queryParams.searchText,
        searchColumns,
      );

      if (searchTextQueries.length > 0) {
        conditions['$or'] = searchTextQueries;
      }
    }
  }

  return conditions;
}

export function setFiltersByParams(
  conditions: any,
  queryFilters: any,
): void {
  if (queryFilters['issuedDateFrom'] || queryFilters['issuedDateTo']) {
    conditions['$and'].push({
      soldAt: createDateRangeFilterExpressions(
        queryFilters['issuedDateFrom'],
        queryFilters['issuedDateTo'],
      ),
    });
  }

  if (queryFilters['customer']) {
    conditions['$and'].push({
      'customer._id': new Types.ObjectId(queryFilters['customer']),
    });
  }

  if (queryFilters['holder']) {
    conditions['$and'].push({
      'holder._id': new Types.ObjectId(queryFilters['holder']),
    });
  }

  if (queryFilters['masterTemplate']) {
    conditions['$and'].push({
      'masterTemplate._id': new Types.ObjectId(queryFilters['masterTemplate']),
    });
  }

}


