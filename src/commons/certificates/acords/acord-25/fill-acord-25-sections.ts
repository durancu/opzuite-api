import { Certificate } from 'src/database/entities/certificate.schema';
import { Company } from 'src/database/entities/company.schema';
import { Insurer } from 'src/database/entities/insurer.schema';
import { User } from 'src/database/entities/user.schema';
import { fillAutomobileLiabilityFields } from './helpers/fill-automobile-liability-fields';
import { fillCertificateGeneralFields } from './helpers/fill-certificate-ids-fields';

import { fillInsuredFields } from './helpers/fill-customer-fields';
import { fillExcessUmbrellaLiabilityFields } from './helpers/fill-excess-umbrella-liability-fields';
import { fillGeneralLiabilityFields } from './helpers/fill-general-liability-fields';
import { fillHolderFields } from './helpers/fill-holder-fields';
import { fillInsurersFields } from './helpers/fill-insurers-fields';
import { fillOtherPoliciesFields } from './helpers/fill-other-policies-fields';
import { fillProducerFields } from './helpers/fill-producer-fields';
import { fillWorkersCompensationFields } from './helpers/fill-workers-compensation-fields';
import { getPoliciesByProduct } from './helpers/policies-by-product';
import { Acord25PdfTemplateV2014NowCerts } from './models/acord-25-pdf-template-v-2014-now-certs';
import { isEmpty } from 'lodash';

export const fillAcord25Sections = (certificate: Partial<Certificate>, company: Company, producerAgents: User[], holder: Insurer, carriersWithCoverages: any[]): Acord25PdfTemplateV2014NowCerts => {

    const template = new Acord25PdfTemplateV2014NowCerts();

    fillCertificateGeneralFields(certificate, template);
    fillProducerFields(company, producerAgents, template);
    fillInsuredFields(certificate, template);
    fillInsurersFields(carriersWithCoverages, template);

    const policiesByProduct = getPoliciesByProduct(carriersWithCoverages);

    fillGeneralLiabilityFields(policiesByProduct, template);
    fillAutomobileLiabilityFields(policiesByProduct, template);
    fillExcessUmbrellaLiabilityFields(policiesByProduct, template);
    fillWorkersCompensationFields(policiesByProduct, template);
    const includeRemarks = !isEmpty(certificate.vehiclesIncluded) || !isEmpty(certificate.cohortsIncluded);
    fillOtherPoliciesFields(policiesByProduct, template, includeRemarks);

    fillHolderFields(holder, template);

    return template;
}



