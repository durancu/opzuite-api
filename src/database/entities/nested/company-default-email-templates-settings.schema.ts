import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class CompanyDefaultEmailTemplatesSettings extends Document {
  // #region Properties (5)

  @Prop()
  public employeeDeleteToManager: string;
  @Prop()
  public employeeDeleteToUser: string;
  @Prop()
  public employeeForgotPasswordConfirmToUser: string;
  @Prop()
  public employeeNewConfirmToUser: string;
  @Prop()
  public employeeNewWelcomeToUser: string;

  // #endregion Properties (5)
}

export const CompanyDefaultEmailTemplatesSettingsSchema = SchemaFactory.createForClass(CompanyDefaultEmailTemplatesSettings);