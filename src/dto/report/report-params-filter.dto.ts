import { IsOptional } from "class-validator";

export class ReportParamsFilterDto {
    // #region Properties (3)

    @IsOptional()
    public field: string;
    @IsOptional()
    public operator: string;
    @IsOptional()
    public value: any;

    // #endregion Properties (3)
}