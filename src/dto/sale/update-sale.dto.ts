import { PartialType } from '@nestjs/mapped-types';
import { IsOptional } from 'class-validator';
import { Company } from 'src/database/entities/company.schema';
import { Sale } from 'src/database/entities/sale.schema';
import { User } from 'src/database/entities/user.schema';
import { CreateSaleDto } from './create-sale.dto';

export const DEFAULT_COMMISSION = 0.1;

export class UpdateSaleDto extends PartialType(CreateSaleDto) {
  // #region Properties (4)

  @IsOptional()
  public _id?: Partial<Sale>;
  @IsOptional()
  public company?: Partial<Company>;
  @IsOptional()
  public updatedBy?: Partial<User>;

  //renewedWith, renewalReferences, wasRenewed, renewedBy

  // #endregion Properties (4)
}
