import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class Communication extends Document {
  // #region Properties (3)

  @Prop()
  public email: boolean;
  @Prop()
  public phone: boolean;
  @Prop()
  public sms: boolean;

  // #endregion Properties (3)
}

export const CommunicationSchema = SchemaFactory.createForClass(Communication);