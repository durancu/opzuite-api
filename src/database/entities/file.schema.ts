//import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { getNanoId } from 'src/utils';
//import * as patchHistory from 'mongoose-patch-history';
import { Company } from './company.schema';
import { Tag, TagSchema } from './nested/tag.schema';
import { User } from './user.schema';

@Schema({
  timestamps: true,
  toJSON: { virtuals: true },
  discriminatorKey: 'kind',
})
export class File extends Document {
  // #region Properties (14)

  @Prop({ default: () => getNanoId(), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
  public company: Company;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public createdBy: User;
  @Prop()
  public name: string;
  @Prop()
  public path: string;
  @Prop()
  public type: string;
  @Prop({ type: [TagSchema] })
  public tags: Tag[];
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public updatedBy: User;

  // #endregion Properties (14)
}

//const  patchHistoryDefault = patchHistory.default;

export const FileSchema = SchemaFactory.createForClass(File);

FileSchema.plugin(mongooseSoftDelete, {
  deletedAt: true,
  deletedBy: true,
  overrideMethods: true,
});
//FileSchema.plugin(patchHistoryDefault, { mongoose, name:'files.versions'})
