import { Body, Controller, Delete, Get, HttpCode, NotFoundException, Param, Post, Put, Req, Response, Scope, UseGuards, UseInterceptors } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { isEmpty, isNull, pickBy } from 'lodash';
import { Model } from 'mongoose';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { IpControlInterceptor } from 'src/commons/interceptors/ip-control.interceptors';
import { Report } from 'src/database/entities/report.schema';
import { CreateReportDto } from 'src/dto/report/create-report.dto';
import { ReportParamsDto } from 'src/dto/report/report-params.dto';
import { HasPermissions } from 'src/modules/auth/guard/has-permissions.decorator';
import { JwtAuthGuard } from 'src/modules/auth/guard/jwt-auth.guard';
import { PermissionsGuard } from 'src/modules/auth/guard/permissions.guard';
import { UpdateReportDto } from '../../dto/report/update-report.dto';
import { ReportService } from './report.service';

@ApiTags('Report')
@UseInterceptors(IpControlInterceptor)
@Controller({ path: 'reports', scope: Scope.REQUEST })
export class ReportController {
  // #region Constructors (1)

  constructor(
    private reportService: ReportService,
    @InjectModel(Report.name) private reportModel: Model<Report>,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (9)

  @Post('/delete')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_DELETE)
  public async batchDelete(@Req() req: Request, @Body() body: any): Promise<any> {
    const codes: string[] = body['codes'];
    return await this.reportService.batchDelete(codes);
  }

  @Post('')
  @HttpCode(201)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_CREATE)
  public async create(
    @Req() req: Request,
    @Body() report: any,
  ): Promise<CreateReportDto> {
    return await this.reportService.save(report);
  }

  @Delete(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_DELETE)
  public async delete(@Param('code') code: string): Promise<Report> {
    return await this.reportService.deleteByCode(code);
  }

  @Get('')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_READ)
  public async findAll(@Req() req: Request, @Response() res): Promise<any> {
    return res.json(await this.reportService.findAll());
  }

  @Get(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_READ)
  public async findByCode(@Param('code') code: string): Promise<any> {
    return await this.reportService.findByCode(code);
  }

  @Post(':code/chart-data')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_READ)
  public async getReportChartData(
    @Param('code') code: string,
    @Body() params: ReportParamsDto,
    @Response() res,
  ): Promise<any> {
    const response = await this.reportService.buildReportChartData(code, params);
    return res.json(response);
  }

  @Post(':code/results')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_READ)
  public async getReportResults(
    @Param('code') code: string,
    @Body() params: ReportParamsDto,
    @Response() res,
  ): Promise<any> {
    const report: any = await this.reportModel
      .findOne({ code: code })
      .populate({
        path: 'createdBy',
        select: 'role name',
      })
      .exec();

    if (!report) {
      throw new NotFoundException(`Report (code: ${code}) not found.`);
    }

    const reportParams: ReportParamsDto = report.params;
    const cleanedParams: ReportParamsDto = pickBy(params, !isNull);
    const appliedParams: ReportParamsDto = isEmpty(cleanedParams) ? reportParams : cleanedParams;

    const results: any = await this.reportService.metricsReport(code, appliedParams);

    const response = {
      report: { ...report['_doc'] },
      metrics: results.metrics,
      records: results.records,
      appliedParams: appliedParams,
    };

    return res.json(response);
  }

  @Post('/search')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_READ)
  public async search(@Body() query: any, @Response() res): Promise<any> {
    return res.json(await this.reportService.search(query.queryParams));
  }

  @Put(':code')
  @HttpCode(200)
  @UseGuards(JwtAuthGuard, PermissionsGuard)
  @HasPermissions(PermissionType.REPORTS_UPDATE)
  ////@UseFilters(MongoFilter)
  public async update(
    @Req() req: Request,
    @Param('code') code: string,
    @Body() report: UpdateReportDto,
  ): Promise<Partial<Report>> {
    return await this.reportService.update(code, report);
  }

  // #endregion Public Methods (9)
}
