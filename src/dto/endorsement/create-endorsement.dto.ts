import { IsOptional } from "class-validator";
import { Company } from "src/database/entities/company.schema";
import { EndorsementItem } from "src/database/entities/nested/endorsement-item.schema";
import { Sale } from "src/database/entities/sale.schema";
import { User } from "src/database/entities/user.schema";

export class CreateEndorsementDto {
    // #region Properties (30)

    @IsOptional()
    public accountingClass?: string;
    @IsOptional()
    public amount?: number;
    @IsOptional()
    public code?: string;
    @IsOptional()
    public company?: Partial<Company>;
    @IsOptional()
    public createdAt?: Date;
    @IsOptional()
    public createdBy?: Partial<User>;
    @IsOptional()
    public description?: string;
    @IsOptional()
    public endorsedAt?: Date;
    @IsOptional()
    public followUpDate?: Date;
    @IsOptional()
    public followUpPerson?: Partial<User>;
    @IsOptional()
    public items?: EndorsementItem[];
    @IsOptional()
    public name?: string;
    @IsOptional()
    public sale?: Sale;
    @IsOptional()
    public seller?: User;
    @IsOptional()
    public status?: string;
    @IsOptional()
    public totalAgencyCommission?: number;
    @IsOptional()
    public totalAgentCommission?: number;
    @IsOptional()
    public totalFinanced?: number;
    @IsOptional()
    public totalFinancedPaid?: number;
    @IsOptional()
    public totalNonCommissionablePremium?: number;
    @IsOptional()
    public totalNonPremium?: number;
    @IsOptional()
    public totalPaid?: number;
    @IsOptional()
    public totalPayables?: number;
    @IsOptional()
    public totalPremium?: number;
    @IsOptional()
    public totalReceivables?: number;
    @IsOptional()
    public totalReceived?: number;
    @IsOptional()
    public totalTaxesAndFees?: number;
    @IsOptional()
    public type?: string;
    @IsOptional()
    public updatedAt?: Date;
    @IsOptional()
    public updatedBy?: User;

    // #endregion Properties (30)
}