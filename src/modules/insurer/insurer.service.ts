
import {
  BadRequestException,
  Inject,
  Injectable,
  NotFoundException,
  Scope
} from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { trim } from 'lodash';
import { Model, Types } from 'mongoose';
import { cleanEmail } from 'src/commons/lib/string-functions';
import { executeAggregatorWithPagination } from 'src/commons/queries/mongo/helpers/query';
import { Insurer } from 'src/database/entities/insurer.schema';
import { CreateInsurerDto } from 'src/dto/insurer/create-insurer.dto';
import { UpdateInsurerDto } from 'src/dto/insurer/update-insurer.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { buildSearchAggregator } from './queries/mongo/aggregators/search.aggregator';
import { buildSearchConditions } from './queries/mongo/helpers/search.conditions';


@Injectable({ scope: Scope.REQUEST })
export class InsurerService {
  // #region Properties (1)

  private sanitizeProfileAttributes = (insurer: UpdateInsurerDto) => {
    if (insurer.contact) {
      insurer.contact.firstName = trim(
        insurer.contact ? insurer.contact.firstName || '' : '',
      );
      insurer.contact.lastName = trim(
        insurer.contact ? insurer.contact.lastName || '' : '',
      );

      insurer.contact.mobilePhone = (insurer.contact.mobilePhone || '').replace(
        /\D/g,
        '',
      );
      insurer.contact.phone = (insurer.contact.phone || '').replace(/\D/g, '');
      insurer.contact.email = cleanEmail(insurer.contact.email);
    }

    insurer.business.name = trim(
      insurer.business ? insurer.business.name || '' : '',
    );
    insurer.business.email = cleanEmail(insurer.business.email);
    insurer.business.primaryPhone = (
      insurer.business.primaryPhone || ''
    ).replace(/\D/g, '');

    insurer.name = trim(insurer.business.name || '');
    insurer.email = insurer.business.email || '';
    insurer.phone = (insurer.business.primaryPhone || '').replace(/\D/g, '');

    return insurer;
  };

  // #endregion Properties (1)

  // #region Constructors (1)

  constructor(
    @InjectModel(Insurer.name) private insurerModel: Model<Insurer>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (12)

  public async batchCreate(insurers: CreateInsurerDto[]) {
    const user = this.req.user.id;
    const company = this.req.user.company;

    insurers = insurers.map((insurer: CreateInsurerDto) => {
      insurer = this.sanitizeProfileAttributes(insurer);
      if (insurer.code) {
        insurer = {
          ...insurer,
          updatedBy: insurer.updatedBy ? insurer.updatedBy : user,
        };
      } else {
        insurer = {
          ...insurer,
          createdBy: insurer.createdBy ? insurer.createdBy : user,
          company: company,
        };
      }
      return insurer;
    });

    const bulkPayload = insurers.map((insurer: Partial<Insurer>) => ({
      updateOne: {
        filter: { code: insurer.code },
        update: { $set: insurer },
        upsert: true,
      },
    }));

    this.insurerModel
      .bulkWrite(bulkPayload)
      .then((bulkWriteOpResult) => {
        console.log('BULK update OK');
        //console.log(JSON.stringify(bulkWriteOpResult, null, 2));
        return bulkWriteOpResult;
      })
      .catch((err) => {
        console.log('BULK update error');
        console.log(JSON.stringify(err, null, 2));
        throw new BadRequestException(
          'Cannot upsert insurers. ' + JSON.stringify(err, null, 2),
        );
      });
  }

  /**
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.insurerModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @returns Promise
   */
  public async deleteAll(): Promise<any> {
    return await this.insurerModel.deleteMany({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async deleteByCode(code: string): Promise<Insurer> {
    const insurer: Insurer = await this.insurerModel
      .findOne({ code: code })
      .exec();

    if (!insurer) {
      throw new NotFoundException(`Insurer (code: ${code}) not found.`);
    }

    return await this.insurerModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.insurerModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<Insurer> {
    const insurer: Insurer = await this.insurerModel
      .findOne({ code: code })
      .exec();

    if (!insurer) {
      throw new NotFoundException(`Insurer (code: ${code}) not found.`);
    }

    return insurer;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<Insurer> {
    const insurer: Insurer = await this.insurerModel
      .findOne({ _id: id })
      .exec();

    if (!insurer) {
      throw new NotFoundException(`Insurer (id: ${id}) was not found`);
    }

    return insurer;
  }

  public async getCatalog(filterCriteria: any): Promise<any> {
    const insurers = await this.insurerModel
      .find(filterCriteria)
      .select('name email phone type _id')
      .sort({ 'name': 1 })
      .exec();

    return {
      carriers: insurers.filter((insurer) => insurer.type === 'CARRIER'),
      brokers: insurers.filter((insurer) => insurer.type === 'BROKER'),
      financers: insurers.filter((insurer) => insurer.type === 'FINANCER'),
    };
  }

  /**
   * @param  {CreateInsurerDto} insurerDto
   * @returns Promise
   */
  public async save(insurerDto: CreateInsurerDto): Promise<Insurer> {
    let insurer: CreateInsurerDto = {
      ...insurerDto,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    };

    insurer = this.sanitizeProfileAttributes(insurer);

    return await this.insurerModel.create(insurer);
  }

  public async search(queryParams?: any): Promise<any> {

    const conditions = buildSearchConditions(queryParams)

    const aggregator = buildSearchAggregator(conditions);
    const result = await executeAggregatorWithPagination(aggregator, queryParams, this.insurerModel);
    return result;
  }

  /**
   * @param  {string} code
   * @param  {UpdateInsurerDto} data
   * @returns Promise
   */
  public async update(code: string, insurerDto: UpdateInsurerDto): Promise<Insurer> {
    const insurerFound: Insurer = await this.insurerModel
      .findOne({ code: code })
      .exec();

    if (!insurerFound) {
      throw new NotFoundException(`Insurer (code: ${code}) not found.`);
    }

    let insurer: UpdateInsurerDto = {
      ...insurerFound['_doc'],
      ...insurerDto,
      updatedBy: this.req.user.id,
    };

    insurer = this.sanitizeProfileAttributes(insurer);

    return await this.insurerModel.findOneAndUpdate(
      { _id: new Types.ObjectId(insurerFound._id) },
      insurer,
      { new: true },
    );
  }

  // #endregion Public Methods (12)
}
