import { Company } from "src/database/entities/company.schema";
import { User } from "src/database/entities/user.schema";
import { Acord25PdfTemplateV2014NowCerts } from "../models/acord-25-pdf-template-v-2014-now-certs";

export const fillProducerFields = (company: Company, producerAgents: User[], template: Acord25PdfTemplateV2014NowCerts) => {
    template.setField('Producer_FullName', company.name);
    if (company.address) {
        template.setField('Producer_MailingAddress_LineOne', company.address.address1);
        template.setField('Producer_MailingAddress_LineTwo', company.address.address2);
        template.setField('Producer_MailingAddress_CityName', company.address.city);
        template.setField('Producer_MailingAddress_StateOrProvinceCode', company.address.state);
        template.setField('Producer_MailingAddress_PostalCode', company.address.zip);
    }
    if (producerAgents.length) {
        const producerAgent = producerAgents[0];
        template.setField('Producer_ContactPerson_FullName', producerAgent.name);
        template.setField('Producer_ContactPerson_PhoneNumber', producerAgent.phone);
        template.setField('Producer_FaxNumber', company.fax);
        template.setField('Producer_ContactPerson_EmailAddress', producerAgent.email);
    }
}