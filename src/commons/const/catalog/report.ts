export const ReportCatalog = {
  groupingCriteria: [
    {
      id: 'full',
      name: 'All values',
    },
    {
      id: 'seller',
      name: 'By Seller',
    },
    {
      id: 'customer',
      name: 'By Customer',
    },
    {
      id: 'location',
      name: 'By Location',
    },
  ],
};
