export const getAlphabeticLetterFromNumber = (number: number): string => {
    if (number < 1 || number > 26) {
        throw new Error('Number must be between 1 and 26');
    }

    const charCode = 64 + number; // 64 es el código de carácter Unicode de '@', que viene justo antes de 'A'
    return String.fromCharCode(charCode);
}