import { PartialType } from "@nestjs/mapped-types";
import { IsOptional } from "class-validator";
import { CreateRoleDto } from "./create-role.dto";

export class UpdateRoleDto extends PartialType(CreateRoleDto) { 
    @IsOptional()
    _id?: string;
    @IsOptional()
    id?: string;
}