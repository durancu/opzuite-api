import { PartialType } from '@nestjs/mapped-types';
import { IsOptional } from 'class-validator';
import { CreateCompanyDto } from './create-company.dto';
export class UpdateCompanyDto extends PartialType(CreateCompanyDto) {
  // #region Properties (2)

  @IsOptional()
  public _id?: string;
  @IsOptional()
  public id?: string;

  // #endregion Properties (2)
}
