import { Inject, Injectable, NotFoundException, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { InjectModel } from '@nestjs/mongoose';
import { escapeRegExp } from 'lodash';
import { Model, Types } from 'mongoose';
import { Company } from 'src/database/entities/company.schema';
import { CreateCompanyDto } from '../../dto/company/create-company.dto';
import { UpdateCompanyDto } from '../../dto/company/update-company.dto';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';

@Injectable({ scope: Scope.REQUEST })
export class CompanyService {
  // #region Constructors (1)

  constructor(
    @InjectModel(Company.name) private companyModel: Model<Company>,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) {}

  // #endregion Constructors (1)

  // #region Public Methods (13)

  /**
   * @returns Promise
   */
  public async batchDelete(codes: string[]): Promise<any> {
    return await this.companyModel.deleteMany({ code: { $in: codes } }).exec();
  }

  /**
   * @returns Promise
   */
  public async deleteAll(): Promise<any> {
    return await this.companyModel.deleteMany({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async deleteByCode(code: string): Promise<Company> {
    const company: Company = await this.companyModel
      .findOne({ code: code })
      .exec();

    if (!company) {
      throw new NotFoundException(`Company (code: ${code}) not found.`);
    }

    return await this.companyModel.findOneAndDelete({ code: code }).exec();
  }

  /**
   * @returns Promise
   */
  public async findAll(): Promise<any> {
    return await this.companyModel.find({}).exec();
  }

  /**
   * @param  {string} code
   * @returns Promise
   */
  public async findByCode(code: string): Promise<Company> {
    const company: Company = await this.companyModel
      .findOne({ code: code })
      .exec();

    if (!company) {
      throw new NotFoundException(`Company (code: ${code}) not found.`);
    }

    return company;
  }

  /**
   * @param  {string} id
   * @returns Promise
   */
  public async findById(id: string): Promise<Company> {
    const company: Company = await this.companyModel
      .findOne({ _id: id })
      .exec();

    if (!company) {
      throw new NotFoundException(`Company (id: ${id}) was not found`);
    }

    return company;
  }

  public async getCatalog(filterCriteria: any): Promise<any> {
    return await this.companyModel
      .find(filterCriteria)
      .select('name _id')
      .sort({ name: 1 })
      .exec();
  }

  public getMyCompany(): Promise<Company> {
    return this.findById(this.req.user.company.toString());
  }

  /**
   * @param  {CreateCompanyDto} companyDto
   * @returns Promise
   */
  public async save(companyDto: CreateCompanyDto): Promise<Company> {
    return await this.companyModel.create({
      ...companyDto,
      createdBy: { _id: this.req.user.id },
      company: { _id: this.req.user.company },
    });
  }

  public async search(queryParams?: any): Promise<any> {
    const sortCriteria = {};
    sortCriteria[queryParams.sortField] =
      queryParams.sortOrder === 'desc' ? -1 : 1;
    const skipCriteria = (queryParams.pageNumber - 1) * queryParams.pageSize;
    const limitCriteria = queryParams.pageSize;

    let filterCriteria = {};

    if (
      queryParams.filter &&
      Object.keys(queryParams.filter).length > 0 &&
      queryParams.filter.constructor === Object
    ) {
      filterCriteria = {
        $or: Object.keys(queryParams.filter).map((key) => {
          return {
            [key]: {
              $regex: new RegExp(
                '.*' + escapeRegExp(queryParams.filter[key]) + '.*',
                'i',
              ),
            },
          };
        }),
      };
    }

    return {
      totalCount: await this.companyModel
        .find(filterCriteria)
        .countDocuments()
        .exec(),
      entities: await this.companyModel
        .find(filterCriteria)
        .select('name')
        .skip(skipCriteria)
        .limit(limitCriteria)
        .sort(sortCriteria)
        .exec(),
    };
  }

  /**
   * @param  {string} code
   * @param  {UpdateCompanyDto} data
   * @returns Promise
   */
  public async update(code: string, company: UpdateCompanyDto): Promise<Company> {
    const companyFound: Company = await this.companyModel
      .findOne({ code: code })
      .exec();

    if (!companyFound) {
      throw new NotFoundException(`Company (code: ${code}) not found.`);
    }

    const companyData: UpdateCompanyDto = {
      ...companyFound['_doc'],
      ...company,
      updatedBy: this.req.user.id,
    };

    return await this.companyModel.findOneAndUpdate(
      { _id: new Types.ObjectId(companyFound._id) },
      companyData,
      { new: true },
    );
  }

  public async updateMyCompany(companyData: UpdateCompanyDto): Promise<Company> {
    const company = this.companyModel
      .findOneAndUpdate(
        { _id: this.req.user.company },
        { ...companyData, updatedBy: { _id: this.req.user.id } },
        { new: true },
      )
      .exec();

    if (!company) {
      throw new NotFoundException(
        `Company:${this.req.user.company} was not found`,
      );
    }
    return company;
  }

  // #endregion Public Methods (13)
}
