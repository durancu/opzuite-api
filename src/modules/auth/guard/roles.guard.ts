import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { HAS_ROLES_KEY } from '../../../commons/const/auth.constants';
import { RoleCategories } from '../../../commons/enum/role-categories.enum';
import { AuthenticatedRequest } from '../interface/authenticated-request.interface';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) { }
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const rolesToValidate = this.reflector.get<RoleCategories[]>(
      HAS_ROLES_KEY,
      context.getHandler(),
    );
    if (!rolesToValidate || rolesToValidate.length == 0) {
      return true;
    }

    const strRolesToValidate: string[] = Object.values(rolesToValidate);

    const {
      user,
    } = context.switchToHttp().getRequest() as AuthenticatedRequest;
    return user.role && strRolesToValidate.includes(user.role.key);
  }
}
