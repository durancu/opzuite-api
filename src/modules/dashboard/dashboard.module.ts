import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Chart, ChartSchema } from 'src/database/entities/chart.schema';
import { Report, ReportSchema } from 'src/database/entities/report.schema';
import { ReportRepository } from 'src/repositories/report.repository';
import { ReportModule } from '../report/report.module';
import { ReportService } from '../report/report.service';
import { DashboardModuleInitializerService } from './dashboard-module-initializer.service';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Chart.name, schema: ChartSchema },
      { name: Report.name, schema: ReportSchema },
    ]),
    ReportModule,
  ],
  controllers: [DashboardController],
  providers: [
    ReportRepository,
    DashboardService,
    ReportService,
    DashboardModuleInitializerService,
  ],
  exports: [
    DashboardService,
    ReportService
  ]
})
export class DashboardModule { }
