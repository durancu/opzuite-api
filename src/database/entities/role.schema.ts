import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { RoleTypes } from 'src/commons/enum/role-types.enum';
import { Company } from './company.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class Role extends Document {
  // #region Properties (12)

  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
  public company: Company;
  @Prop()
  public createdAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public createdBy: User;
  @Prop()
  public description: string;
  @Prop({ default: null })
  public hierarchy: number;
  @Prop()
  public key: string;
  @Prop()
  public name: string;
  @Prop({ type: [SchemaTypes.String], default: [] })
  public permissions: PermissionType[];
  @Prop({ default: RoleTypes.EMPLOYEE })
  public type: string;
  @Prop()
  public updatedAt: Date;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public updatedBy: User;

  // #endregion Properties (12)
}

export const RoleSchema = SchemaFactory.createForClass(Role);

RoleSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });

export { RoleTypes };
