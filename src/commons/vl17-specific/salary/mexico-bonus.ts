import { RoleCategories } from 'src/commons/enum/role-categories.enum';
import { roundAmount } from 'src/commons/lib/math-functions';

const DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE = 50000;
const DEFAULT_FEE_BONUS_PERCENT = 0.3;
const DEFAULT_TIP_BONUS_PERCENT = 1;
const DEFAULT_PERMIT_BONUS_PERCENT = 0.2;
const DEFAULT_SALES_COMMISION_PERCENT = 0.01;
const DEFAULT_AFTER_TARGET_COMMISSION = 100;
const DEFAULT_EMPLOYEES_COUNT_TARGET = 20;
const DEFAULT_EMPLOYEES_COUNT_COMMISSION = 400;
const DEFAULT_LOCATION_TOTAL_SALES_BONUS_PERCENT = 0.005;
const DEFAULT_25K_BONUS_PERCENT = 0.005;
//const DEFAULT_50K_PLUS_BONUS_PERCENT = 0.01;
const DEFAULT_TRAINEE_CASH_BONUS_AFTER_SALES = 50;

/**
 * Calculates total bonus as sum of Fees + Permits + Tips
 *
 * @param  {number} feesAmount
 * @param  {number} permitsAmount
 * @param  {number} tipsAmount
 */
export function totalNonPremiumBonus(
  feesAmount: number,
  permitsAmount: number,
) {
  return (
    percentBonusPerFees(feesAmount) + percentBonusPerPermits(permitsAmount)
  );
}

/**
 * Calculates bonus according to Fee sales
 *
 * @param  {number} feesAmount
 * @param  {number} commisionPercent
 */
export function percentBonusPerFees(
  feesAmount: number,
  commisionPercent: number = DEFAULT_FEE_BONUS_PERCENT,
) {
  return feesAmount * commisionPercent;
}

/**
 * Calculates bonus according to Permit sales
 *
 * @param  {number} permitsAmount
 * @param  {number=DEFAULT_PERMIT_BONUS_PERCENT} commisionPercent
 */
export function percentBonusPerPermits(
  permitsAmount: number,
  commisionPercent: number = DEFAULT_PERMIT_BONUS_PERCENT,
) {
  return permitsAmount * commisionPercent;
}

/**
 * Calculates bonus according to Tips sales
 *
 * @param  {number} tipsAmount
 * @param  {number=DEFAULT_TIP_BONUS_PERCENT} commisionPercent
 * @returns number
 */
export function percentBonusPerTips(
  tipsAmount: number,
  commisionPercent: number = DEFAULT_TIP_BONUS_PERCENT,
) {
  return tipsAmount * commisionPercent;
}

//PERCENTAGE OF SALES
/**
 * Calculates bonus based on a fixed percentage of total sales
 *
 * @param  {number} totalSales
 * @param  {number=DEFAULT_SALES_COMMISION_PERCENT} percent
 * @returns number
 */
export function percentOfSalesFixed(
  totalSales: number,
  percent: number = DEFAULT_SALES_COMMISION_PERCENT,
) {
  return totalSales * percent;
}

//COMMISSION AFTER TARGET
/**
 * Calculates
 *
 * @param  {number} totalSales
 * @param  {number=DEFAULT_AFTER_TARGET_COMMISSION} commission
 * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} target
 */
export function cashBonusAfterMeetSalesTarget(
  totalSales: number,
  commission: number = DEFAULT_AFTER_TARGET_COMMISSION,
  monthSalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
) {
  return totalSales > monthSalesTarget ? commission : 0;
}

//COMMISSION AFTER TARGET
/**
 * Calculates
 *
 * @param  {number} totalSales
 * @param  {number=DEFAULT_AFTER_TARGET_COMMISSION} commission
 * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} target
 */
export function percentBonusAfterMeetSalesTarget(
  totalSales: number,
  percent: number = DEFAULT_SALES_COMMISION_PERCENT,
  monthSalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
) {
  return totalSales > monthSalesTarget ? totalSales * percent : 0;
}

export function cashBonusAfterEmployeesCount(
  employeesCount: number,
  employeesTarget: number = DEFAULT_EMPLOYEES_COUNT_TARGET,
  employeesTargetCommission: number = DEFAULT_EMPLOYEES_COUNT_COMMISSION,
) {
  return employeesCount >= employeesTarget ? employeesTargetCommission : 0;
}

export function cashBonusAfterEmployeesTotalSales(
  employeesSales: number,
  employeeSalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
) {
  return employeesSales >= employeeSalesTarget
    ? DEFAULT_AFTER_TARGET_COMMISSION
    : 0;
}

/**
 * Calculates a bonus paid to location managers, according to the formula: 
 * TOTAL OFFICE SALES > GOAL (ej: 50K) * EMPLOYEES COUNT - 

 * @param  {number} locationTotalSales
 * @param  {number} locationEmployeesCount
 * @param  {number} managerTotalSales
 * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} expectedMonthSalesBySeller
 * @param  {number=DEFAULT_LOCATION_TOTAL_SALES_BONUS_PERCENT} salesBonusPercent
 * @returns number
 */

function locationSalesBonusAfterTarget(
  locationTotalSales: number,
  locationEmployeesCount: number,
  managerTotalSales: number,
  expectedMonthSalesBySeller: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
  salesBonusPercent: number = DEFAULT_LOCATION_TOTAL_SALES_BONUS_PERCENT,
) {
  return locationTotalSales >
    expectedMonthSalesBySeller * locationEmployeesCount
    ? (locationTotalSales - managerTotalSales) * salesBonusPercent
    : 0;
}

/**
 * Calculates bonus according to employee total sales in last month.
 * Scales are described as the following table:
 *
 * Start   End     %       Cash
 * 0       25      0       0
 * 25+     50      0.005   0
 * 50+     100     0.01    50
 * 100+    150     0.01    100
 * 150+    200     0.01    150
 * 200+    250     0.01    200
 *
 * @param  {number} employeeTotalSales
 * @param  {number=DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE} employeeMonthlySalesTarget
 */

function variablePercentAndCashSalesBonus(
  employeeTotalSales: number,
  employeeMonthlySalesTarget: number = DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
) {
  let cashCommission = 0;
  let percent = DEFAULT_SALES_COMMISION_PERCENT;

  if (
    employeeTotalSales > employeeMonthlySalesTarget / 2 &&
    employeeTotalSales < employeeMonthlySalesTarget
  )
    percent = DEFAULT_25K_BONUS_PERCENT;
  else {
    const mult = Math.floor(employeeTotalSales / employeeMonthlySalesTarget);
    cashCommission = (mult * employeeMonthlySalesTarget) / 1000;
  }
  return employeeTotalSales * percent + cashCommission;
}

/**
 * Calculates Salary Bonus by Role according to Mexico bonus calculation rules
 *
 * @param  {string} employeeCategory
 * @param  {number} employeeTotalSales
 * @param  {number} extraBonus
 * @param  {number} officeEmployees
 * @param  {number} officeTotalSales
 */
export function salesBonusByEmployeeCategoryMexico(
  employeeCategory: string,
  employeeTotalSales: number,
  nonPremiumBonus: number,
  officeEmployees: number,
  officeTotalSales: number,
) {
  let premiumBonus = 0;
  switch (employeeCategory) {
    case RoleCategories.CERTIFICATES:
      premiumBonus =
        percentOfSalesFixed(employeeTotalSales) +
        cashBonusAfterMeetSalesTarget(employeeTotalSales);
      break;

    case RoleCategories.ENDORSEMENTS:
      break;

    case RoleCategories.COUNTRY_MANAGER:
      premiumBonus =
        cashBonusAfterEmployeesCount(officeEmployees) +
        cashBonusAfterEmployeesTotalSales(officeTotalSales);
      break;

    case RoleCategories.LOCATION_MANAGER:
      premiumBonus =
        variablePercentAndCashSalesBonus(employeeTotalSales) +
        locationSalesBonusAfterTarget(
          officeTotalSales,
          officeEmployees,
          employeeTotalSales,
        );
      break;

    case RoleCategories.SELLER:
    case RoleCategories.COMPANY_MANAGER:
      premiumBonus = variablePercentAndCashSalesBonus(employeeTotalSales);
      break;

    case RoleCategories.TRAINEE:
      premiumBonus =
        percentBonusAfterMeetSalesTarget(
          DEFAULT_MONTHLY_SALES_TARGET_BY_EMPLOYEE,
        ) + DEFAULT_TRAINEE_CASH_BONUS_AFTER_SALES;

      break;
    case RoleCategories.ADMIN:
    default:
      premiumBonus = 0;
  }

  return premiumBonus + nonPremiumBonus;
}

export function bonusByEmployeeCategory(
  employeeCategory: string,
  userCountry = 'USA',
  employeeTotalSales: number,
  employeeTotalPermits: number,
  employeeTotalFees: number,
  officeEmployees: number,
  officeTotalSales: number,
): any {
  let bonus = 0;

  const nonPremiumBonus = totalNonPremiumBonus(
    employeeTotalFees,
    employeeTotalPermits,
  );

  switch (userCountry) {
    //TODO: Roll it back to MEX, and set calculations for USA
    case 'USA':
      break;
    default:
      bonus = salesBonusByEmployeeCategoryMexico(
        employeeCategory,
        employeeTotalSales,
        nonPremiumBonus,
        officeEmployees,
        officeTotalSales,
      );
      break;
  }

  return roundAmount(bonus);
}
