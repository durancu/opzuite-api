
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import { nanoid } from 'nanoid';
import { DEFAULT_ADDRESS } from 'src/commons/const/defaults/address.default';
import { Location } from './location.schema';
import { Address, AddressSchema } from './nested/address.schema';
import { CompanySettings, CompanySettingsSchema } from './nested/company-settings.schema';
import { User } from './user.schema';

@Schema()
export class Company extends Document {
    // #region Properties (23)

    @Prop({ type: AddressSchema, default: DEFAULT_ADDRESS })
    public address: Address;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: true })
    public createdBy: User;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Location', required: false })
    public defaultLocation: Location;
    @Prop({ unique: true, required: true, dropDups: true })
    public email: string;
    @Prop()
    public fax: string;
    @Prop()
    public industry: string;
    @Prop()
    public logo: string;
    @Prop({ unique: true, required: true, dropDups: true })
    public name: string;
    @Prop({ type: [String], required: false })
    public otherPhones: string[];
    @Prop()
    public primaryPhone: string;
    @Prop()
    public primaryPhoneExtension: string;
    @Prop({ type: Boolean, default: false })
    public restrictedAccessByIP: boolean;
    @Prop()
    public secondaryPhone: string;
    @Prop()
    public secondaryPhoneExtension: string;
    @Prop()
    public sector: string;
    @Prop({ type: CompanySettingsSchema })
    public settings: CompanySettings;
    @Prop({ type: SchemaTypes.Date })
    public startedAt: Date;
    // headquarter (H), point of sale (P), office (O)
    @Prop()
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;
    @Prop()
    public website: string;

    // #endregion Properties (23)
}

export const CompanySchema = SchemaFactory.createForClass(Company);

CompanySchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
