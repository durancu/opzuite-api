
class ApiException {
  message?: string;
  data?: any;
  meta?: any;
  code?: string;
}

export { ApiException };

