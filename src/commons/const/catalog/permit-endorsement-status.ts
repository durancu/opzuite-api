export const PERMIT_ENDORSEMENT_STATUS = [
  { name: 'Entered in system', id: 'ENTERED' },
  { name: 'All applications sent', id: 'SENT' },
  { name: 'All applications processed', id: 'PROCESSED' },
  { name: 'Customer documentation received', id: 'CUSTOMER_RECEIVED' },
  { name: 'Customer documentation approved', id: 'CUSTOMER_APPROVED' },
  { name: 'Suspended for miscellaneous reason', id: 'SUSPENDED' },
  { name: 'COMPLETE', id: 'COMPLETE' },
]

