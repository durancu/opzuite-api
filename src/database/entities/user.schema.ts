import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { compare, genSaltSync, hash } from 'bcrypt';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
import * as uniqueValidator from 'mongoose-unique-validator';
import { nanoid } from 'nanoid';
import { DEFAULT_ADDRESS } from 'src/commons/const/defaults/address.default';
import { DEFAULT_COMMUNICATION } from 'src/commons/const/defaults/communication.default';
import { DEFAULT_EMAIL_SETTINGS } from 'src/commons/const/defaults/email-settings.default';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { UserStatus } from 'src/commons/enum/user-status.enum';
import { Company } from './company.schema';
import { Location } from './location.schema';
import { Address, AddressSchema } from './nested/address.schema';
import { Communication, CommunicationSchema } from './nested/communication.schema';
import { EmailSettings, EmailSettingsSchema } from './nested/email-settings.schema';
import { EmployeeInfo, EmployeeInfoSchema } from './nested/employee-info.schema';
import { Role, RoleTypes } from './role.schema';

@Schema({
    timestamps: true,
    toJSON: { virtuals: true }
})
export class User extends Document {
    // #region Properties (34)

    @Prop({ type: AddressSchema, default: DEFAULT_ADDRESS })
    public address: Address;
    @Prop({ type: [SchemaTypes.String] })
    public authorizedIpAddresses: string[];
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: CommunicationSchema, default: DEFAULT_COMMUNICATION })
    public communication: Communication;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public company: Company;
    @Prop()
    public confirmToken: string;
    @Prop({ default: 'USA' })
    public country: string;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public createdBy: User;
    @Prop()
    public dob: Date;
    @Prop({ unique: true, required: true, dropDups: true })
    public email: string;
    @Prop({ type: EmailSettingsSchema, default: DEFAULT_EMAIL_SETTINGS })
    public emailSettings: EmailSettings;
    @Prop({ type: EmployeeInfoSchema, default: {} })
    public employeeInfo: EmployeeInfo;
    @Prop()
    public firstName: string;
    @Prop()
    public gender: string;
    @Prop({ default: 'en' })
    public language: string;
    @Prop()
    public lastName: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Location' })
    public location: Location;
    @Prop({ unique: true, required: false, dropDups: true })
    public mobilePhone: string;
    @Prop()
    public name: string;
    @Prop()
    public password: string;
    @Prop()
    public passwordToken: string;
    @Prop({ type: [SchemaTypes.String] })
    public permissions: PermissionType[];
    @Prop({ required: false })
    public phone: string;
    @Prop({ type: SchemaTypes.Map, default: {} })
    public preferences: any;
    @Prop({ required: false })
    public profilePhoto: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Role', required: true })
    public role: Role;
    @Prop({ default: UserStatus.UNCONFIRMED })
    public status: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public supervisor: User;
    @Prop({ default: 'CDT' })
    public timezone: string;
    @Prop({ default: RoleTypes.EMPLOYEE })
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public updatedBy: User;
    @Prop({ unique: true, required: true, dropDups: true })
    public username: string;
    @Prop()
    public website: string;

    // #endregion Properties (34)

    // #region Public Methods (1)

    public async comparePassword(password: string): Promise<boolean> {
        return await compare(password, this.password);
    }

    // #endregion Public Methods (1)
}

export const UserSchema = SchemaFactory.createForClass(User);

UserSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
UserSchema.plugin(uniqueValidator);

/* UserSchema.virtual('comparePassword').get(async function (password) {
    return await compare(password, this.password);
}); */

async function preSaveHook(next) {
    // Only run this function if password was modified
    if (!this.isModified('password')) return next();

    // Hash the password
    const salt = genSaltSync();
    const password = await hash(this.password, salt);
    this.set('password', password);
    next();
}

UserSchema.pre<User>('save', preSaveHook);

UserSchema.pre('findOneAndUpdate', async function (this) {
    const updateAction: any = this.getUpdate();
    const update: any = { ...updateAction };

    // Only run this function if password was modified
    if (update.password) {
        // Hash the password
        const salt = genSaltSync();
        update.password = await hash(updateAction.password, salt);
        this.setUpdate(update);
    }
});

UserSchema.virtual('sales', { ref: 'Sale', localField: '_id', foreignField: 'seller' });

export { preSaveHook };
