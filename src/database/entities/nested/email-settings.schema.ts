import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ActivityRelatesEmail, ActivityRelatesEmailSchema } from './activity-relates-email.schema';

@Schema()
export class EmailSettings extends Document {
    // #region Properties (3)

    @Prop({ type: ActivityRelatesEmailSchema })
    public activityRelatesEmail: ActivityRelatesEmail;
    @Prop()
    public emailNotification: boolean;
    @Prop()
    public sendCopyToPersonalEmail: boolean;

    // #endregion Properties (3)
}

export const EmailSettingsSchema = SchemaFactory.createForClass(EmailSettings);