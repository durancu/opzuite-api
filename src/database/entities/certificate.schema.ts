//import * as mongoose from 'mongoose';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import * as mongooseSoftDelete from 'mongoose-delete';
//import * as patchHistory from 'mongoose-patch-history';
import { nanoid } from 'nanoid';
import { CertificateCoverTemplate } from './certificate-cover-template.schema';
import { CertificateMasterTemplate } from './certificate-master-template.schema';
import { Company } from './company.schema';
import { Customer } from './customer.schema';
import { Insurer } from './insurer.schema';
import { Cohort } from './nested/cohort.schema';
import { VehicleInfo } from './nested/vehicle.info.schema';
import { Sale } from './sale.schema';
import { User } from './user.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true }, discriminatorKey: 'kind' })
export class Certificate extends Document {
    // #region Properties (21)

    @Prop({ type: [String] })
    public ccEmails: string[];
    @Prop({ type: [String] })
    public ccFaxNumbers: string[];
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public company: Company;
    @Prop({ default: "" })
    public coverPage: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'CertificateCover' })
    public coverTemplate: CertificateCoverTemplate;
    @Prop()
    public createdAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public createdBy: User;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Customer' })
    public customer: Customer;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Insurer' })
    public holder: Insurer;
    @Prop({ type: [SchemaTypes.ObjectId], ref: 'Insurer' })
    public insurers: Insurer[]
    @Prop({ required: false, default: new Date() })
    public issuedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'CertificateMasterTemplate' })
    public masterTemplate: CertificateMasterTemplate;
    @Prop()
    public name: string;
    @Prop()
    public number: string;
    @Prop()
    public revisionNumber: string;
    @Prop([{ type: SchemaTypes.ObjectId, ref: 'Sale' }])
    public policies: Sale[]
    @Prop({ type: [SchemaTypes.ObjectId], ref: 'Sale' })
    public policiesIncluded: Sale[];
    @Prop({ type: [SchemaTypes.ObjectId], ref: 'Cohort' })
    public cohortsIncluded: Cohort[];
    @Prop({ type: SchemaTypes.ObjectId, ref: 'Company' })
    public producer: Company;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
    public updatedBy: User;
    @Prop()
    public vehiclesIncluded: VehicleInfo[]

    // #endregion Properties (21)
}

//const  patchHistoryDefault = patchHistory.default;

export const CertificateSchema = SchemaFactory.createForClass(Certificate);

CertificateSchema.plugin(mongooseSoftDelete, { deletedAt: true, deletedBy: true, overrideMethods: true });
//CertificateSchema.plugin(patchHistoryDefault, { mongoose, name:'certificates.versions'})
