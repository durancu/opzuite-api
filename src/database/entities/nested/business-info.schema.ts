
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import * as uniqueValidator from 'mongoose-unique-validator';
import { nanoid } from 'nanoid';
import { Address, AddressSchema } from './address.schema';
import { ContactInfo, ContactInfoSchema } from './contact-info.schema';
import { VehicleInfo, VehicleInfoSchema } from './vehicle.info.schema';

@Schema()
export class BusinessInfo extends Document {
  // #region Properties (19)

  @Prop({ type: AddressSchema })
  public address: Address;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: [ContactInfoSchema], required: false, default: [] })
  public contacts: ContactInfo[];
  @Prop({ default: '' })
  public email: string;
  @Prop({ default: '' })
  public fax: string;
  @Prop({ default: '' })
  public industry: string;
  @Prop({ default: '' })
  public logo: string;
  @Prop({ default: '' })
  public mcNumber: string;
  @Prop({ default: '' })
  public naicCode: string;
  @Prop({ default: '' })
  public name: string;
  @Prop({ type: [String], default: [] })
  public otherPhones: string[];
  @Prop({ default: '' })
  public primaryPhone: string;
  @Prop({ default: '' })
  public primaryPhoneExtension: string;
  @Prop()
  public secondaryPhone: string;
  @Prop({ default: '' })
  public secondaryPhoneExtension: string;
  @Prop({ default: '' })
  public sector: string;
  @Prop({ default: '' })
  public txDMV: string;
  @Prop({ default: ''/* unique: true, dropDups: true, nullable:true */ })
  public usDOT: string;
  @Prop({ type: [VehicleInfoSchema], required: false, default: [] })
  public vehicles: VehicleInfo[];
  @Prop({ default: '' })
  public website: string;

  // #endregion Properties (19)
}

export const BusinessInfoSchema = SchemaFactory.createForClass(BusinessInfo)

BusinessInfoSchema.plugin(uniqueValidator);
