export const ENDORSEMENT_STATUS = [
  { name: 'Entered in System', id: 'ENTERED_IN_SYSTEM' },
  { name: 'Request sent to Carrier/MGA', id: 'REQUEST_SENT_TO_CARRIER/MGA' },
  { name: 'Insured Invoiced', id: 'INSURED_INVOICED' },
  { name: 'Insured Documentation Received', id: 'INSURED_DOCUMENTATION_RECEIVED' },
  { name: 'Carrier/MGA Confirmation Received', id: 'CARRIER_MGA_CONFIRMATION_RECEIVED' },
  { name: 'Suspended for miscellaneous reason', id: 'SUSPENDED_FOR_MISCELLANEOUS_REASON' },
  { name: 'COMPLETE', id: 'COMPLETE' },
]
