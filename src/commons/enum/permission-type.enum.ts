export enum PermissionType {
  //USERS
  USERS = "users",
  USERS_CREATE = "users.create",
  USERS_READ = "users.read",
  USERS_UPDATE = "users.update",
  USERS_DELETE = "users.delete",

  //CERTIFICATES
  CERTIFICATES = "certificates",
  CERTIFICATES_CREATE = "certificates.create",
  CERTIFICATES_READ = "certificates.read",
  CERTIFICATES_UPDATE = "certificates.update",
  CERTIFICATES_DELETE = "certificates.delete",

  //FILES
  FILES = "files",
  FILES_CREATE = "files.create",
  FILES_READ = "files.read",
  FILES_UPDATE = "files.update",
  FILES_DELETE = "files.delete",

  //POLICIES
  POLICIES = "policies",
  POLICIES_CREATE = "policies.create",
  POLICIES_READ = "policies.read",
  POLICIES_UPDATE = "policies.update",
  POLICIES_DELETE = "policies.delete",
  POLICIES_RENEW = "policies.renew",
  POLICIES_CANCEL = "policies.cancel",

  //POLICY ENDORSEMENTS
  POLICIES_ENDORSE = "policies.endorse",
  POLICIES_ENDORSE_CREATE = "policies.endorse.create",
  POLICIES_ENDORSE_READ = "policies.endorse.read",
  POLICIES_ENDORSE_UPDATE = "policies.endorse.update",
  POLICIES_ENDORSE_DELETE = "policies.endorse.delete",

  //POLICY ENDORSEMENTS ITEM
  POLICIES_ENDORSE_ITEM = "policies.endorse.item",
  POLICIES_ENDORSE_ITEM_CREATE = "policies.endorse.item.create",
  POLICIES_ENDORSE_ITEM_READ = "policies.endorse.item.read",
  POLICIES_ENDORSE_ITEM_UPDATE = "policies.endorse.item.update",
  POLICIES_ENDORSE_ITEM_DELETE = "policies.endorse.item.delete",

  //POLICY ENDORSEMENTS ADVANCED
  POLICIES_ENDORSE_ADVANCED = "policies.endorse.advanced",
  POLICIES_ENDORSE_ADVANCED_CREATE = "policies.endorse.advanced.create",
  POLICIES_ENDORSE_ADVANCED_READ = "policies.endorse.advanced.read",
  POLICIES_ENDORSE_ADVANCED_UPDATE = "policies.endorse.advanced.update",
  POLICIES_ENDORSE_ADVANCED_DELETE = "policies.endorse.advanced.delete",

  //POLICY CERTIFICATES
  POLICIES_CERTIFICATES = "certificates",
  POLICIES_CERTIFICATES_CREATE = "certificates.create",
  POLICIES_CERTIFICATES_READ = "certificates.read",
  POLICIES_CERTIFICATES_UPDATE = "certificates.update",
  POLICIES_CERTIFICATES_DELETE = "certificates.delete",

  //APPLICATIONS
  APPLICATIONS = "applications",
  APPLICATIONS_CREATE = "applications.create",
  APPLICATIONS_READ = "applications.read",
  APPLICATIONS_UPDATE = "applications.update",
  APPLICATIONS_DELETE = "applications.delete",

  //APPLICATIONS
  APPLICATIONS_ITEMS = "applications.items",
  APPLICATIONS_ITEMS_CREATE = "applications.items.create",
  APPLICATIONS_ITEMS_READ = "applications.items.read",
  APPLICATIONS_ITEMS_UPDATE = "applications.items.update",
  APPLICATIONS_ITEMS_DELETE = "applications.items.delete",

  //CUSTOMERS
  CUSTOMERS = "customers",
  CUSTOMERS_CREATE = "customers.create",
  CUSTOMERS_READ = "customers.read",
  CUSTOMERS_UPDATE = "customers.update",
  CUSTOMERS_DELETE = "customers.delete",

  //CARRIERS
  CARRIERS = "carriers",
  CARRIERS_CREATE = "carriers.create",
  CARRIERS_READ = "carriers.read",
  CARRIERS_UPDATE = "carriers.update",
  CARRIERS_DELETE = "carriers.delete",

  //CARRIERS COMMISIONS
  CARRIERS_COMMISSIONS = "carriers.commissions",
  CARRIERS_COMMISSIONS_CREATE = "carriers.commissions.create",
  CARRIERS_COMMISSIONS_READ = "carriers.commissions.read",
  CARRIERS_COMMISSIONS_UPDATE = "carriers.commissions.update",

  //MGAS
  MGAS = "mgas",
  MGAS_CREATE = "mgas.create",
  MGAS_READ = "mgas.read",
  MGAS_UPDATE = "mgas.update",
  MGAS_DELETE = "mgas.delete",

  //MGAS COMMISIONS
  MGAS_COMMISSIONS = "mgas.commissions",
  MGAS_COMMISSIONS_CREATE = "mgas.commissions.create",
  MGAS_COMMISSIONS_READ = "mgas.commissions.read",
  MGAS_COMMISSIONS_UPDATE = "mgas.commissions.update",

  //FINANCERS
  FINANCERS = "financers",
  FINANCERS_CREATE = "financers.create",
  FINANCERS_READ = "financers.read",
  FINANCERS_UPDATE = "financers.update",
  FINANCERS_DELETE = "financers.delete",

  //LOCATIONS
  LOCATIONS = "locations",
  LOCATIONS_CREATE = "locations.create",
  LOCATIONS_READ = "locations.read",
  LOCATIONS_UPDATE = "locations.update",
  LOCATIONS_DELETE = "locations.delete",

  //PAYROLLS
  PAYROLLS = "payrolls",
  PAYROLLS_CREATE = "payrolls.create",
  PAYROLLS_READ = "payrolls.read",
  PAYROLLS_UPDATE = "payrolls.update",
  PAYROLLS_CANCEL = "payrolls.cancel",
  PAYROLLS_RUN = "payrolls.run",
  PAYROLLS_DELETE = "payrolls.delete",

  //COMPANY
  COMPANIES = "companies",
  COMPANIES_CREATE = "companies.create",
  COMPANIES_READ = "companies.read",
  COMPANIES_UPDATE = "companies.update",
  COMPANIES_DELETE = "companies.delete",

  //MY-COMPANY
  MY_COMPANY = "my-company",
  MY_COMPANY_CREATE = "my-company.create",
  MY_COMPANY_READ = "my-company.read",
  MY_COMPANY_UPDATE = "my-company.update",
  MY_COMPANY_DELETE = "my-company.delete",

  //SETTINGS
  SETTINGS = "settings",
  SETTINGS_READ = "my-company.read",
  SETTINGS_UPDATE = "my-company.update",

  //REPORTS
  REPORTS = "reports",
  REPORTS_CREATE = "reports.create",
  REPORTS_READ = "reports.read",
  REPORTS_UPDATE = "reports.update",
  REPORTS_DELETE = "reports.delete",

  //DASHBOARDS
  DASHBOARDS = "dashboards",
  DASHBOARDS_CREATE = "dashboards.create",
  DASHBOARDS_READ = "dashboards.read",
  DASHBOARDS_UPDATE = "dashboards.update",
  DASHBOARDS_DELETE = "dashboards.delete",

  //MY-LOCATION
  MY_LOCATION = "my-location",
  MY_LOCATION_CREATE = "my-location.create",
  MY_LOCATION_READ = "my-location.read",
  MY_LOCATION_UPDATE = "my-location.update",
  MY_LOCATION_DELETE = "my-location.delete",

  //MY-COUNTRY
  MY_COUNTRY = "my-country",
  MY_COUNTRY_CREATE = "my-country.create",
  MY_COUNTRY_READ = "my-country.read",
  MY_COUNTRY_UPDATE = "my-country.update",
  MY_COUNTRY_DELETE = "my-country.delete",

  //MY-COUNTRY
  SYSTEM = "system",
  SYSTEM_CREATE = "system.create",
  SYSTEM_READ = "system.read",
  SYSTEM_UPDATE = "system.update",
  SYSTEM_DELETE = "system.delete",
}

