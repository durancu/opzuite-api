import { Injectable, NotFoundException, Scope } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { lowerCase } from 'lodash';
import * as moment from 'moment';
import { Model, Types } from 'mongoose';
import { COMPANY } from 'src/commons/const/project.constants';
import { GroupingCriteria } from 'src/commons/enum/grouping-criteria.enum';
import { MetricsCalMethod } from 'src/commons/enum/metrics-calc-method.enum';
import { createDateRangeFilterExpressions } from 'src/commons/lib/aggregator-functions';
import * as DateFactory from 'src/commons/lib/date-functions';
import { roundAmount } from 'src/commons/lib/math-functions';
import { hasAdminRoleAccess } from 'src/commons/lib/role-functions';
import { bonusByEmployeeCategory } from 'src/commons/vl17-specific/salary/mexico-bonus';
import { Endorsement } from 'src/database/entities/endorsement.schema';
import { Sale } from 'src/database/entities/sale.schema';
import { User } from 'src/database/entities/user.schema';
import { MetricsParamsFilterDto } from 'src/dto/metrics/metrics-params-filter.dto';
import { MetricsParamsDto } from 'src/dto/metrics/metrics-params.dto';
import { UserRepository } from 'src/repositories/user.repository';


const SALE_FIELD_SELLER = 'seller';
const SALE_FIELD_CUSTOMER = 'customer';
const SALE_FIELD_LOCATION = 'location';

export interface MetricSeller {
  readonly id: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly employeeCategory: string;
  readonly country: string;
}

@Injectable({ scope: Scope.REQUEST })
export class MetricsService {
  constructor(
    @InjectModel(Sale.name) private saleModel: Model<Sale>,
    @InjectModel(Endorsement.name) private endorsementModel: Model<Endorsement>,
    private readonly userRepository: UserRepository,
  ) { }

  async getSalesMetricsByCoverages(params: MetricsParamsDto): Promise<any> {
    let conditions = {};
    conditions = {
      $and: [
        { deleted: false },
        {
          soldAt: createDateRangeFilterExpressions(
            params.dateFrom,
            params.dateTo,
          ),
        },
      ],
    };

    const filters: MetricsParamsFilterDto[] = params.filters || [];

    filters.map(({ field, value }: MetricsParamsFilterDto) => {
      const filterExpression = {};
      if (
        [SALE_FIELD_SELLER, SALE_FIELD_CUSTOMER, SALE_FIELD_LOCATION].includes(
          lowerCase(field),
        )
      ) {
        //TODO: evaluate operator to set the proper expression, only using EQUAL for now
        filterExpression[`${field}._id`] = new Types.ObjectId(value);
      } else if (['broker', 'carrier'].includes(lowerCase(field))) {
        filterExpression[`items.${field}._id`] = new Types.ObjectId(value);
      } else {
        filterExpression[field] = value;
      }
      conditions[`$and`].push(filterExpression);
    });

    const groupingId = this.setGroupingIdExpression(
      params.groupByModel,
      params.groupByFields,
      MetricsCalMethod.COVERAGES
    );

    const query = this.saleModel.aggregate();

    query
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'users',
        localField: 'seller',
        foreignField: '_id',
        as: 'seller',
      })
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: true });
    if (
      params.groupByModel === GroupingCriteria.BROKER ||
      params.groupByModel === GroupingCriteria.CARRIER ||
      params.filters.find(
        (param: MetricsParamsFilterDto) =>
          param.field === GroupingCriteria.BROKER.toString() ||
          param.field === GroupingCriteria.CARRIER.toString(),
      )
    ) {
      query
        .unwind({ path: '$items', preserveNullAndEmptyArrays: true })
        .lookup({
          from: 'insurers',
          localField: 'items.carrier',
          foreignField: '_id',
          as: 'items.carrier',
        })
        .unwind({ path: '$items.carrier', preserveNullAndEmptyArrays: true })
        .lookup({
          from: 'insurers',
          localField: 'items.broker',
          foreignField: '_id',
          as: 'items.broker',
        })
        .unwind({ path: '$items.broker', preserveNullAndEmptyArrays: true });
    }
    query
      .unwind({ path: '$customer', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'customers',
        localField: 'customer',
        foreignField: '_id',
        as: 'customer',
      });
    query.unwind({ path: '$customer', preserveNullAndEmptyArrays: true });
    query
      .unwind({ path: '$location', preserveNullAndEmptyArrays: true })
      .lookup({
        from: 'locations',
        localField: 'location',
        foreignField: '_id',
        as: 'location',
      });
    query.unwind({ path: '$location', preserveNullAndEmptyArrays: true });

    if (conditions) {
      query.match(conditions);
    }

    let groupExpression = {};
    (params.fields || []).map((field) => {
      const fieldName =
        field.trim() === 'items.premium'
          ? 'totalPremium'
          : field.trim() === 'items.amount'
            ? 'totalReceivables'
            : field.trim();
      return (groupExpression[''.concat(fieldName)] = {
        $sum: '$'.concat(field),
      });
    });

    groupExpression['_id'] = groupingId;
    if (params.withCount) {
      groupExpression['count'] = { $sum: 1 };
    }

    groupExpression = {
      ...groupExpression,
      ...{
        seller: { $first: '$seller.name' },
        location: { $first: '$location.business.name' },
        roles: { $first: '$seller.roles' },
        country: { $first: '$seller.country' },
        customer: { $first: '$customer.name' },
        totalPremium: {
          $sum: '$items.premium',
        },
        totalAgentCommission: {
          $sum: '$totalAgentCommission',
        },
        totalTaxesAndFees: {
          $sum: '$totalTaxesAndFees',
        },
        totalNonPremium: {
          $sum: '$totalNonPremium',
        },
        totalAgencyCommission: {
          $sum: '$totalAgencyCommission',
        },
        year: { $first: { $year: '$soldAt' } },
        month: { $first: { $month: '$soldAt' } },
        day: { $first: { $dayOfMonth: '$soldAt' } },
      },
    };

    query.group(groupExpression);

    let sortCriteria = {};

    if ([GroupingCriteria.YEAR.toString(), GroupingCriteria.MONTH.toString(), GroupingCriteria.DAY.toString()].includes(params.groupByModel)) {
      sortCriteria['year'] = -1;
      sortCriteria['month'] = -1;
      sortCriteria['day'] = -1;
    } else if (params.sortField && params.sortField.length > 0) {
      const order = params.sortOrder && params.sortOrder === 'asc' ? 1 : -1;
      sortCriteria = {};
      sortCriteria[params.sortField] = order;
    } else {
      sortCriteria['totalPremium'] = -1;
    }

    query.sort(sortCriteria);

    return query.exec();
  }

  async getSalesMetricsByEndorsements(params: MetricsParamsDto): Promise<any> {
    let conditions = {};
    conditions = {
      $and: [{ deleted: false }],
    };

    if (params.dateFrom || params.dateTo) {
      conditions['$and'].push({
        endorsedAt: createDateRangeFilterExpressions(
          params.dateFrom,
          params.dateTo,
        ),
      });
    }

    //console.log('conditions:', conditions);

    const filters: MetricsParamsFilterDto[] = params.filters || [];

    filters.forEach(({ field, value }: MetricsParamsFilterDto) => {
      const filterExpression = {};
      if (
        [SALE_FIELD_SELLER, SALE_FIELD_CUSTOMER, SALE_FIELD_LOCATION].includes(
          lowerCase(field),
        )
      ) {
        //TODO: evaluate operator to set the proper expression, only using EQUAL for now
        filterExpression[`${field}._id`] = new Types.ObjectId(value);
      } else if (['broker', 'carrier'].includes(lowerCase(field))) {
        filterExpression[`items.${field}._id`] = new Types.ObjectId(value);
      } else {
        filterExpression['sale.' + field] = value;
      }
      conditions[`$and`].push(filterExpression);
    });

    const groupingId = this.setGroupingIdExpression(
      params.groupByModel,
      params.groupByFields,
      MetricsCalMethod.ENDORSEMENTS
    );

    const query = this.endorsementModel.aggregate();

    query
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'sales',
        localField: 'sale',
        foreignField: '_id',
        as: 'sale',
      })
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .addFields({
        followUpPerson: { $ifNull: ['$followUpPerson', '$sale.seller'] },
      })
      .addFields({
        seller: { $ifNull: ['$seller', '$sale.seller'] },
      })
      .addFields({
        endorsedAt: { $ifNull: ['$endorsedAt', '$sale.soldAt'] },
      });

    if (params && params.filters && (
      params.groupByModel === GroupingCriteria.BROKER ||
      params.groupByModel === GroupingCriteria.CARRIER ||
      params.filters.find(
        (param: MetricsParamsFilterDto) =>
          param.field === GroupingCriteria.BROKER.toString() ||
          param.field === GroupingCriteria.CARRIER.toString(),
      ))
    ) {
      query
        .unwind({ path: '$sale.items', preserveNullAndEmptyArrays: true })
        .lookup({
          from: 'insurers',
          localField: 'sales.items.carrier',
          foreignField: '_id',
          as: 'items.carrier',
        })
        .unwind({ path: '$items.carrier', preserveNullAndEmptyArrays: true })
        .lookup({
          from: 'insurers',
          localField: 'sales.items.broker',
          foreignField: '_id',
          as: 'items.broker',
        })
        .unwind({ path: '$items.broker', preserveNullAndEmptyArrays: true });
    }

    query
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'users',
        localField: 'seller',
        foreignField: '_id',
        as: 'seller',
      })
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false });

    query
      .unwind({ path: '$sale.location', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'locations',
        localField: 'sale.location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: false })

      .unwind({ path: '$sale.customer', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'customers',
        localField: 'sale.customer',
        foreignField: '_id',
        as: 'customer',
      })
      .unwind({ path: '$customer', preserveNullAndEmptyArrays: false });

    if (conditions) {
      query.match(conditions);
    }

    let groupExpression = {};
    (params.fields || []).forEach((field) => {
      const fieldName =
        field.trim() === 'items.premium'
          ? 'totalPremium'
          : field.trim() === 'items.amount'
            ? 'totalReceivables'
            : field.trim();
      return (groupExpression[''.concat(fieldName)] = {
        $sum: '$'.concat(field),
      });
    });

    groupExpression['_id'] = groupingId;
    if (params.withCount) {
      groupExpression['count'] = { $sum: 1 };
    }

    groupExpression = {
      ...groupExpression,
      ...{
        seller: { $first: '$seller.name' },
        location: { $first: '$location.business.name' },
        role: { $first: '$seller.role' },
        employeeCategory: { $first: '$seller.employeeInfo.category' },
        country: { $first: '$seller.country' },
        customer: { $first: '$customer.name' },
        totalPremium: {
          $sum: '$totalPremium',
        },
        totalAgentCommission: {
          $sum: '$totalAgentCommission',
        },
        totalTaxesAndFees: {
          $sum: '$totalTaxesAndFees',
        },
        totalNonPremium: {
          $sum: '$totalNonPremium',
        },
        totalAgencyCommission: {
          $sum: '$totalAgencyCommission',
        },
        year: { $first: { $year: '$sale.soldAt' } },
        month: { $first: { $month: '$sale.soldAt' } },
        day: { $first: { $dayOfMonth: '$sale.soldAt' } },
      },
    };

    query.group(groupExpression);

    let sortCriteria = {};

    if ([GroupingCriteria.YEAR.toString(), GroupingCriteria.MONTH.toString(), GroupingCriteria.DAY.toString()].includes(params.groupByModel)) {
      sortCriteria['year'] = -1;
      sortCriteria['month'] = -1;
      sortCriteria['day'] = -1;
    } else if (params.sortField && params.sortField.length > 0) {
      const order = params.sortOrder && params.sortOrder === 'asc' ? 1 : -1;
      sortCriteria = {};
      sortCriteria[params.sortField] = order;
    } else {
      sortCriteria['totalPremium'] = -1;
    }

    query.sort(sortCriteria);

    return query.exec();
  }

  async getPayrollSalesMetricsByEndorsements(
    params: MetricsParamsDto,
  ): Promise<any> {
    let conditions = {};
    conditions = {
      $and: [
        { deleted: false },
        { 'sale.deleted': false },
        { 'sale.type': 'POLICY' },
      ],
    };

    if (params.dateFrom || params.dateTo) {
      conditions['$and'].push({
        endorsedAt: createDateRangeFilterExpressions(
          params.dateFrom,
          params.dateTo,
        ),
      });
    }

    const query = this.endorsementModel.aggregate();

    query
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'sales',
        localField: 'sale',
        foreignField: '_id',
        as: 'sale',
      })
      .unwind({ path: '$sale', preserveNullAndEmptyArrays: false })
      .addFields({
        followUpPerson: { $ifNull: ['$followUpPerson', '$sale.seller'] },
      })
      .addFields({
        seller: { $ifNull: ['$seller', '$sale.seller'] },
      })
      .addFields({
        endorsedAt: { $ifNull: ['$endorsedAt', '$sale.soldAt'] },
      });

    query
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'users',
        localField: 'seller',
        foreignField: '_id',
        as: 'seller',
      })
      .unwind({ path: '$seller', preserveNullAndEmptyArrays: false });

    query
      .unwind({ path: '$seller.role', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'roles',
        localField: 'seller.role',
        foreignField: '_id',
        as: 'role',
      })
      .unwind({ path: '$role', preserveNullAndEmptyArrays: false });

    query
      .unwind({ path: '$sale.location', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'locations',
        localField: 'sale.location',
        foreignField: '_id',
        as: 'location',
      })
      .unwind({ path: '$location', preserveNullAndEmptyArrays: false })

      .unwind({ path: '$sale.customer', preserveNullAndEmptyArrays: false })
      .lookup({
        from: 'customers',
        localField: 'sale.customer',
        foreignField: '_id',
        as: 'customer',
      })
      .unwind({ path: '$customer', preserveNullAndEmptyArrays: false });

    if (conditions) {
      query.match(conditions);
    }

    query.group({
      seller: { $first: '$seller.name' },
      location: { $first: '$location.business.name' },
      role: { $first: '$seller.role.name' },
      employeeCategory: { $first: '$seller.employeeInfo.category' },
      country: { $first: '$seller.country' },
      customer: { $first: '$customer.name' },
      totalPremium: {
        $sum: '$totalPremium',
      },
      totalAgentCommission: {
        $sum: '$totalAgentCommission',
      },
      totalTaxesAndFees: {
        $sum: '$totalTaxesAndFees',
      },
      totalNonPremium: {
        $sum: '$totalNonPremium',
      },
      totalAgencyCommission: {
        $sum: '$totalAgencyCommission',
      },
      totalNonCommissionablePremium: {
        $sum: '$totalNonCommissionablePremium',
      },
      _id: {
        id: '$seller',
        label: '$seller.name',
      },
    });

    let sortCriteria = {};
    sortCriteria['totalPremium'] = -1;

    if (params.sortField && params.sortField.length > 0) {
      const order = params.sortOrder && params.sortOrder === 'asc' ? 1 : -1;
      sortCriteria = {};
      sortCriteria[params.sortField] = order;
    }

    query.sort(sortCriteria);

    return query.exec();
  }

  getDateMatchExpressionByRange(dateRange: string): any {

    const dates = DateFactory.dateRangeByName(dateRange);

    return dateRange
      ? {
        $gte: dates.start,
        $lte: dates.end,
      }
      : { $lte: new Date() };
  }

  setGroupingIdExpression(
    groupingCriteria: string,
    fields: string[] = [],
    calcMethod: string = MetricsCalMethod.COVERAGES
  ): any {
    let idExpression = null;

    switch (groupingCriteria) {
      case GroupingCriteria.SELLER:
        idExpression = {
          id: '$seller._id',
          label: '$seller.name',
          location: '$location.name',
          role: '$seller.role',
          employeeCategory: '$seller.employeeInfo.category',
          country: '$seller.country',
        };

        fields.forEach(
          (field) =>
          (idExpression[''.concat(field.trim())] = '$seller.'.concat(
            field.trim(),
          )),
        );
        break;

      case GroupingCriteria.CUSTOMER:
        idExpression = {
          id: '$customer._id',
          label: '$customer.name',
          country: '$customer.country',
        };

        fields.forEach(
          (field) =>
          (idExpression[''.concat(field.trim())] = '$customer.'.concat(
            field.trim(),
          )),
        );
        break;

      case GroupingCriteria.CARRIER:
        idExpression = {
          id: '$items.carrier._id',
          label: '$items.carrier.name',
        };

        fields.forEach(
          (field) =>
          (idExpression[''.concat(field.trim().split('.').join('_'))] =
            '$items.carrier.'.concat(field.trim())),
        );

        break;

      case GroupingCriteria.BROKER:
        idExpression = {
          id: '$items.broker._id',
          label: '$items.broker.name',
        };

        fields.forEach(
          (field) =>
          (idExpression[''.concat(field.trim().split('.').join('_'))] =
            '$items.broker.'.concat(field.trim())),
        );

        break;

      case GroupingCriteria.LOCATION:
        idExpression = {
          id: calcMethod === MetricsCalMethod.COVERAGES ? '$location._id' : '$location._id',
          label: calcMethod === MetricsCalMethod.COVERAGES ? '$location.business.name' : '$location.business.name',
          country: calcMethod === MetricsCalMethod.COVERAGES ? '$location.country' : '$location.country',
        };
        break;

      case GroupingCriteria.COUNTRY:
        idExpression = {
          id: calcMethod === MetricsCalMethod.COVERAGES ? '$country' : '$sale.country',
          label: calcMethod === MetricsCalMethod.COVERAGES ? '$country' : '$sale.country',
        };
        break;

      case GroupingCriteria.STATUS:
        idExpression = {
          id: calcMethod === MetricsCalMethod.COVERAGES ? '$status' : '$sale.status',
          label: calcMethod === MetricsCalMethod.COVERAGES ? '$status' : '$sale.status',
        };
        break;

      case GroupingCriteria.TYPE:
        idExpression = {
          id: calcMethod === MetricsCalMethod.COVERAGES ? '$type' : '$sale.type',
          label: calcMethod === MetricsCalMethod.COVERAGES ? '$type' : '$sale.type',
        };
        break;

      case GroupingCriteria.YEAR:
        idExpression = {
          id: { $year: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt' },
          label: { $year: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt' },
        };
        break;

      case GroupingCriteria.MONTH:
        idExpression = {
          id: {
            month: {
              $month: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
            },
            year: {
              $year: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
            }
          },
          "label": {
            "$concat": [
              {
                $dateToString: {
                  'format': "%m",
                  'date': calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
                }
              }, "/", {
                $toString: {
                  "$year": calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
                }
              }]
          }
        };
        break;

      case GroupingCriteria.DAY:
        idExpression = {
          id: {
            day: {
              $dayOfMonth: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
            },
            month: {
              $month: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
            },
            year: {
              $year: calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
            }
          },
          "label": {
            "$concat": [
              {
                $dateToString: {
                  'format': "%m",
                  'date': calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
                }
              }, "/",
              {
                $dateToString: {
                  'format': "%d",
                  'date': calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
                }
              }, "/", {
                $toString: {
                  "$year": calcMethod === MetricsCalMethod.COVERAGES ? '$soldAt' : '$sale.soldAt'
                }
              }]
          }
        };
        break;
    }
    return idExpression;
  }

  async getEmployeesSalaryMetrics(
    dateFrom: Date,
    dateTo: Date,
  ): Promise<any> {
    /* 
        DE UN EMPLEADO: 
        
        SALES TOTAL: Suma de todos los totalPremium de las ventas de tipo Policy (totalSales by Seller type Policy)
        FEES TOTAL: Suma de todos los totalTaxesAndFees de las ventas de tipo Policy (totalSales by Seller type Policy)
        PERMITS TOTAL: Suma de todos los totalPremium de las ventas de tipo Permit (totalSales by Seller type Permit)
        TIPS TOTAL: Suma de todos los totalAgentCommission de las ventas de cualquier tipo (totalSales by Seller type Policy) & (totalSales by Seller type Permit)
        
        */

    //CALCULATE ALL POLICY SALES METRICS BY SELLER

    const policySalesBySellerParams: MetricsParamsDto = {
      model: 'sale',
      dateFrom: dateFrom,
      dateTo: dateTo,
      groupByModel: GroupingCriteria.SELLER,
      groupByFields: [],
      filters: [{ field: 'type', operator: '=', value: 'POLICY' }],
      fields: [
        'totalPremium',
        'totalAgentCommission',
        'totalTaxesAndFees',
        'totalNonPremium',
        'totalAgencyCommission',
      ],
      withCount: true,
      withRecords: false,
      sortField: 'totalPremium',
      sortOrder: 'desc',
    };

    const policySalesBySellerMetrics: any[] =
      await this.getPayrollSalesMetricsByEndorsements(
        policySalesBySellerParams,
      );

    const permitSalesBySellerParams: MetricsParamsDto = {
      model: 'sale',
      dateFrom: dateFrom,
      dateTo: dateTo,
      groupByModel: GroupingCriteria.SELLER,
      groupByFields: [],
      filters: [{ field: 'type', operator: '=', value: 'PERMIT' }],
      fields: [
        'totalPremium',
        'totalAgentCommission',
        'totalTaxesAndFees',
        'totalAgencyCommission',
      ],
      withCount: true,
      withRecords: false,
      sortField: 'totalPremium',
      sortOrder: 'desc',
    };

    const permitSalesBySellerMetrics: any[] = await this.getSalesMetricsByEndorsements(
      permitSalesBySellerParams,
    );

    const policyIds: MetricSeller[] = [...policySalesBySellerMetrics].map(
      ({ _id }: any) => _id,
    );
    const permitIds: MetricSeller[] = [...permitSalesBySellerMetrics].map(
      ({ _id }: any) => _id,
    );

    const sellers = this.uniqueBy(policyIds, permitIds, 'id');

    sellers.forEach((seller: any) => {
      const sellerPolicyMetrics = policySalesBySellerMetrics.find(
        (policySeller: any) => seller.id === policySeller._id.id,
      );
      if (sellerPolicyMetrics) {
        seller['policyMetrics'] = {
          totalPremium: roundAmount(sellerPolicyMetrics['totalPremium'] - sellerPolicyMetrics['totalNonCommissionablePremium']),
          totalNonCommissionablePremium: roundAmount(
            sellerPolicyMetrics['totalNonCommissionablePremium'],
          ),
          totalAgentCommission: roundAmount(
            sellerPolicyMetrics['totalAgentCommission'],
          ),
          totalTaxesAndFees: roundAmount(
            sellerPolicyMetrics['totalTaxesAndFees'],
          ),
          totalNonPremium: roundAmount(sellerPolicyMetrics['totalNonPremium']),
          totalAgencyCommission: roundAmount(
            sellerPolicyMetrics['totalAgencyCommission'],
          ),
        };
      }
      const sellerPermitMetrics = permitSalesBySellerMetrics.find(
        (permitSeller: any) => seller.id === permitSeller._id.id,
      );

      if (sellerPermitMetrics) {
        seller['permitMetrics'] = {
          totalPremium: roundAmount(sellerPermitMetrics['totalPremium']),
          totalAgentCommission: roundAmount(
            sellerPermitMetrics['totalAgentCommission'],
          ),
          totalTaxesAndFees: roundAmount(
            sellerPermitMetrics['totalTaxesAndFees'],
          ),
          totalAgencyCommission: roundAmount(
            sellerPermitMetrics['totalAgencyCommission'],
          ),
        };
      }

      seller['totalPremium'] = roundAmount(
        seller['policyMetrics']?.totalPremium,
      );

      seller['totalNonCommissionablePremium'] = roundAmount(
        seller['policyMetrics']?.totalNonCommissionablePremium,
      );

      seller['totalNonPremium'] = roundAmount(
        seller['policyMetrics']?.totalNonPremium,
      );

      seller['totalDownPayment'] = seller['totalNonPremium'];

      seller['totalTaxesAndFees'] =
        roundAmount(seller['policyMetrics']?.totalTaxesAndFees) +
        roundAmount(seller['permitMetrics']?.totalTaxesAndFees);

      seller['totalFees'] = seller['totalTaxesAndFees'];

      seller['totalPermits'] = roundAmount(
        seller['permitMetrics']?.totalPremium,
      );

      seller['totalTips'] =
        roundAmount(seller['policyMetrics']?.totalAgentCommission) +
        roundAmount(seller['permitMetrics']?.totalAgentCommission);

      return seller;
    });

    return sellers;
  }

  async getProfitsMetrics(
    currentUser: Partial<User>,
    month: number,
    year: number,
    seller?: string,
  ): Promise<any> {
    const dateFrom: Date = new Date(
      moment([year, month - 1, COMPANY.payrollDay])
        .subtract(1, 'month')
        .toISOString(),
    );
    const dateTo: Date = new Date(
      moment([year, month - 1, COMPANY.payrollDay])
        .subtract(1, 'day')
        .toISOString(),
    );

    const params: MetricsParamsDto = {
      model: 'sale',
      dateFrom: dateFrom,
      dateTo: dateTo,
      groupByModel: GroupingCriteria.SELLER,
      fields: [
        'totalCharge',
        'tips',
        'permits',
        'fees',
        'totalPremium',
        'downPayment',
        'profits',
      ],
      withCount: true,
      sortField: 'totalPremium',
      sortOrder: 'desc',
    };

    let employeeMetrics = await this.getSalesMetricsByEndorsements(params);

    employeeMetrics = employeeMetrics.map(
      (metric: {
        _id: any;
        totalCharge: number;
        totalPremium: number;
        downPayment: number;
        tips: number;
        permits: number;
        fees: number;
        profits: number;
      }) => {
        const result = metric._id;
        result['totalCharge'] = roundAmount(metric.totalCharge);
        result['totalPremium'] = roundAmount(metric.totalPremium);
        result['downPayment'] = roundAmount(metric.downPayment);
        result['tips'] = roundAmount(metric.tips);
        result['permits'] = roundAmount(metric.permits);
        result['fees'] = roundAmount(metric.fees);
        result['profits'] = roundAmount(metric.profits);
        return result;
      },
    );

    const officeTotalSales =
      employeeMetrics && employeeMetrics.length
        ? employeeMetrics.reduce(
          (accumulator: any, item: { totalPremium: any }) =>
            accumulator + item.totalPremium,
          0,
        )
        : 0;

    let users = [];

    if (seller) {
      try {
        const user: any = await this.userRepository.findById(seller);
        users.push(user);
      } catch (e) {
        throw new NotFoundException(`User with id:${seller} not found`);
      }
    } else {
      const options = {
        populate: {
          role: 'id hierarchy name',
          location: ''
        }
      }
      users = await this.userRepository.findAllActiveWithLocation(options);
    }

    return users
      .filter((user) => !hasAdminRoleAccess(user.role))
      .map((user) => {
        const userMetrics = employeeMetrics.find(({ id }) => id == user.id);

        const result = {
          ...user._doc,
          totalCharge: userMetrics ? userMetrics.totalCharge : 0,
          downPayment: userMetrics ? userMetrics.downPayment : 0,
          totalPremium: userMetrics ? userMetrics.totalPremium : 0,
          tips: userMetrics ? userMetrics.tips : 0,
          fees: userMetrics ? userMetrics.fees : 0,
          permits: userMetrics ? userMetrics.permits : 0,
          profits: userMetrics ? userMetrics.profits : 0,
          sellerName: user.name,
        };

        result['salesBonus'] = bonusByEmployeeCategory(
          user.employeeInfo.category,
          user.location,
          result.totalPremium,
          result.permits,
          result.fees,
          employeeMetrics.length,
          officeTotalSales,
        );
        return result;
      });
  }

  async getUserPerformanceMetrics(
    user: Partial<User>,
    month: number,
    year: number,
    seller?: string,
  ): Promise<any> {
    return await this.getProfitsMetrics(user, month, year, seller);
  }

  async getAllEmployeeSalaryMetrics(employeeMetrics: any) {
    const options = {
      populate: {
        role: 'id hierarchy name',
        location: ''
      }
    }

    const users: any[] = await this.userRepository.findAllActiveWithLocation(options);

    return users
      .filter((user) => !hasAdminRoleAccess(user.role))
      .map((user) => {
        const userMetrics = employeeMetrics.find(({ id }) => id == user.id);
        const salaryMetrics = this.setEmployeeSalaryMetric(user, userMetrics);
        return salaryMetrics;
      });
  }

  async getEmployeeSalaryMetricBySeller(
    sellerId: string,
    employeeMetrics: any,
  ) {
    const result = [];
    try {
      const options = {
        populate: {
          location: ''
        }
      }
      const user: any = await this.userRepository.findById(sellerId, options);
      const userMetrics = employeeMetrics.find(({ id }) => id === user.id);
      const salaryMetrics = this.setEmployeeSalaryMetric(user, userMetrics);

      result.push(salaryMetrics);
    } catch (e) {
      throw new NotFoundException(`User with id:${sellerId} not found`);
    }

    return result;
  }

  setEmployeeSalaryMetric(user: any, userMetrics: any) {
    return {
      id: user.id,
      employeeCategory: user.employeeInfo.category,
      totalPremium: userMetrics ? userMetrics.totalPremium : 0,
      totalNonPremium: userMetrics ? userMetrics.totalNonPremium : 0,
      totalTaxesAndFees: userMetrics ? userMetrics.totalTaxesAndFees : 0,
      totalAgentCommission: userMetrics ? userMetrics.totalAgentCommission : 0,
      location: user.location ? user.location : null,
      locationId: user.location ? user.location.id : null,
      sellerName: user.name,
    };
  }

  public uniqueBy = (array1: any[], array2: any[], comparisonField: string) => {
    for (let i = 0, l = array1.length; i < l; i++) {
      for (let j = 0, ll = array2.length; j < ll; j++) {
        if (array1[i][comparisonField] === array2[j][comparisonField]) {
          array1.splice(i, 1, array2[j]);
          break;
        }
      }
    }
    return array1;
  };
}
