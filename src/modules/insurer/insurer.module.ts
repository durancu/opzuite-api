import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { InsurerSchema } from 'src/database/entities/insurer.schema';
import { InsurerRepository } from 'src/repositories/insurer.repository';
import { InsurerModuleInitializerService } from './insurer-module-initializer.service';
import { InsurerController } from './insurer.controller';
import { InsurerService } from './insurer.service';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Insurer', schema: InsurerSchema },
    ])
  ],
  controllers: [
    InsurerController
  ],
  providers: [
    InsurerService,
    InsurerRepository,
    InsurerModuleInitializerService,
  ],
  exports: [
    InsurerService,
    InsurerRepository
  ]
})
export class InsurerModule { }
