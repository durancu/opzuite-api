import {
  Injectable,
  OnModuleInit
} from '@nestjs/common';

@Injectable()
export class LocationModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(Location Module) is initialized...');
  }
}
