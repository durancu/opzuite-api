import { IsOptional } from "class-validator";
import { ReportParams } from "src/database/entities/nested/report-params.schema";

export class CreateReportDto {
  // #region Properties (6)

  @IsOptional()
  config?: any;
  @IsOptional()
  description?: string;
  @IsOptional()
  name?: string;
  @IsOptional()
  params?: ReportParams;
  @IsOptional()
  permissions?: string[];
  @IsOptional()
  scope?: string;

  // #endregion Properties (6)
}
