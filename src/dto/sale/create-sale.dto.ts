import { IsBoolean, IsDateString, IsMongoId, IsOptional } from "class-validator";
import { Customer } from "src/database/entities/customer.schema";
import { Endorsement } from "src/database/entities/endorsement.schema";
import { Location } from "src/database/entities/location.schema";
import { Product } from "src/database/entities/nested/product.schema";
import { SaleItem } from "src/database/entities/nested/sale-item.schema";
import { User } from "src/database/entities/user.schema";

export class CreateSaleDto {
  // #region Properties (22)

  @IsOptional()
  @IsBoolean()
  public autoRenew?: boolean;
  @IsOptional()
  @IsDateString()
  public cancelledAt?: Date;
  @IsOptional()
  public checksumSanity?: number;
  @IsOptional()
  public country?: string;
  @IsMongoId()
  public customer?: Partial<Customer>;
  @IsOptional()
  public details?: any;
  @IsDateString()
  @IsOptional()
  public effectiveAt?: Date;
  @IsOptional()
  public endorsements?: Partial<Endorsement>[];
  @IsOptional()
  public expiresAt?: Date;
  @IsOptional()
  public isChargeItemized?: boolean;
  @IsOptional()
  public items?: SaleItem[];
  public lineOfBusiness?: string;
  @IsOptional()
  public location?: Partial<Location>;
  @IsOptional()
  public monthlyPayment?: number;
  @IsOptional()
  public number?: string;
  @IsOptional()
  public products?: Partial<Product>[];
  @IsOptional()
  public renewalFrequency?: string;
  @IsOptional()
  public seller?: Partial<User>;
  @IsOptional()
  public soldAt?: Date;
  @IsOptional()
  public states?: string;
  @IsOptional()
  public status?: string;
  @IsOptional()
  public type?: string;

  // #endregion Properties (22)
}