import { PartialType } from "@nestjs/mapped-types";
import { IsOptional } from "class-validator";
import { CreateEndorsementDto } from "./create-endorsement.dto";

export class UpdateEndorsementDto extends PartialType(CreateEndorsementDto) {
    @IsOptional()
    _id?: string;
    @IsOptional()
    id?: string;
}