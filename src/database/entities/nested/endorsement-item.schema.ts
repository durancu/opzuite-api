
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { nanoid } from 'nanoid';
import { User } from '../user.schema';


@Schema()
export class EndorsementItem extends Document {
    // #region Properties (22)

    @Prop()
    public accountingClass: string;
    @Prop({ default: 0, required: false })
    public amount: number;
    @Prop({ default: 0, required: false })
    public amountPaid: number;
    @Prop({ default: 0, required: false })
    public balance: number;
    @Prop({ default: () => nanoid(6), required: false })
    public code: string;
    @Prop({ default: '$' })
    public commissionUnit: string;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public createdBy: User;
    @Prop({ default: '' })
    public description: string;
    @Prop()
    public endorsedAt: Date;
    /* @Prop({ type: SchemaTypes.ObjectId, ref: 'Endorsement', required: false })
    public endorsement: Endorsement; */
    @Prop()
    public followUpDate: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public followUpPerson: User;
    @Prop({ type: SchemaTypes.Map, required: false })
    public nonCommissionableReference: any;
    @Prop({ type: SchemaTypes.Map })
    public otherDetails: any;
    @Prop({ type: [SchemaTypes.Map], required: false })
    public payments: any[];
    /* @Prop({ type: SchemaTypes.ObjectId, ref: 'Sale', required: false })
    public sale: Partial<Sale>; */
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public seller: User;
    @Prop()
    public status: string;
    @Prop()
    public type: string;
    @Prop()
    public updatedAt: Date;
    @Prop({ type: SchemaTypes.ObjectId, ref: 'User', required: false })
    public updatedBy: User;

    // #endregion Properties (22)
}

export const EndorsementItemSchema = SchemaFactory.createForClass(EndorsementItem);