export const COMPANY = {
  payrollDay: 21
}

export const SALE_FIELD_SELLER = 'seller';
export const SALE_FIELD_CUSTOMER = 'customer';
export const SALE_FIELD_LOCATION = 'location';

export const SUPER_ADMIN_HIERARCHY = 1;
export const ADMIN_HIERARCHY = 20;

export const EMPTY_STRING = '';

export const S3_PROFILE_PHOTO_FOLDER = 'profile/photo';
export const S3_USERS_FOLDER = 'users';
export const S3_PROFILE_PHOTO_DEFAULT_PATH = 'media/profile/default-profile-photo.png';

export const ADDITIONAL_REMARKS_SCHEDULE_TEMPLATE_CODE = "101";

