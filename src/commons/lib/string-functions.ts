import { camelCase, startCase, toLower, trim } from 'lodash';

export const toTitle = (text: string) => {
  return trim(startCase(camelCase(text))).replace(' Llc', ' LLC').replace('Dba', 'DBA');
};

export const cleanEmail = (email: string) => {
  return trim(toLower(email));
}

export function titleCase(str) {
  return str
    .toLowerCase()
    .split(' ')
    .map(function (word) {
      return word.charAt(0).toUpperCase() + word.slice(1);
    })
    .join(' ');
}