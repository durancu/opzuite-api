import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { ReportParamsFilter, ReportParamsFilterSchema } from './report-params-filter.schema';

@Schema({ timestamps: true, toJSON: { virtuals: true } })
export class ReportParams extends Document {
  @Prop()
  model: string;
  @Prop({type:[ReportParamsFilterSchema]})
  filters: ReportParamsFilter[];
  @Prop()
  groupByModel: string;
  @Prop()
  groupByFields: string[];
  @Prop()
  dateFrom: Date;
  @Prop()
  dateTo: Date;
  @Prop()
  fields: string[];
  @Prop()
  withCount: boolean;
  @Prop()
  withRecords: boolean;
  @Prop()
  sortOrder: string;
  @Prop()
  sortField: string;
}

export const ReportParamsSchema = SchemaFactory.createForClass(ReportParams);