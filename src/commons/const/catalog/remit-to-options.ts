export const REMIT_TO_OPTIONS = [
  { name: 'Carrier', id: 'CARRIER' },
  { name: 'MGA', id: 'MGA' },
  { name: 'Finance Company', id: 'FINANCE_COMPANY' },
  { name: 'Agent', id: 'AGENT' },
];
