import { SetMetadata } from '@nestjs/common';
import { PermissionType } from 'src/commons/enum/permission-type.enum';
import { HAS_PERMISSIONS_KEY } from '../../../commons/const/auth.constants';

export const HasPermissions = (...args: PermissionType[]) => SetMetadata(HAS_PERMISSIONS_KEY, args);
