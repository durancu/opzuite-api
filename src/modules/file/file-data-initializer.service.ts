import {
    Injectable,
    OnModuleInit
} from '@nestjs/common';

@Injectable()
export class FileModuleInitializerService
  implements OnModuleInit {

  async onModuleInit(): Promise<void> {
    console.log('(File Module) is initialized...');
  }
}
