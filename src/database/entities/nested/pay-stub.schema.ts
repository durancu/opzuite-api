import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, SchemaTypes } from 'mongoose';
import { nanoid } from 'nanoid';
import { Location } from '../location.schema';
import { User } from '../user.schema';
import { PayAddon, PayAddonSchema } from './pay-addon.schema';

@Schema()
export class PayStub extends Document {
  // #region Properties (29)

  @Prop({ type: [PayAddonSchema] })
  public addons?: Partial<PayAddon>[];
  @Prop()
  public bonusFrequency: string;
  @Prop({ default: () => nanoid(6), required: false })
  public code: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'User' })
  public employee: User;
  @Prop()
  public employeeCategory: string;
  @Prop()
  public employeeCountry: string;
  @Prop({ type: SchemaTypes.ObjectId, ref: 'Location' })
  public employeeLocation: Location;
  @Prop()
  public employeeName: string;
  @Prop()
  public hourlyRate: number;
  @Prop()
  public normalHoursWorked: number;
  @Prop()
  public overtimeHoursWorked: number;
  @Prop({ type: [SchemaTypes.Map] })
  public saleCommissionPlans: any;
  @Prop()
  public totalDiscount: number;
  @Prop()
  public totalDiscountDescription: string;
  @Prop()
  public totalDownPayment: number;
  @Prop()
  public totalExtraBonus: number;
  @Prop()
  public totalExtraBonusDescription: string;
  @Prop()
  public totalFees: number;
  @Prop()
  public totalNetSalary: number;
  @Prop()
  public totalNonPremium: number;
  @Prop()
  public totalPermits: number;
  @Prop()
  public totalPremium: number;
  @Prop()
  public totalRegularSalary: number;
  @Prop()
  public totalReimbursement: number;
  @Prop()
  public totalReimbursementDescription: string;
  @Prop()
  public totalSaleBonus: number;
  @Prop()
  public totalSales: number;
  @Prop()
  public totalTaxesAndFees: number;
  @Prop()
  public totalTips: number;

  // #endregion Properties (29)
}

export const PayStubSchema = SchemaFactory.createForClass(PayStub);