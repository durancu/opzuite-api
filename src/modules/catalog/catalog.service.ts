import { Inject, Injectable, Scope } from '@nestjs/common';
import { REQUEST } from '@nestjs/core';
import { CompanyCatalog } from 'src/commons/const/catalog/company';
import { Countries } from 'src/commons/const/catalog/countries';
import { COVERAGES_TYPES } from 'src/commons/const/catalog/coverages-types';
import { CUSTOMER_TYPES } from 'src/commons/const/catalog/customerTypes';
import { DateCatalog } from 'src/commons/const/catalog/date';
import { PERMIT_ENDORSEMENT_ITEM_STATUS, POLICY_ENDORSEMENT_ITEM_STATUS } from 'src/commons/const/catalog/endorsement-item-status';
import { ENDORSEMENT_ITEM_TYPES } from 'src/commons/const/catalog/endorsement-item-types';
import { ENDORSEMENT_ITEMS } from 'src/commons/const/catalog/endorsement-items';
import { ENDORSEMENT_STATUS } from 'src/commons/const/catalog/endorsement-status';
import { ENDORSEMENT_TYPES } from 'src/commons/const/catalog/endorsement-types';
import { Industries } from 'src/commons/const/catalog/industries';
import { LINES_OF_BUSINESS } from 'src/commons/const/catalog/lines-of-business';
import { PERMIT_ENDORSEMENT_ITEM_TYPES } from 'src/commons/const/catalog/permit-endorsement-item-types';
import { PERMIT_ENDORSEMENT_ITEMS } from 'src/commons/const/catalog/permit-endorsement-items';
import { PERMIT_ENDORSEMENT_STATUS } from 'src/commons/const/catalog/permit-endorsement-status';
import { PERMIT_ENDORSEMENT_TYPES } from 'src/commons/const/catalog/permit-endorsement-types';
import { PERMIT_TYPES, PERMIT_TYPES_BY_STATE } from 'src/commons/const/catalog/permits-types';
import { POLICY_STATUS } from 'src/commons/const/catalog/policy-status';
import { REMIT_TO_OPTIONS } from 'src/commons/const/catalog/remit-to-options';
import { REPORT_FILTER_MODEL } from 'src/commons/const/catalog/report/report-filter-model';
import { SALE_STATUS } from 'src/commons/const/catalog/sale-status';
import { SALE_TYPES } from 'src/commons/const/catalog/sales-types';
import { States } from 'src/commons/const/catalog/states';
import { RoleCategories } from 'src/commons/enum/role-categories.enum';
import { SALE_COMMISSION_PLANS } from 'src/commons/vl17-specific/sale-commission-plans';
import { Company } from 'src/database/entities/company.schema';
import { AuthenticatedRequest } from '../auth/interface/authenticated-request.interface';
import { CertificateCoverService } from '../certificate/certificate-cover.service';
import { CertificateTemplateService } from '../certificate/certificate-template.service';
import { CustomerService } from '../customer/customer.service';
import { InsurerService } from '../insurer/insurer.service';
import { LocationService } from '../location/location.service';
import { PayrollService } from '../payroll/payroll.service';
import { ReportService } from '../report/report.service';
import { RoleService } from '../user/role.service';
import { UserService } from '../user/user.service';

@Injectable({ scope: Scope.REQUEST })
export class CatalogService {
  // #region Constructors (1)

  constructor(
    private userService: UserService,
    private locationService: LocationService,
    private customerService: CustomerService,
    private insurerService: InsurerService,
    private reportService: ReportService,
    private roleService: RoleService,
    private payrollService: PayrollService,
    private certificateTemplateService: CertificateTemplateService,
    private certificateCoverService: CertificateCoverService,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) { }

  // #endregion Constructors (1)

  // #region Public Methods (1)

  public async getCatalog(company: Partial<Company>): Promise<any> {
    const entitiesFilter = { company: company, deleted: false };
    const employeesFilter = { ...entitiesFilter };
    employeesFilter['employeeInfo.category'] = {
      $nin: [RoleCategories.SUPERADMIN, RoleCategories.ADMIN],
    };

    const insurers: any = await this.insurerService.getCatalog(entitiesFilter);
    const roles: any = await this.roleService.getCatalog(entitiesFilter);

    return {
      availablePayPeriods: await this.payrollService.getAvailablePayPeriods(),
      brokers: insurers.brokers,
      saleCommissionPlans: SALE_COMMISSION_PLANS,
      carriers: insurers.carriers,
      certificateMasterTemplates: await this.certificateTemplateService.getCatalog(entitiesFilter),
      certificateCoverTemplates: await this.certificateCoverService.getCatalog(entitiesFilter),
      coverages: COVERAGES_TYPES,
      customers: await this.customerService.getCatalog(entitiesFilter),
      customerTypes: CUSTOMER_TYPES,
      dateRanges: DateCatalog.ranges,
      employees: await this.userService.getCatalog(employeesFilter),
      endorsementStatus: ENDORSEMENT_STATUS,
      endorsementTypes: ENDORSEMENT_TYPES,
      endorsementItems: ENDORSEMENT_ITEMS,
      endorsementItemTypes: ENDORSEMENT_ITEM_TYPES,
      employeeRateFrequencies: CompanyCatalog.employeeRateFrequencies,
      financers: insurers.financers,
      industries: Industries,
      linesOfBusiness: LINES_OF_BUSINESS,
      locations: await this.locationService.getCatalog(entitiesFilter),
      locationAvailableCountries: CompanyCatalog.locations.availableCountries,
      locationPayFrequencies: CompanyCatalog.locations.payrollFrequencies,
      permitEndorsementStatus: PERMIT_ENDORSEMENT_STATUS,
      coverageStatus: POLICY_STATUS,
      policyEndorsementItemStatus: POLICY_ENDORSEMENT_ITEM_STATUS,
      permitEndorsementItemStatus: PERMIT_ENDORSEMENT_ITEM_STATUS,
      permitEndorsementTypes: PERMIT_ENDORSEMENT_TYPES,
      permitEndorsementItems: PERMIT_ENDORSEMENT_ITEMS,
      permitEndorsementItemTypes: PERMIT_ENDORSEMENT_ITEM_TYPES,
      permitTypes: PERMIT_TYPES,
      permitTypesByState: PERMIT_TYPES_BY_STATE,
      renewalFrequencies: CompanyCatalog.sales.renewalFrequencies,
      saleStatus: SALE_STATUS,
      saleTypes: SALE_TYPES,
      remitToOptions: REMIT_TO_OPTIONS,
      reports: await this.reportService.getCatalog({}),
      reportFilterModel: REPORT_FILTER_MODEL,
      roles: roles,
      states: States,
      countries: Countries,
      users: await this.userService.getCatalog(entitiesFilter),

    };
  }

  // #endregion Public Methods (1)
}
